/**
* Buscador Alojamiento js.
*/

var myCombo;
$(document).ready(function(){
  
  $('#edit-buscador-llegada-datepicker-popup-0').datepicker({
    dateFormat:'dd-mm-yy', 
    minDate: 0, 
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa']
  });
  $('#edit-buscador-salida-datepicker-popup-0').datepicker({
    dateFormat:'dd-mm-yy', 
    minDate: 1,
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa']
  });
  
  myCombo = new dhtmlXComboFromSelect("edit-buscador-ubicacion");
  myCombo.enableFilteringMode("between");
  
  //Click del boton de busqueda de alojamiento
  $("#envioFiltroAlojamiento").click(function() {
    $('#formFiltro').submit();
  });
  
  // Placeholder ubicacion
  $('.dhxcombo_input').click(function(){
    $('.dhxcombo_input').val('');
  });

});

function envioFiltro() {
  
  var filtros = ['tid:8688'];
  
  var control = null;
  
  var ctrUbicacion = $("input[name='buscador_ubicacion']");
  var ctrLlegada = $("#edit-buscador-llegada-datepicker-popup-0");
  var ctrSalida = $("#edit-buscador-salida-datepicker-popup-0");
  var ctrHuespedes = $("#edit-buscador-huespedes");
  
  var ctrPrecioNoche = $("input[name='field_precio_desde']");
  var ctrValoracion = $("input[class='valoracion']");
  var ctrTipoAlojamiento = $("input[class='tipo_de_alojamiento']");
  var ctrZonaTuristica = $("input[class='zona_turistica']");
  var ctrServicios = $("input[class='servicios']");
  
  var url = window.location.href;
  
  if (ctrUbicacion.length > 0 && ctrUbicacion.val().length > 0) {
    
    var filtro_ubicacion = ctrUbicacion.val();
    if(filtro_ubicacion.indexOf(" ") != -1){
      filtro_ubicacion = filtro_ubicacion.split(' ');
      filtro_ubicacion = filtro_ubicacion[filtro_ubicacion.length-1];
    }    
    filtros.push(filtro_ubicacion);
  
  
    if(ctrLlegada.length > 0 && ctrLlegada.val().length > 0){
      if(ctrSalida.length > 0 && ctrSalida.val().length > 0){
        var llegada = convertir_fecha_reserva(ctrLlegada.val());
        var salida = convertir_fecha_reserva(ctrSalida.val());
        filtros.push('im_reserved:%5B'+llegada+' TO '+salida+'%5D');
        filtros.push('im_available:%5B'+llegada+' TO '+salida+'%5D');
      } else {
        $('.message').show('');
        $('.message').html('Seleccione la salida de su destino');
        return;
      }
    } else {
      if(ctrSalida.length > 0 && ctrSalida.val().length > 0){
        $('.message').show('');
        $('.message').html('Seleccione la llegada a destino');
        return;
      }
    }
    
    if (ctrHuespedes.length > 0 && ctrHuespedes.val().length > 0) {
      var filtro_huesped = ctrHuespedes.val();
      var texto_filtros = '';
      if(filtro_huesped.indexOf(" ") != -1){
        filtro_huesped = filtro_huesped.split(' ');
        for(i = 1; i < filtro_huesped.length; i++) {
          texto_filtros = texto_filtros + ' ' + filtro_huesped[i];
        }
        //filtro_huesped = filtro_huesped[filtro_huesped.length-1];
      }
      filtros.push(texto_filtros);
    }
    
    if (ctrPrecioNoche.length > 0) {
      var filtro_precio =  $('input[name=field_precio_desde]:checked', '#formFiltrosAlojamientos').val();
      if(filtro_precio != undefined)
        filtros.push(filtro_precio);
    } /*else {
      //Reviso si esta aplicado y no existe el campo, lo mantengo
      if(url.indexOf("fs_precio") != -1){
        var trim_filtro_precio = url.split('fs_precio:');
        trim_filtro_precio = trim_filtro_precio[1].split('%5D');
        var filtro_precio =  'fs_precio:' + trim_filtro_precio[0] + '%5D';
        filtros.push(filtro_precio);
      }
    }*/
    
    if (ctrValoracion.length > 0) {
      $(".valoracion").each(function(index) {
        var filtro_valoracion =  $(this).filter(':checked').val(); 
        if(filtro_valoracion != undefined)
          filtros.push(filtro_valoracion);
      });
    } /*else {
      //Reviso si esta aplicado y no existe el campo, lo mantengo
      if(url.indexOf("im_valoracion") != -1){
        var trim_filtro_valoracion = url.split('im_valoracion:');
        for(i = 1; i < trim_filtro_valoracion.length; i++) {
          var filtro_valoracion = trim_filtro_valoracion[i].split('%20');
          filtro_valoracion =  'im_valoracion:' + filtro_valoracion[0];
          filtros.push(filtro_valoracion);
        }
      }
    }*/
    
    if (ctrTipoAlojamiento.length > 0) {
      $(".tipo_de_alojamiento").each(function(index) {
        var filtro_tipo =  $(this).filter(':checked').val(); 
        if(filtro_tipo != undefined) {
          if(filtro_tipo.indexOf(" ") >= 0) {
            var trim_filtro_tipo = filtro_tipo.split(':'); 
            filtro_tipo = trim_filtro_tipo[0]+':"'+trim_filtro_tipo[1]+'"';
          }
          filtros.push(filtro_tipo);
        }
      });
    } /*else {
      //Reviso si esta aplicado y no existe el campo, lo mantengo
      if(url.indexOf("ss_tipo_alojamiento") != -1){
        var trim_filtro_tipo = url.split('ss_tipo_alojamiento:');
        for(i = 1; i < trim_filtro_tipo.length; i++) {
          if(trim_filtro_tipo[i].indexOf("%22") != -1){
            var filtro_tipo = trim_filtro_tipo[i].split('%22%20');
            filtro_tipo = 'ss_tipo_alojamiento:' + filtro_tipo[0] + '%22';
          } else {
            var filtro_tipo = trim_filtro_tipo[i].split('%20');
            filtro_tipo = 'ss_tipo_alojamiento:' + filtro_tipo[0];
          }
          filtros.push(filtro_tipo);
        }
      }
    }*/
    
    if (ctrZonaTuristica.length > 0) {
      $(".zona_turistica").each(function(index) {
        var filtro_zona =  $(this).filter(':checked').val(); 
        if(filtro_zona != undefined) {
          if(filtro_zona.indexOf(" ") >= 0) {
            var trim_filtro_zona = filtro_zona.split(':'); 
            filtro_zona = trim_filtro_zona[0]+':"'+trim_filtro_zona[1]+'"';
          }
          filtros.push(filtro_zona);
        }
      });
    } /*else {
      //Reviso si esta aplicado y no existe el campo, lo mantengo
      if(url.indexOf("ss_zona_turistica") != -1){
        var trim_filtro_zona = url.split('ss_zona_turistica:');
        for(i = 1; i < trim_filtro_zona.length; i++) {
          if(trim_filtro_zona[i].indexOf("%22") != -1){
            var filtro_zona = trim_filtro_zona[i].split('%22%20');
            filtro_zona = 'ss_zona_turistica:' + filtro_zona[0] + '%22';
          } else {
            var filtro_zona = trim_filtro_zona[i].split('%20');
            filtro_zona = 'ss_zona_turistica:' + filtro_zona[0];
          }
          filtros.push(filtro_zona);
        }
      }
    }*/
    
    if (ctrServicios.length > 0) {
      $(".servicios").each(function(index) {
        var filtro_servicios =  $(this).filter(':checked').val(); 
        if(filtro_servicios != undefined) {
          if(filtro_servicios.indexOf(" ") >= 0) {
            var trim_filtro_tipo = filtro_servicios.split(':'); 
            filtro_servicios = trim_filtro_tipo[0]+':"'+trim_filtro_tipo[1]+'"';
          }
          filtros.push(filtro_servicios);
        }
      });
    } /*else {
      //Reviso si esta aplicado filtro tid que no correspndan a Ubicacion, lo mantengo
      if(url.indexOf("tid") != -1){
        var trim_filtro_tid = url.split('tid:');
        for(i = 1; i < trim_filtro_tid.length; i++) {
          var filtro_tid = trim_filtro_tid[i].split('%20');
          if(ctrUbicacion.val().indexOf(filtro_tid[0]) == -1){
            filtro_tid = 'tid:' + filtro_tid[0];
            filtros.push(filtro_tid);
          }
        }
      }
    }*/
    
    var text_filtros = '';
    for(i = 0; i < filtros.length; i++){
      text_filtros = text_filtros + ' ' + filtros[i];
    }
    
    //console.log(text_filtros);
    
    location.href = '/search/result/filters=' + text_filtros;
  
  } else {
    $('#message-form').show('');
    $('#message-text').html('Seleccione Ciudad / Localidad de destino');
    window.scrollTo(0,0);
  }
  
}

/*
 * Convertir fecha a formato reserva (Ymd)
*/
function convertir_fecha_reserva(fecha){
  var fecha_salida = '';
  fecha = fecha.split('-');
  fecha_salida = fecha[2] + fecha[1] + fecha[0];
  return fecha_salida;
}