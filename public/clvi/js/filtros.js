function envio_form(){
  document.forms["formFiltro"].submit();
}

function ocultarSelects(exclude) {
  if(exclude == 'provincias' || exclude == 'ciudades') {
    if(exclude != 'ciudades') {
      $(".select-group.ciudades.hidden").hide();
      $(".select-group.ciudades.hidden .form-select").val('');
    }
    $(".select-group.barrios").hide();
    $(".select-group.barrios .form-select").val('');
  } else {
    if(exclude == 'productos') { 
      $(".select-group.subproductos").hide();
      $(".select-group.subproductos .form-select").val('');
    } else {
      if(exclude == 'servicios') { 
        $(".select-group.subservicios").hide();
        $(".select-group.subservicios .form-select").val('');
      } else {
        if(exclude == 'rurales') { 
          $(".select-group.subrurales").hide();
          $(".select-group.subrurales .form-select").val('');
        } else {
          if(exclude != 'subrubros' && exclude != 'marcas') {
            $(".select-group.subrubros.hidden").hide();
            $(".select-group.subrubros.hidden .form-select").val('');
          }
          if(exclude != 'marcas') {
            $(".select-group.marcas.hidden").hide();
            $(".select-group.marcas.hidden .form-select").val('');
          }
          $(".select-group.modelos.hidden").hide();
          $(".select-group.filtrosInmuebles.hidden").hide();
          $(".select-group.modelos.hidden  .form-select").val('');
          $(".select-group.filtrosInmuebles.hidden  .form-select").val('');
        }
      }
    }
  }
}

jQuery(document).ready(function ($) {
  
  $('.form-select').change(function() {
    if($(this).val() != '') {
      $(this).parent().addClass('filtro-aplicado');
    } else {
      $(this).parent().removeClass('filtro-aplicado');
    }
  });
  
  $('#subrubro_link').change(function() {
    ocultarSelects();
    var padre_id = $(this).val();
    $(".select-group.subrubros.hidden").each(function( index ) {
      if($(this).attr('data-padre') == padre_id)
        $(this).show();
    });
  });
  
  $('#tipo_vehiculo_link').change(function() {
    ocultarSelects('subrubros');
    var padre_id = $(this).val();
    $(".select-group.marcas.hidden").each(function( index ) {
      if($(this).attr('data-padre') == padre_id)
        $(this).show();
    });
  });
  
  $('.select-marcas').change(function() {
    ocultarSelects('marcas');
    var padre_id = $(this).val();
    $(".select-group.modelos.hidden").each(function( index ) {
      if($(this).attr('data-padre') == padre_id)
        $(this).show();
    });
  });
  if($(".select-marcas").length == 1) {
    var selectValue = $(".select-marcas").val();
    if(selectValue != '') {
      $(".select-marcas").parent().addClass('filtro-aplicado');
      $(".select-group.modelos.hidden").each(function( index ) {
        if($(this).attr('data-padre') == selectValue) {
          if($(this).children('.form-select').val() != '')
            $(this).addClass('filtro-aplicado');
          $(this).show();
        }
      });
    }  
  }
  $(".select-group.filtrosVehiculos").each(function( index ) {
    if($(this).children('.form-select').val() != '')
      $(this).addClass('filtro-aplicado');
  });
  
  $('#tipo_inmueble_link').change(function() {
    ocultarSelects('subrubros');
    var padre_id = $(this).val();
    $(".select-group.filtrosInmuebles.hidden").each(function( index ) {
      if($(this).attr('data-padre') == padre_id)
        $(this).show();
    });
  });
  $(".select-group.filtrosInmuebles").each(function( index ) {
    if($(this).children('.form-select').val() != '')
      $(this).addClass('filtro-aplicado');
  });
  
  $('#provincia_select').change(function() {
    ocultarSelects('provincias');
    var padre_id = $(this).val();
    if(padre_id == '' || padre_id == 'tid:3173') {
      $(".select-group.barrios.hidden").each(function( index ) {
        if($(this).attr('data-padre') == 'tid:3194')
          $(this).show();
      });
    }
    $(".select-group.ciudades.hidden").each(function( index ) {
      if($(this).attr('data-padre') == padre_id)
        $(this).show();
    });
  });
  if($("#provincia_select").length == 1) {
    var selectValue = $("#provincia_select").val();
    if(selectValue != '') {
      $(".select-group.ciudades.hidden").each(function( index ) {
        if($(this).attr('data-padre') == selectValue)
          $(this).show();
      });
    } else {
      $(".select-group.barrios.hidden").each(function( index ) {
        if($(this).attr('data-padre') == 'tid:3194')
          $(this).show();
      });
    }
  }
  $(".select-group.filtrosUbicacion").each(function( index ) {
    if($(this).children('.form-select').val() != '')
      $(this).addClass('filtro-aplicado');
  });
  
  $('.select-ciudad').change(function() {
    ocultarSelects('ciudades');
    var padre_id = $(this).val();
    $(".select-group.barrios").each(function( index ) {
      if($(this).attr('data-padre') == padre_id)
        $(this).show();
    });
  });
  if($(".select-ciudad").length >= 1) {
    $(".select-ciudad").each(function( index ) {
      var selectValue = $(this).val();
      if(selectValue != '') {
        $(".select-group.barrios").each(function( index ) {
          if($(this).attr('data-padre') == selectValue)
            $(this).show();
        });
      }
    });  
  }
  
  $('#rubro_productos_link').change(function() {
    ocultarSelects('productos');
    var padre_id = $(this).val();
    $(".select-group.subproductos.hidden").each(function( index ) {
      if($(this).attr('data-padre') == padre_id)
        $(this).show();
    });
  });
  if($("#rubro_productos_link").length == 1) {
    var selectValue = $("#rubro_productos_link").val();
    if(selectValue != '') {
      $(".select-group.subproductos.hidden").each(function( index ) {
      if($(this).attr('data-padre') == selectValue)
        $(this).show();
      });
    }  
  }
  
  $('#rubro_servicios_link').change(function() {
    ocultarSelects('servicios');
    var padre_id = $(this).val();
    $(".select-group.subservicios.hidden").each(function( index ) {
      if($(this).attr('data-padre') == padre_id)
        $(this).show();
    });
  });
  if($("#rubro_servicios_link").length == 1) {
    var selectValue = $("#rubro_servicios_link").val();
    if(selectValue != '') {
      $(".select-group.subservicios.hidden").each(function( index ) {
      if($(this).attr('data-padre') == selectValue)
        $(this).show();
      });
    }  
  }
  
  $('#rubro_rurales_link').change(function() {
    ocultarSelects('rurales');
    var padre_id = $(this).val();
    $(".select-group.subrurales.hidden").each(function( index ) {
      if($(this).attr('data-padre') == padre_id)
        $(this).show();
    });
  });
  if($("#rubro_rurales_link").length == 1) {
    var selectValue = $("#rubro_rurales_link").val();
    if(selectValue != '') {
      $(".select-group.subrurales.hidden").each(function( index ) {
      if($(this).attr('data-padre') == selectValue)
        $(this).show();
      });
    }  
  }
  $(".select-group.filtrosVarios").each(function( index ) {
    if($(this).children('.form-select').val() != '')
      $(this).addClass('filtro-aplicado');
  });
    
});