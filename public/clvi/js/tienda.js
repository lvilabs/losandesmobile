var mapa_envio;
var marcador = null;
var infowindow = null;

jQuery(document).ready(function ($) {

function ajax_compra_datos(data_post){
  
  $.ajax({
    method: "POST",
    url: "/tienda/compra_datos",
    data: data_post,
    beforeSend:function(){
      $(".loader").show();
      $('#btn-compra-datos').hide();
    },
    success:function(msg){
      try{
        var json = JSON.parse(msg);
        if(json.status == 'error'){
          $('.help-block.with-errors').html(json.mensaje);
          $('.help-block.with-errors').show();
        }
      } catch(e){
        $('#content-pasos-compra').html(msg);
        //Paso a pasos
        $('#paso-uno').removeClass('activo');
        $('#paso-uno').addClass('superado');
        $('#paso-dos').addClass('activo');
        //Seleccion de lugar
        $('.seleccion_lugar input[type=radio]').on('change', function() {
          if($(this).val()=='sucursal') {
              $('#datos_direccion').hide();
              $('#datos_lugares').show();
          } else {
              $('#datos_lugares').hide();
              $('#datos_direccion').show();
          }
        });
        /*$('#compraEntregasForm').formValidation({
          framework: 'bootstrap',
          fields: {
            lugar: {
              validators: {
                callback: {
                  message: 'Debe seleccionar el lugar de entrega',
                  callback: function (value, validator, $field) {
                    return 0;
                  }
                }
              }
            }
          }
        });
        $('#compraEntregasForm').validator();*/
        //Ajax compra entrega
        $('#compraEntregasForm').submit(function(event) {
          ajax_validacion_entrega($(this).serialize());
          event.preventDefault();
        });
      }
      $('.loader').hide();
      $('#btn-compra-datos').show();
    },
    error: function (request, status, error) {
      $('.help-block.with-errors').html('Ocurrió un error, intente nuevamente');
      $('.help-block.with-errors').show();
      $('.loader').hide();
      $('#btn-compra-datos').show();
    }
  });
}

function ajax_validacion_entrega(data_post){
  //Validar campos entrega
  if($('input[name=entrega_lugar]:checked', '#compraEntregasForm').val() == 'sucursal'){
    if($('#edit-lugar').val() == 0){
      $('.help-block.with-errors').html('Debe seleccionar el lugar de entrega');
      $('.help-block.with-errors').show();
      return;
    } else {
      ajax_compra_entrega(data_post);
    }
  } else {
    if($('input[name=entrega_lugar]:checked', '#compraEntregasForm').val() != 'sucursal') {
      var validado = 1;
      var mensaje = '';
      if($('input[name=entrega_direccion]').val() == ''){
        mensaje = mensaje + 'Debe ingresar la dirección<br>';
        validado = 0;
      }
      if($('input[name=entrega_ciudad]').val() == ''){
        mensaje = mensaje + 'Debe ingresar la ciudada<br>';
        validado = 0;
      }
      if($('input[name=entrega_provincia]').val() == ''){
        mensaje = mensaje + 'Debe ingresar la provincia<br>';
        validado = 0;
      }
      if($('input[name=entrega_cp]').val() == ''){
        mensaje = mensaje + 'Debe ingresar el código postal<br>';
        validado = 0;
      }
      if(validado) {
        var direccion = $('input[name=entrega_direccion]').val()+', '+$('input[name=entrega_ciudad]').val()+', '+$('input[name=entrega_provincia]').val();
        var uid = $('input[name=vendedor_uid]').val();
        var recuest_compra_cobertura = $.ajax({
          method: "POST",
          url: "/tienda/compra_zona_cobertura",
          data: { entrega_direccion: direccion, vendedor_uid: uid, metodo_envio: $('input[name=entrega_lugar]:checked', '#compraEntregasForm').val() },
          beforeSend:function(){
            $(".loader").show();
            $('#btn-compra-envios').hide();
          },
          success:function(msg){
            var result = JSON.parse(msg);
            if(result.status == 'error'){
              $('.help-block.with-errors').html(result.mensaje);
              $('.help-block.with-errors').show();
              $(".loader").hide();
              $('#btn-compra-envios').show();
            } else {
              $('#edit-zona-id').val(result.mensaje.zona_id);
              ajax_compra_entrega(data_post);
            }
          },
          error: function (request, status, error) {
            $('.help-block.with-errors').html('Ocurrió un error, intente nuevamente');
            $('.help-block.with-errors').show();
            $('.loader').hide();
            $('#btn-compra-envios').show();
          }
        });
      } else {
        $('.help-block.with-errors').html(mensaje); 
        $('.help-block.with-errors').show(); 
        return;
      }
    }
  }
}

function ajax_compra_entrega(data_post){
  $.ajax({
    method: "POST",
    url: "/tienda/compra_entregas",
    data: data_post,
    beforeSend:function(){
      $(".loader").show();
      $('#btn-compra-envios').hide();
    },
    success:function(msg){
      try{
        var json = JSON.parse(msg);
        if(json.status == 'error'){
          $('.help-block.with-errors').html(json.mensaje);
          $('.help-block.with-errors').show();
        }
      } catch(e){
        $('#content-pasos-compra').html(msg);
        //Paso a pasos
        $('#paso-dos').removeClass('activo');
        $('#paso-dos').addClass('superado');
        $('#paso-tres').addClass('activo');
        $('#compraPagosForm').submit(function(event) {
          event.preventDefault();
        });
        //Ajax compra MercadoPago
        $('#btn-compra-mercadopago').click(function(event) {
          ajax_compra_pago($('#compraPagosForm').serialize(), 'mercadopago');
          event.preventDefault();
        });
        //Ajax compra DineroMail
        $('#btn-compra-dineromail').click(function(event) {
          ajax_compra_pago($('#compraPagosForm').serialize(), 'dineromail');
          event.preventDefault();
        });
        //Ajax compra TodoPago
        $('#btn-compra-todopago').click(function(event) {
          ajax_compra_pago($('#compraPagosForm').serialize(), 'todopago');
          event.preventDefault();
        });
        //Ajax compra Domicilio
        $('#btn-compra-domicilio').click(function(event) {
          ajax_compra_pago($('#compraPagosForm').serialize(), 'domicilio');
          event.preventDefault();
        });
      }
      $(".loader").hide();
      $('#btn-compra-envios').show();
    },
    error: function (request, status, error) {
      $('.help-block.with-errors').html('Ocurrió un error, intente nuevamente');
      $('.help-block.with-errors').show();
      $('.loader').hide();
      $('#btn-compra-envios').show();
    }
  });
}

function ajax_compra_pago(data_post, servicio){
  $.ajax({
    method: "POST",
    url: "/tienda/compra_pagos/" + servicio,
    data: data_post,
    beforeSend:function(){
      $(".loader").show();
      $('#btn-compra-dineromail').hide();
      $('#btn-compra-mercadopago').hide();
      $('#btn-compra-domicilio').hide();
    },
    success:function(msg){
      try{
        var json = JSON.parse(msg);
        if(servicio == 'domicilio') {
          var status = json.status;
          if(json.status == 'error') status = 'cancel';
          window.location.replace("/tienda/back_url/" + json.mensaje.venta_id + "/" + status + "/domicilio");
        }
        if(json.status == 'error'){
          $('.help-block.with-errors').html(json.mensaje);
          $('.help-block.with-errors').show();
        }
      } catch(e){
        $('html').html(msg);
      }
      $(".loader").hide();
      $('#btn-compra-dineromail').show();
      $('#btn-compra-mercadopago').show();
      $('#btn-compra-domicilio').show();
    },
    error: function (request, status, error) {
      $('.help-block.with-errors').html('Ocurrió un error, intente nuevamente');
      $('.help-block.with-errors').show();
      $('.loader').hide();
      $('#btn-compra-domicilio').show();
    }
  });
}

$(window).load(function(e) {
  //Modales compra
  $('.open-popup-link').magnificPopup({
    type:'inline',
    midClick: true,
    closeBtnInside:true,
    callbacks: {
      open: function() {
        if(mapa_envio!=null) {
          google.maps.event.trigger(mapa_envio, 'resize');
          mapa_envio.setCenter(marcador.position);
          infowindow.open(mapa_envio, marcador);
        }
      },
    }
  });
  
  $('#compraDatosForm').submit(function(event) {
    if($('#RecaptchaField2').css('display') != 'none') {
      if(validarCaptcha)
        ajax_compra_datos($(this).serialize());
    } else {
      ajax_compra_datos($(this).serialize());
    }
    event.preventDefault();
  });
	
  // MAPA 
  $('a.lugar_entrega').click(function() {
    var latitud = $(this).attr('data-latitud');
    var longitud = $(this).attr('data-longitud');
    var nombre = $(this).attr('data-nombre');
    var direccion = $(this).attr('data-direccion');
    mostrar_lugar_entrega(latitud, longitud, nombre, direccion);
    return false;
  });
  if($('#mapa_envio').length>0)
    crear_mapa();
  $('.lugar_entrega:first').click(); // Marcamos en el mapa el primer lugar del listado.
  
  //Costo envios
  $('.calcular_costo_envio').click(function() {
    var direccion = $('#direccion_costo_envio').val();
    var uid = $('#vendedor_uid_costo_envio').val();
    if(direccion.length<6) {
      $('.help-block.with-errors-envio').html('Debe Ingresar una dirección válida.');
      return;
    }
    $.ajax({
      method: "POST",
      url: "/tienda/compra_zona_cobertura",
      data: { entrega_direccion: direccion, vendedor_uid: uid },
      beforeSend:function(){
        $(".loader-envio").show();
        $('.calcular_costo_envio').hide();
      },
      success:function(msg){
        var result = JSON.parse(msg);
        if(result.status == 'error'){
          $('.help-block.with-errors-envio').html(result.mensaje);
          $('.help-block.with-errors-envio').show();
        } else {
          var html = '';
          for(i=0; i<result.envios.length; i++) {
            html += '<br>'+result.envios[i].nombre+' $'+result.envios[i].precio;
          }
          $('.help-block.with-errors-envio').html(html);
          $('.help-block.with-errors-envio').show();
        }
        $(".loader-envio").hide();
        $('.calcular_costo_envio').show();
      }
    });
  });
  if(window.location.hash=='#modal-compra-datos') {
    // Abrimos popup de compra
    $('.open-popup-link').click();
  }
});/* End load function*/

});/* End jQuery*/

function crear_mapa() {
  var posicion = new google.maps.LatLng(-31.36, -64.2);
  var opciones = {
    zoom: 12,
    center: posicion,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  document.getElementById('mapa_envio').style.display = 'block';
  mapa_envio = new google.maps.Map(document.getElementById("mapa_envio"), opciones);
  infowindow = new google.maps.InfoWindow({
    content: "Cargando..."
  });
}

function mostrar_lugar_entrega(latitud, longitud, nombre, direccion) {
  var posicion = new google.maps.LatLng(latitud, longitud);
  if(marcador!=null) {
    marcador.setMap(null);
  }
  marcador = new google.maps.Marker({
    position: posicion,
    map: mapa_envio,
    title: nombre
  });
  infowindow.setContent('<strong>'+nombre+'</strong><br>'+direccion);
  google.maps.event.addListener(marcador, 'click', function() {
    infowindow.open(mapa_envio, marcador);
  });
  mapa_envio.setCenter(posicion);
  infowindow.open(mapa_envio, marcador);
}