//Gmaps
var geocoder;
var map;
var markersArray = [];
function placeMarker(position, map) {
  var coord = position.toString().split(",");
  document.getElementById("map-latitud").value = coord[0].replace("(","");
  document.getElementById("map-longitud").value = coord[1].replace(")","");
  map.clearOverlays();
  var marker = new google.maps.Marker({
    position: position,
    map: map
  });
  markersArray.push(marker);
  map.panTo(position);
}
function codeAddress(address) { 
  if (geocoder) { 
    geocoder.geocode( { 'address': address}, function(results, status) { 
      if (status == google.maps.GeocoderStatus.OK) { 
        var coord = String(results[0].geometry.location);
        coord = coord.split(", ");
        document.getElementById("map-latitud").value = coord[0].replace("(","");
        document.getElementById("map-longitud").value = coord[1].replace(")","");
        
        var latlng = new google.maps.LatLng(coord[0].replace("(",""),coord[1].replace(")",""));
        map.clearOverlays();
        var marker = new google.maps.Marker({
          position: latlng,
          map: map
        });
        markersArray.push(marker);
        map.panTo(latlng);
        map.setZoom(16);
      } else { 
        alert("No se pudo generar la ubicación en el mapa por el siguiente error: " + status); 
      } 
    }); 
  } 
}
function generarMapa() {
  var latitud = -31.409912194070973;
  var longitud = -64.19105529785156;
  if(document.getElementById("map-latitud").value!='') {
    latitud = document.getElementById("map-latitud").value;
    longitud = document.getElementById("map-longitud").value;
  }
  var mapOptions = {
    center: new google.maps.LatLng(latitud, longitud),
    zoom: 11,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    sensor: true
  };
  map = new google.maps.Map(document.getElementById("multimedia_mapas"), mapOptions);
  geocoder = new google.maps.Geocoder(); 
  //Marca direccion cargada
  var provincia = document.getElementById("select-provincia").options[document.getElementById("select-provincia").selectedIndex].text;
  var id_provincia = document.getElementById("select-provincia").value;
  if(id_provincia != ""){
    var ciudad = document.getElementById("select-ciudad-" + id_provincia).options[document.getElementById("select-ciudad-" + id_provincia).selectedIndex].text;
  } else {
    var ciudad = "";
  }
  var calle = document.getElementById("calle").value;
  var numero = document.getElementById("altura").value;
  var direccion = 'Córdoba, Córdoba, Argentina';
  if(provincia != '' && provincia != '- Provincia -'){
    if(provincia != 'Uruguay'){
      direccion = ', Argentina';
    } else {
      direccion = '';
    }
    if(ciudad == 'Otra' || ciudad == '' || ciudad == '- Ciudad -'){
      direccion = provincia+direccion;
    } else {
      direccion = ciudad+', '+provincia+direccion;
    }
    if(calle != '' && numero != ''){
      direccion = calle+' '+numero+', '+direccion;
    } else {
      if(calle != '' && numero == ''){
        direccion = calle+', '+direccion;
      }
    }
  }
  codeAddress(direccion);
  //Marca click en mapa
  google.maps.event.addListener(map, 'click', function(e) {
    placeMarker(e.latLng, map);
  });
  google.maps.Map.prototype.clearOverlays = function() {
    for (var i = 0; i < markersArray.length; i++ ) {
      markersArray[i].setMap(null);
    }
  }
  document.getElementById("multimedia_mapas").style.display = 'block';
}

function cambioRubro(rubro){
  document.forms["formSelectorRubro"].submit();
}

function cambioRubroModal(rubro){
  $('#rubroModal').val(rubro);
  document.forms["formSelectorRubro"].submit();
}

function masContenido(contenedor, ejecutor){
  var el = document.getElementById(contenedor);
  var de = document.getElementById(ejecutor);
  if(el.style.display == 'none'){
    el.style.display = 'block';
    de.className = "mas-campos close";
  } else {
    el.style.display = 'none';
    de.className = "mas-campos";
  }
}
function mostrarKilometros(check, contenedor){
  var el = document.getElementById(contenedor);
  if(check.value == 1)
    el.style.display = 'block';
  else
    el.style.display = 'none';
}
function mostrarEtapaConst(check, contenedor){
  var el = document.getElementById(contenedor);
  if(check.value == 1)
    el.style.display = 'block';
  else
    el.style.display = 'none';
}
function mostrarCiudad(id_provincia){
  var del = document.getElementsByClassName('ciudad');
  for(var i=0;i<del.length;i++){
    del[i].style.display = 'none';
  }
  var el = document.getElementById('ciudad-' + id_provincia.value);
  el.style.display = 'block';
}
function mostrarBarrio(id_ciudad){
  var del = document.getElementsByClassName('barrio');
  for(var i=0;i<del.length;i++){
    del[i].style.display = 'none';
  }
  var el = document.getElementById('barrio-' + id_ciudad.value);
  if(el != undefined){
    el.style.display = 'block';
  }
}
function bloquearSubmit(){
  var botonEnviar = document.getElementById('submit-aviso');
  botonEnviar.disabled = true;
  botonEnviar.value = "Procesando...";
  botonEnviar.style.cursor = "default";
  if($('input[type="file"]').length >= 1) {
    if(isCanvasSupported()) {
      redimensionar_fotos();
    } else {
      if(!validarTamanoFotos()) {
        return false;
      }
    }
  }
  procesarYEnviar();
}
function procesarYEnviar() {
  var cantidad = 0;
  for (var key in fin_procesar_foto) {
    cantidad++;
  }
  if(cantidad==0) {
    var botonEnviar = document.getElementById('submit-aviso');
    botonEnviar.value = "Enviando...";
    document.forms["formCreacionAviso"].submit();
    return;
  } else {
    setTimeout('procesarYEnviar()', 1000);
  }
}
 
var fin_procesar_foto = {}; // variable para detectar cuando se han procesado todas las fotos.

function redimensionar_fotos() {
  var filesToUpload = $('input[type="file"]')[0].files;
  var file = filesToUpload[0];
  if(file!=undefined) {
    redimensionar_foto(file, 'foto');
  }
  filesToUpload = $('input[type="file"]')[1].files;
  file = filesToUpload[0];
  if(file!=undefined) {
    redimensionar_foto(file, 'foto1');
  }
  filesToUpload = $('input[type="file"]')[2].files;
  file = filesToUpload[0];
  if(file!=undefined) {
    redimensionar_foto(file, 'foto2');
  }
}

function redimensionar_foto(file, nombre) {
  if(!file.type.match(/image.*/)) {
    return false;
  }
  fin_procesar_foto[nombre] = false;
  // Create an image
  var img = document.createElement("img");
  // Create a file reader
  var reader = new FileReader();
  // Set the image once loaded into file reader
  reader.onload = function(e) {
    
    img.onload = function() {
      // Esperamos que cargue la imagen para poder tomar sus dimensiones.
      var canvas = document.createElement("canvas");
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);

      var MAX_WIDTH = 800;
      var MAX_HEIGHT = 600;
      var width = img.width;
      var height = img.height;
      
      var x_ratio = MAX_WIDTH / width;
      var y_ratio = MAX_HEIGHT / height;

      if((width <= MAX_WIDTH) && (height <= MAX_HEIGHT)){
        width = width;
        height = height;
      } else if((x_ratio * height) < MAX_HEIGHT) {
        height = Math.ceil(x_ratio * height);
        width = MAX_WIDTH;
      } else {
        width = Math.ceil(y_ratio * width);
        height = MAX_HEIGHT;
      }
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");
      ctx.beginPath();
      ctx.rect(0, 0, width, height);
      ctx.fillStyle = "#FFFFFF";
      ctx.opacity = 1.0;
      ctx.imageSmoothingQuality = 'high';
      ctx.fill(); // Rellenamos color blanco para PNG con fondo transparente.
      ctx.drawImage(img, 0, 0, width, height);
      var tipo_imagen = 'image/jpeg'; // file.type; // Forzamos a que convierta a jpeg ya que no es buena la compresion en png
      var dataurl = canvas.toDataURL(tipo_imagen); 
      // Quitamos la metadata [<media type>][;charset=<character set>][;base64],<data>
      dataurl = dataurl.replace(/^data:image\/(png|jpg|jpeg|gif);base64,/, '');
      var input = document.createElement('input');
      input.type = 'hidden';
      input.name = nombre+'[data]';
      input.value = dataurl;
      var input_type = document.createElement('input');
      input_type.type = 'hidden';
      input_type.name = 'field_aviso_'+nombre+'[type]';
      input_type.value = tipo_imagen;
      var form = document.getElementById('formCreacionAviso');
      form.appendChild(input);
      form.appendChild(input_type);
      $('input[name="field_aviso_'+nombre+'"]').remove();
      $('input[name="'+nombre+'[data]"]').attr('name', 'field_aviso_'+nombre+'[data]');
      delete fin_procesar_foto[nombre];
    };
    img.src = e.target.result;
  }
  // Load files into file reader
  reader.readAsDataURL(file);
}

function isCanvasSupported() {
  var elem = document.createElement('canvas');
  return !!(elem.getContext && elem.getContext('2d'));
}

// Para el caso que no se pueda redimensionar, validamos antes de enviar.
function validarTamanoFotos() {
  var mensaje = '';
  var filesToUpload = $('input[type="file"]')[0].files;
  var file = filesToUpload[0];
  if(file!=undefined) {
    if(file.size>300*1024) {
      mensaje += 'La foto es demasiado grande.<br>';
      $('input[name="field_aviso_foto"]').parent('.campo').addClass('campo-error');
    }
  }
  filesToUpload = $('input[type="file"]')[1].files;
  file = filesToUpload[0];
  if(file!=undefined) {
    if(file.size>300*1024) {
      mensaje += 'La foto 1 es demasiado grande.<br>';
      $('input[name="field_aviso_foto1"]').parent('.campo').addClass('campo-error');
    }
  }
  filesToUpload = $('input[type="file"]')[2].files;
  file = filesToUpload[0];
  if(file!=undefined) {
    if(file.size>300*1024) {
      mensaje += 'La foto 2 es demasiado grande.<br>';
      $('input[name="field_aviso_foto2"]').parent('.campo').addClass('campo-error');
    }
  }
  if(mensaje!='') {
    mensaje += 'Tamaño máximo de 300kb.<br>';
    $('.aviso .message').html(mensaje);
    $('.aviso .message').show();
    $('html, body').animate({ scrollTop: 0 }, 'fast');
    return false;
  }
  return true;
}

// Tabs creación de aviso
function seccionForm(seccion, accion){
  $('.content-form').hide('');
  $('#'+seccion).show('');
  if(accion == 'edicion')
    $('.tab-form').removeClass('selected');  
  $('.'+seccion).addClass('selected');
  window.scrollTo(0, 0);
  
  if(seccion != 'content-datos') {
    $('.btn-anterior').show('');
    switch(seccion) {
      case 'content-ubicacion':
        $('.btn-anterior').attr('href', "javascript:seccionForm('content-datos', '" + accion + "');");
        if(accion == 'creacion') {
          $('#submit-aviso').attr('onClick', "javascript:seccionForm('content-propiedad', '" + accion + "');");
          $('.content-datos').attr('onClick', "javascript:seccionForm('content-datos', '" + accion + "');");
          $('.'+seccion).attr('onClick', "javascript:seccionForm('" + seccion + "', '" + accion + "');");
        }
        setTimeout(generarMapa, 1000, 'inicial');
        break;
      case 'content-propiedad':
        $('.btn-anterior').attr('href', "javascript:seccionForm('content-ubicacion', '" + accion + "');");
        if(accion == 'creacion') {
          $('#submit-aviso').attr('onClick', "javascript:seccionForm('content-multimedia', '" + accion + "');");
          $('.'+seccion).attr('onClick', "javascript:seccionForm('" + seccion + "', '" + accion + "');");
        }
        break;
      case 'content-multimedia':
        $('.btn-anterior').attr('href', "javascript:seccionForm('content-propiedad', '" + accion + "');");
        if(accion == 'creacion') {
          $('#submit-aviso').attr('onClick', "javascript:seccionForm('content-calendario', '" + accion + "');");
          $('.'+seccion).attr('onClick', "javascript:seccionForm('" + seccion + "', '" + accion + "');");
        }
        break;
      case 'content-calendario':
        $('.btn-anterior').attr('href', "javascript:seccionForm('content-multimedia', '" + accion + "');");
        if(accion == 'creacion') {
          $('#submit-aviso').attr('onClick', "javascript:seccionForm('content-pagos', '" + accion + "');");
          $('.'+seccion).attr('onClick', "javascript:seccionForm('" + seccion + "', '" + accion + "');");
        }
        break;
      case 'content-pagos':
        $('.btn-anterior').attr('href', "javascript:seccionForm('content-calendario', '" + accion + "');");
        if(accion == 'creacion') {
          $('#submit-aviso').attr('onClick', 'javascript:bloquearSubmit();');
          $('#submit-aviso').val('Guardar');
          $('.'+seccion).attr('onClick', "javascript:seccionForm('" + seccion + "', '" + accion + "');");
        }
        break;
    } 
  } else {
    $('.btn-anterior').hide('');
  }
}

//Generar reserva desde el administrador de avisos
function adminGenerarReserva(nid, startDate, endDate) {
  var urlAjax = '/calendario/save_reserva_admin/' + nid + '/' + startDate + '/' + endDate;
  $.ajax({
  url: urlAjax,
  cache: false})
  .done(function(data) {
  });
}

function mostrar_precio_uva() {
  if($('input[name=field_aviso_precio]').val() != '') {
    $.get( "/administrar/obtener_precio_uva/" + $('input[name=field_aviso_precio]').val() + "/" + $('input[name=field_aviso_moneda]:checked', '#formCreacionAviso').val(), function( data ) {
      if(data != '' && data != 0)
        $('.mont-convertido-uva').html('<span class="monto">' + data + ' UVAS</span>');
      else
        $('.mont-convertido-uva').html('<span class="error">No se pudo calcular el precio UVA, intente mas tarde.</span>');        
    });
  }
}

jQuery(document).ready(function($){
  // Habilitar/Desahabilitar pagos opcionales
  // Garantia
  if(!$('#checkbox_garantia').is(':checked')) {
    $('#deposito-monto').show('fast');
    $('#field_aviso_garantia_monto').val('');
  }
  $('#checkbox_garantia').bind('touchstart', function(){
    if($('#checkbox_garantia').is(':checked')) {
      $('#deposito-monto').show('fast');
      $('#field_aviso_garantia_monto').val('');
    } else {
      $('#deposito-monto').hide('fast');
    }
  });
  // Limpieza
  if($('#radio_limpieza_1').is(':checked') || $('#radio_limpieza_2').is(':checked')) {
    $('#limpieza-monto').show('fast');
    $('#field_aviso_limpieza_monto').val('');
  }
  $('#radio_limpieza_1').bind('touchstart', function(){
    $('#limpieza-monto').show('fast');
    $('#field_aviso_limpieza_monto').val('');
  });
  $('#radio_limpieza_2').bind('touchstart', function(){
    $('#limpieza-monto').show('fast');
    $('#field_aviso_limpieza_monto').val('');
  });
  $('#radio_limpieza_3').bind('touchstart', function(){
    $('#limpieza-monto').hide('fast');
  });
  // Desayuno
  if($('#radio_desayuno_1').is(':checked') || $('#radio_desayuno_2').is(':checked')) {
    $('#desayuno-monto').show('fast');
    $('#field_aviso_desayuno_monto').val('');
  }
  $('#radio_desayuno_1').bind('touchstart', function(){
    $('#desayuno-monto').show('fast');
    $('#field_aviso_desayuno_monto').val('');
  });
  $('#radio_desayuno_2').bind('touchstart', function(){
    $('#desayuno-monto').show('fast');
    $('#field_aviso_desayuno_monto').val('');
  });
  $('#radio_desayuno_3').bind('touchstart', function(){
    $('#desayuno-monto').hide('fast');
  });
  // Cochera
  if($('#radio_cochera_1').is(':checked') || $('#radio_cochera_2').is(':checked')) {
    $('#cochera-monto').show('fast');
    $('#field_aviso_cochera_monto').val('');
  }
  $('#radio_cochera_1').bind('touchstart', function(){
    $('#cochera-monto').show('fast');
    $('#field_aviso_cochera_monto').val('');
  });
  $('#radio_cochera_2').bind('touchstart', function(){
    $('#cochera-monto').show('fast');
    $('#field_aviso_cochera_monto').val('');
  });
  $('#radio_cochera_3').bind('touchstart', function(){
    $('#cochera-monto').hide('fast');
  });
  
  if($("#aviso-operaciones").length > 0) {
    // Mostrar/Ocutar precio UVA
    if($("#aviso-operaciones").val()==2) {
      $(".form-precio-uva").show();
      $(".mont-convertido-uva").show();
      $(".cotizacion-uva").show();
    } else {
      $(".form-precio-uva").hide();
      $(".mont-convertido-uva").hide();
      $(".cotizacion-uva").hide();
    }
  }
  $("#aviso-operaciones").change(function() {
    // Mostrar/Ocutar precio UVA
    if($("#aviso-operaciones").val()==2) {
      $(".form-precio-uva").show();
      $(".mont-convertido-uva").show();
      $(".cotizacion-uva").show();
    } else {
      $(".form-precio-uva").hide();
      $(".mont-convertido-uva").hide();
      $(".cotizacion-uva").hide();
      $("#checkbox_precio_uva").val(0);
      $("#checkbox_precio_uva").attr('checked', '');
    }
  });
  
  // Calcular precios UVA
  if($("#checkbox_precio_uva").length > 0) {
    if($("#checkbox_precio_uva").attr('checked') == 'checked')
      mostrar_precio_uva();
    $('input[name=field_aviso_precio]').change(function() {
      $('.mont-convertido-uva').html('<a href="javascript:void(0);" onclick="mostrar_precio_uva();">Recalcular UVAS</a>');
    });
    
    $('input[name=field_aviso_moneda]').click(function() {
      $('.mont-convertido-uva').html('<a href="javascript:void(0);" onclick="mostrar_precio_uva();">Recalcular UVAS</a>');
    });
    
  }
  $("#checkbox_precio_uva").click(function() {
    if(this.checked == true)
      mostrar_precio_uva();
    else
      $('.mont-convertido-uva').html('');
  });
  
  // Ocultar/Mostrar telefono whatsapp
  if($("#checkbox_ocultar_whatsapp").length > 0) {
    if($("#checkbox_ocultar_whatsapp").attr("checked")){
      $("#telefono_whatsapp").attr("value", "");
      $("#telefono_whatsapp").css('background-color', '#D6D6D6');
    }
    $("#checkbox_ocultar_whatsapp").click(function(){
      if($(this).is(':checked')){
        $("#telefono_whatsapp").attr("value", "");
        $("#telefono_whatsapp").css('background-color', '#D6D6D6');
      } else {
        $("#telefono_whatsapp").css('background-color', 'white');
        if($("#telefono_whatsapp").attr("data-hidden") != '')
          $("#telefono_whatsapp").attr("value", $("#telefono_whatsapp").attr("data-hidden"));
      }
    });
  }
  
  // Ordenar combo Provincias
  if($("#select-provincia").length > 0) {
    var selectToSort = $("#select-provincia");
    selectToSort.children('option').each(function(index) {
      if($(this).html() == 'Mendoza')
        $(this).prependTo(selectToSort);
    });
    var selectTop = selectToSort.children('option')[1];
    selectToSort.prepend(selectTop);
  }
  
});