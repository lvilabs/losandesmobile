/*
 * Envio de page view para ga y cxense
 */
function send_page_view() {
  
  // Google Analytics
  ga('send', 'pageview', window.location.pathname);
  
  // Cxense
  if (typeof window.cX !== 'undefined') {
      if (cX.initializePage instanceof Function) {
          cX.initializePage();
      }
      if (cX.setSiteId instanceof Function) {
          cX.setSiteId('9222344500647882885');
      }
      if (cX.sendPageViewEvent instanceof Function) {
          cX.sendPageViewEvent(); // Cxense
      }
  }
  
  // ComScore
  var dax_src = '';
  $('img').each(function(index) {
    if($(this).attr('src').indexOf('scorecardresearch.com') > 0)
      dax_src = $(this).attr('src');
  });
  if(dax_src != '') {
    dax_src = dax_src.replace('http://b.scorecardresearch.com', '');
    // <![CDATA[
    var ns_p;
    function udm_(e) {
        var t = "comScore=", n = document, r = n.cookie, i = "", s = "indexOf", o = "substring",
            u = "length", a = 2048, f, l = "&ns_", c = "&", h, p, d, v, m = window,
            g = m.encodeURIComponent || escape;
        if (r[s](t) + 1)for (d = 0, p = r.split(";"), v = p[u]; d < v; d++)h = p[d][s](t), h + 1 && (i = c + unescape(p[d][o](h + t[u])));
        e += l + "_t=" + +(new Date) + l + "c=" + (n.characterSet || n.defaultCharset || "") + "&c8=" + g(n.title) + i + "&c7=" + g(n.URL) + "&c9=" + g(n.referrer), e[u] > a && e[s](c) > 0 && (f = e[o](0, a - 8).lastIndexOf(c), e = (e[o](0, f) + l + "cut=" + g(e[o](f + 1)))[o](0, a)), n.images ? (h = new Image, m.ns_p || (ns_p = h), h.src = e) : n.write("<", "p", "><", 'img src="', e, '" height="1" width="1" alt="*"', "><", "/p", ">")
    };
    udm_('http' + (document.location.href.charAt(4) == 's' ? 's://sb' : '://b') +
        '.scorecardresearch.com' + dax_src);
    // ]]>
  }
  
}
/*
 * Envio de eventos para ga
 */
function send_event_ga(category, action, label) {
  category = category || 'evento_sin_categoria';
  action = action || 'evento_sin_accion';
  label = label || '';
  if (typeof(ga) !== 'undefined') {
    ga('REDLaVoz.send', 'event', category, action, label);
  }
}
/*
 * Envio de eventos para cxense
 */
function send_event_cx(type, customParameters, origin) {
  // console.log(type + ', ' + customParameters + ', ' + origin);
  customParameters = customParameters || {};
  origin = origin || 'gcl-website';
  if (typeof cX !== 'undefined') {
    cX.callQueue.push(['setEventAttributes', {
      origin: origin,
      persistedQueryId: '652c9e049df18eee90bbad0955aefd2c0feb9082'
    }]);
    cX.callQueue.push(['sendEvent', type, customParameters]);
  }
}

var bloqueoScroll = false;
function checkScroll() {
  
  var activePage = $('body').height(),
  screenHeight = document.documentElement.clientHeight,
  scrolled = $(window).scrollTop() + $('.banner.top').height() + $('.bottom .sub').height(),
  scrollEnd = activePage - screenHeight;
  
  if (scrolled >= scrollEnd && bloqueoScroll == false) {
    
    $('.spinner').show();
    
    var filtros = $('#filtros').val();
    var page_inicio = $('#pagina_inicio').val();
    var page_fin = $('#pagina_fin').val();
    
    if(page_inicio == page_fin) { 
      // Ajax resultado
      if($('#infinity').length > 0) {
        $.ajax({
          url: '/search/infinity_result' + filtros + '/page=' + page_inicio,
          dataType: 'html',
          beforeSend: function( xhr ) {
            page_inicio = parseInt(page_inicio) + 1;
            $('#pagina_inicio').val(page_inicio);
            bloqueoScroll = true;
          },
          success: function(html) {
            if(html.length < 10) {
              bloqueoScroll = true;
            } else {
              var pathname = window.location.pathname;
              if(pathname.indexOf('/page=') > 0) {
                pathname = pathname.split('/');
                pathname.splice(pathname.length-1, 1);  
                pathname = pathname.join('/');
              }
              history.pushState(null, "", pathname + '/page=' + page_inicio);
              $('#infinity').append(html);
              $('#pagina_fin').val(page_inicio);
              
              // Analyticas
              send_page_view();
              // send_event_ga('gcl-categories', $('#ga_categoria').attr('value'), '');
              // ga('REDLaVoz.set', 'dimension4', $('#ga_categoria').attr('value'));
              
              $('.link-search-contacto').magnificPopup({
                type:'inline',
                midClick: true,
                closeBtnInside:true,
                callbacks: {
                  open: function() {
                    
                  },
                }
              });
              $('.link-search-contacto').click(function(){
                $('#edit-contactar-vendedor-aviso').val($(this).attr('data-nid'));
                $('#contacto-titulo').html($(this).attr('data-title'));
                $('#contactar-vendedor-formulario .messages').hide('');
              });
              
              // Favoritos
              checkFavourites();
              $(".favourite").click(function(event) {
                var nid = $(this).attr('data-nid');
                addFavourites(nid);
              });
              
              //Precios UVAS
              if($('.search-precio-uva').length > 0) {
                $('.search-precio-uva').each(function(index) {
                  if($(this).attr('data-precio') != '' && $(this).attr('data-precio') != 'consultar') {
                    var id_aviso = $(this).attr('data-aviso');
                    var moneda = 1;
                    if($(this).attr('data-moneda') == 'U$S' || $(this).attr('data-moneda') == 2)
                      moneda = 2;
                    $.get( "/administrar/obtener_precio_uva/" + $(this).attr('data-precio') + "/" + moneda, function( data ) {
                      if(data != '' && data != '0,00')
                        $('.search-precio-uva.' + id_aviso).html(data + ' UVAS');
                      else
                        $('.search-precio-uva.' + id_aviso).html('');        
                    });
                  }
                });
              }
              
              bloqueoScroll = false;
            }
            $('.spinner').hide();
          }
        });
      }
      // Ajax favoritos
      if($('#infinity-favoritos').length > 0) {
        $.ajax({
          url: '/administrar/infinity_favoritos/page=' + page_inicio,
          dataType: 'html',
          beforeSend: function( xhr ) {
            page_inicio = parseInt(page_inicio) + 1;
            $('#pagina_inicio').val(page_inicio);
            bloqueoScroll = true;
          },
          success: function(html) {
            if(html.length < 10) {
              bloqueoScroll = true;
            } else {
              var pathname = window.location.pathname;
              if(pathname.indexOf('/page=') > 0) {
                pathname = pathname.split('/');
                pathname.splice(pathname.length-1, 1);  
                pathname = pathname.join('/');
              }
              history.pushState(null, "", pathname + '/page=' + page_inicio);
              $('#infinity-favoritos').append(html);
              $('#pagina_fin').val(page_inicio);
              
              // Analyticas
              send_page_view();
              
              // Favoritos
              checkFavourites();
              $(".favourite").click(function(event) {
                var nid = $(this).attr('data-nid');
                addFavourites(nid);
              });
              
              bloqueoScroll = false;
            }
            $('.spinner').hide();
          }
        });
      }
      // Ajax historial
      if($('#infinity-historial').length > 0) {
        $.ajax({
          url: '/administrar/infinity_historial/page=' + page_inicio,
          dataType: 'html',
          beforeSend: function( xhr ) {
            page_inicio = parseInt(page_inicio) + 1;
            $('#pagina_inicio').val(page_inicio);
            bloqueoScroll = true;
          },
          success: function(html) {
            if(html.length < 10) {
              bloqueoScroll = true;
            } else {
              var pathname = window.location.pathname;
              if(pathname.indexOf('/page=') > 0) {
                pathname = pathname.split('/');
                pathname.splice(pathname.length-1, 1);  
                pathname = pathname.join('/');
              }
              history.pushState(null, "", pathname + '/page=' + page_inicio);
              $('#infinity-historial').append(html);
              $('#pagina_fin').val(page_inicio);
              
              // Analyticas
              send_page_view();
              
              bloqueoScroll = false;
            }
            $('.spinner').hide();
          }
        });
      }
    }
  }
}

function addFavourites(nid) {
  var $shortlist = [];
  if (Cookies.get('clviFavourites')) {
    $unstrung = $.parseJSON(Cookies.get('clviFavourites'));
    var found = $.inArray(nid, $unstrung);
    if (found >= 0) {
      $unstrung.splice(found, 1);
      if($('#favoritos-'+nid).length >= 1)
        $('#favoritos-'+nid).hide('');
    } else {
      $unstrung.push(nid);
    }
    if($unstrung.length >= 1)
      $stringify = JSON.stringify($unstrung);
    else 
      $stringify = '';
    Cookies.set('clviFavourites', $stringify);
  } else {
    $shortlist.push(nid);
    $stringify = JSON.stringify($shortlist);
    Cookies.set('clviFavourites', $stringify);
  }
  if($stringify == '') { 
    $('.favourite-top i').addClass('far');
    $('.favourite-top i').removeClass('fas');
  } else {
    $('.favourite-top i').addClass('fas');
    $('.favourite-top i').removeClass('far');
  }
  //console.log("Cookie contains: " + Cookies.get('clviFavourites'));
  //checkFavourites();
}
function checkFavourites() {
  if(Cookies.get('clviFavouritesOld')) {
    if(!logged_in) {
      Cookies.set('clviFavouritesOld', '');
    } else {
      var oldFavoritos = $.parseJSON(Cookies.get('clviFavouritesOld'));
      if(oldFavoritos != '') {
        if(Cookies.get('clviFavourites')) {
          var favoritos = $.parseJSON(Cookies.get('clviFavourites'));
          for(var i = 0; i < oldFavoritos.length; i++){
            var found = $.inArray(oldFavoritos[i], favoritos);
            if (found >= 0) {
              //favoritos.splice(found, 1);
            } else {
              favoritos.push(oldFavoritos[i]);
            }
          }
          $stringify = JSON.stringify(favoritos);  
        } else {
          $stringify = JSON.stringify(oldFavoritos);
        }
        Cookies.set('clviFavourites', $stringify);
      }
    }
  }
  $(".favourite").each(function() {
    var nid = $(this).attr('data-nid');
    if (Cookies.get('clviFavourites')) {
      $unstrung = $.parseJSON(Cookies.get('clviFavourites'));
      var found = $.inArray(nid, $unstrung);
      if (found >= 0)
        $(this).prop("checked", true);
    }
  });
}
function detectFavourites() {
  if (Cookies.get('clviFavourites')) {
    var favoritos = Cookies.get('clviFavourites');
    var favoritosArray = $.parseJSON(favoritos);
    favoritosArray.sort();
    favoritosArray = JSON.stringify(favoritosArray);
    if(Cookies.get('clviFavouritesOld')) {
      var favoritosOld = $.parseJSON(Cookies.get('clviFavouritesOld'));
      favoritosOld.sort();
      favoritosOld = JSON.stringify(favoritosOld);
      if(favoritosArray != favoritosOld) {
        updateFavourites(favoritos);
      }      
    } else {
      updateFavourites(favoritos);
    }
  }
}
function updateFavourites(favoritos) {
  // Si no esta logueado muestro mensaje
  if(!logged_in) {
    $('#mensaje-favoritos-nologin').show('');
  // Actualizo favoritos
  } else {
    $.ajax({
      url: '/user/update_favoritos',
      type: 'get',
      data: {favoritos: favoritos},
      dataType: 'json',
      success: function(json) {
        $stringify = JSON.stringify(json);
        Cookies.set('clviFavouritesOld', $stringify);
      }
    });
  }
}

function addHistorial(nid) {
  var $shortlist = [];
  var $data = [];
  var date = new Date();
  var time = date.getTime();
  if (Cookies.get('clviHistorial')) {
    $unstrung = $.parseJSON(Cookies.get('clviHistorial'));
    var founded = -1;
    for(i = 0; i < $unstrung.length; i++) {
      var found = $.inArray(nid, $unstrung[i]);
      if (found >= 0)
        founded = i;
    }
    $data = [nid, time];
    if (founded != -1) {
      $unstrung.splice(founded, 1);
      $unstrung.push($data);
    } else {
      if($unstrung.length >= 60)
        $unstrung.pop();
      $unstrung.push($data);
    }
    if($unstrung.length >= 1)
      $stringify = JSON.stringify($unstrung);
    else 
      $stringify = '';
    Cookies.set('clviHistorial', $stringify, { expires: 1 });
  } else {
    $data = [nid, time];
    $shortlist.push($data);
    $stringify = JSON.stringify($shortlist);
    Cookies.set('clviHistorial', $stringify, { expires: 1 });
  }
  //console.log("Cookie contains: " + Cookies.get('clviHistorial'));
}

function addContactoCookies(datos) {
  var datos_array = datos.split("&");
  var datos_contacto = [];
  var contacto_mail = 0;
  var contacto_tel = 0;
  for (i = 0; i < datos_array.length; i++) {
    var campo = datos_array[i].split("=");
    if(campo[0] == 'contactar_vendedor_nombre' || campo[0] == 'contactar_vendedor_telefono' || campo[0] == 'contactar_vendedor_mail') {
      if(campo[0] == 'contactar_vendedor_telefono' && campo[1] != '') contacto_tel = 1;
      if(campo[0] == 'contactar_vendedor_mail' && campo[1] != '') contacto_mail = 1;
      datos_contacto.push([campo[0], campo[1]]);
    }
  }
  if(contacto_tel == 1 || contacto_mail == 1) {
    $stringify = JSON.stringify(datos_contacto);
    Cookies.set('clviContact', $stringify); 
  }
  //console.log("Cookie contains: " + Cookies.get('clviContact'));
}

window.onbeforeunload = function () {
  if($(".spinner").length > 0) {
    window.scrollTo(0, 0); 
  }
}

jQuery(document).ready(function ($) {
  cargar_datos_cuenta(0);
  cargar_datos_menu(0);
  $("#formCreacionAviso input").keydown(verificar_label);
  $("#formCreacionAviso textarea").keydown(verificar_label);
  $("#formCreacionAviso select").change(verificar_label);
  $("#formCreacionAviso input").focusout(verificar_label);
  $("#formCreacionAviso textarea").focusout(verificar_label);
  $("#formCreacionAviso select").focusout(verificar_label);
  revisar_todos_labels();
  
  $('#formContact').submit(function(event) {
    if($('#RecaptchaField').css('display') != 'none') {
      if(!validarCaptcha)
        event.preventDefault();
    }
  });
  
  // Si estamos en la ficha contamos la visita via ajax y agregamos al historial
  if($('.section-aviso article.aviso').length > 0 || $('.ficha-mob').length > 0){
    var pathname = window.location.pathname;
    pathname = pathname.split('/');
    contador_visitas_ajax(pathname[3]);
    addHistorial(pathname[3]);
  }
  
  //Modal contacto resultado de busqueda
  if($('.link-search-contacto').length > 0) {
    $('.link-search-contacto').magnificPopup({
      type:'inline',
      midClick: true,
      closeBtnInside:true,
      callbacks: {
        open: function() {
          
        },
      }
    });
    $('.link-search-contacto').click(function(){
      $('#edit-contactar-vendedor-aviso').val($(this).attr('data-nid'));
      $('#contacto-titulo').html($(this).attr('data-title'));
      $('#contactar-vendedor-formulario .messages').hide('');
    });
    $('#contactar-vendedor-formulario').submit(function(event) {
      event.preventDefault();
      //Validacion de campos
      $('#contactar-vendedor-formulario .messages').hide('');
      $('.form-text').removeClass('error');
      $('.form-textarea').removeClass('error');
      var envio = true;
      var mensaje = '';
      if($("#edit-contactar-vendedor-telefono").val() == "" && $("#edit-contactar-vendedor-mail").val() == ""){
        $("#edit-contactar-vendedor-telefono").addClass('error');
        $("#edit-contactar-vendedor-mail").addClass('error');
        mensaje = 'Campos requeridos';
        envio = false;
      } else {
        if($("#edit-contactar-vendedor-telefono").val() != "") {
          if(telefonoFormValidate($("#edit-contactar-vendedor-telefono").val()) === false){
            $("#edit-contactar-vendedor-telefono").addClass('error');
            mensaje = 'Campo no válido';
            envio = false;
          }
        }
        if($("#edit-contactar-vendedor-mail").val() != "") {
          if(mailFormValidate($("#edit-contactar-vendedor-mail").val()) === false){
            $("#edit-contactar-vendedor-mail").addClass('error');
            mensaje = 'Campo no válido';
            envio = false;
          }
        }
      }
      if($("#edit-captcha-response").length>0 && $("#edit-captcha-response").val() == "") {
        $("#contactar-vendedor-formulario input[name=captcha_response]").addClass('error');
        mensaje = 'Campos requeridos';
        envio = false;
      }
      if(envio == false) {
        $('#contactar-vendedor-formulario .messages').html(mensaje);
        $('#contactar-vendedor-formulario .messages').show('');
        return false;
      } else {
        $("#edit-contactar-vendedor-submit").attr("value", "Enviando...");
        $("#edit-contactar-vendedor-submit").attr('disabled', 'disabled');
        ajaxEnvioContacto();
      }
    });
  }
  
  $('.llamarMultipleClose').click(function() {
    $(".llamarMultiple").hide('');
  });
  $('.contactar .llamar').click(function() {
    if($(".llamarMultiple").length > 0) {
      $(".llamarMultiple").show('');
    } else {
      var nid = $('[name="contactar_vendedor_aviso"]').val();
      var datos_contacto = '';
      if (Cookies.get('clviContact'))
        datos_contacto = Cookies.get('clviContact');
      $.ajax({
        url: '/node/contador_vista/contacto_telefono/'+nid,
        method: 'POST',
        data: {contacto: datos_contacto},
        dataType: 'json',
        async: false,
        success: function(data) {
        }
      });
      // Evento contacto ga
      // send_event_ga('contacto', 'telefono', 'mobile');
    }
  });
  $('.contactar .llamarMultiple a').click(function() {
    var nid = $('[name="contactar_vendedor_aviso"]').val();
    var datos_contacto = '';
    if (Cookies.get('clviContact'))
      datos_contacto = Cookies.get('clviContact');
    $.ajax({
      url: '/node/contador_vista/contacto_telefono/'+nid,
      method: 'POST',
      data: {contacto: datos_contacto},
      dataType: 'json',
      async: false,
      success: function(data) {
      }
    });
    // Evento contacto ga
    // send_event_ga('contacto', 'telefono', 'mobile');
  });
  $('.smsMultipleClose').click(function() {
    $(".smsMultiple").hide('');
  });
  $('.contactar .sms').click(function() {
    if($(".smsMultiple").length > 0) {
      $(".smsMultiple").show('');
    } else {
      var nid = $('[name="contactar_vendedor_aviso"]').val();
      var datos_contacto = '';
      if (Cookies.get('clviContact'))
        datos_contacto = Cookies.get('clviContact');
      $.ajax({
        url: '/node/contador_vista/contacto_sms/'+nid,
        method: 'POST',
        data: {contacto: datos_contacto},
        dataType: 'json',
        async: false,
        success: function(data) {
        }
      });
      // Evento contacto ga
      // send_event_ga('contacto', 'sms', 'mobile');
    }
  });
  $('.contactar .smsMultiple a').click(function() {
    var nid = $('[name="contactar_vendedor_aviso"]').val();
    var datos_contacto = '';
    if (Cookies.get('clviContact'))
      datos_contacto = Cookies.get('clviContact');
    $.ajax({
      url: '/node/contador_vista/contacto_sms/'+nid,
      method: 'POST',
      data: {contacto: datos_contacto},
      dataType: 'json',
      async: false,
      success: function(data) {
      }
    });
    // Evento contacto ga
    // send_event_ga('contacto', 'sms', 'mobile');
  });
  $('.contactar .whatsapp').click(function() {
    var nid = $('[name="contactar_vendedor_aviso"]').val();
    var datos_contacto = '';
    if (Cookies.get('clviContact'))
      datos_contacto = Cookies.get('clviContact');
    $.ajax({
      url: '/node/contador_vista/contacto_whatsapp/'+nid,
      method: 'POST',
      data: {contacto: datos_contacto},
      dataType: 'json',
      async: false,
      success: function(data) {
      }
    });
    // Evento contacto ga
    // send_event_ga('contacto', 'whatsapp', 'mobile');
  });
  $('.contactar .enviar').click(function() {
    var nid = $('[name="contactar_vendedor_aviso"]').val();
    var datos_contacto = '';
    if (Cookies.get('clviContact'))
      datos_contacto = Cookies.get('clviContact');
    $.ajax({
      url: '/node/contador_vista/contacto_mail/'+nid,
      method: 'POST',
      data: {contacto: datos_contacto},
      dataType: 'json',
      async: false,
      success: function(data) {
      }
    });
    // Evento contacto ga
    // send_event_ga('contacto', 'mail', 'mobile');
  });
  
  // Scroll infinito
  if($(".spinner").length > 0) {
    $(window).on('scroll', checkScroll); 
    document.addEventListener("touchmove", checkScroll, false);
  }
  
  // Favoritos
  if($('.search-main').length > 0 || $('.aviso').length > 0 || $('.ficha-mob').length > 0) {
    checkFavourites();
    $(".favourite").click(function(event) {
      var nid = $(this).attr('data-nid');
      addFavourites(nid);
    });
  }
  
  //Modal Informativo
  /*if($("#bkg").length){
    if(!Cookies.get('clviInfoFavoritos')) {   
      $("#bkg").click(function () {    
        $("#popover").hide();
        $("#bkg").fadeOut("500"); 
        if($("#popover.Resultado").length > 0) {
          $(".favourite-results").css("z-index", "0");
        }
      });
      if (document.getElementById('bkg').style.visibility == 'hidden') {
        document.getElementById('bkg').style.visibility = '';
        $("#bkg").hide();
      }
      $("#bkg").fadeIn(500, "linear", function () { $("#popover").show(800, "swing"); });
      if($("#popover.Resultado").length > 0) {
        $(".favourite-result").css("z-index", "9999");
      }
      Cookies.set('clviInfoFavoritos', 1, { expires: 7 });
    }
  }*/
  
  // Expandir buscador
  $('.icono-buscar').click(function() {
    $('.filtro.avanzados').hide('');
    $('.fondo').animate({
      width: '92%'
    }, 800, function() { $('.buscador-hidde').show('fast');
    });
  });
  // Ocultar buscador
  $('.search-close').click(function() {
    $('.fondo').animate({
      width: '24px'
    }, 800, function() {  
      $('.filtro.avanzados').show('fast');
    });
    $('.buscador-hidde').hide('');
  });
  // Mostrar buscador
  /*if($('.key-active').length > 0){
    $('.filtro.avanzados').hide('');
    $('.fondo').animate({
      width: '92%'
    }, 800, function() {
    });
    $('.buscador-hidde').show('');
  }*/
  
  // Datos Contacto desde cookie
  if($('.formContacto').length > 0 || $('#contactar-vendedor-formulario').length > 0) {
    var contacto_nombre = '';
    var contacto_telefono = '';
    var contacto_mail = '';
    if (Cookies.get('clviContact')) {
      $unstrung = $.parseJSON(Cookies.get('clviContact'));
      for (i = 0; i < $unstrung.length; i++) {
        if($unstrung[i][0] == 'contactar_vendedor_nombre' && $unstrung[i][1] != '')
          contacto_nombre = $unstrung[i][1];
        if($unstrung[i][0] == 'contactar_vendedor_telefono' && $unstrung[i][1] != '')
          contacto_telefono = $unstrung[i][1];
        if($unstrung[i][0] == 'contactar_vendedor_mail' && $unstrung[i][1] != '')
          contacto_mail = $unstrung[i][1];
      }
    }
    if($('[name=contactar_vendedor_nombre]').val() == '' && contacto_nombre != '')
      $('[name=contactar_vendedor_nombre]').val(contacto_nombre);
    if($('[name=contactar_vendedor_telefono]').val() == '' && contacto_telefono != '')
      $('[name=contactar_vendedor_telefono]').val(contacto_telefono);
    if($('[name=contactar_vendedor_mail]').val() == '' && contacto_mail != '')
      $('[name=contactar_vendedor_mail]').val(contacto_mail);
  }
  
  if($('#matricula-profesional').length > 0) {
    $('#subrubros-servicios').change(function() {
      if($(this.options[this.selectedIndex]).closest('optgroup').prop('label') == 'Oficios y Profesiones') {
        $('#matricula-profesional').show();
      } else {
        $('#matricula-profesional').hide();
      }
    });
    if($('#subrubros-servicios :selected').parent().attr('label') == 'Oficios y Profesiones') {
      $('#matricula-profesional').show();
    }
  }
  
  if($('.precio-uva').length > 0) {
    ficha_mostrar_precio_uva();
  }
  if($('.search-precio-uva').length > 0) {
    $('.search-precio-uva').each(function(index) {
      if($(this).attr('data-precio') != '' && $(this).attr('data-precio') != 'consultar') {
        var id_aviso = $(this).attr('data-aviso');
        var moneda = 1;
        if($(this).attr('data-moneda') == 'U$S' || $(this).attr('data-moneda') == 2)
          moneda = 2;
        $.get( "/administrar/obtener_precio_uva/" + $(this).attr('data-precio') + "/" + moneda, function( data ) {
          if(data != '' && data != '0,00')
            $('.search-precio-uva.' + id_aviso).html(data + ' UVAS');
          else
            $('.search-precio-uva.' + id_aviso).html('');        
        });
      }
    });
  }
  
  // Actualizar campo teléfono de contacto
  if($("#tel_vendedor_1").length > 0) {
    $("#tel_vendedor_1").blur(function() {
      unificar_telefonos_contacto();
    });
    $("#tel_vendedor_2").blur(function() {
      unificar_telefonos_contacto();
    });
    $("#tel_vendedor_3").blur(function() {
      unificar_telefonos_contacto();
    });
  }
  
});

var usuario = null; // Si esta logueado, habra datos. 
var datos_cuenta_verificado = false;
var favoritos = '';
var logged_in = 0;
/*
 * Debido a que la mayoria de las paginas estan en cache, debemos cargar el
 * menu de administracion por ajax o el boton ingresar si no esta logueado.
 */
function cargar_datos_cuenta(intentos) {
  if(intentos>3) {
    // Si la respuesta es erronea hacemos hasta 4 intentos y cortamos.
    return;
  }
  $.ajax({
    type: "GET",
    url: "/user/datos_cuenta",
    cache: false,
    dataType: 'json',
    success: function(data) {
      if(data.status==1) {
        $('#datos_cuenta').html(data.html);
        usuario = data.usuario;
        datos_cuenta_verificado = true;
        // Ocultamos el captcha para usuarios logueados.
        if(usuario!=null && usuario.uid>0) {
          $('.g-recaptcha').css('display', 'none');
          logged_in = 1;
          if(usuario.favoritos != '') {
            favoritos = usuario.favoritos;
            Cookies.set('clviFavouritesOld', favoritos);
            if(Cookies.get('clviFavourites')) {
              var favoritosOld = $.parseJSON(favoritos);
              var favoritosNew = $.parseJSON(Cookies.get('clviFavourites'));
              for(var i = 0; i < favoritosOld.length; i++){
                var found = $.inArray(favoritosOld[i], favoritosNew);
                if (found >= 0) {
                  //favoritos.splice(found, 1);
                } else {
                  favoritosNew.push(favoritosOld[i]);
                }
              }
              $stringify = JSON.stringify(favoritosNew);
              Cookies.set('clviFavourites', $stringify);
            } else {
              Cookies.set('clviFavourites', favoritos);
            }
          }
        }
        
        //Modal Informativo
        if($("#bkg").length && $("#bkg").css('display') == 'block'){
          $("#bkg").click(function () {    
            if($("#popover.Resultado").length > 0) {
              $(".favourite-top").css("z-index", "0");
            }
          });
          if($("#popover.Resultado").length > 0) {
            $(".favourite-top").css("z-index", "9999");
          }            
        }
        
      } else {
        setTimeout('cargar_datos_cuenta('+(intentos+1)+')', 4000);
      }
    },
    error: function() {
      setTimeout('cargar_datos_cuenta('+(intentos+1)+')', 4000);
    }
  });
}
function cargar_datos_menu(intentos) {
  if(intentos>3) {
    // Si la respuesta es erronea hacemos hasta 4 intentos y cortamos.
    return;
  }
  $.ajax({
    type: "GET",
    url: "/user/datos_menu",
    cache: false,
    dataType: 'json',
    success: function(data) {
      if(data.status==1) {
        $('#datos_menu').html(data.html);
      } else {
        setTimeout('cargar_datos_menu('+(intentos+1)+')', 4000);
      }
    },
    error: function() {
      setTimeout('cargar_datos_menu('+(intentos+1)+')', 4000);
    }
  });
}

/*
 * Verifica que exista un label con el input, textarea o select.
 * Si no existe y el campo no esta vacio entonces lo genera.
 */
function verificar_label() {
  if($(this).attr('name') == 'field_aviso_tel_vendedor_1' || $(this).attr('name') == 'field_aviso_tel_vendedor_2' || $(this).attr('name') == 'field_aviso_tel_vendedor_3')
    return;
  if($(this).val().trim()=='') {
    $(this).parent().children('label').remove();
  } else {
    if ($(this).parent().children('label').length === 0) {
      if($(this).is('input') || $(this).is('textarea')) {
        if($(this).attr('placeholder')!=undefined) {
          $(this).after('<label class="placeholder">'+$(this).attr('placeholder')+'</label>');
        }
      } else if($(this).is('select')) {
        var titulo = $(this).find('option:first').attr('label');
        if(titulo.startsWith('- ')) {
          titulo = titulo.substr(2);
        }
        if(titulo.endsWith(' -')) {
          titulo = titulo.substr(0, titulo.length-2);
        }
        $(this).after('<label class="placeholder">'+titulo+'</label>');
      }
    }
  }
}

/*
 * Contar visita a aviso via ajax
 */
function contador_visitas_ajax(nid) {
  var datos_contacto = '';
  if (Cookies.get('clviContact'))
    datos_contacto = Cookies.get('clviContact');
  $.ajax({
    url: '/node/contador_vista/visita/' + nid,
    method: 'POST',
    data: {contacto: datos_contacto},
    dataType: 'html',
    success: function(data) {
      //$("#contador-visitas").html(data);
    }
  });
}

function revisar_todos_labels(event) {
  $('input, textarea, select').each(function() {
    $(this).blur();
  });
}

function envioBusquedaConcesionarias() {
  document.formBusquedaConcesionaria.submit();
}

function envioBusquedaInmobiliarias() {
  document.formBusquedaInmobiliaria.submit();
}

var validarCaptcha = false;
var CaptchaCallbackFicha = function() {
  grecaptcha.render('RecaptchaField1', {'sitekey' : '6LdSsBEUAAAAAGQocGczp382xUonwLRe0ToX7wlt'});
};
var CaptchaCallbackFichaTienda = function() {
  grecaptcha.render('RecaptchaField1', {'sitekey' : '6LdSsBEUAAAAAGQocGczp382xUonwLRe0ToX7wlt'});
  grecaptcha.render('RecaptchaField2', {'sitekey' : '6LdSsBEUAAAAAGQocGczp382xUonwLRe0ToX7wlt', 'callback' : function(response){ 
    if(response.length != 0) validarCaptcha = true;
  }});
};
var CaptchaCallbackContacto = function() {
  grecaptcha.render('RecaptchaField', {'sitekey' : '6LdSsBEUAAAAAGQocGczp382xUonwLRe0ToX7wlt', 'callback' : function(response){ 
    if(response.length != 0) validarCaptcha = true;
  }});
};

/** 
* Unifica los teléfonos de contactos en un solo registro
* 
*/
function unificar_telefonos_contacto() {
  var tel_unificado = '';
  if($("#tel_vendedor_1").val() != '')
    tel_unificado = $("#tel_vendedor_1").val();
  if($("#tel_vendedor_2").val() != '')
    tel_unificado = tel_unificado + ' / ' + $("#tel_vendedor_2").val();
  if($("#tel_vendedor_3").val() != '')
    tel_unificado = tel_unificado + ' / ' + $("#tel_vendedor_3").val();
  $("#edit-field-aviso-tel-vendedor-value").val(tel_unificado);
}

ajaxEnvioContacto = function () {
  var datos_get = '';
  var buscador = '';
  if($("#edit-contactar-vendedor-buscador").length > 0)
    buscador = $("#edit-contactar-vendedor-buscador").val();
  
  datos_get+='contactar_vendedor_nombre='+encodeURI($("#edit-contactar-vendedor-nombre").val());
  datos_get+='&contactar_vendedor_telefono='+encodeURI($("#edit-contactar-vendedor-telefono").val());
  datos_get+='&contactar_vendedor_mail='+encodeURI($("#edit-contactar-vendedor-mail").val());
  datos_get+='&contactar_vendedor_consulta='+encodeURI($("#edit-contactar-vendedor-consulta").val());
  datos_get+='&contactar_vendedor_aviso='+encodeURI($("#edit-contactar-vendedor-aviso").val());
  datos_get+='&contactar_vendedor_buscador='+buscador;
  
  if($('#contactar-vendedor-formulario .g-recaptcha').length>0) {
    datos_get+='&g-recaptcha-response='+encodeURI($('#g-recaptcha-response').val());
  }
  $("#contactar-vendedor-formulario input").removeClass('error');
  $("#contactar-vendedor-formulario textarea").removeClass('error');
  $("#contactar-vendedor-formulario .form-mensaje").removeClass('error');
  $("#contactar-vendedor-formulario .form-mensaje").html('');
  
  addContactoCookies(datos_get);
  
  $.ajax({
      type: "GET",
      dataType: "json",
      cache: false,
      url: "/contacto/contacto_vendedor?" + datos_get,
      success: function (data) {
        var error = false;
        if(data.status == 'error') {
          $('#contactar-vendedor-formulario .messages').html(data.mensaje);
          error = true;
        } else {
          $('#contactar-vendedor-formulario .messages').html(data.mensaje);
          // Evento contacto ga
          // send_event_ga('contacto', 'resultado', 'mobile');
        }
        $('#contactar-vendedor-formulario .messages').show('');
        $("#edit-contactar-vendedor-submit").attr("value", "Consultar");
        $("#edit-contactar-vendedor-submit").removeAttr('disabled');
        if(error == false)
          setTimeout(modalClose, 3000);
      },
      error: function (e, t, n) {
        if (t === "timeout") {
          $('#contactar-vendedor-formulario .messages').html(data.mensaje);
        } else {
          $('#contactar-vendedor-formulario .messages').html(data.mensaje);
        }
        $('#contactar-vendedor-formulario .messages').show('');
        $("#edit-contactar-vendedor-submit").attr("value", "Consultar");
        $("#edit-contactar-vendedor-submit").removeAttr('disabled');
      }
  })
};
modalClose = function () {
  $('#contactar-vendedor-formulario .messages').hide('');
  $.colorbox.close()
}
telefonoFormValidate = function(phone){
  var valid = false;
  var numbers = phone.match(/\d/g);
  if(!numbers){
    valid = true;
  } else {
    if(numbers.length < 4) {
      //no es valido porque el número es muy corto (no tiene la cantidad necesaria de numeros)
    } else if(phone.match(/^[0-9 \-\(\)\.\+\/\*#]+$/)) {
      if(phone.match(/[\-\(\)\.\+\/\*#]{3,}/)) {
        //no es valido porque tiene más de 3 caracteres especiales juntos.
      } else {
        var max_allowed = numbers - 2;
        valid = true;
      }
    }
  }
  return valid;
};
mailFormValidate = function(mail){
  var valid = false;
  if(mail.match(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/))
    valid = true;
  return valid;
};

function ficha_mostrar_precio_uva() {
  if($('.precio-uva').attr('data-precio') != '' && $('.precio-uva').attr('data-precio') != 'consultar') {
    $.get( "/administrar/obtener_precio_uva/" + $('.precio-uva').attr('data-precio') + "/" + $('.precio-uva').attr('data-moneda'), function( data ) {
      if(data != '' && data != 0)
        $('.precio-uva').html(data + ' UVAS');
      else
        $('.precio-uva').html('');               
    });
  }
}

setInterval(function() { detectFavourites(); }, 15000);
// LIVE: 6LdSsBEUAAAAAGQocGczp382xUonwLRe0ToX7wlt
// LOCAL CHINO: 6LeN3RMUAAAAANhd678e2AGerR9eJr_Gt32jZK-K