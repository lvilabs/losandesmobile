/**
* Buscador Alojamiento js.
*/

var myCombo;
$(document).ready(function(){
  
  $('#edit-buscador-tipo-inmueble').change(function() {
    $('.transacciones').hide('');
    if($(this).val() == '') {
      $('#trans').show('');
    } else {
      var tid = $('option:selected', this).attr('data-tid');
      $('#trans-' + tid).show('');
    }
  });
  
  $('#edit-buscador-tipo-auto').change(function() {
    $('.marca').hide('');
    if($(this).val() == '') {
      $('#marca').show('');
      $('#modelo').show('');
    } else {
      var tid = $('option:selected', this).attr('data-tid');
      $('#marca-' + tid).show('');
      // Si es motos, oculto el select de modelo
      if(tid == 6326)
        $('.modelo').hide('');
    }
  });
  
  $('.marca').change(function() {
    $('.modelo').hide('');
    var tid = $('option:selected', this).attr('data-tid');
    $('#modelo-' + tid).show('');
  });
  
});

function envioFiltro() {
  
  var filtros = ['tid:' + $("input[name='tidPadre']").val()];
  var tid_inmueble = $('#edit-buscador-tipo-inmueble option:selected').attr('data-tid');
  var ctrInmueble = $("#edit-buscador-tipo-inmueble");
  
  var tid_auto = $('#edit-buscador-tipo-auto option:selected').attr('data-tid');
  var ctrAuto = $("#edit-buscador-tipo-auto");
  
  if(tid_inmueble != undefined) {
    var ctrTransaccion = $("#edit-buscador-transaccion-" + tid_inmueble);
  } else {
    var ctrTransaccion = $("#edit-buscador-transaccion");
  }
  
  if(tid_auto != undefined) {
    var ctrMarca = $("#edit-buscador-marca-" + tid_auto);
    var marca_auto = $('#edit-buscador-marca-' + tid_auto + ' option:selected').attr('data-tid');
  } else {
    var ctrMarca = $("#edit-buscador-marca");
  }
  
  if(marca_auto != undefined) {
    var ctrModelo = $("#edit-buscador-modelo-" + marca_auto);
  } else {
    var ctrModelo = $("#edit-buscador-modelo");
  }
  
  var url = window.location.href;
   
  if (ctrInmueble.length > 0 && ctrInmueble.val().length > 0) {
    var filtro_inmueble = ctrInmueble.val();
    var texto_filtros = '';
    if(filtro_inmueble.indexOf(" ") != -1){
      filtro_inmueble = filtro_inmueble.split(' ');
      for(i = 1; i < filtro_inmueble.length; i++) {
        texto_filtros = texto_filtros + ' ' + filtro_inmueble[i];
      }
    }
    filtros.push(texto_filtros);
  }
  
  if (ctrTransaccion.length > 0 && ctrTransaccion.val().length > 0) {
    var filtro_transaccion = ctrTransaccion.val();
    var texto_filtros = '';
    if(filtro_transaccion.indexOf(" ") != -1){
      filtro_transaccion = filtro_transaccion.split(' ');
      for(i = filtro_transaccion.length - 1; i < filtro_transaccion.length; i++) {
        texto_filtros = texto_filtros + ' ' + filtro_transaccion[i];
      }
    }
    filtros.push(texto_filtros);
  }
  
  if (ctrAuto.length > 0 && ctrAuto.val().length > 0) {
    var filtro_auto = ctrAuto.val();
    var texto_filtros = '';
    if(filtro_auto.indexOf(" ") != -1){
      filtro_auto = filtro_auto.split(' ');
      for(i = filtro_auto.length - 1; i < filtro_auto.length; i++) {
        texto_filtros = texto_filtros + ' ' + filtro_auto[i];
      }
    }
    filtros.push(texto_filtros);
  }
  
  if (ctrMarca.length > 0 && ctrMarca.val().length > 0) {
    var filtro_marca = ctrMarca.val();
    var texto_filtros = '';
    if(filtro_marca.indexOf(" ") != -1){
      filtro_marca = filtro_marca.split(' ');
      for(i = filtro_marca.length - 1; i < filtro_marca.length; i++) {
        texto_filtros = texto_filtros + ' ' + filtro_marca[i];
      }
    }
    filtros.push(texto_filtros);
  }
  
  if (ctrModelo.length > 0 && ctrModelo.val().length > 0) {
    var filtro_modelo = ctrModelo.val();
    var texto_filtros = '';
    if(filtro_modelo.indexOf(" ") != -1){
      filtro_modelo = filtro_modelo.split(' ');
      for(i = filtro_modelo.length - 1; i < filtro_modelo.length; i++) {
        texto_filtros = texto_filtros + ' ' + filtro_modelo[i];
      }
    }
    filtros.push(texto_filtros);
  }
  
  var text_filtros = '';
  if(filtros.length == 1) {
    text_filtros = filtros[0] + '%20';
  } else {
    for(i = 0; i < filtros.length; i++){
      text_filtros = text_filtros + ' ' + filtros[i];
    }
  }
  
  //console.log(text_filtros);
  
  location.href = '/search/result/filters=' + text_filtros;
  
}