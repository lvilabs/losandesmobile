/**
 * Modified Star Rating - jQuery plugin
 *
 */
jQuery(document).ready(function () {
  
  var stars = $('.fivestar-widget .star');
  var select = $(".form-select-fivestar");
  var currentValue = $(".form-select-fivestar").val();
  
  stars.mouseover(function(){
    drain();
    fill(this);
  }).mouseout(function(){
    drain();
    reset();
  });
  
  stars.click(function(){
    currentValue = $(this).children('a').attr('href');
    currentValue = currentValue.split('#');
    currentValue = currentValue[1];
    // Save the currentValue to the hidden select field.
    select.val(currentValue);
    return false;
  });
  
  function drain() {
    stars
      .filter('.on').removeClass('on').end()
      .filter('.hover').removeClass('hover').end();
  }
  
  function fill(el){
    var index = stars.index(el) + 1;
    stars
      .children('a').css('width', '100%').end()
      .filter(':lt(' + index + ')').addClass('hover').end();
  }
  
  function reset(){
    var starValue = currentValue/100 * stars.size();
    var percent = (starValue - Math.floor(starValue)) * 100;
    stars.filter(':lt(' + Math.floor(starValue) + ')').addClass('on').end();
    if (percent > 0) {
      stars.eq(Math.floor(starValue)).addClass('on').children('a').css('width', percent + "%").end().end();
    }
  }
  
});
