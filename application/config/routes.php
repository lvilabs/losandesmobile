<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
//print $_SERVER['REQUEST_URI']; die();

$route['default_controller'] = "welcome";
$route['movil'] = "welcome";
//Nodos
$route['node/(:num)'] = "node/id/$1";
$route['node/(:num)/edit'] = "node_add/edit/$1";
$route['avisos/(:any)/(:num)/(:any)'] = "node/id/$2";
$route['aviso/(:any)'] = "node/alias/$1"; // Avisos antiguos buscaguia. Ej: aviso/gastronomía-y-delivery/fellini-resto-bar
$route['propiedad/(:num)/(:any)'] = "argenprop/id/$1";
$route['nota/(:any)'] = "nota/id/$1";

// Paginas
$route['ofertas'] = "page/ofertas";
$route['productos'] = "page/productos";
$route['tiendas_lavoz'] = "page/tiendas_lavoz";
$route['concesionarias'] = "page/concesionarias";
$route['inmobiliarias'] = "page/inmobiliarias";
$route['autos'] = "page/autos";
$route['inmuebles'] = "page/inmuebles";
$route['faq'] = "page/ayuda";
$route['contact'] = "page/contact";
$route['noticias'] = "nota/listado";

//Buscador, prehomes y taxonomias
$route['404_override'] = 'redireccion/check';
//Administrador
$route['contactos_recibidos'] = "administrar/contactos_recibidos";
$route['estado_cuenta'] = "administrar/estado_cuenta";
$route['user/(:num)/edit'] = "user/edit/$1";
$route['registro-particular'] = "user/register/particular";
$route['registro-comercio'] = "user/register/comercio";

$route['node/(:num)/publicar'] = "administrar/publicar/$1";
$route['node/(:num)/despublicar'] = "administrar/despublicar/$1";
$route['estado_cuenta'] = "administrar/estado_cuenta";
$route['contactos_recibidos'] = "administrar/contactos_recibidos";
$route['compra/(:num)/completar-datos'] = "administrar/completar_datos/$1";
$route['compra/(:num)/confirmar'] = "compra/confirmar/$1";
$route['node/(:num)/compra_(:any)'] = "compra/redirigir/$1/$2";
$route['compra/(:num)/estado/(:any)/(:any)'] = "compra/estado/$1/$2/$3";
$route['sitio/(:any)'] = "search/check_url_sitio/$1";//Alojamientos
$route['node/add/aviso-alquiler-temporario'] = "node_add/alojamiento";
$route['sitemap.xml'] = "sitemap";

/* End of file routes.php */
/* Location: ./application/config/routes.php */