<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['adslot_home'] = array(
    array(
        'adunit'  => 'dfp-anuncio_previo',
        'adblock' => '/6955764/clasificados/home/anuncio_previo',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit'  => 'dfp-rectangle_1',
        'adblock' => '/6955764/clasificados/home/rectangle_1',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit'  => 'dfp-zocalo',
        'adblock' => '/6955764/clasificados/home/zocalo',
        'adsizes' => '[300, 100]'
    ),
    array(
        'adunit'  => 'dfp-topsite',
        'adblock' => '/6955764/clasificados/home/topsite_expandible',
        'adsizes' => '[[300, 100], [300, 50]]'
    ),
    array(
        'adunit'  => 'dfp-oportunidad1',
        'adblock' => '/6955764/clasificados/home/oportunidad1',
        'adsizes' => '[100, 100]'
    ),
    array(
        'adunit'  => 'dfp-oportunidad2',
        'adblock' => '/6955764/clasificados/home/oportunidad2',
        'adsizes' => '[100, 100]'
    ),
    array(
        'adunit'  => 'dfp-oportunidad3',
        'adblock' => '/6955764/clasificados/home/oportunidad3',
        'adsizes' => '[100, 100]'
    ),
    /*array(
        'adunit'  => 'dfp-oportunidad4',
        'adblock' => '/6955764/clasificados/home/oportunidad4',
        'adsizes' => '[100, 100]'
    ),
    array(
        'adunit'  => 'dfp-oportunidad5',
        'adblock' => '/6955764/clasificados/home/oportunidad5',
        'adsizes' => '[100, 100]'
    ),
    array(
        'adunit'  => 'dfp-oportunidad6',
        'adblock' => '/6955764/clasificados/home/oportunidad6',
        'adsizes' => '[100, 100]'
    )*/
);

$config['adslot_prehome'] = array(
    array(
        'adunit'  => 'dfp-anuncio_previo',
        'adblock' => '/6955764/clasificados/portadas/anuncio_previo',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit'  => 'dfp-rectangle_1',
        'adblock' => '/6955764/clasificados/portadas/rectangle_1',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit'  => 'dfp-topsite',
        'adblock' => '/6955764/clasificados/portadas/topsite_expandible',
        'adsizes' => '[[300, 50], [300, 100]]'
    ),
    array(
        'adunit'  => 'dfp-zocalo',
        'adblock' => '/6955764/clasificados/portadas/zocalo',
        'adsizes' => '[300, 100]'
    )
);

$config['adslot_resultado'] = array(
    array(
        'adunit' => 'dfp-anuncio_previo',
        'adblock' => '/6955764/clasificados/resultados/anuncio_previo',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit' => 'dfp-rectangle_1',
        'adblock' => '/6955764/clasificados/resultados/rectangle_1',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit' => 'dfp-rectangle_2',
        'adblock' => '/6955764/clasificados/resultados/rectangle_2',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit' => 'dfp-topsite',
        'adblock' => '/6955764/clasificados/resultados/topsite_expandible',
        'adsizes' => '[[300, 100], [300, 50]]'
    ),
    array(
        'adunit'  => 'dfp-zocalo',
        'adblock' => '/6955764/clasificados/resultados/zocalo',
        'adsizes' => '[300, 100]'
    )
);

$config['adslot_ficha'] = array(
    array(
        'adunit' => 'dfp-anuncio_previo',
        'adblock' => '/6955764/clasificados/ficha/anuncio_previo',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit' => 'dfp-rectangle',
        'adblock' => '/6955764/clasificados/ficha/rectangle',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit' => 'dfp-topsite',
        'adblock' => '/6955764/clasificados/ficha/topsite_expandible',
        'adsizes' => '[[300, 50], [300, 100]]'
    ),
    array(
        'adunit'  => 'dfp-zocalo',
        'adblock' => '/6955764/clasificados/ficha/zocalo',
        'adsizes' => '[300, 100]'
    )
);

$config['adslot_nota'] = array(
    array(
        'adunit'  => 'dfp-rectangle_1',
        'adblock' => '/6955764/clasificados/portadas/rectangle_1',
        'adsizes' => '[300, 250]'
    ),
    array(
        'adunit'  => 'dfp-topsite',
        'adblock' => '/6955764/clasificados/portadas/topsite_expandible',
        'adsizes' => '[[300, 50], [300, 100]]'
    ),
    array(
        'adunit'  => 'dfp-zocalo',
        'adblock' => '/6955764/clasificados/portadas/zocalo',
        'adsizes' => '[300, 100]'
    )
);