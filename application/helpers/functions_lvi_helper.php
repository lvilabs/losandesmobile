<?php 

    function obtener_tids_url($filters)
    {
        $array_tids = array();
        $filters = str_replace('%22', '"', $filters);
        $filters = str_replace('%20', ' ', $filters);
        $filters = str_replace('%3A', ':', $filters);
        $filters = str_replace('%5b', '', $filters);
        $filters = str_replace('%5d', '', $filters);
        $filters = str_replace('%5B', '', $filters);
        $filters = str_replace('%5D', '', $filters);
        $filters = str_replace(' TO ', '-TO-', $filters);
        $filtros_comillas = explode('"', $filters);
        foreach($filtros_comillas as $key => $filtro_comilla){
          if($key%2!=0)
            $filtros_comillas[$key] = str_replace(' ', '_', $filtro_comilla);
        }
        $filters = implode('', $filtros_comillas);
        $sin_filters = explode('=', $filters);
        if(isset($sin_filters[1])){
          $tids = explode(' ', $sin_filters[1]);
          foreach($tids as $tid){
            if($tid != ''){
              if(strpos($tid,'tid:')!==false){
                $tid_int = explode(':', $tid);
                $array_tids[] = $tid_int[1];
              } elseif(strpos($tid,'fs_precio:')!==false) {
                $tid_int = explode(':', $tid);
                $array_tids[] = str_replace('-TO-', ' a ', 'Precio '.$tid_int[1]);
              } elseif(strpos($tid,'fs_kilometros:')!==false) {
                $tid_int = explode(':', $tid);
                $array_tids[] = str_replace('-TO-', ' a ', 'Km '.$tid_int[1]);
              } elseif(strpos($tid,'iss_anio:')!==false) {
                $tid_int = explode(':', $tid);
                $array_tids[] = str_replace('-TO-', ' a ', 'Año '.$tid_int[1]);
              } elseif(strpos($tid,'is_moneda_k:')!==false) {
                $tid_int = explode(':', $tid);
                if($tid_int[1] == 1){
                  $array_tids[] = '$';
                } elseif($tid_int[1] == 2) {
                  $array_tids[] = 'U$S';
                } elseif($tid_int[1] == 3) {
                  $array_tids[] = 'UVA';
                }
              } elseif(strpos($tid,'is_uid:')!==false) {                $array_tids[] = 'Vendedor';
                $partes = explode(':', $tid);
              } elseif(strpos($tid,'im_reserved:')!==false) {                $tid_int = explode(':', $tid);
                if(strpos($tid_int[1], '-TO-') !== FALSE) {
                  $fechas = explode('-TO-', $tid_int[1]);
                  $fecha1 = convertir_fecha_solr($fechas[0]);
                  $fecha2 = convertir_fecha_solr($fechas[1]);
                  $array_tids[] = 'Fecha '.$fecha1.' a '.$fecha2;
                } else {
                  $fecha = convertir_fecha_solr($tid_int[1]);
                  $array_tids[] = 'Fecha '.$fecha;
                }
              } elseif(strpos($tid,'is_capacidad:')!==false) {
                $tid_int = explode(':', $tid);
                $array_tids[] = 'Huéspedes '.$tid_int[1];              } else {
                $tid_int = explode(':', $tid);
                $array_tids[] = str_replace('_', ' ', $tid_int[1]);
              }
            }
          }
        }
        return $array_tids;
    }
    
    function obtener_subrubros_listado_tid($tids, $listado_rubros)
    {
        $listado_subrubro = array();
        $cant_tids = count($tids);
        switch($cant_tids){
          case 1:
            $listado_subrubro = $listado_rubros->$tids[0];
            break;
          case 2:
            $listado_subrubro = $listado_rubros->$tids[0];
            $listado_subrubro = $listado_subrubro->$tids[1];
            break;
          case 3:
            $listado_subrubro = $listado_rubros->$tids[0];
            $listado_subrubro = $listado_subrubro->$tids[1];
            if(is_numeric($tids[2])){
              $listado_subrubro = $listado_subrubro->$tids[2];
            } else {
              foreach($listado_subrubro as $ksub => $vsub){
                if(is_numeric($ksub)){
                  if($vsub->name == $tids[2]){
                    $listado_subrubro = $listado_subrubro->$ksub;  
                    break;
                  }
                }
              }
            }
            break;
        }
        $sin_tid = true;
        if(is_object($listado_subrubro)){
          foreach($listado_subrubro as $ksub => $vsub){
            if(is_numeric($ksub))
              $sin_tid = false;
          }
        }
        if($sin_tid) $listado_subrubro = array();
        return $listado_subrubro;
    }
    
    function convertir_fecha($fecha)
    {
        $fecha_unix= $fecha;
        $ahora=time();
        $segundos=$ahora-$fecha_unix;
        $dias=floor($segundos/86400);
        $mod_hora=$segundos%86400;
        $horas=floor($mod_hora/3600);
        $mod_minuto=$mod_hora%3600;
        $minutos=floor($mod_minuto/60);
        if($horas<=0){
            if($minutos==1){
                $tiempo=$minutos." minuto";    
            } elseif ($minutos<=0) {
                $tiempo=" unos segundos";    
            } else {
                $tiempo=$minutos." minutos";    
            }                        
        }elseif($dias<=0){
            if($horas==1){
                $tiempo=$horas." hora";    
            } else {
                $tiempo=$horas." horas ";
            }
        }else{
            if($dias==1){
                $tiempo=$dias." d&iacute;a";    
            } else {
                $tiempo=$dias." d&iacute;as ";
            }
        }
        return $tiempo;
        
    }
    
    function nombre_mes($nro) {
            switch($nro) {
                    case "01":
                            $mes = "Enero";
                            break;
                    case "02":
                            $mes = "Febrero";
                            break;
                    case "03":
                            $mes = "Marzo";
                            break;
                    case "04":
                            $mes = "Abril";
                            break;
                    case "05":
                            $mes = "Mayo";
                            break;
                    case "06":
                            $mes = "Junio";
                            break;
                    case "07":
                            $mes = "Julio";
                            break;
                    case "08":
                            $mes = "Agosto";
                            break;
                    case "09":
                            $mes = "Septiembre";
                            break;
                    case "10":
                            $mes = "Octubre";
                            break;
                    case "11":
                            $mes = "Noviembre";
                            break;
                    case "12":
                            $mes = "Diciembre";
                            break;
            
            }
            return $mes;
    }
    
    
    function comscore($path, $taxonomia = null) {
        if($path == '') {
          $partes[0] = '';
        } else {
          $path= str_replace("/index.php","", $path);
          $path= strtolower($path);
          if (substr($path, 0, 1)=="/") $path= substr($path,1);
          $partes= explode("/",$path);
        }
        switch($partes[0]) {
            case "":
                    $comscore_path = "home.index";
                    break;
            case "node":
            case "avisos":
                    (isset($taxonomia['parametro1']))?$parametro1 = $taxonomia['parametro1']:$parametro1 = '';
                    (isset($taxonomia['parametro2']))?$parametro2 = $taxonomia['parametro2']:$parametro2 = '';
                    $rubro = $taxonomia['rubro_raiz'];
                    ($parametro2 != '') ? $comscore_path= $rubro.".ficha." . $parametro1 . "." . $parametro2 : $comscore_path= $rubro.".ficha." . $parametro1;
                    $comscore_path = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($comscore_path, ENT_QUOTES, 'UTF-8'));
                    $comscore_path = preg_replace('@[^a-z0-9_./&=]+@', '', $comscore_path);
                    if(!empty($partes[2]) && ($partes[2]=='publicar' || $partes[2]=='edit')) {
                      $comscore_path= "home.micuenta";
                    }
                    break;        
            case "search":
                    (isset($taxonomia['parametro1']))?$parametro1 = $taxonomia['parametro1']:$parametro1 = '';
                    $rubro = 'home';
                    if(isset($taxonomia['rubro_raiz']))
                      $rubro = $taxonomia['rubro_raiz'];
                    $comscore_path= $rubro.".resultados&ns_search_term=" . $parametro1 . "&ns_search_result=" . $taxonomia['total_result'];
                    break;
            case "node_add":
                    $comscore_path= "home.crearnuevoanuncio";
                    break;
            case 'productos':
                    $comscore_path= "productos.prehome";
                    break;
            case 'ofertas':
                    $comscore_path= "ventaonline.ofertas";
                    break;
            case 'tiendas_lavoz':
                    $comscore_path= "ventaonline.tiendas";
                    break;
            case 'administrar':
                    $comscore_path= "home.micuenta.misavisos";
                    break;
            case 'administrar':
                    $comscore_path= "home.micuenta.misavisos";
                    break;
            default:
                    $comscore_path = "no_encontrado.".str_replace("/",".", $path);
                    break;
        }
        $comscore_path = strtolower($comscore_path);
        $comscore_site_name= "";
        ?>
            <!-- Begin comScore Inline Tag 1.1111.15 -->
            <img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6906399&amp;ns_site=<?php echo $comscore_site_name; ?>&amp;name=<?php echo $comscore_path; ?>" height="1" width="1" alt="*">
            <!-- End comScore Inline Tag -->
            <script>
              var _comscore = _comscore || [];
              _comscore.push({ c1: "2", c2: "6906409" });
              (function() {
                var s = document.createElement("script"), el = document.getElementsByTagName("script")[0];
                s.async = true;
                s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
                el.parentNode.insertBefore(s, el);
              })();
            </script>
            <noscript>
              <img src="http://b.scorecardresearch.com/p?c1=2&c2=6906409&cv=2.0&cj=1" />
            </noscript>
        <?php
        //echo "Nombre Sitio: " . $comscore_site_name ."<br>Path: " . $comscore_path."<br>Path real: " . $partes[0]."<br>";
            
    }

    function analytics() {
    ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2721986-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
    
            gtag('config', 'UA-2721986-2');
        </script>
        <?php
    }
    
    function meta_og($node, $head_title, $head_description) {
      $config=& get_instance();
    ?>
      <?php if(!empty($node)) { 
        $precio = 0;
        $moneda = 'ARS';
        if($node['field_aviso_precio'] != 'consultar' && $node['field_aviso_precio'] != 1) $precio = $node['field_aviso_precio'];
        if($node['field_aviso_moneda'] == 2) $moneda = 'USD';
      ?>
      <meta property="og:title" content="<?php echo $head_title; ?>" />
      <meta property="og:description" content="<?php echo $head_description; ?>" />
      <meta property="og:url" content="<?php echo $config->config->item('sitio_host').$_SERVER['REQUEST_URI']; ?>" />
      <meta property="og:image" content="<?php echo $node['field_aviso_fotos'][0] ?>" />
      <meta property="og:price:amount" content="<?php echo $precio; ?>" />
      <meta property="og:price:currency" content="<?php echo $moneda; ?>" />
      <meta property="og:type" content="article" />
      <?php } else { 
        switch($config->uri->segments[1]) {
          case 'autos':
            $title = 'Autos, compra y venta de vehículos - Clasificados La Voz';
            $description = 'Encontrar el auto nuevo o usado que estás buscando. Comprar y vender en Córdoba con Clasificados La Voz.';
            $url = $config->config->item('sitio_host').$_SERVER['REQUEST_URI'];
            break;
          case 'inmuebles':
            $title = 'Inmuebles, compra, venta y alquileres - Clasificados La Voz';
            $description = 'Encontrar la casa, departamento o lote que buscas para comprar o alquilar. Conseguir la mayor oferta inmobiliaria de Córdoba con Clasificados La Voz.';
            $url = $config->config->item('sitio_host').$_SERVER['REQUEST_URI'];
            break;
          case 'productos':
            $title = 'Productos y Ofertas Online - Clasificados La Voz';
            $description = 'Productos online en Córdoba, aprovechar las promociones y descuentos en Clasificados La Voz. Publicar aviso gratis y vender de todo.';
            $url = $config->config->item('sitio_host').$_SERVER['REQUEST_URI'];
            break;
          case 'alojamientos':
            $title = 'Alojamientos en Córdoba - Casas, cabañas y departamentos.';
            $description = 'Reservá online tu alojamiento en Córdoba: casas, cabañas en las sierras y departamentos temporarios todo el año.';
            $url = $config->config->item('sitio_host').$_SERVER['REQUEST_URI'];
            break;
          case 'ofertas':
            $title = 'Ofertas de productos Clasificados La Voz.';
            $description = 'Oferta de productos online en Córdoba con descuentos de Clasificados La Voz. Publicar aviso gratis y vender de todo.';
            $url = $config->config->item('sitio_host').$_SERVER['REQUEST_URI'];
            break;
          case 'search':
            $title = $head_title;
            $description = $head_description;
            $url = $config->config->item('sitio_host').'/'.$config->uri->uri_string;
            break;
          default:
            $title = $config->config->item('header_title');
            $description = $config->config->item('header_description');
            $url = $config->config->item('sitio_host');
            break;
        }
      ?>
      <meta property="og:title" content="<?php echo $title; ?>" />
      <meta property="og:description" content="<?php echo $description; ?>" />
      <meta property="og:url" content="<?php echo $url; ?>" />
      <meta property="og:image" content="<?php echo $config->config->item('sitio_origen'); ?>/sites/clasificadoslavoz.com.ar/themes/principal/img/mobile-clasificadoslavoz-icon.png" />
      <meta property="og:type" content="website" />
      <?php } ?>
    <?php
    }
    
    function quitar_filtro_url($filtro, $parametros, $url_base) {        
      $url = $url_base;
      if(strpos($filtro,'Precio')!==false) $filtro = 'fs_precio';
      if(strpos($filtro,'$')!==false) $filtro = 'is_moneda_k';
      if(strpos($filtro,'U$S')!==false) $filtro = 'is_moneda_k';
      if(strpos($filtro,'UVA')!==false) $filtro = 'is_moneda_k';
      if(strpos($filtro,'Km')!==false) $filtro = 'fs_kilometros';
      if(strpos($filtro,'Año')!==false) $filtro = 'iss_anio';
      if(strpos($filtro,'Vendedor')!==false) $filtro = 'is_uid';      
      if(strpos($filtro,'Fecha')!==false) $filtro = 'im_reserved';      
      if(isset($parametros)) {
        foreach($parametros as $parametro){
          if(!empty($parametro)){
            if(strpos($parametro,'filters=')===false){
              if(strpos($parametro,'page=')===false)
                $url .= $parametro.'/';
            } else {
              $filters = str_replace('%22', '"', $parametro);
              $filters = str_replace('%20', ' ', $filters);
              $filters = str_replace(' TO ', '-TO-', $filters);
              $filtros_comillas = explode('"', $filters);
              foreach($filtros_comillas as $key => $filtro_comilla){
                if($key%2!=0)
                  $filtros_comillas[$key] = str_replace(' ', '-', $filtro_comilla);
              }
              $filters = implode('"', $filtros_comillas);
              $sin_filters = explode('=', $filters);      
              if(isset($sin_filters[1])){
                $filtros = explode(' ', $sin_filters[1]);
                $url_filtros = '';
                foreach($filtros as $dfiltro){
                  $dfiltro = str_replace('-', ' ', $dfiltro);
                  if($filtro == 'fs_precio' && strpos($dfiltro,'is_moneda_k')!==false){
                    break;
                  }
                  if(!empty($dfiltro) && strpos($dfiltro,$filtro)===false){
                    $dfiltro = str_replace('"', '%22', $dfiltro); 
                    if($url_filtros == '')
                      $url_filtros .= 'filters='.$dfiltro;
                    else
                      $url_filtros .= ' '.$dfiltro;
                  }
                }
              }
              $url .= $url_filtros;
              if(!empty($url_filtros)) {
                if(strpos($url, '%20')===FALSE) {
                  $url .= '%20/';
                } else {
                  $url .= '/';
                }
              }
            }
          }
        }
      }
      return $url; 
    }
    
    /*
    * Redireccinar UR web de taxonomias a su correspondiente en mobile
    *
    */
    function redireccion_taxonomy($url){
      $url_param = explode('/', $url);
      foreach($url_param as $param){
        if(is_numeric($param))
          $tid = $param;
      }
      $url_final = 'search/result/filters=tid%3A'.$tid;
      return $url_final;
    }
    
    /*
    * Redireccinar UR web de prehomes a su correspondiente en mobile
    *
    */
    function redireccion_prehomes($url){
      if($url == 'autos'){
        $tid = 6323;
      } elseif($url == 'inmuebles') {
        $tid = 6330; 
      }
      $url_final = 'search/result/filters=tid%3A'.$tid;
      return $url_final;
    }
    
    /*
    * Obtener la URL web desde la URL mobile
    *
    */
    function obtener_url_web($url){
      $url_final = '';
      
      //URL buscador
      if(strpos($url, 'search/result') !== false){
        $url_sections = explode('/', $url);
                
        $key = '';
        $filters = '';
        $page = '';
        if(isset($url_sections[2]) && strpos($url_sections[2],'filters=')===false){
          $key = $url_sections[2];
          if(isset($url_sections[3]) && $url_sections[3] != null){
            if(strpos($url_sections[3],'filters=')===false)
              $page = $url_sections[3];
            else
              $filters = $url_sections[3];
          }
          if(isset($url_sections[4]) && $url_sections[4] != null){
            if(strpos($url_sections[4],'filters=')===false)
              $page = $url_sections[4];
            else
              $filters = $url_sections[4];
          }
        } else {
          $filters = $url_sections[2];
          if(isset($url_sections[3]) && $url_sections[3] != null){
            if(strpos($url_sections[3],'page=')===false)
              $key = $url_sections[3];
            else
              $page = $url_sections[3];
          }
          if(isset($url_sections[4]) && $url_sections[4] != null){
            if(strpos($url_sections[4],'page=')===false)
              $key = $url_sections[4];
            else
              $page = $url_sections[4];
          }
        }      
                
        $search_final = '';
        if($key != '') $search_final .= $key;
        if($filters != ''){ if($search_final == '') $search_final .= '?'.$filters; else $search_final .= '/?'.$filters; }
        if($page != '') $search_final .= '&'.$page;
        $url_final = 'search/apachesolr_search/'.$search_final;
      } elseif(strpos($url, 'filtros/avanzados') !== false) {
        $url_final = 'search/apachesolr_search';
      } else {
        $url_final = $url;
      } 
      return $url_final;
    }
    
    /*
    * Obtener nombre de aviso por rubro
    *
    */
    function obtener_aviso_rubro($rubro){
      switch($rubro){
        case 'aviso_campo':
          $aviso = 'Campo';
          break;
        case 'aviso_cochera':
          $aviso = 'Cochera';
          break;
        case 'aviso_casa':
          $aviso = 'Casa';
          break;
        case 'aviso_departamento':
          $aviso = 'Departamento';
          break;
        case 'aviso_galpon_deposito':
          $aviso = 'Galpó o Depósito';
          break;
        case 'aviso_local':
          $aviso = 'Local';
          break;
        case 'aviso_oficina':
          $aviso = 'Oficina';
          break;
        case 'aviso_terreno_lote':
          $aviso = 'Terreno o Lote';
          break;
        case 'aviso_4x4':
          $aviso = '4x4';
          break;
        case 'aviso_auto':
          $aviso = 'Auto';
          break;
        case 'aviso_plan_ahorro':
          $aviso = 'Plan de Ahorro';
          break;
        case 'aviso_utilitario':
          $aviso = 'Utilitario';
          break;
        case 'aviso_moto':
          $aviso = 'Moto';
          break;
        case 'aviso_repuesto':
          $aviso = 'Repuesto';
          break;
        case 'aviso_producto':
          $aviso = 'Producto';
          break;
        case 'aviso_servicio':
          $aviso = 'Servicio';
          break;
        case 'aviso_rural':
          $aviso = 'Rural';
          break;
      }
      return $aviso;
    }
    
    /*
    * Validacion formulario de aviso
    *
    */
    function validar_form_aviso($data, $files){
      $status = array();
      $validado = 1;
      $mensaje = '';
      $campos = array();
      //Tipo de unidad
      if(isset($data['field_aviso_tipo_unidad']) && empty($data['field_aviso_tipo_unidad'])){
        $validado = 0;
        $mensaje .= '<li>El tipo de unidad es requerido.</li>';
        $campos[] = 'field_aviso_tipo_unidad'; 
      }
      //Tipo de campo
      if(isset($data['field_aviso_tipo_campo']) && empty($data['field_aviso_tipo_campo'])){
        $validado = 0;
        $mensaje .= '<li>El tipo de campo es requerido.</li>';
        $campos[] = 'field_aviso_tipo_campo'; 
      }
      //Tipo de dpto
      if(isset($data['field_aviso_tipo_unidad_dpto']) && empty($data['field_aviso_tipo_unidad_dpto'])){
        $validado = 0;
        $mensaje .= '<li>El tipo de unidad es requerido.</li>';
        $campos[] = 'field_aviso_tipo_unidad_dpto'; 
      }
      //Tipo de terreno
      if(isset($data['field_aviso_tipo_terreno']) && empty($data['field_aviso_tipo_terreno'])){
        $validado = 0;
        $mensaje .= '<li>El tipo de terreno es requerido.</li>';
        $campos[] = 'field_aviso_tipo_terreno'; 
      }
      //Tipo de alojamiento
      if(isset($data['field_aviso_tipo_unidad_tmp']) && empty($data['field_aviso_tipo_unidad_tmp'])){
        $validado = 0;
        $mensaje .= '<li>El tipo de alojamiento es requerido.</li>';
        $campos[] = 'field_aviso_tipo_unidad_tmp'; 
      }
      //Cantidad de personas
      if(isset($data['field_aviso_capacidad']) && empty($data['field_aviso_capacidad'])){
        $validado = 0;
        $mensaje .= '<li>La cantidad de personas es requerida.</li>';
        $campos[] = 'field_aviso_capacidad'; 
      }
      //Marca Modelo 4x4
      if(isset($data['marca_modelo_4x4']) && empty($data['marca_modelo_4x4'])){
        $validado = 0;
        $mensaje .= '<li>La marca y modelo son requeridas.</li>';
        $campos[] = 'marca_modelo_4x4'; 
      }
      //Marca Modelo Auto
      if(isset($data['marca_modelo_autos']) && empty($data['marca_modelo_autos'])){
        $validado = 0;
        $mensaje .= '<li>La marca y modelo son requeridas.</li>';
        $campos[] = 'marca_modelo_autos'; 
      }
      //Marca Modelo Planes Ahorro
      if(isset($data['marca_modelo_planes_de_ahorro']) && empty($data['marca_modelo_planes_de_ahorro'])){
        $validado = 0;
        $mensaje .= '<li>La marca y modelo son requeridas.</li>';
        $campos[] = 'marca_modelo_planes_de_ahorro'; 
      }
      //Marca Modelo Utilitarios
      if(isset($data['marca_modelo_utilitarios']) && empty($data['marca_modelo_utilitarios'])){
        $validado = 0;
        $mensaje .= '<li>La marca y modelo son requeridas.</li>';
        $campos[] = 'marca_modelo_utilitarios'; 
      }
      //Marca Moto
      if(isset($data['marca_motos']) && empty($data['marca_motos'])){
        $validado = 0;
        $mensaje .= '<li>La marca es requerida.</li>';
        $campos[] = 'marca_motos'; 
      }
      //Subrubro repuesto
      if(isset($data['subrubro_repuestos']) && empty($data['subrubro_repuestos'])){
        $validado = 0;
        $mensaje .= '<li>El subrubro es requerido.</li>';
        $campos[] = 'subrubro_repuestos'; 
      }
      //Rubro Producto
      if(isset($data['rubros_producto']) && empty($data['rubros_producto'])){
        $validado = 0;
        $mensaje .= '<li>El rubro es requerido.</li>';
        $campos[] = 'rubros_producto'; 
      }
      //Rubro Servicio
      if(isset($data['rubros_servicio']) && empty($data['rubros_servicio'])){
        $validado = 0;
        $mensaje .= '<li>El rubro es requerido.</li>';
        $campos[] = 'rubros_servicio'; 
      }
      //Rubro Rural
      if(isset($data['rubros_rural']) && empty($data['rubros_rural'])){
        $validado = 0;
        $mensaje .= '<li>El rubro es requerido.</li>';
        $campos[] = 'rubros_rural'; 
      }
      //Operacion
      if(isset($data['field_aviso_operacion']) && empty($data['field_aviso_operacion'])){
        $validado = 0;
        $mensaje .= '<li>La operación es requerida.</li>';
        $campos[] = 'field_aviso_operacion'; 
      }
      //Titulo
      if(isset($data['field_aviso_titulo'])){
        if(empty($data['field_aviso_titulo'])){
          $validado = 0;
          $mensaje .= '<li>El título es requerido.</li>';
          $campos[] = 'field_aviso_titulo'; 
        } elseif(strlen($data['field_aviso_titulo']) < 4){
          $validado = 0;
          $mensaje .= '<li>El título debe tener 4 o más caracteres.</li>';
          $campos[] = 'field_aviso_titulo'; 
        } elseif(strlen($data['field_aviso_titulo']) > 80){
          $validado = 0;
          $mensaje .= '<li>El título no bebe superar los 80 caracteres.</li>';
          $campos[] = 'field_aviso_titulo'; 
        }
      }
      //Descripcion
      if(isset($data['field_aviso_descripcion']) && empty($data['field_aviso_descripcion'])){
        $validado = 0;
        $mensaje .= '<li>La descripción es requerida.</li>';
        $campos[] = 'field_aviso_descripcion'; 
      }
      //Provincia
      if(isset($data['provincia']) && empty($data['provincia'])){
        $validado = 0;
        $mensaje .= '<li>La provincia es requerida.</li>';
        $campos[] = 'provincia'; 
      }
      //Ciudad
      if(isset($data['ciudad-'.$data['provincia']]) && empty($data['ciudad-'.$data['provincia']])){
        $validado = 0;
        $mensaje .= '<li>La ciudad es requerida.</li>';
        $campos[] = 'ciudad'; 
      }
      //Anio
      if(isset($data['field_aviso_usado_anio']) && !empty($data['field_aviso_usado_anio'])){
        if(!is_numeric($data['field_aviso_usado_anio'])){
          $validado = 0;
          $mensaje .= '<li>El año debe ser numérico.</li>';
          $campos[] = 'field_aviso_usado_anio'; 
        } elseif($data['field_aviso_usado_anio'] < 1960 || $data['field_aviso_usado_anio'] > date('Y')) {
          $validado = 0;
          $mensaje .= '<li>El año debe ser mayor o igual a 1960 y menor o igual al actual.</li>';
          $campos[] = 'field_aviso_usado_anio'; 
        }
      }
      //Kilometro
      if(isset($data['field_aviso_usado']) && $data['field_aviso_usado'] == 1){
        if(isset($data['field_aviso_kilometros']) && empty($data['field_aviso_kilometros'])){
          $validado = 0;
          $mensaje .= '<li>Los kilómetros son requeridos.</li>';
          $campos[] = 'field_aviso_kilometros'; 
        }
      }
      //Telefono
      if(isset($data['field_aviso_telefono']) && empty($data['field_aviso_telefono'])){
        $validado = 0;
        $mensaje .= '<li>El teléfono del vendedor es requerido.</li>';
        $campos[] = 'field_aviso_telefono'; 
      }
      //Mail
      if(isset($data['field_aviso_mail']) && empty($data['field_aviso_mail'])){
        $validado = 0;
        $mensaje .= '<li>El E-mail del vendedor es requerido.</li>';
        $campos[] = 'field_aviso_mail'; 
      }
      //Precio
      if(isset($data['field_aviso_precio'])){
        if(empty($data['field_aviso_precio'])){
          $validado = 0;
          $mensaje .= '<li>El precio es requerido.</li>';
          $campos[] = 'field_aviso_precio'; 
        } elseif(!preg_match("/^[0-9.]+$/", $data['field_aviso_precio'])){
          $validado = 0;
          $mensaje .= '<li>El precio debe ser solo números y puntos.</li>';
          $campos[] = 'field_aviso_precio'; 
        }
      }
      //Moneda
      if(!isset($data['field_aviso_moneda']) || empty($data['field_aviso_moneda'])){
        $validado = 0;
        $mensaje .= '<li>La moneda es requerida.</li>';
        $campos[] = 'field_aviso_moneda'; 
      }
      //Forma de pago
      if(isset($data['field_aviso_forma_pago']) && empty($data['field_aviso_forma_pago'])){
        $validado = 0;
        $mensaje .= '<li>La forma de pago es requerida.</li>';
        $campos[] = 'field_aviso_forma_pago'; 
      }
      //Tipo de plan
      if(isset($data['field_aviso_tipo_plan']) && empty($data['field_aviso_tipo_plan'])){
        $validado = 0;
        $mensaje .= '<li>El tipo de plan es requerido.</li>';
        $campos[] = 'field_aviso_tipo_plan'; 
      }
      //Altura de calle
      if(isset($data['field_aviso_altura']) && !empty($data['field_aviso_altura']) && !is_numeric($data['field_aviso_altura'])){
        $validado = 0;
        $mensaje .= '<li>La altura de la calle debe ser solo numérica.</li>';
        $campos[] = 'field_aviso_altura'; 
      }
      //Costo de limpieza
      if(isset($data['field_aviso_limpieza']) && $data['field_aviso_limpieza'] == 3){
        if(empty($data['field_aviso_limpieza_monto']) || !is_numeric($data['field_aviso_limpieza_monto'])){
          $validado = 0;
          $mensaje .= '<li>El monto de limpieza es requerido y solo numérico.</li>';
          $campos[] = 'field_aviso_limpieza_monto'; 
        }
      }
      //Costo de desayuno
      if(isset($data['field_aviso_desayuno']) && $data['field_aviso_desayuno'] == 3){
        if(empty($data['field_aviso_desayuno_monto']) || !is_numeric($data['field_aviso_desayuno_monto'])){
          $validado = 0;
          $mensaje .= '<li>El monto de desayuno es requerido y solo numérico.</li>';
          $campos[] = 'field_aviso_desayuno_monto'; 
        }
      }
      //Costo de cochera
      if(isset($data['field_aviso_cobro_cochera']) && $data['field_aviso_cobro_cochera'] == 3){
        if(empty($data['field_aviso_cochera_monto']) || !is_numeric($data['field_aviso_cochera_monto'])){
          $validado = 0;
          $mensaje .= '<li>El monto de cochera es requerido y solo numérico.</li>';
          $campos[] = 'field_aviso_cochera_monto'; 
        }
      }
      
      //Validar peso y extension de archivo
      $i = 1;
      foreach($files as $kfile => $vfile){
        if(!empty($vfile['type'])){
          if($vfile['size'] > 307200){
            $validado = 0;
            $mensaje .= '<li>La foto número '.$i.' supera los 300kb.</li>';
            $campos[] = $kfile; 
          }
          if(!in_array($vfile['type'], array('image/jpg', 'image/jpeg', 'image/png', 'image/gif'))){
            $validado = 0;
            $mensaje .= '<li>La foto número '.$i.' tiene una extensión no válida.</li>';
            $campos[] = $kfile; 
          }
          $cant_fotos++;
        } elseif(!empty($vfile['name'])) {
          $validado = 0;
          $mensaje .= '<li>La foto número '.$i.' supera los 300kb.</li>';
          $campos[] = $kfile;
          $cant_fotos++;
        }
        $i++;
      }
      
      // Validamos fotos para alojamiento
      if($data['field_aviso_categoria'] == 'aviso_alquiler_temporario') {
        if(empty($data['nid'])) {
          if(!isset($data['field_aviso_foto']) || empty($data['field_aviso_foto'])) {
            $validado = 0;
            $mensaje .= '<li>El aviso tiene que tener al menos una foto.</li>';
          }
        }
      }
      
      $status = array('validado' => $validado, 'mensaje' => '<ul>'.$mensaje.'</ul>', 'campos' => $campos);      
      return $status;
    }
    
    /*
    * Validacion formulario de registro
    *
    */
    function validar_form_register($data){
      $status = array();
      $validado = 1;
      $mensaje = '';
      $campos = array();
      
      //Mail
      if(isset($data['username']) && empty($data['username'])){
        $validado = 0;
        $mensaje .= '<li>El mail es requerido.</li>';
        $campos[] = 'username'; 
      }
      //Password
      if($data['accion'] == 'edit') {
        if(isset($data['password']) && !empty($data['password'])){
          if($data['repassword'] != $data['password']) {
            $validado = 0;
            $mensaje .= '<li>Las contraseñas no coinciden.</li>';
            $campos[] = 'repassword'; 
          }
        }
      } else  {  
        if(isset($data['password']) && empty($data['password'])){
          $validado = 0;
          $mensaje .= '<li>La contraseña es requerida.</li>';
          $campos[] = 'password'; 
        } else {
          if(isset($data['repassword']) && empty($data['repassword'])){
            $validado = 0;
            $mensaje .= '<li>La confirmación de la contraseña es requerida.</li>';
            $campos[] = 'repassword'; 
          } elseif($data['repassword'] != $data['password']) {
            $validado = 0;
            $mensaje .= '<li>Las contraseñas no coinciden.</li>';
            $campos[] = 'repassword'; 
          }
        }
      }
      //Nombre
      if(isset($data['nombre']) && empty($data['nombre'])){
        $validado = 0;
        $mensaje .= '<li>El nombre es requerido.</li>';
        $campos[] = 'nombre'; 
      }
      //Apellido
      if(isset($data['apellido']) && empty($data['apellido'])){
        $validado = 0;
        $mensaje .= '<li>El apellido es requerido.</li>';
        $campos[] = 'apellido'; 
      }
      //Provincia
      if(isset($data['provincia']) && empty($data['provincia'])){
        $validado = 0;
        $mensaje .= '<li>La provincia es requerida.</li>';
        $campos[] = 'provincia'; 
      }
      //Ciudad
      if(isset($data['ciudad-'.$data['provincia']]) && empty($data['ciudad-'.$data['provincia']])){
        $validado = 0;
        $mensaje .= '<li>La ciudad es requerida.</li>';
        $campos[] = 'ciudad'; 
      }
      //Condicion
      if(isset($data['condiciones']) && empty($data['condiciones'])){
        $validado = 0;
        $mensaje .= '<li>La aceptación de condiciones de uso es requeridas.</li>';
        $campos[] = 'condiciones'; 
      }
      
      if($data['tipo_usuario'] == 'comercio') {
        //Nombre Sucursal
        if($data['sucursal'] == 1) {
          if(isset($data['nombreSucursal']) && empty($data['nombreSucursal'])){
            $validado = 0;
            $mensaje .= '<li>El nombre de la sucursal es requerido.</li>';
            $campos[] = 'nombreSucursal'; 
          }
        }
        //Calle
        if(isset($data['calle']) && empty($data['calle'])){
          $validado = 0;
          $mensaje .= '<li>El nombre de calle es requerido.</li>';
          $campos[] = 'calle'; 
        }
        //Altura
        if(isset($data['altura']) && empty($data['altura'])){
          $validado = 0;
          $mensaje .= '<li>La altura de la calle es requerida.</li>';
          $campos[] = 'altura'; 
        }
        //CP
        if(isset($data['codigoPostal']) && empty($data['codigoPostal'])){
          $validado = 0;
          $mensaje .= '<li>El código postal es requerido.</li>';
          $campos[] = 'codigoPostal'; 
        }
        //Tipo comercio
        if(isset($data['tipoComercio']) && empty($data['tipoComercio'])){
          $validado = 0;
          $mensaje .= '<li>El tipo de comercio es requerido.</li>';
          $campos[] = 'tipoComercio'; 
        }
        //Telefono responsable
        if(isset($data['telefono2']) && empty($data['telefono2'])){
          $validado = 0;
          $mensaje .= '<li>El teléfono del responsable es requerido.</li>';
          $campos[] = 'telefono2'; 
        }
        //Razon social
        if(isset($data['razonSocial']) && empty($data['razonSocial'])){
          $validado = 0;
          $mensaje .= '<li>La razón social es requerida.</li>';
          $campos[] = 'razonSocial'; 
        }
        //Cuit
        if(isset($data['cuit']) && empty($data['cuit'])){
          $validado = 0;
          $mensaje .= '<li>El CUIT es requerido.</li>';
          $campos[] = 'cuit'; 
        } elseif(!preg_match('/^\d{11}$/', $data['cuit'])) {
          $validado = 0;
          $mensaje .= '<li>El CUIT no es válido.</li>';
          $campos[] = 'cuit'; 
        }
        //Iva
        if(isset($data['iva']) && empty($data['iva'])){
          $validado = 0;
          $mensaje .= '<li>La condición del IVA es requerida.</li>';
          $campos[] = 'iva'; 
        }
        //Telefono WhatsApp
        if(isset($data['telefono_whatsapp']) && strlen($data['telefono_whatsapp'])>0) {
          if(strlen($data['telefono_whatsapp']) < 12 || strlen($data['telefono_whatsapp']) > 14) {
            $validado = 0;
            $mensaje .= '<li>El teléfono de WhatsApp es incorrecto. No agregue el 15.</li>';
            $campos[] = 'telefono_whatsapp'; 
          }
        }
        //Domicilio fiscal
        if(!isset($data['domicilioFiscalComercial'])) {
          //Calle fiscal
          if(isset($data['calleFiscal']) && empty($data['calleFiscal'])){
            $validado = 0;
            $mensaje .= '<li>La calle del domicilio fiscal es requerida.</li>';
            $campos[] = 'calleFiscal'; 
          }
          //Altura fiscal
          if(isset($data['alturaFiscal']) && empty($data['alturaFiscal'])){
            $validado = 0;
            $mensaje .= '<li>La altura de la calle del domicilio fiscal es requerida.</li>';
            $campos[] = 'alturaFiscal'; 
          }
          //CP fiscal
          if(isset($data['codigoPostalFiscal']) && empty($data['codigoPostalFiscal'])){
            $validado = 0;
            $mensaje .= '<li>El código postal del domicilio fiscal es requerido.</li>';
            $campos[] = 'codigoPostalFiscal'; 
          }
          //Provincia fiscal
          if(isset($data['provinciaFiscal']) && empty($data['provinciaFiscal'])){
            $validado = 0;
            $mensaje .= '<li>La provincia del domicilio fiscal es requerida.</li>';
            $campos[] = 'provinciaFiscal'; 
          }
          //Ciudad fiscal
          if(isset($data['ciudadFiscal-'.$data['provinciaFiscal']]) && empty($data['ciudadFiscal-'.$data['provinciaFiscal']])){
            $validado = 0;
            $mensaje .= '<li>La ciudad del domicilio fiscal es requerida.</li>';
            $campos[] = 'ciudadFiscal'; 
          }
        }
      }
      
      $status = array('validado' => $validado, 'mensaje' => '<ul>'.$mensaje.'</ul>', 'campos' => $campos);      
      return $status;
    }
    
    //Obtener Campos de Filtro segun busqueda
    function obtener_filtros_busqueda($tids, $listado_filtros)
    {
      $rubros_secundario = array(6325, 6329, 6324, 6326, 6328, 6327, 6338, 6331, 6339, 6334, 6332, 6337, 6335, 6336, 6333);
      $filtros_busqueda = array();
      if(isset($tids[0])){
        foreach($listado_filtros as $filtro){
          if($filtro['tid'] == $tids[0]){
            if(isset($tids[1]) && is_numeric($tids[1]) && in_array($tids[1], $rubros_secundario)){
              foreach($filtro['child_data'][0]['child_data'] as $subfiltro){
                if($subfiltro['tid'] == $tids[1]){
                  //dos tids
                  $filtros_busqueda = $subfiltro['child_data'];
                  break;
                }
              }
            } else {
              //Un tid
              $filtros_busqueda = $filtro['child_data'];
            }
            break;
          }
        }
      }
      return $filtros_busqueda;
    }
    
    //Conivierte un texto para ser utilizado como class, id o name de elementos
    function texto_a_class($texto)
    {
      $class = '';
      $class = mb_strtolower($texto);
      $class = str_replace(' ', '_', $class);
      $class = str_replace('á', 'a', $class);
      $class = str_replace('é', 'e', $class);
      $class = str_replace('í', 'i', $class);
      $class = str_replace('ó', 'o', $class);
      $class = str_replace('ú', 'u', $class);
      $class = str_replace('ñ', 'n', $class);
      return $class;
    }
    
    //Obtener ultimo tid de filtro
    function obtener_ultimo_tid($filtro)
    {
      $tid_explode = explode(' tid', $filtro);
      if(count($tid_explode) > 1){
        $filtro = 'tid' . array_pop($tid_explode);
      }
      return $filtro;
    }
    
    //Desencriptar datos devueltos de servicios
    function servicio_decrypt($string, $key) {
      $result = '';
      $string = base64_decode($string);
      for($i=0; $i<strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key))-1, 1);
        $char = chr(ord($char)-ord($keychar));
        $result.=$char;
      }
      return $result;
    }
    
    //Decodificar los caracteres especiales de textos que vienen de js.
    function texto_js_a_php($texto_js)
    {
      $texto_php = $texto_js;
      $texto_php = str_replace('u00aa', 'ª', $texto_php);
      $texto_php = str_replace('u00c1', 'Á', $texto_php);
      $texto_php = str_replace('u00c9', 'É', $texto_php);
      $texto_php = str_replace('u00cd', 'Í', $texto_php);
      $texto_php = str_replace('u00d3', 'Ó', $texto_php);
      $texto_php = str_replace('u00da', 'Ú', $texto_php);
      $texto_php = str_replace('u00e1', 'á', $texto_php);
      $texto_php = str_replace('u00e9', 'é', $texto_php);
      $texto_php = str_replace('u00ed', 'í', $texto_php);
      $texto_php = str_replace('u00f3', 'ó', $texto_php);
      $texto_php = str_replace('u00fa', 'ú', $texto_php);
      $texto_php = str_replace('u00d1', 'Ñ', $texto_php);
      $texto_php = str_replace('u00f1', 'ñ', $texto_php);
      $texto_php = str_replace('u00b0', '°', $texto_php);
      $texto_php = str_replace('u00ba', '°', $texto_php);
      $texto_php = str_replace('u00a1', '¡', $texto_php);
      $texto_php = str_replace('u201c', '"', $texto_php);
      $texto_php = str_replace('u00e0', 'á', $texto_php);
      $texto_php = str_replace('u00e8', 'é', $texto_php);
      $texto_php = str_replace('u00ec', 'í', $texto_php);
      $texto_php = str_replace('u00f2', 'ó', $texto_php);
      $texto_php = str_replace('u00f9', 'ú', $texto_php);
      return $texto_php;
    }

    function validar_cuit($cuit) {
      $valido = FALSE;
      $suma = 0;
      if(strlen($cuit)!=11) {
        return FALSE;
      }
      if(!is_numeric($cuit)) {
        return FALSE;
      }
      $suma += intval(substr($cuit, 0, 1)) * 5;
      $suma += intval(substr($cuit, 1, 1)) * 4;
      $suma += intval(substr($cuit, 2, 1)) * 3;
      $suma += intval(substr($cuit, 3, 1)) * 2;
      $suma += intval(substr($cuit, 4, 1)) * 7;
      $suma += intval(substr($cuit, 5, 1)) * 6;
      $suma += intval(substr($cuit, 6, 1)) * 5;
      $suma += intval(substr($cuit, 7, 1)) * 4;
      $suma += intval(substr($cuit, 8, 1)) * 3;
      $suma += intval(substr($cuit, 9, 1)) * 2;
      $suma += intval(substr($cuit, 10, 1)) * 1;
      if(round($suma/11, 0) == ($suma / 11)) {
        return TRUE;
      }
      return FALSE;
    }
    
    // Convertir fecha solr (YYmmdd) a formato dd-mm-YY
    function convertir_fecha_solr($fecha) {
      $fecha_return = '';
      $fecha = str_replace('[', '', $fecha);
      $fecha = str_replace(']', '', $fecha);
      $anio = substr($fecha, 0, -4);
      $mes = substr($fecha, 4, -2);
      $dia = substr($fecha, 6);
      $fecha_return = $dia.'-'.$mes.'-'.$anio;
      return $fecha_return;
    }

    function verificar_recaptcha($response, $secret) {
      $datos = array();
      $datos['secret'] = $secret;
      $datos['response'] = $response;
      //$datos['remoteip'] = contactanos_get_client_ip();
      $params = 'secret='.$datos['secret'].'&response='.$datos['response'];
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      $output = curl_exec($ch);
      curl_close($ch);
      $json = json_decode($output);
      return $json;
    }
    
    function ads_get_keys_category($categoria, $key = '') {
      $tipos = array();
      $rubros = explode('/', $categoria);
      if(isset($rubros[1]) && $rubros[1] == 'vehiculos') {
        $tipos[] = 'vehiculos';
        if(isset($rubros[2])) {
          switch($rubros[2]){   
            case '4x4 - 4x2 y todo terreno':
              $tipos[] = '4x4';
              break;
            case 'autos':
              $tipos[] = 'autos';
              break;
            case 'accesorios y repuestos':
              $tipos[] = 'repuestos';
              break;
            case 'motos - cuadriciclos y náutica':
              $tipos[] = 'motos';
              break;
            case 'planes de ahorro':
              $tipos[] = 'planesahorro';
              break;
            case 'utilitarios - pick-up y camiones':
              $tipos[] = 'utilitarios';
              break;
          }
        }
        $marca_busqueda = 0;
        if($key != '') {
          $target_data = ads_get_target_key($key);
          if(!empty($target_data)) { 
            $target[] = $target_data;
            $marca_busqueda = 1;
          }
        }
        if(isset($rubros[3]) && !$marca_busqueda) {
          $target[] = array('key' => 'clasificados_marca', 'value' => strtolower(str_replace(' ', '', $rubros[3])));
        }
      }
      if(isset($rubros[1]) && $rubros[1] == 'inmuebles') {
        $target = array();
        $tipos[] = 'inmuebles';
        if(isset($rubros[2])) {
          switch($rubros[2]){   
            case 'campos':
              $tipos[] = 'campos';
              break;  
            case 'casas':
              $tipos[] = 'casas';
              break;  
            case 'cocheras':
              $tipos[] = 'cocheras';
              break;  
            case 'departamentos':
              $tipos[] = 'departamentos';
              break;  
            case 'emprendimientos':
              $tipos[] = 'emprendimientos';
              break;  
            case 'galpones y depósitos':
              $tipos[] = 'depositos';
              break;  
            case 'locales':
              $tipos[] = 'locales';
              break;  
            case 'oficinas':
              $tipos[] = 'oficinas';
              break; 
            case 'terrenos y lotes':
              $tipos[] = 'terrenos';
              break;
          }
        }
        if(isset($rubros[3])) {
          $target[] = array('key' => 'clasificados_oper', 'value' => strtolower(str_replace('_', '', $rubros[3])));
        }
      }
      if(isset($rubros[1]) && in_array($rubros[1], array('productos', 'servicios', 'rurales'))) {      
        $tipos[] = $rubros[1];
        if(isset($rubros[2])) {
          $target[] = array('key' => 'clasificados_rubro', 'value' => strtolower(str_replace('_', '', $rubros[2])));
        }
      }
      if(isset($rubros[1]) && $rubros[1] == 'alojamientos') {      
        $tipos[] = $rubros[1];
      }
      $tipos_text = '';
      foreach($tipos as $tipo) {
        if($tipos_text == '')
          $tipos_text = $tipo;
        else
          $tipos_text .= "', '".$tipo;
      }
      if($tipos_text != '')
        $target[] = array('key' => 'clasificados_tipo', 'value' => $tipos_text);
      
      return $target;
      
    }
    
    function ads_get_banner($adslot) {
      if($adslot == 'dfp-anuncio_previo') {
      $html_banner = "
<div class='banner'>
  <div style='display:none'>
    <div id='$adslot' data-format='interstitial'>
      <script>
      googletag.cmd.push(function() { googletag.display('$adslot'); });
      </script>
    </div>
  </div>
</div>";
      } elseif($adslot == 'dfp-rectangle') {
      $html_banner = "
<div class='banner footer'>
  <div id='$adslot'>
    <script>
      googletag.cmd.push(function() { googletag.display('$adslot'); });
    </script>
  </div>
</div>";
      } elseif($adslot == 'dfp-zocalo') {
      $html_banner = "
<div class='banner zocalo'>
  <div id='$adslot' data-format='footerFixed'>
    <script>
      googletag.cmd.push(function() { googletag.display('$adslot'); });
    </script>
  </div>
</div>";
      } else {
        $html_banner = "
<div class='banner'>
  <div id='$adslot'>
    <script>
      googletag.cmd.push(function() { googletag.display('$adslot'); });
    </script>
  </div>
</div>";
      }
      return $html_banner;
    }
    
    function ads_get_target_key($key) {
      $target = array();
      if (strpos($key, 'chevrolet') !== FALSE)
        $target = array('key' => 'clasificados_marca', 'value' => 'chevrolet');
      if (strpos($key, 'audi') !== FALSE)
        $target = array('key' => 'clasificados_marca', 'value' => 'audi');
      if (strpos($key, 'volkswagen') !== FALSE)
        $target = array('key' => 'clasificados_marca', 'value' => 'volkswagen');
      if (strpos($key, 'vw') !== FALSE)
        $target = array('key' => 'clasificados_marca', 'value' => 'volkswagen');
      if (strpos($key, 'fiat') !== FALSE)
        $target = array('key' => 'clasificados_marca', 'value' => 'fiat');
      if (strpos($key, 'peugeot') !== FALSE)
        $target = array('key' => 'clasificados_marca', 'value' => 'peugeot');
      if (strpos($key, 'ford') !== FALSE)
        $target = array('key' => 'clasificados_marca', 'value' => 'ford');
      return $target;
    }
?>