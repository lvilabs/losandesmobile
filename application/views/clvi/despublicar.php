<div id="content" class="admin despublicacion">
<div class="container">
  <div class="row">
    <?php if(!empty($respuesta->mensaje)) : ?>
    <div class="twelvecol mensaje clear">
      <span class="<?php $respuesta->status!=0?'error':''; ?>"><?php echo $respuesta->mensaje; ?></span><br>
      <?php if(!empty($respuesta->errores)) : ?>
      <span class="error">
      <?php
        foreach($respuesta->errores as $error) {
          echo $error.'.<br>';
        }
      ?>
      </span>
      <?php endif; ?>
    </div>
    <?php endif; ?>
    <form method="post">
    <div class="twelvecol">
      <input type="hidden" name="nid" value="<?php echo $nid; ?>">
      <input type="hidden" name="confirmar" value="<?php echo $respuesta->confirmar; ?>">
      <input type="submit" name="submit" value="Despublicar">
      <input type="button" name="cancelar" value="Cancelar">
    </div>
    </form>
  </div>
</div>
</div>