<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<style type="text/css">
   .campo{margin: 10px;}
   span.detalle{font-size: 0.8em; font-style: italic;}
</style>
<h1>Clasificados La Voz - Test Carga de Avisos</h1>
<h2>Selección de Rubro</h2>
<form name="formRubros" class="formRubros" action="/testnodeadd" method="post">
  <label>Rubros: </label>
  <select name="selectRubro">
    <option value=""></option>
    <optgroup label="Inmuebles">
    <option value="aviso_campo">Campos</option>
    <option value="aviso_casa">Casas</option>
    <option value="aviso_cochera">Cocheras</option>
    <option value="aviso_departamento">Departamentos</option>
    <option value="aviso_galpon_deposito">Galpones y Depósitos</option>
    <option value="aviso_local">Local</option>
    <option value="aviso_oficina">Oficinas</option>
    <option value="aviso_terreno_lote">Terrenos y Lotes</option>
    </optgroup>
    <optgroup label="Autos">
    <option value="aviso_4x4">4x4, 4x2 y todo terreno</option>
    <option value="aviso_auto">Autos</option>
    <option value="aviso_plan_ahorro">Planes de Ahorro</option>
    <option value="aviso_utilitario">Utilitarios, pick-up y camiones</option>
    <option value="aviso_moto">Motos, cuadriciclos y náutica</option>
    <option value="aviso_repuesto">Accesorios y Repuestos</option>
    </optgroup>
    <option value="aviso_producto">Productos</option>
    <option value="aviso_servicio">Servicios</option>
    <option value="aviso_rural">Rurales</option>
  </select>
  <input id="submit-contacto" value="Enviar" type="submit"/>
</form>
<?php if(isset($rubro) && $rubro != '') { ?>
  <h2>Formulario de <?php print $rubro; ?></h2>
  <form name="formAviso" class="formContacto" action="/testnodeadd/submit_form" enctype="multipart/form-data" method="post">
    <div class="campo"><label><span style="color:red;">*</span> Categoría: </label><input name="field_aviso_categoria" type="text" value="<?php print $rubro; ?>" /></div>
    <!-- Tipo unidad casa -->
    <?php if(array_key_exists('field_aviso_tipo_unidad', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_unidad']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_unidad']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_unidad']['key']; ?>">
        <?php if($options['field_aviso_tipo_unidad']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_unidad']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Tipo campo -->
    <?php if(array_key_exists('field_aviso_tipo_campo', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_campo']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_campo']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_campo']['key']; ?>">
        <?php if($options['field_aviso_tipo_campo']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_campo']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Tipo unidad dpto -->
    <?php if(array_key_exists('field_aviso_tipo_unidad_dpto', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_unidad_dpto']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_unidad_dpto']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_unidad_dpto']['key']; ?>">
        <?php if($options['field_aviso_tipo_unidad_dpto']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_unidad_dpto']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Tipo terreno -->
    <?php if(array_key_exists('field_aviso_tipo_terreno', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_terreno']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_terreno']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_terreno']['key']; ?>">
        <?php if($options['field_aviso_tipo_terreno']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_terreno']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Marca Modelo 4x4 -->
    <?php if(array_key_exists('marca_modelo_4x4', $options)) { ?>
      <div class="campo"><label><?php if($options['marca_modelo_4x4']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['marca_modelo_4x4']['name']; ?>:</label>
      <select name="<?php print $options['marca_modelo_4x4']['key']; ?>">
        <?php if($options['marca_modelo_4x4']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['marca_modelo_4x4']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
          <?php foreach($option['data_child'] as $option2){ ?>
                <option value="<?php print $option2['key']; ?>">- <?php print $option2['name']; ?></option>
          <?php      
            }
          } ?>
      </select></div>
    <?php } ?>
    <!-- Marca Modelo Auto -->
    <?php if(array_key_exists('marca_modelo_autos', $options)) { ?>
      <div class="campo"><label><?php if($options['marca_modelo_autos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['marca_modelo_autos']['name']; ?>:</label>
      <select name="<?php print $options['marca_modelo_autos']['key']; ?>">
        <?php if($options['marca_modelo_autos']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['marca_modelo_autos']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
          <?php foreach($option['data_child'] as $option2){ ?>
                <option value="<?php print $option2['key']; ?>">- <?php print $option2['name']; ?></option>
          <?php      
            }
          } ?>
      </select></div>
    <?php } ?>
    <!-- Marca Modelo Plan Ahorro -->
    <?php if(array_key_exists('marca_modelo_planes_de_ahorro', $options)) { ?>
      <div class="campo"><label><?php if($options['marca_modelo_planes_de_ahorro']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['marca_modelo_planes_de_ahorro']['name']; ?>:</label>
      <select name="<?php print $options['marca_modelo_planes_de_ahorro']['key']; ?>">
        <?php if($options['marca_modelo_planes_de_ahorro']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['marca_modelo_planes_de_ahorro']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
          <?php foreach($option['data_child'] as $option2){ ?>
                <option value="<?php print $option2['key']; ?>">- <?php print $option2['name']; ?></option>
          <?php      
            }
          } ?>
      </select></div>
    <?php } ?>
    <!-- Marca Modelo utilitario -->
    <?php if(array_key_exists('marca_modelo_utilitarios', $options)) { ?>
      <div class="campo"><label><?php if($options['marca_modelo_utilitarios']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['marca_modelo_utilitarios']['name']; ?>:</label>
      <select name="<?php print $options['marca_modelo_utilitarios']['key']; ?>">
        <?php if($options['marca_modelo_utilitarios']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['marca_modelo_utilitarios']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
          <?php foreach($option['data_child'] as $option2){ ?>
                <option value="<?php print $option2['key']; ?>">- <?php print $option2['name']; ?></option>
          <?php      
            }
          } ?>
      </select></div>
    <?php } ?>
    <!-- Marca moto -->
    <?php if(array_key_exists('marca_motos', $options)) { ?>
      <div class="campo"><label><?php if($options['marca_motos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['marca_motos']['name']; ?>:</label>
      <select name="<?php print $options['marca_motos']['key']; ?>">
        <?php if($options['marca_motos']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['marca_motos']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Subrubro repuesto -->
    <?php if(array_key_exists('subrubro_repuestos', $options)) { ?>
      <div class="campo"><label><?php if($options['subrubro_repuestos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['subrubro_repuestos']['name']; ?>:</label>
      <select name="<?php print $options['subrubro_repuestos']['key']; ?>">
        <?php if($options['subrubro_repuestos']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['subrubro_repuestos']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
          <?php foreach($option['data_child'] as $option2){ ?>
                <option value="<?php print $option2['key']; ?>">- <?php print $option2['name']; ?></option>
          <?php      
            }
          } ?>
      </select></div>
    <?php } ?>
    <!-- Rubro Producto -->
    <?php if(array_key_exists('rubros_producto', $options)) { ?>
      <div class="campo"><label><?php if($options['rubros_producto']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['rubros_producto']['name']; ?>:</label>
      <select name="<?php print $options['rubros_producto']['key']; ?>">
        <?php if($options['rubros_producto']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['rubros_producto']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
          <?php foreach($option['data_child'] as $option2){ ?>
             <option value="<?php print $option2['key']; ?>">- <?php print $option2['name']; ?></option>
             <?php foreach($option2['data_child'] as $option3){ ?>
                <option value="<?php print $option3['key']; ?>">- - <?php print $option3['name']; ?></option>
              <?php 
              }      
            }
          } ?>
      </select></div>
    <?php } ?>
    <!-- Rubro Servicio -->
    <?php if(array_key_exists('rubros_servicio', $options)) { ?>
      <div class="campo"><label><?php if($options['rubros_servicio']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['rubros_servicio']['name']; ?>:</label>
      <select name="<?php print $options['rubros_servicio']['key']; ?>">
        <?php if($options['rubros_servicio']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['rubros_servicio']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
          <?php foreach($option['data_child'] as $option2){ ?>
             <option value="<?php print $option2['key']; ?>">- <?php print $option2['name']; ?></option>
             <?php foreach($option2['data_child'] as $option3){ ?>
                <option value="<?php print $option3['key']; ?>">- - <?php print $option3['name']; ?></option>
              <?php 
              }      
            }
          } ?>
      </select></div>
    <?php } ?>
    <!-- Rubro Rural -->
    <?php if(array_key_exists('rubros_rural', $options)) { ?>
      <div class="campo"><label><?php if($options['rubros_rural']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['rubros_rural']['name']; ?>:</label>
      <select name="<?php print $options['rubros_rural']['key']; ?>">
        <?php if($options['rubros_rural']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['rubros_rural']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
          <?php foreach($option['data_child'] as $option2){ ?>
             <option value="<?php print $option2['key']; ?>">- <?php print $option2['name']; ?></option>
          <?php       
            }
          } ?>
      </select></div>
    <?php } ?>
    <!-- Operacion -->
    <?php if(array_key_exists('field_aviso_operacion', $options) && in_array($rubro, array('aviso_casa', 'aviso_campo', 'aviso_cochera', 'aviso_departamento', 'aviso_galpon_deposito', 'aviso_local', 'aviso_oficina', 'aviso_terreno_lote', 'aviso_producto', 'aviso_rural'))) { ?>
      <div class="campo"><label><?php if($options['field_aviso_operacion']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_operacion']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_operacion']['key']; ?>">
        <?php if($options['field_aviso_operacion']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_operacion']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Titulo -->
    <div class="campo"><label><span style="color:red;">*</span> Título del Aviso: </label><input name="field_aviso_titulo" type="text" value="" /></div>
    <!-- Descripci&oacute;n -->
    <div class="campo"><label><span style="color:red;">*</span> Descripción: </label><textarea name="field_aviso_descripcion"></textarea></div>
    <!-- Apto Escritura -->
    <?php if(array_key_exists('field_aviso_apto_escritura', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_apto_escritura']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_apto_escritura']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_apto_escritura']['key']; ?>">
        <?php if($options['field_aviso_apto_escritura']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_apto_escritura']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Provincia -->
    <?php if(array_key_exists('ubicacion', $options)) { ?>
      <div class="campo"><label><?php if($options['ubicacion']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['ubicacion']['name']; ?>:</label>
      <select name="<?php print $options['ubicacion']['key']; ?>">
        <?php if($options['ubicacion']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['ubicacion']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
          <?php foreach($option['data_child'] as $option2){ ?>
             <option value="<?php print $option2['key']; ?>">- <?php print $option2['name']; ?></option>
             <?php foreach($option2['data_child'] as $option3){ ?>
                <option value="<?php print $option3['key']; ?>">- - <?php print $option3['name']; ?></option>
              <?php 
              }      
            }
          } ?>
      </select></div>
    <?php } ?>
    <!-- Tipo Barrio -->
    <?php if(array_key_exists('field_aviso_tipo_barrio', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_barrio']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_barrio']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_barrio']['key']; ?>">
        <?php if($options['field_aviso_tipo_barrio']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_barrio']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Zona -->
    <?php if(array_key_exists('field_aviso_zona', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_zona']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_zona']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_zona']['key']; ?>">
        <?php if($options['field_aviso_zona']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_zona']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select><br>
        <span class="detalle">Solo para ciudad de Córdoba.</span>
      </div>
    <?php } ?>
    <?php if(in_array($rubro, array('aviso_casa', 'aviso_campo', 'aviso_cochera', 'aviso_departamento', 'aviso_galpon_deposito', 'aviso_local', 'aviso_oficina', 'aviso_terreno_lote', 'aviso_producto', 'aviso_servicio', 'aviso_rural'))){ ?>
      <!-- Calle -->
      <div class="campo"><label>Calle: </label><input name="field_aviso_calle" type="text" value="" /></div>
      <!-- Altura -->
      <div class="campo"><label>Altura: </label><input name="field_aviso_altura" type="text" value="" /><br>
        <span class="detalle">Solo números.</span>
      </div>
    <?php } ?>
    <!-- Confidencial -->
    <?php if(array_key_exists('field_aviso_confidencial', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_confidencial']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_confidencial']['name']; ?>:</label>
      <?php foreach($options['field_aviso_confidencial']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_confidencial']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <?php if(in_array($rubro, array('aviso_campo', 'aviso_terreno_lote'))){ ?>
      <!-- Num Manzana -->
      <div class="campo"><label>Num. Manzana: </label><input name="field_aviso_num_manzana" type="text" value="" /></div>
      <!-- Num Lote -->
      <div class="campo"><label>Num. Lote: </label><input name="field_aviso_num_lote" type="text" value="" /></div>
    <?php } ?>
    <!-- Mapa -->
      <div class="campo"><label><strong>Mapa: </strong></label><br>
        <label>Mapa Latitud: </label><input name="field_aviso_latitud" type="text" value="" /><br>
        <label>Mapa Longitud: </label><input name="field_aviso_longitud" type="text" value="" />
      </div>
    <!-- Uso -->
    <?php if(array_key_exists('field_aviso_usado', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['field_aviso_usado']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_usado']['name']; ?>:</strong></label><br />
      <?php foreach($options['field_aviso_usado']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="radio" name="<?php print $options['field_aviso_usado']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <?php if(in_array($rubro, array('aviso_4x4', 'aviso_auto', 'aviso_moto', 'aviso_plan_ahorro', 'aviso_utilitario'))){ ?>
      <!-- Anio -->
      <div class="campo"><label><span style="color:red;">*</span>Año: </label><input name="field_aviso_usado_anio" type="text" value="2013" /><br>
        <span class="detalle">Solo números y formato de año YYYY. Mayor a 1960 y menor o igual al actual.</span>
      </div>
      <!-- Kilometros -->
      <div class="campo"><label>Kilómetros: </label><input name="field_aviso_kilometros" type="text" value="" /><br>
        <span class="detalle">Requerido si se selecciona usado.</span>
      </div>
    <?php } ?>
    <!-- Combustible -->
    <?php if(array_key_exists('field_aviso_combustible', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_combustible']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_combustible']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_combustible']['key']; ?>">
        <?php if($options['field_aviso_combustible']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_combustible']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Telefono -->
    <div class="campo"><label><span style="color:red;">*</span> Teléfono Vendedor: </label><input name="field_aviso_telefono" type="text" value="" /></div>
    <!-- Ocultar Telefono -->
    <?php if(array_key_exists('field_aviso_ocultar_tel', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_ocultar_tel']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_ocultar_tel']['name']; ?>:</label>
      <?php foreach($options['field_aviso_ocultar_tel']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_ocultar_tel']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Mail -->
    <div class="campo"><label><span style="color:red;">*</span> Mail Vendedor: </label><input name="field_aviso_mail" type="text" value="" /></div>
    <!-- Ocultar Mail -->
    <?php if(array_key_exists('field_aviso_ocultar_mail', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_ocultar_mail']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_ocultar_mail']['name']; ?>:</label>
      <?php foreach($options['field_aviso_ocultar_mail']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_ocultar_mail']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Precio -->
    <div class="campo"><label><span style="color:red;">*</span> Precio: </label><input name="field_aviso_precio" type="text" value="" /><br>
      <span class="detalle">Solo números y punto.</span>
    </div>
    <!-- Moneda -->
    <?php if(array_key_exists('field_aviso_moneda', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['field_aviso_moneda']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_moneda']['name']; ?>:</strong></label><br />
      <?php foreach($options['field_aviso_moneda']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="radio" name="<?php print $options['field_aviso_moneda']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Ocultar Precio -->
    <?php if(array_key_exists('field_aviso_ocultar_precio', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_ocultar_precio']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_ocultar_precio']['name']; ?>:</label>
      <?php foreach($options['field_aviso_ocultar_precio']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_ocultar_precio']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Recibe menor -->
    <?php if(array_key_exists('field_aviso_recibe_menor', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['field_aviso_recibe_menor']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_recibe_menor']['name']; ?>:</strong></label><br />
      <?php foreach($options['field_aviso_recibe_menor']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="radio" name="<?php print $options['field_aviso_recibe_menor']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Forma de Pago -->
    <?php if(array_key_exists('field_aviso_forma_pago', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_forma_pago']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_forma_pago']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_forma_pago']['key']; ?>">
        <?php if($options['field_aviso_forma_pago']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_forma_pago']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Apto Credito -->
    <?php if(array_key_exists('field_aviso_apto_credito', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_apto_credito']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_apto_credito']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_apto_credito']['key']; ?>">
        <?php if($options['field_aviso_apto_credito']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_apto_credito']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Antiguedad -->
    <?php if(in_array($rubro, array('aviso_casa', 'aviso_cochera', 'aviso_departamento', 'aviso_galpon_deposito', 'aviso_local', 'aviso_oficina'))){ ?>
      <div class="campo"><label>Antigüedad: </label><input name="field_aviso_antiguedad" type="text" value="" /></div>
    <?php } ?>
    <!-- Estrenar -->
    <?php if(array_key_exists('field_aviso_estrenar', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_estrenar']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_estrenar']['name']; ?>:</label>
      <?php foreach($options['field_aviso_estrenar']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_estrenar']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Etapa Construccion -->
    <?php if(array_key_exists('field_aviso_etapa_construccion', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_etapa_construccion']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_etapa_construccion']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_etapa_construccion']['key']; ?>">
        <?php if($options['field_aviso_etapa_construccion']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_etapa_construccion']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select><br>
        <span class="detalle">Solo si se selecciona A Estrenar.</span>
      </div>
    <?php } ?>
    <!-- Cantidad privados -->
    <?php if(in_array($rubro, array('aviso_oficina'))){ ?>
      <div class="campo"><label>Cantidad de Privados: </label><input name="field_aviso_cantidad_privados" type="text" value="" /></div>
    <?php } ?>
    <!-- Orientacion -->
    <?php if(array_key_exists('field_aviso_ubicacion', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_ubicacion']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_ubicacion']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_ubicacion']['key']; ?>">
        <?php if($options['field_aviso_ubicacion']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_ubicacion']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Piso numero -->
    <?php if(in_array($rubro, array('aviso_departamento'))){ ?>
      <div class="campo"><label>Piso Número: </label><input name="field_aviso_piso_numero" type="text" value="" /></div>
    <?php } ?>
    <!-- Cantidad Dormitorios -->
    <?php if(array_key_exists('field_aviso_cantidad_dormitorios', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_cantidad_dormitorios']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_cantidad_dormitorios']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_cantidad_dormitorios']['key']; ?>">
        <?php if($options['field_aviso_cantidad_dormitorios']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_cantidad_dormitorios']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Cantidad Plantas -->
    <?php if(array_key_exists('field_aviso_cantidad_plantas', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_cantidad_plantas']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_cantidad_plantas']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_cantidad_plantas']['key']; ?>">
        <?php if($options['field_aviso_cantidad_plantas']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_cantidad_plantas']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Cantidad Banios -->
    <?php if(array_key_exists('field_aviso_cantidad_banios', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_cantidad_banios']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_cantidad_banios']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_cantidad_banios']['key']; ?>">
        <?php if($options['field_aviso_cantidad_banios']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_cantidad_banios']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Tipo Banios -->
    <?php if(array_key_exists('field_aviso_tipo_banio', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_banio']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_banio']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_banio']['key']; ?>">
        <?php if($options['field_aviso_tipo_banio']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_banio']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Tipo techo industrial -->
    <?php if(array_key_exists('field_aviso_tipo_techo', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_techo']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_techo']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_techo']['key']; ?>">
        <?php if($options['field_aviso_tipo_techo']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_techo']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Tipo porton -->
    <?php if(array_key_exists('field_aviso_tipo_porton', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_porton']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_porton']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_porton']['key']; ?>">
        <?php if($options['field_aviso_tipo_porton']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_porton']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Cantidad Cochera -->
    <?php if(array_key_exists('field_aviso_cantidad_cocheras', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_cantidad_cocheras']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_cantidad_cocheras']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_cantidad_cocheras']['key']; ?>">
        <?php if($options['field_aviso_cantidad_cocheras']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_cantidad_cocheras']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Cobertura Cochera -->
    <?php if(array_key_exists('field_aviso_cobertura_cochera', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_cobertura_cochera']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_cobertura_cochera']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_cobertura_cochera']['key']; ?>">
        <?php if($options['field_aviso_cobertura_cochera']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_cobertura_cochera']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Tipo Cochera -->
    <?php if(array_key_exists('field_aviso_tipo_cochera', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_cochera']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_cochera']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_cochera']['key']; ?>">
        <?php if($options['field_aviso_tipo_cochera']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_cochera']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <?php if(in_array($rubro, array('aviso_casa', 'aviso_galpon_deposito', 'aviso_local', 'aviso_oficina'))){ ?>
      <!-- Superficie Total -->
      <div class="campo"><label>Superficie Total: </label><input name="field_aviso_sup_total" type="text" value="" /></div>
      <!-- Superfcie Cubierta -->
      <div class="campo"><label>Superficie Cubierta: </label><input name="field_aviso_sup_cubierta" type="text" value="" /></div>
    <?php } ?>
    <?php if(in_array($rubro, array('aviso_campo'))){ ?>
      <!-- Distancia Pavimento -->
      <div class="campo"><label>Distancia del Pavimento: </label><input name="field_aviso_distancia_pavimento" type="text" value="" /></div>
      <!-- Cantidad Hectarias -->
      <div class="campo"><label>Cantidad de Hectarias: </label><input name="field_aviso_cantidad_hectarias" type="text" value="" /></div>
    <?php } ?>
    <?php if(in_array($rubro, array('aviso_galpon_deposito'))){ ?>
      <!-- Ancho entrada -->
      <div class="campo"><label>Ancho de la Entrada: </label><input name="field_aviso_ancho_entrada" type="text" value="" /></div>
      <!-- Alto entrada -->
      <div class="campo"><label>Alto de la Entrada: </label><input name="field_aviso_alto_entrada" type="text" value="" /></div>
      <!-- Cantidad columnas -->
      <div class="campo"><label>Cantidad de Columnas: </label><input name="field_aviso_cantidad_columnas" type="text" value="" /></div>
      <!-- Cantidad naves -->
      <div class="campo"><label>Cantidad de Naves: </label><input name="field_aviso_cantidad_naves" type="text" value="" /></div>
      <!-- Patron municipal -->
      <div class="campo"><label>Patrón de Municipalidad Córdoba: </label><input name="field_aviso_patron_municipalidad" type="text" value="" /></div>
    <?php } ?>
    <?php if(in_array($rubro, array('aviso_terreno_lote'))){ ?>
      <!-- Superficie terreno -->
      <div class="campo"><label>Superficie Terreno: </label><input name="field_aviso_superficie_terreno" type="text" value="" /></div>
      <!-- Metros frente -->
      <div class="campo"><label>Metros de Frente: </label><input name="field_aviso_metros_frente" type="text" value="" /></div>
      <!-- Metros fondo -->
      <div class="campo"><label>Metros de Fondo: </label><input name="field_aviso_metros_fondo" type="text" value="" /></div>
      <!-- Superficie construible -->
      <div class="campo"><label>Superficie Construible: </label><input name="field_aviso_superficie_construib" type="text" value="" /></div>
    <?php } ?>
    <!-- Expensas -->
    <?php if(in_array($rubro, array('aviso_cochera', 'aviso_departamento', 'aviso_local', 'aviso_oficina'))){ ?>
      <div class="campo"><label>Expensas: </label><input name="field_aviso_expensas" type="text" value="" /></div>
    <?php } ?>
    <!-- Ganaderia -->
    <?php if(array_key_exists('field_aviso_ganaderia', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_ganaderia']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_ganaderia']['name']; ?>:</label>
      <?php foreach($options['field_aviso_ganaderia']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_ganaderia']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Agricultura -->
    <?php if(array_key_exists('field_aviso_agricultura', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_agricultura']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_agricultura']['name']; ?>:</label>
      <?php foreach($options['field_aviso_agricultura']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_agricultura']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Casa Principal -->
    <?php if(array_key_exists('field_aviso_casa_principal', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_casa_principal']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_casa_principal']['name']; ?>:</label>
      <?php foreach($options['field_aviso_casa_principal']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_casa_principal']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Casa de Casero -->
    <?php if(array_key_exists('field_aviso_casa_casero', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_casa_casero']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_casa_casero']['name']; ?>:</label>
      <?php foreach($options['field_aviso_casa_casero']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_casa_casero']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- En Edificio -->
    <?php if(array_key_exists('field_aviso_en_edificio', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_en_edificio']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_en_edificio']['name']; ?>:</label>
      <?php foreach($options['field_aviso_en_edificio']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_en_edificio']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Galeria o Shopping -->
    <?php if(array_key_exists('field_aviso_galeria_shopping', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_galeria_shopping']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_galeria_shopping']['name']; ?>:</label>
      <?php foreach($options['field_aviso_galeria_shopping']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_galeria_shopping']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Vigilania -->
    <?php if(array_key_exists('field_aviso_vigilancia', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_vigilancia']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_vigilancia']['name']; ?>:</label>
      <?php foreach($options['field_aviso_vigilancia']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_vigilancia']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Baulera -->
    <?php if(array_key_exists('field_aviso_baulera', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_baulera']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_baulera']['name']; ?>:</label>
      <?php foreach($options['field_aviso_baulera']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_baulera']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Propiedad Ocupada -->
    <?php if(array_key_exists('field_aviso_propiedad_ocupada', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_propiedad_ocupada']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_propiedad_ocupada']['name']; ?>:</label>
      <?php foreach($options['field_aviso_propiedad_ocupada']['opciones'] as $option){ ?>
        <input type="checkbox" name="<?php print $options['field_aviso_propiedad_ocupada']['key']; ?>" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>      
    <?php } ?>
    <!-- Ultima actividad -->
    <?php if(in_array($rubro, array('aviso_local'))){ ?>
      <div class="campo"><label>Última Actividad: </label><input name="field_aviso_ultima_actividad" type="text" value="" /></div>
    <?php } ?>
    <!-- Tipo Edificio -->
    <?php if(array_key_exists('field_aviso_tipo_edificio', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_edificio']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_edificio']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_edificio']['key']; ?>">
        <?php if($options['field_aviso_tipo_edificio']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_edificio']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Condicion inmueble -->
    <?php if(array_key_exists('field_aviso_condicion', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_condicion']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_condicion']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_condicion']['key']; ?>">
        <?php if($options['field_aviso_condicion']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_condicion']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Deptos por piso -->
    <?php if(in_array($rubro, array('aviso_departamento'))){ ?>
      <div class="campo"><label>Departamentos por Piso: </label><input name="field_aviso_dpto_piso" type="text" value="" /></div>
    <?php } ?>
    <!-- Cantidad pisos -->
    <?php if(in_array($rubro, array('aviso_departamento'))){ ?>
      <div class="campo"><label>Cantidad de Pisos: </label><input name="field_aviso_cantidad_pisos" type="text" value="" /></div>
    <?php } ?>
    <!-- Tipo plan -->
    <?php if(array_key_exists('field_aviso_tipo_plan', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_tipo_plan']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_tipo_plan']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_tipo_plan']['key']; ?>">
        <?php if($options['field_aviso_tipo_plan']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_tipo_plan']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <?php if(in_array($rubro, array('aviso_plan_ahorro'))){ ?>
      <!-- Valor cuota -->
      <div class="campo"><label>Valor Cuota: </label><input name="field_aviso_valor_cuota" type="text" value="" /><br>
        <span class="detalle">Valor de la Cuota en Pesos.</span>
      </div>
      <!-- Valor cuota promedio -->
      <div class="campo"><label>Valor Cuota promedio: </label><input name="field_aviso_valor_cuota_promedio" type="text" value="" /><br>
        <span class="detalle">Valor de la Cuota promedio en Pesos.</span>
      </div>
      <!-- Valor cuota suscripcion -->
      <div class="campo"><label>Valor cuota suscripción: </label><input name="field_aviso_valor_cuota_suscrip" type="text" value="" /><br>
        <span class="detalle">Valor de la Cuota suscripción en Pesos.</span>
      </div>
    <?php } ?>
    <!-- Segmento Auto y plan -->
    <?php if(array_key_exists('segmento_autos', $options)) { ?>
      <div class="campo"><label><?php if($options['segmento_autos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['segmento_autos']['name']; ?>:</label>
      <select name="<?php print $options['segmento_autos']['key']; ?>">
        <?php if($options['segmento_autos']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['segmento_autos']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Version -->
    <?php if(in_array($rubro, array('aviso_4x4', 'aviso_auto', 'aviso_plan_ahorro', 'aviso_utilitario'))){ ?>
      <div class="campo"><label>Versión: </label><input name="field_aviso_version" type="text" value="" /></div>
    <?php } ?>
    <!-- Segmento moto -->
    <?php if(array_key_exists('segmento_motos', $options)) { ?>
      <div class="campo"><label><?php if($options['segmento_motos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['segmento_motos']['name']; ?>:</label>
      <select name="<?php print $options['segmento_motos']['key']; ?>">
        <?php if($options['segmento_motos']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['segmento_motos']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Cilindradas -->
    <?php if(in_array($rubro, array('aviso_moto'))){ ?>
      <div class="campo"><label>Cilindrada: </label><input name="field_aviso_cilindrada" type="text" value="" /></div>
    <?php } ?>
    <!-- Color -->
    <?php if(array_key_exists('color', $options)) { ?>
      <div class="campo"><label><?php if($options['color']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['color']['name']; ?>:</label>
      <select name="<?php print $options['color']['key']; ?>">
        <?php if($options['color']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['color']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Transmision -->
    <?php if(array_key_exists('field_aviso_transmision', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_transmision']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_transmision']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_transmision']['key']; ?>">
        <?php if($options['field_aviso_transmision']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_transmision']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Cantidad de puertas -->
    <?php if(array_key_exists('field_aviso_cant_puertas', $options)) { ?>
      <div class="campo"><label><?php if($options['field_aviso_cant_puertas']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['field_aviso_cant_puertas']['name']; ?>:</label>
      <select name="<?php print $options['field_aviso_cant_puertas']['key']; ?>">
        <?php if($options['field_aviso_cant_puertas']['requerido'] == 0) { ?>
          <option value=""></option>
        <?php } ?>
        <?php foreach($options['field_aviso_cant_puertas']['opciones'] as $option){ ?>
          <option value="<?php print $option['key']; ?>"><?php print $option['name']; ?></option>
        <?php } ?>
      </select></div>
    <?php } ?>
    <!-- Foto -->
    <div class="campo"><label><strong>Fotos: </strong></label><br>
      <label>Foto 1: </label><input type="file" name="field_aviso_foto" ><br>
      <label>Foto 2: </label><input type="file" name="field_aviso_foto1" ><br>
      <span class="detalle">Extensiones permitidas: gif, png, jpg, jpeg. Tamaño máximo 300kb.</span>
    </div>
    <!-- Video -->
    <div class="campo"><label>Video: </label><input name="field_aviso_video" type="text" value="" /><br>
      <span class="detalle">Solo url de youtube.</span>
    </div>
    <!-- Ambientes Casa -->
    <?php if(array_key_exists('ambientes_casas', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['ambientes_casas']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['ambientes_casas']['name']; ?>:</strong></label><br />
      <?php foreach($options['ambientes_casas']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['ambientes_casas']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Ambientes depto -->
    <?php if(array_key_exists('ambientes_departamentos', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['ambientes_departamentos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['ambientes_departamentos']['name']; ?>:</strong></label><br />
      <?php foreach($options['ambientes_departamentos']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['ambientes_departamentos']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Ambientes local -->
    <?php if(array_key_exists('ambientes_locales', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['ambientes_locales']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['ambientes_locales']['name']; ?>:</strong></label><br />
      <?php foreach($options['ambientes_locales']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['ambientes_locales']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Ambientes oficina -->
    <?php if(array_key_exists('ambientes_oficinas', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['ambientes_oficinas']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['ambientes_oficinas']['name']; ?>:</strong></label><br />
      <?php foreach($options['ambientes_oficinas']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['ambientes_oficinas']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Instalaciones Casa y Depto -->
    <?php if(array_key_exists('instalaciones_casas_y_departamentos', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['instalaciones_casas_y_departamentos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['instalaciones_casas_y_departamentos']['name']; ?>:</strong></label><br />
      <?php foreach($options['instalaciones_casas_y_departamentos']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_casas_y_departamentos']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Instalaciones Campo -->
    <?php if(array_key_exists('instalaciones_campos', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['instalaciones_campos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['instalaciones_campos']['name']; ?>:</strong></label><br />
      <?php foreach($options['instalaciones_campos']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_campos']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Instalaciones galpon -->
    <?php if(array_key_exists('instalaciones_galpones_y_depositos', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['instalaciones_galpones_y_depositos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['instalaciones_galpones_y_depositos']['name']; ?>:</strong></label><br />
      <?php foreach($options['instalaciones_galpones_y_depositos']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_galpones_y_depositos']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Instalaciones local -->
    <?php if(array_key_exists('instalaciones_locales', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['instalaciones_locales']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['instalaciones_locales']['name']; ?>:</strong></label><br />
      <?php foreach($options['instalaciones_locales']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_locales']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Instalaciones oficina -->
    <?php if(array_key_exists('instalaciones_oficinas', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['instalaciones_oficinas']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['instalaciones_oficinas']['name']; ?>:</strong></label><br />
      <?php foreach($options['instalaciones_oficinas']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_oficinas']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Amenities Depto -->
    <?php if(array_key_exists('amenities_departamentos', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['amenities_departamentos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['amenities_departamentos']['name']; ?>:</strong></label><br />
      <?php foreach($options['amenities_departamentos']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['amenities_departamentos']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Servicios Casa -->
    <?php if(array_key_exists('servicios_casas', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['servicios_casas']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['servicios_casas']['name']; ?>:</strong></label><br />
      <?php foreach($options['servicios_casas']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_casas']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Servicios Campo -->
    <?php if(array_key_exists('servicios_campos', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['servicios_campos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['servicios_campos']['name']; ?>:</strong></label><br />
      <?php foreach($options['servicios_campos']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_campos']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Servicios Depto -->
    <?php if(array_key_exists('servicios_departamentos', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['servicios_departamentos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['servicios_departamentos']['name']; ?>:</strong></label><br />
      <?php foreach($options['servicios_departamentos']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_departamentos']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Servicios galpon -->
    <?php if(array_key_exists('servicios_galpones_depositos', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['servicios_galpones_depositos']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['servicios_galpones_depositos']['name']; ?>:</strong></label><br />
      <?php foreach($options['servicios_galpones_depositos']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_galpones_depositos']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Servicios local -->
    <?php if(array_key_exists('servicios_locales', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['servicios_locales']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['servicios_locales']['name']; ?>:</strong></label><br />
      <?php foreach($options['servicios_locales']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_locales']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Servicios oficina -->
    <?php if(array_key_exists('servicios_oficinas', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['servicios_oficinas']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['servicios_oficinas']['name']; ?>:</strong></label><br />
      <?php foreach($options['servicios_oficinas']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_oficinas']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Confort -->
    <?php if(array_key_exists('confort', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['confort']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['confort']['name']; ?>:</strong></label><br />
      <?php foreach($options['confort']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['confort']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Seguridad -->
    <?php if(array_key_exists('seguridad', $options)) { ?>
      <div class="campo"><label><strong><?php if($options['seguridad']['requerido'] == 1){ print '<span style="color:red;">*</span> '; } print $options['seguridad']['name']; ?>:</strong></label><br />
      <?php foreach($options['seguridad']['opciones'] as $option){ ?>
        <label><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['seguridad']['key']; ?>[<?php print $option['key']; ?>]" value="<?php print $option['key']; ?>" />
      <?php } ?>
      </div>
    <?php } ?>
    <!-- Terminos y condicioens -->
    <div class="campo"><label><span style="color:red;">*</span>Acepto las condiciones de utilización del servicio:</label>
      <input type="checkbox" name="field_aviso_terminos" value="1" checked="checked" />
    </div>
    <!-- Recibir mails -->
    <div class="campo"><label>Aceptación recepción de ofertas y promociones:</label>
      <input type="checkbox" name="field_aviso_promociones" value="1" checked="checked" />
    </div>
    
    <input id="submit-contacto" value="Crear Aviso" type="submit"/>
  </form>
<?php } ?>
</body>