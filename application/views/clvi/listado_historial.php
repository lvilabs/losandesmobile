<div id="content">

<?php if($listado->total == 0){ ?>
<!-- sin resultados -->
<section class="noticias">
  <div class="noticias sinResultados">
    <div class="fondoResaltado">
      <h2>No tienes avisos en tu historial</h2>
      <p>Navega por las fichas de tus avisos preferidos.</p>
    </div>
  </div>
</section>
<!-- fin resultados -->
<?php } else { ?>

<div>
<?php  
  $count = 1;
  $time_actual = time();
  
  $historial = array_reverse($historial);
  // Quitar duplicados
  $id_historial = array();
  foreach($historial as $id) {
    if(!in_array($id[0], $id_historial))
      array_push($id_historial, $id[0]);  
  }
  
  foreach($id_historial as $id){
    
    $item = array();
    foreach($listado as $listKey => $listId) {
      if($listId->nid == $id)
        $item = $listado->$listKey;
    }
    
    if(isset($item->nid)){
      
      $texto_time = '';
      $fecha_visto = 0;
      foreach($historial as $item_historial) {
        if($item->nid == $item_historial[0]) {
          $time_visto = substr($item_historial[1], 0, 10);
          //$time_visto = str_replace('.', '', substr($item_historial[1], 0, 11));
        }
      }
      $intervalo = round(($time_actual - $time_visto) / 60);
      if($intervalo >= 60) {
        $intervalo_hora = floor($intervalo / 60);
        if($intervalo_hora >= 24) {
          $intervalo_dias = floor($intervalo_hora / 24);
          $intervalo_hora = floor($intervalo_hora - ($intervalo_dias * 24));
          $horas = '';
          if($intervalo_hora >= 1)
            $horas = ' '.$intervalo_hora.' h';
          if($intervalo_dias == 1) 
            $texto_time = 'Hace '.$intervalo_hora.' día'.$horas;
          else
            $texto_time = 'Hace '.$intervalo_hora.' dias'.$horas;
        } else {
          $intervalo_minutos = floor($intervalo - ($intervalo_hora * 60));
          $minutos = '';
          if($intervalo_minutos >= 1)
            $minutos = ' '.$intervalo_minutos.' min';
          if($intervalo_hora == 1) 
            $texto_time = 'Hace '.$intervalo_hora.' hora'.$minutos;
          else
            $texto_time = 'Hace '.$intervalo_hora.' horas'.$minutos;
        }
      } else {
        if($intervalo == 1)
          $texto_time = 'Hace '.$intervalo.' minuto';
        else
          $texto_time = 'Hace '.$intervalo.' minutos';
      }
      
      switch($item->type){
        case 'Motos, cuadriciclos y náutica':
          $item->type = 'Motos';
          break;
        case '4x4, 4x2 y todo terreno':
          $item->type = '4x4';
          break;
        case 'Utilitarios, pick-up y camiones':
          $item->type = 'Utilitarios';
          break;
        case 'Accesorios y Repuestos':
          $item->type = 'Repuestos';
          break;
      }
?>
  <div class="search-main">
    <div class="search-item" id="favoritos-<?php echo $item->nid ?>">
      <a href="/<?php echo $item->url_amigable ?>" alt="<?php echo $item->title ?>">  
        <div class="sc-img">
          <?php if($item->type == 'Productos') { ?>
            <img alt="<?php echo $item->subrubro.' '.$item->title.' a '.$item->field_aviso_moneda_value.$item->field_aviso_precio_value; ?>" src="<?php echo urldecode($item->filepath_imagen_producto_sc); ?>" />
          <?php } else { ?>
            <img alt="<?php echo $item->subrubro.' '.$item->title.' a '.$item->field_aviso_moneda_value.$item->field_aviso_precio_value; ?>" src="<?php echo urldecode($item->filepath_app); ?>" />
          <?php } ?>
          <div class="sc-price">
            <div><?php if($item->field_aviso_moneda_value != '') echo $item->field_aviso_moneda_value; else echo $item->field_aviso_precio_value; ?><?php if($item->field_aviso_precio_value != 'consultar') echo $item->field_aviso_precio_value; ?></div>
            
            <?php if(isset($item->precio_uva) && $item->precio_uva) { ?>
              <div class="search-precio-uva <?php print $item->nid; ?>" data-precio="<?php print str_replace('.', '', $item->field_aviso_precio_value); ?>" data-moneda="<?php print $item->field_aviso_moneda_value; ?>" data-aviso="<?php print $item->nid; ?>"></div>
            <?php } ?>
            
            <?php if($item->disponible_venta) { ?>
              <p class="mensajePago">Precio en 1 pago</p>
            <?php } ?>
          </div>
        </div>
        <div class="sc-content">
          <?php if($count <= 4) { ?>
            <h2 class="title-result"><?php echo $item->title; ?></h2>
          <?php } elseif($count <= 8) { ?>
            <h3 class="title-result"><?php echo $item->title; ?></h3>
          <?php } else { ?>
            <h4 class="title-result"><?php echo $item->title; ?></h4>
          <?php } ?>
          <div class="sc-features">
            <?php if(!empty($item->descuento)) { ?>
              <span class="descuento"><?php print '<strong>'.$item->descuento->porcentaje.'%</strong> OFF'; ?></span>
            <?php } ?>
            <?php if($item->disponible_venta) { ?>
              <a title="Comprar el producto" href="/<?php echo $item->url_amigable ?>#modal-compra-datos" class="btn-comprar"><span></span>Comprar</a>
            <?php } else { ?>
              <p class="sizemed"><a href="/search/result/<?php echo str_replace('"', '%22', $item->filtro_subrubro); ?>"><i class="fa fa-circle"></i><?php echo $item->type.' - '.$item->subrubro ?></a></p>
            <?php } ?>
            <div class="tiempo">
              <span class="fa"><i class="fas fa-history"></i></span>
              <?php print $texto_time; ?>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
<?php
    }
    $count++;
  }
?>
</div>
<?php
  $aviso_pagina = $listado->page * $listado->page_result;
  if($aviso_pagina < $listado->total){
    $url_explode = explode('/', $_SERVER['REQUEST_URI']);
    $url_implode = '';
    $pagina_actual = 1;
    foreach($url_explode as $url){
      if(strpos($url,'page=')===false && $url != '')
        $url_implode .= '/'.$url;
      if(strpos($url,'page=')!==false) {
        $page_explode = explode('page=', $url);
        $pagina_actual = $page_explode[1];
      }
    }  
    $url_implode = str_replace('/administrar/favoritos', '', $url_implode);
?>
  <input type="hidden" id="filtros" name="filtros" value="<?php print $url_implode; ?>" />
  <input type="hidden" id="pagina_inicio" name="pagina_inicio" value="<?php print $pagina_actual; ?>" />
  <input type="hidden" id="pagina_fin" name="pagina_fin" value="<?php print $pagina_actual; ?>" />
  <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
  </div>
  <a href="#"” class="back-to-top">
    <i class="fa fa-arrow-circle-up"></i>
  </a>
<?php
  }
}
?>
</div>