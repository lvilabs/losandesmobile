<div id="content" class="admin">
<div class="container">
  <div class="row" id="consultas">
    <div class="twelvecol">
<?php
$estados = array(0=>'', 1=>'No leído', 2=>'Leído');
foreach($contactos as $key => $item) {
?>
  <article class="aviso clearfix">
    <div class="thumb">
      <img src="<?php echo urldecode($this->config->item('static_sitio').'/'.$item->aviso->foto->filepath); ?>" alt="<?php echo $item->nombre; ?>" />
    </div>
    <div class="datos_consulta">
      <h2><?php echo $item->nombre; ?></h2>
      <div class="impar">Fecha: <?php echo $item->email; ?></div>
      <div class="par">Estado: <?php echo $estados[$item->estado]; ?></div>
      <div class="impar">Teléfono: <?php echo $item->telefono; ?></div>
      <div class="par">Email: <?php echo $item->email; ?></div>
    </div>
    <div class="texto_consulta">
      <span><?php echo $item->body; ?></span>
    </div>
  </article>
<?php
}
?>
    </div>
  </div>
  <nav id="menu_consulta" class="main menu_consulta" style="display: none; ">
    <ul class="sub">
	   <li><a onclick="toggle_menu('menu_consulta');" href="/consultas/leido">Leído</a></li>
	   <li><a onclick="toggle_menu('menu_consulta');" href="/consultas/noleido">No Leído</a></li>
     <li><a onclick="toggle_menu('menu_consulta');" href="javascript:void(0);">Volver</a></li>
	  </ul>
  </nav>
<script type="text/javascript">function toggle_menu(id, nid) {
       var e = document.getElementById(id);
       var div_avisos = document.getElementById('avisos');
       if(e.style.display == 'block') {
          e.style.display = 'none';
          div_avisos.style.display = 'block';
       } else {
          e.style.display = 'block';
          div_avisos.style.display = 'none';
       }
    }
</script>
</div>

</div>