<div id="content">
	<article class="aviso">
    <div class="container">
      <div class="row"> 
        <div class="cajaTitulo">
          <div class="titulo"><h1>Contáctanos</h1></div>  
        </div>
        
        <?php if(!empty($mensaje)) : ?>
          <?php if(isset($mensaje->status)) : ?>
          <div class="twelvecol mensaje clear">
            <span class="<?php echo $mensaje->status; ?>"><?php echo $mensaje->mensaje; ?></span>
          </div>
          <?php else : ?>
          <div class="twelvecol mensaje clear">
            <span class="error"><?php echo $mensaje; ?></span>
          </div>
          <?php endif; ?>
        <?php endif; ?>
        
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="twelvecol">
          <div class="content cuerpo">
            <p>Puedes enviar un mensaje usando el formulario de contacto de abajo.</p>
            <a name="formulario_contacto"></a>  
            <form name="formContacto" id="formContact" class="formContacto" action="/page/send_contact" method="post">
              <div class="campo"><input name="contacto_nombre" type="text" placeholder="Tu nombre" required></div>
              <div class="campo"><input name="contacto_telefono" type="text" placeholder="Tu teléfono" ></div>
              <div class="campo"><input name="contacto_mail" type="email" placeholder="Tu E-mail" required></div>
              <div class="campo"><input name="contacto_asunto" type="text" placeholder="Asunto" required></div>
              <div class="campo"><textarea name="contacto_consulta" rows="5" placeholder="Escriba su consulta aquí" required></textarea></div>
              <div class="g-recaptcha" id="RecaptchaField"></div>
              <input id="submit-contacto" name="" type="submit"/>
            </form>
          </div>  
        </div>
      </div>
    </div>
  </article>
</div>