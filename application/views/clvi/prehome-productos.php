<div id="content" class="page-productos">
  <h1 class="DN">Ofertas de productos en Córdoba</h1>
<div class="container">
  <div class="row">
    <div class="cabecera">
      <?php
      if(isset($imagen_head) && !empty($imagen_head)) {
      ?>
      <!-- <img alt="Promociones de productos en Clasificados La Voz" src="<?php print $this->config->item('static_sitio').$imagen_head; ?>"> -->
      <?php
      }
      ?>
    </div>
  </div>
</div>

<!-- 
<section class="noticias">
<div class="container">
  <div class="row">
    <section><h2 class="">Comercios oficiales</h2></section>
  </div>
</div>

<div id="slider-fotos" class="swipe multimedia-ficha" style="max-width:230px;">
  <div class='swipe-wrap'>
    <?php
      shuffle($tiendas);
      foreach($tiendas as $key => $item) {
    ?>
    <div><a href="/search/result/filters=is_uid:<?php echo $item->uid; ?>%20" rel="search"><img alt="Logo de comercio <?php echo $item->nombre; ?>" title="<?php echo $item->nombre; ?>" src="<?php echo urldecode($this->config->item('sitio_origen').$item->logo) ?>" width="100%" height="80"/></a></div>
    <?php } ?>
  </div>
</div>
<div id="content-slider-fotos" class="content-slider tiendas" >
  <button class="flickity-prev-next-button previous" onclick="slider.prev();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(15,0)"></path></svg></button>
  <button class="flickity-prev-next-button next" onclick="slider.next();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(85,100) rotate(180)"></path></svg></button>
</div>
<div class="">
  <a href="tiendas_lavoz" rel="search">Ver todos los comercios</a>
</div>
</section>
-->

<section class="noticias">
  <div class="container">
    <div class="row">
      <section><h2 class="">Productos Destacados</h2></section>
    </div>
  </div>
  <div id="slider-fotos" class="swipe multimedia-ficha" style="max-width:230px;">
    <div class='swipe-wrap'>
      <?php foreach($destacados as $item) { ?>
        <div class="CajaAviso">
          <div class="imgAviso">
            <a href="<?php echo $item->url; ?>" title="<?php echo $item->titulo; ?>">
              <span></span>
              <img src="<?php echo urldecode($item->foto); ?>" alt="<?php echo $item->titulo; ?> en productos La Voz" title="Inagen producto <?php echo $item->titulo; ?>"></a>
          </div>
          <div class="AvisoDescripcion">
            <a href="/avisos/productos/1030170/skin-para-ps4.html" title="<?php echo $item->titulo; ?>"><h4><?php echo $item->titulo; ?></h4></a>
            <div><span class="precio"><?php echo $item->precio; ?></span></div>  
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
  <div id="content-slider-fotos" class="content-slider tiendas" >
    <button class="flickity-prev-next-button previous" onclick="slider.prev();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(15,0)"></path></svg></button>
    <button class="flickity-prev-next-button next" onclick="slider.next();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(85,100) rotate(180)"></path></svg></button>
  </div>
</section>

</div>
<script type="text/javascript" src="/public/js/swipe.js"></script>
<script>
window.slider = new Swipe(document.getElementById('slider-fotos'),{continuous: true});
</script>