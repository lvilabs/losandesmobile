<div id="content" class="admin">
<div class="container">
  <div class="row" id="dineromail">
    <?php if(!empty($respuesta->mensaje)) : ?>
    <div class="twelvecol mensaje clear">
      <span class="error"><?php echo $respuesta->mensaje; ?></span>
    </div>
    <?php endif; ?>
    <div class="twelvecol">
    <?php if($respuesta->status!=1) : ?>
      <form action="/administrar/dineromail_confirmar/<?php echo $respuesta->compra->id_cd; ?>" method="POST">
      <input type="submit" class="form-submit" value="Reintentar" id="edit-submit" name="op">
      <input type="hidden" name="compra_id" value="<?php echo $respuesta->compra->id_cd; ?>">
      </form>
    <?php else: ?>
      Ahora ingresará a la plataforma de pago seguro DineroMail, donde deberá seleccionar la forma de pago para el anuncio.<br>
      Hasta que el pago sea confirmado y acreditado por Dineromail, su aviso será publicado como aviso gratuito.
      El plazo de duración del destaque o mejora contratado comenzará a contar desde el momento de acreditación del pago.
      <form action="/administrar/dineromail_form" method="POST">
      <input type="submit" class="form-submit" value="Aceptar" id="edit-submit" name="op">
      <input type="hidden" name="compra_id" value="<?php echo $respuesta->compra->id_cd; ?>">
      </form>
    <?php endif; ?>
    </div>
  </div>
</div>
</div>