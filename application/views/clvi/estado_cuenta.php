<div id="content" class="admin estado_cuenta">
<div class="container">
  <div class="row" id="espacios">
    <?php if(!empty($mensaje)) : ?>
    <div class="twelvecol mensaje clear">
      <span class="error"><?php echo $mensaje; ?></span>
    </div>
    <?php endif; ?>
    <div class="twelvecol">
<?php
  $espacios = json_decode(json_encode($espacios), true);
  $show_espacios_reusables = json_decode(json_encode($show_espacios_reusables), true);
  $espacios_detalle = json_decode(json_encode($espacios_detalle), true);//print_r($espacios_detalle);
  $rubros = json_decode(json_encode($rubros), true);
  $creditos_papel_concesionaria = json_decode(json_encode($creditos_papel_concesionaria), true);
  $creditos_papel_inmobiliaria = json_decode(json_encode($creditos_papel_inmobiliaria), true);
  $compras = json_decode(json_encode($compras), true);
  $i = 1;
  $j = 1;
?>
      <div class="bloque-left clearfix">
        <div><h2 class="titulo">Espacios disponibles</h2></div>
  <?php if(count($show_espacios) == 0): ?>
        <div class="row odd clearfix">
          <div>No tiene espacios asignados.</div>
        </div>
  <?php else: ?>
        <div class="row TitEstado even clearfix">
          <div class="espacios header-left espacios-nombre">Tipo</div>
          <div class="espacios header espacios-disponible">Disponibles</div>
          <div class="espacios header espacios-disponible"><?php (empty($espacios_reusables))? print 'Usados' : print 'Aplicados'; ?></div>
          <div class="espacios header espacios-total">Total</div>
        </div>
  <?php endif; ?>
  <?php foreach($show_espacios as $espacio_nid => $label): ?>
        <div class="row <?php print ($i%2) ? 'even' : 'odd'; $i++; ?> ver-mas clearfix" data-detalle-id="<?php print 'detalle-' . $espacio_nid; ?>">
          <div class="espacios header-left espacios-nombre"><?php print $label; ?></div>
          <?php
            $cantidad = $espacios[$espacio_nid]['cantidad'];
            $usados = $espacios[$espacio_nid]['usados'];
            $usado_dia = 0;
            if(is_numeric($show_espacios_reusables[$espacio_nid]['disponibles']) && (int)$show_espacios_reusables[$espacio_nid]['disponibles'] < (int)$cantidad) {
              $usado_dia = 1;
              $cantidad = '<p class="msg-espacio">'.$show_espacios_reusables[$espacio_nid]['disponibles'].'</p>';
              if($show_espacios_reusables[$espacio_nid]['disponibles'] == 0)
                $usado_dia_mensaje = '* No tienes espacios '.$label.' disponibles ya que superaste las publicaciones permitidas en el día. Debes esperar hasta mañana para poder utilizarlos.';
              else
                $usado_dia_mensaje = '* Los espacios Disponibles corresponden a las publicaciones restantes con el destaque '.$label.' que tenés disponibles en el día de la fecha. Tenés que esperar hasta mañana para poder utilizar todos los espacios de tu cuenta. <a href="#">Más información</a>';
            }
          ?>
          <div class="espacios espacios-disponible"><?php print $cantidad; ?></div>
          <div class="espacios espacios-disponible"><?php print $usados; ?></div>
          <div class="espacios espacios-total"><?php if($espacio_nid == 6) print '-'; else print $espacios[$espacio_nid]['total']; ?></div>
        </div>
        <?php if($usado_dia) : ?>
        <p class="msg-importante"><?php print $usado_dia_mensaje; ?></p>
        <?php endif; ?>
  <?php endforeach; ?>
      </div>
      <div class="bloque-right">
<?php foreach($show_espacios as $espacio_nid => $label): ?>
        <div id="detalle-<?php print $espacio_nid; ?>" class="oculto GloboDetalle">
        <span class="punta"></span>
          <div class="Gris">
            <div><h4>Detalle de <i><?php print $label; ?></i> disponibles</h4></div>
<?php
          //$espacios = _publicacion_avisos_espacios_adquiridos_rubro($usuario_id, $espacio_nid);
          $x = 0;
          $detalles = $espacios_detalle[$espacio_nid];
          foreach($detalles as $item) :
?>
            <div class="<?php print ($x%2==0)?'odd':'even'; ?>">
            <?php
              print 'Disponible '.$item['cantidad'].' de '.$item['total'].' aplicable a avisos de: ';
              $rubro_ids = split(',', $item['rubros']);
              for($i=0; $i<count($rubro_ids); $i++) {

                print ($i!=0)? '; ':' ';
                if(array_key_exists($rubro_ids[$i], $rubros)) {
                  print $rubros[$rubro_ids[$i]]['title'];
                }
              }
              if($item['reusable']==1)
                print ' (reutilizable)';
            ?>
            </div>
<?php
          $x++;
          endforeach;
?>
          </div>
        </div>
<?php endforeach; ?>
      </div>
      
      <?php if(isset($espacios_clin) && !empty($espacios_clin)): ?>
        <div class="bloque-left clearfix">
          <div><h2 class="titulo">Espacios utilizados por avisos creados en Clin CPI</h2></div>
          <p class="msg-importante">Las siguientes cantidades representan a destaques de Clasificados La Voz que se utilizaron sobre avisos creados en el sitio de Clin CPI y figuran como usados en la tabla de Espacios disponibles.</p>
          <div class="row TitEstado even clearfix">
            <div class="espacios header-left compra-detalle">Tipo</div>
            <div class="espacios header espacios-disponible">Cantidad</div>
          </div>
      <?php foreach($espacios_clin as $espacio_nid => $espacio): ?>
          <div class="row <?php print ($i%2) ? 'even' : 'odd'; $i++; ?> ver-mas clearfix" data-detalle-id="<?php print 'detalle-' . $espacio_nid; ?>">
            <div class="espacios header-left compra-detalle"><?php print $espacio->label; ?></div>
            <div class="espacios espacios-disponible"><?php print $espacio->cantidad; ?></div>
          </div>
      <?php endforeach; ?>
        </div>
      <?php endif; ?>
      
      
    </div>
    <?php if(!empty($espacios_reusables)): ?>
    <div class="msg-estado-cuenta">
      <h3>¿Cómo leer el nuevo estado de cuentas?</h3>
      <p>* La diferencia corresponde a las publicaciones permitidas de espacios en el día. Debes esperar hasta mañana para poder utilizar todos.</p>
    </div>
    <?php endif; ?>
  </div>
  <div class="row" id="creditos">
  <?php
    if(!empty($creditos_papel_concesionaria) || !empty($creditos_papel_inmobiliaria) || !empty($creditos_papel_comercio)) :
  ?>
    <div class="separador"></div>
    <div class="bloque-left clearfix webpapel">
      <div class="">
        <h2>Mejoras (Créditos Web Papel)</h2>
      </div>
      <div class="row TitEstado even clearfix">
        <div class="espacios header-left espacios-nombre">Tipo</div>
        <div class="espacios header espacios-disponible">Disponibles</div>
        <div class="espacios header espacios-usados-mes">Usados Mes Actual</div>
      </div>
      <?php if(!empty($creditos_papel_concesionaria)): ?>
      <div class="row clearfix">
        <div class="espacios header-left espacios-nombre">Aviso Papel Concesionaria</div>
        <div class="espacios espacios-disponible"><?php print $creditos_papel_concesionaria[0]->cantidad; ?></div>
        <div class="espacios espacios-usados-mes"><?php print $creditos_papel_concesionaria[0]->usados_mes; ?></div>
      </div>
      <?php endif; ?>
      <?php if(!empty($creditos_papel_inmobiliaria)): ?>
      <div class="row clearfix">
        <div class="espacios header-left espacios-nombre">Aviso Papel Inmobiliaria</div>
        <div class="espacios espacios-disponible"><?php print $creditos_papel_inmobiliaria[0]->cantidad; ?></div>
        <div class="espacios espacios-usados-mes"><?php print $creditos_papel_inmobiliaria[0]->usados_mes; ?></div>
      </div>
      <?php endif; ?>
      <?php if(!empty($creditos_papel_comercio)): ?>
      <div class="row clearfix">
        <div class="espacios header-left espacios-nombre">Aviso Papel Comercio</div>
        <div class="espacios espacios-disponible"><?php print $creditos_papel_comercio[0]->cantidad; ?></div>
        <div class="espacios espacios-usados-mes"><?php print $creditos_papel_comercio[0]->usados_mes; ?></div>
      </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
  </div>
  <div class="row" id="compras">
    <div class="twelvecol">
      <h2>Compras realizadas</h2>
      <div class="desc">
        <span class="aplicado"></span>Pagado.
      </div>
      <div class="desc clear">
        <span class="espera"></span>En espera de pago.
      </div>
      <div class="BoxComprasRealizadas">
        <div class="clear">
          <div class="compra header compra-fecha">Fecha</div>
          <div class="compra header compra-detalle">Detalle</div>
          <div class="compra header compra-total">Precio</div>
          <?php
          $i=0;
          foreach($compras as $compra) {
          ?>
          <div class="row-compra clear">
            <div class="compra compra-fecha">
            <?php
              print $compra['clasificados_compra_compra_fecha'];
            ?>
            </div>
            <div class="compra compra-detalle">
              <?php print $compra['detalle']; ?>
            </div>
            <div class="compra compra-total">$<?php print $compra['clasificados_compra_compra_total']; ?></div>
          </div>
          <?php 
            $i++;
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>