<div id="content">
  
  <div class="aviso users-forms">
    <div class="cajaTitulo">
      <div class="titulo">
        <h1>Recuperar clave</h1>
      </div>
    </div>
  
    <div class="message">
      <?php echo validation_errors(); ?>
      <?php if(!empty($mensaje)) echo $mensaje; ?>
    </div>

    <div class="content cuerpo">
      <form name="formLogin" class="formAviso login" action="/user/send_password" method="post">
        <hr />
        <input type="hidden" name="token" value="<?php print $token; ?>" />
        <div class="campo"><input type="email" name="usermail" placeholder="E-mail" value="" required></div>
        <input type="submit" name="submit" value="Recuperar clave" class="boton" />
      </form>
    </div>
  </div>

</div>