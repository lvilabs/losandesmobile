<div id="content">
<header class="barraInfo">
<?php if($key != ''){ ?>
  <span class="filtro map"><a id="link-map" href="/search/map/<?php print $key; ?>/<?php print $parametros_filtro; ?>" rel="search">Map</a></span>
<?php } else { ?>
  <span class="filtro map"><a id="link-map" href="/search/map/<?php print $parametros_filtro; ?>" rel="search">Map</a></span>
<?php } ?>
  <span class="filtro"><a href="/search/result/<?php print $key; ?>/<?php print $parametros_filtro; ?>" rel="search">listado</a></span>
</header>

<?php if(!empty($filtros)){ ?>
<ul class="filtroActivo">	
<?php foreach($filtros as $filtro){ ?>
  <li><a href="<?php print str_replace(':', '%3A', $filtro['url']); ?>" rel="search"><?php print $filtro['nombre']; ?><span></span></a></li>
<?php   } ?>
</ul>
<?php } ?>

<section class="filtros">
  <form name="formFiltro" class="formFiltro" action="/filtros/submit_form/<?php print $parametros_filtro; ?>" method="post">
  <?php 
    $existe_principal = array_intersect($rubros_principales, array(6323, 6330, 6106, 6000, 6017));
    $existe_secundario = array_intersect($rubros_principales, array(6325, 6329, 6324, 6326, 6328, 6327, 6338, 6331, 6339, 6334, 6332, 6337, 6335, 6336, 6333));     
    $existe_modelo = array_intersect($rubros_principales, array(6324, 6325, 6328, 6327)); 
    $existe_subrubro = array_intersect($rubros_principales, array(6329, 6106, 6000, 6017));
    $existe_tipo_vehiculo = array_intersect($rubros_principales, array(6323));
    $existe_inmueble = array_intersect($rubros_principales, array(6330));
    $inmuebles_tipo = array(6331, 6334, 6333);
  ?>
  
  <nav id="main-sub" class="main-sub">
    
    <?php if(!empty($key)){ ?>
      <input type="hidden" name="key" value="<?php print $key.'/'; ?>"/>
    <?php } ?>
    
    <?php if(!empty($existe_principal)){ ?>
      <input type="hidden" name="raiz" value="<?php print 'tid:'.current($existe_principal); ?>"/>
      <?php if(!empty($existe_secundario)){ ?>
        <input type="hidden" name="sub-raiz" value="<?php print 'tid:'.current($existe_secundario); ?>"/>
      <?php } ?>
    <?php } ?>
    
    <?php
      foreach($campos_filtros as $id => $data){
        if(in_array($campos_filtros[$id]['label'], array('Tipo de Vendedor', 'Tipo de Barrio', 'Apto Crédito', 'Apto Escritura', 'Tipo de Unidad', 'Cantidad de Dormitorios', 'Estado', 'Tipo de Terreno', 'Tipo de Combustible', 'Subrubro'))) {
          unset($campos_filtros[$id]);
        }
      }
    ?>
    
    <?php
    foreach($campos_filtros as $campo){ ?>
      
      <?php if(!isset($campo['label'])){ ?>
        <a onclick="toggle_menu('subrubro'); ocultar_select('subrubro', '', '');" href="javascript:;" rel="noindex">Subrubro</a>
        <?php 
          $filtro_subrubro = array();
        ?>
        <ul class="sub" id="subrubro" style="display: none;">
        <?php foreach($campos_filtros as $subrubro){ ?>
          <li><a onclick="seleccion_opcion('subrubro', '<?php echo ucwords(mb_strtolower($subrubro['name'])); ?>', '<?php echo str_replace('"', '-', $subrubro['filtro']); ?>', '');" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($subrubro['name'])); ?></a></li>
          <?php
            if(in_array($subrubro['tid'], $filtros_nombres)){
              $filtro_subrubro['name'] = ucwords(mb_strtolower($subrubro['name'])); 
              $filtro_subrubro['filtro'] = $subrubro['filtro']; 
            }
          ?>
        <?php } ?>
        </ul>
        <li><a class="option-select" id="subrubro_select" <?php if(empty($filtro_subrubro)) print 'style="display:none;"'; ?> rel="noindex"><?php if(!empty($filtro_subrubro)) print $filtro_subrubro['name']; ?></a></li>
        <input type="hidden" name="subrubro" id="subrubro_link" value="<?php if(!empty($filtro_subrubro)) print $filtro_subrubro['filtro']; ?>" />
        <?php break; ?>
      <?php } ?>
      
      <?php if(!empty($existe_modelo) && $campo['label'] == 'Marca y Modelo'){ ?>
        <a onclick="toggle_menu('<?php echo texto_a_class($campo['label']); ?>'); ocultar_select('<?php echo texto_a_class($campo['label']); ?>', '', 'modelo');" href="javascript:;" rel="noindex">Marca</a>
      <?php } elseif(!empty($existe_subrubro) && ($campo['label'] == 'Subrubro' || $campo['label'] == 'Rubros')){ ?>
        <a onclick="toggle_menu('<?php echo texto_a_class($campo['label']); ?>'); ocultar_select('<?php echo texto_a_class($campo['label']); ?>', '', 'subsubrubro');" href="javascript:;" rel="noindex"><?php echo ucwords(mb_strtolower($campo['label'])) ?></a>
      <?php } elseif(!empty($existe_tipo_vehiculo) && $campo['label'] == 'Tipo de Vehiculo'){ ?>
        <a onclick="toggle_menu('<?php echo texto_a_class($campo['label']); ?>'); ocultar_select('<?php echo texto_a_class($campo['label']); ?>', '', 'marca');" href="javascript:;" rel="noindex"><?php echo ucwords(mb_strtolower($campo['label'])) ?></a>
      <?php } elseif(!empty($existe_inmueble) && $campo['label'] == 'Tipo de Inmueble'){ ?>
        <a onclick="toggle_menu('<?php echo texto_a_class($campo['label']); ?>'); ocultar_select('<?php echo texto_a_class($campo['label']); ?>', '', 'inmueble');" href="javascript:;" rel="noindex"><?php echo ucwords(mb_strtolower($campo['label'])) ?></a>
      <?php } else { ?>
        <a onclick="toggle_menu('<?php echo texto_a_class($campo['label']); ?>'); ocultar_select('<?php echo texto_a_class($campo['label']); ?>', '', '');" href="javascript:;" rel="noindex"><?php echo ucwords(mb_strtolower($campo['label'])) ?></a>
      <?php } ?>
      <?php 
        $filtro_marca = array();
        $filtro_rubro = array();
        $filtro_opcion = array();
        $filtro_inmueble = array();
      ?>
      <ul class="sub" id="<?php echo texto_a_class($campo['label']); ?>" style="display: none;">
        <?php foreach($campo['child_data'] as $opcion){ ?>
          <?php if(!empty($existe_modelo) && $campo['label'] == 'Marca y Modelo'){ ?>
            <li><a onclick="seleccion_opcion('<?php echo texto_a_class($campo['label']); ?>', '<?php echo ucwords(mb_strtolower($opcion['name'])); ?>', '<?php echo str_replace('"', '-', $opcion['filtro']); ?>', ''); mostrar_modelo(<?php print $opcion['tid']; ?>);" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($opcion['name'])); ?></a></li>
            <?php
              if(in_array($opcion['tid'], $filtros_nombres)){
                $filtro_marca['tid'] = $opcion['tid'];
                $filtro_marca['name'] = ucwords(mb_strtolower($opcion['name'])); 
                $filtro_marca['filtro'] = $opcion['filtro']; 
              }
            ?>
          <?php } elseif(!empty($existe_subrubro) && ($campo['label'] == 'Subrubro' || $campo['label'] == 'Rubros')){ ?>
            <li><a onclick="seleccion_opcion('<?php echo texto_a_class($campo['label']); ?>', '<?php echo ucwords(mb_strtolower($opcion['name'])); ?>', '<?php echo str_replace('"', '-', $opcion['filtro']); ?>', ''); mostrar_subrubro(<?php print $opcion['tid']; ?>);" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($opcion['name'])); ?></a></li>
            <?php
              if(in_array($opcion['tid'], $filtros_nombres)){
                $filtro_rubro['tid'] = $opcion['tid'];
                $filtro_rubro['name'] = ucwords(mb_strtolower($opcion['name'])); 
                $filtro_rubro['filtro'] = $opcion['filtro']; 
              }
            ?>
          <?php } elseif(!empty($existe_tipo_vehiculo) && $campo['label'] == 'Tipo de Vehiculo'){ ?>
            <li><a onclick="seleccion_opcion('<?php echo texto_a_class($campo['label']); ?>', '<?php echo ucwords(mb_strtolower($opcion['name'])); ?>', '<?php echo str_replace('"', '-', $opcion['filtro']); ?>', ''); mostrar_marca(<?php print $opcion['tid']; ?>);" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($opcion['name'])); ?></a></li>
          <?php } elseif(!empty($existe_inmueble) && $campo['label'] == 'Tipo de Inmueble'){ ?>
            <li><a onclick="seleccion_opcion('<?php echo texto_a_class($campo['label']); ?>', '<?php echo ucwords(mb_strtolower($opcion['name'])); ?>', '<?php echo str_replace('"', '-', $opcion['filtro']); ?>', ''); mostrar_inmueble(<?php print $opcion['tid']; ?>);" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($opcion['name'])); ?></a></li>
            <?php
              if(in_array($opcion['tid'], $filtros_nombres)){
                $filtro_inmueble['tid'] = $opcion['tid'];
                $filtro_inmueble['name'] = ucwords(mb_strtolower($opcion['name'])); 
                $filtro_inmueble['filtro'] = $opcion['filtro']; 
              }
            ?>
          <?php } else { ?>
            <li><a onclick="seleccion_opcion('<?php echo texto_a_class($campo['label']); ?>', '<?php echo ucwords(mb_strtolower($opcion['name'])); ?>', '<?php echo str_replace('"', '-', $opcion['filtro']); ?>', '');" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($opcion['name'])); ?></a></li>
            <?php
              if(in_array($opcion['name'], $filtros_nombres) || ($opcion['tid'] != 0 && in_array($opcion['tid'], $filtros_nombres))){
                $filtro_opcion['tid'] = $opcion['tid'];
                $filtro_opcion['name'] = ucwords(mb_strtolower($opcion['name'])); 
                $filtro_opcion['filtro'] = str_replace('"', '-', $opcion['filtro']); 
              }
            ?>
          <?php } ?>
        <?php } ?>
      </ul>
      <li><a class="option-select" id="<?php echo texto_a_class($campo['label']); ?>_select" <?php if(empty($filtro_marca) && empty($filtro_rubro) && empty($filtro_opcion)) print 'style="display:none;"'; ?> rel="noindex"><?php if(!empty($filtro_marca)){print $filtro_marca['name'];}elseif(!empty($filtro_rubro)){print $filtro_rubro['name'];}elseif(!empty($filtro_opcion)){print $filtro_opcion['name'];} ?></a></li>
      <input type="hidden" name="<?php echo texto_a_class($campo['label']); ?>" id="<?php echo texto_a_class($campo['label']); ?>_link" value="<?php if(!empty($filtro_marca)){print $filtro_marca['filtro'];}elseif(!empty($filtro_rubro)){print $filtro_rubro['filtro'];}elseif(!empty($filtro_opcion)){print $filtro_opcion['filtro'];} ?>" />
      
      <!-- Marcas -->
      <?php 
        $filtro_marca_vehiculo = array();
      ?>
      <?php if(!empty($existe_tipo_vehiculo) && trim($campo['label']) == 'Tipo de Vehiculo'){ ?>
        <?php foreach($campo['child_data'] as $vehiculo){ ?>
          <?php if(!empty($vehiculo['child_data'])){ ?>
            <div id="contenedor-marca-<?php print $vehiculo['tid']; ?>" class="marca" <?php if(empty($filtro_marca_vehiculo) || $vehiculo['tid'] != $filtro_marca_vehiculo['tid']) print 'style="display:none;"'; ?>>
              <?php if($vehiculo['name'] == 'Accesorios y Repuestos'){ ?>
                <a onclick="toggle_menu('marca-<?php print $vehiculo['tid']; ?>'); ocultar_select('marca-<?php print $vehiculo['tid']; ?>', 'marca', '');" href="javascript:;" rel="noindex">Subrubro</a>
              <?php } else { ?>
                <a onclick="toggle_menu('marca-<?php print $vehiculo['tid']; ?>'); ocultar_select('marca-<?php print $vehiculo['tid']; ?>', 'marca', '');" href="javascript:;" rel="noindex">Marca</a>
              <?php } ?>
              <ul class="sub" id="marca-<?php print $vehiculo['tid']; ?>" style="display: none;">
              <?php foreach($vehiculo['child_data'][0]['child_data'] as $marca){ ?>
                <li><a onclick="seleccion_opcion('marca-<?php print $vehiculo['tid']; ?>', '<?php echo ucwords(mb_strtolower($marca['name'])); ?>', '<?php echo str_replace('"', '-', $marca['filtro']); ?>', 'marca');" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($marca['name'])); ?></a></li>
                <?php
                  if(in_array($marca['tid'], $filtros_nombres)){
                    $filtro_marca_vehiculo['name'] = ucwords(mb_strtolower($marca['name'])); 
                    $filtro_marca_vehiculo['filtro'] = $marca['filtro']; 
                  }
                ?>
              <?php } ?>
              </ul>
              <li><a class="option-select" id="marca-<?php print $vehiculo['tid']; ?>_select" <?php if(empty($filtro_marca_vehiculo)) print 'style="display:none;"'; ?> rel="noindex"><?php if(!empty($filtro_marca_vehiculo['name'])) print $filtro_marca_vehiculo['name']; ?></a></li>
            </div>
          <?php } ?>
        <?php } ?>
        <input type="hidden" name="marca" id="marca_link" value="<?php if(!empty($filtro_marca_vehiculo['filtro'])) print $filtro_marca_vehiculo['filtro']; ?>" />
      <?php } ?>
      
      <!-- Modelos -->
      <?php 
        $filtro_modelo = array();
      ?>
      <?php if(!empty($existe_modelo) && $campo['label'] == 'Marca y Modelo'){ ?>
        <?php foreach($campo['child_data'] as $marca){ ?>
          <?php if(!empty($marca['child_data'])){ ?>
            <div id="contenedor-modelo-<?php print $marca['tid']; ?>" class="modelo" <?php if(empty($filtro_marca) || $marca['tid'] != $filtro_marca['tid']) print 'style="display:none;"'; ?>>
              <a onclick="toggle_menu('modelo-<?php print $marca['tid']; ?>'); ocultar_select('modelo-<?php print $marca['tid']; ?>', 'modelo', '');" href="javascript:;" rel="noindex">Modelo</a>
              <ul class="sub" id="modelo-<?php print $marca['tid']; ?>" style="display: none;">
              <?php foreach($marca['child_data'] as $modelo){ ?>
                <li><a onclick="seleccion_opcion('modelo-<?php print $marca['tid']; ?>', '<?php echo ucwords(mb_strtolower($modelo['name'])); ?>', '<?php echo str_replace('"', '-', $modelo['filtro']); ?>', 'modelo');" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($modelo['name'])); ?></a></li>
                <?php
                  if(in_array($modelo['tid'], $filtros_nombres)){
                    $filtro_modelo['name'] = ucwords(mb_strtolower($modelo['name'])); 
                    $filtro_modelo['filtro'] = $modelo['filtro']; 
                  }
                ?>
              <?php } ?>
              </ul>
              <li><a class="option-select" id="modelo-<?php print $marca['tid']; ?>_select" <?php if(empty($filtro_modelo)) print 'style="display:none;"'; ?> rel="noindex"><?php if(!empty($filtro_modelo['name'])) print $filtro_modelo['name']; ?></a></li>
            </div>
          <?php } ?>
        <?php } ?>
        <input type="hidden" name="modelo" id="modelo_link" value="<?php if(!empty($filtro_modelo['filtro'])) print $filtro_modelo['filtro']; ?>" />
      <?php } ?>
      
      <!-- Subrubros -->
      <?php 
        $filtro_subrubro = array();
      ?>
      <?php if(!empty($existe_subrubro) && ($campo['label'] == 'Subrubro' || $campo['label'] == 'Rubros')){ ?>
        <?php foreach($campo['child_data'] as $subrubro){ ?>
          <?php if(!empty($subrubro['child_data'])){ ?>
            <div id="contenedor-subsubrubro-<?php print $subrubro['tid']; ?>" class="subsubrubro" <?php if(empty($filtro_rubro) || $subrubro['tid'] != $filtro_rubro['tid']) print 'style="display:none;"'; ?>>
              <a onclick="toggle_menu('subsubrubro-<?php print $subrubro['tid']; ?>'); ocultar_select('subsubrubro-<?php print $subrubro['tid']; ?>', 'subsubrubro', '');" href="javascript:;" rel="noindex">Subrubro</a>
              <ul class="sub" id="subsubrubro-<?php print $subrubro['tid']; ?>" style="display: none;">
              <?php foreach($subrubro['child_data'] as $subsubrubro){ ?>
                <li><a onclick="seleccion_opcion('subsubrubro-<?php print $subrubro['tid']; ?>', '<?php echo ucwords(mb_strtolower($subsubrubro['name'])); ?>', '<?php echo str_replace('"', '-', $subsubrubro['filtro']); ?>', 'subsubrubro');" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($subsubrubro['name'])); ?></a></li>
                <?php
                  if(in_array($subsubrubro['tid'], $filtros_nombres)){
                    $filtro_subrubro['name'] = ucwords(mb_strtolower($subsubrubro['name'])); 
                    $filtro_subrubro['filtro'] = $subsubrubro['filtro']; 
                  }
                ?>
              <?php } ?>
              </ul>
              <li><a class="option-select" id="subsubrubro-<?php print $subrubro['tid']; ?>_select" <?php if(empty($filtro_subrubro)) print 'style="display:none;"'; ?> rel="noindex"><?php print $filtro_subrubro['name']; ?></a></li>
            </div>
          <?php } ?>
        <?php } ?>
        <input type="hidden" name="subsubrubro" id="subsubrubro_link" value="<?php print $filtro_subrubro['filtro']; ?>" />
      <?php } ?>
      
      <!-- Tipo Inmueble -->
      <?php 
        $filtro_tipo_inmueble = array();
      ?>
      <?php if(!empty($existe_inmueble) && $campo['label'] == 'Tipo de Inmueble'){ ?>
        <?php foreach($campo['child_data'] as $inmueble){ ?>
          <?php if(!empty($inmueble['child_data']) && in_array($inmueble['tid'], $inmuebles_tipo)){ ?>
            <div id="contenedor-inmueble-<?php print $inmueble['tid']; ?>" class="inmueble" <?php if(empty($filtro_tipo_inmueble) || $inmueble['tid'] != $filtro_tipo_inmueble['tid']) print 'style="display:none;"'; ?>>
              <a onclick="toggle_menu('inmueble-<?php print $inmueble['tid']; ?>'); ocultar_select('inmueble-<?php print $inmueble['tid']; ?>', 'inmueble', '');" href="javascript:;" rel="noindex">Tipo</a>
              <ul class="sub" id="inmueble-<?php print $inmueble['tid']; ?>" style="display: none;">
              <?php foreach($inmueble['child_data'][1]['child_data'] as $tipo){ ?>
                <li><a onclick="seleccion_opcion('inmueble-<?php print $inmueble['tid']; ?>', '<?php echo ucwords(mb_strtolower($tipo['name'])); ?>', '<?php echo str_replace('"', '-', $tipo['filtro']); ?>', 'inmueble');" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($tipo['name'])); ?></a></li>
                <?php
                  if(in_array($tipo['tid'], $filtros_nombres)){
                    $filtro_tipo_inmueble['name'] = ucwords(mb_strtolower($tipo['name'])); 
                    $filtro_tipo_inmueble['filtro'] = $tipo['filtro']; 
                  }
                ?>
              <?php } ?>
              </ul>
              <li><a class="option-select" id="inmueble-<?php print $inmueble['tid']; ?>_select" <?php if(empty($filtro_tipo_inmueble)) print 'style="display:none;"'; ?> rel="noindex"><?php if(!empty($filtro_tipo_inmueble['name'])) print $filtro_tipo_inmueble['name']; ?></a></li>
            </div>
          <?php } ?>
        <?php } ?>
        <input type="hidden" name="inmueble" id="inmueble_link" value="<?php if(!empty($filtro_tipo_inmueble['filtro'])) print $filtro_tipo_inmueble['filtro']; ?>" />
      <?php } ?>
      
    <?php } ?>
    
    <!-- Provincia -->
    <?php 
      $filtro_provincia = array();
    ?>
    <a onclick="toggle_menu('provincia'); ocultar_select('provincia', '', 'ciudad');" href="javascript:;" rel="noindex">Provincia</a>
    <ul class="sub" id="provincia" style="display: none;">
      <?php foreach($campos_provincia as $opcion){ ?>
        <li><a onclick="seleccion_opcion('provincia', '<?php echo ucwords(mb_strtolower($opcion['name'])); ?>', '<?php echo str_replace('"', '-', $opcion['filtro']); ?>', ''); mostrar_ciudad(<?php print $opcion['tid']; ?>);" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($opcion['name'])); ?></a></li>
        <?php
          if(in_array($opcion['tid'], $filtros_nombres)){
            $filtro_provincia['tid'] = $opcion['tid'];
            $filtro_provincia['name'] = ucwords(mb_strtolower($opcion['name'])); 
            $filtro_provincia['filtro'] = $opcion['filtro']; 
          }
        ?>
      <?php } ?>
    </ul>
    <li><a class="option-select" id="provincia_select" <?php if(empty($filtro_provincia)) print 'style="display:none;"'; ?> rel="noindex"><?php if(!empty($filtro_provincia['name'])) print $filtro_provincia['name']; ?></a></li>
    <input type="hidden" name="provincia" id="provincia_link" value="<?php if(!empty($filtro_provincia['filtro'])) print $filtro_provincia['filtro']; ?>" />
    
    <!-- Ciudad -->
    <?php 
      $filtro_ciudad = array();
    ?>
    <?php foreach($campos_provincia as $provincia){ ?>
    <div id="contenedor-ciudad-<?php print $provincia['tid']; ?>" class="ciudad" <?php if(empty($filtro_provincia) || $provincia['tid'] != $filtro_provincia['tid']) print 'style="display:none;"'; ?>>
      <a onclick="toggle_menu('ciudad-<?php print $provincia['tid']; ?>'); ocultar_select('ciudad-<?php print $provincia['tid']; ?>', 'ciudad', 'barrio');" href="javascript:;" rel="noindex">Ciudad</a>
      <ul class="sub" id="ciudad-<?php print $provincia['tid']; ?>" style="display: none;">
      <?php foreach($provincia['child_data'] as $ciudad){ ?>
        <li><a onclick="seleccion_opcion('ciudad-<?php print $provincia['tid']; ?>', '<?php echo ucwords(mb_strtolower($ciudad['name'])); ?>', '<?php echo str_replace('"', '-', $ciudad['filtro']); ?>', 'ciudad'); mostrar_barrio(<?php print $ciudad['tid']; ?>);" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($ciudad['name'])); ?></a></li>
        <?php
          if(in_array($ciudad['tid'], $filtros_nombres)){
            $filtro_ciudad['tid'] = $ciudad['tid'];
            $filtro_ciudad['name'] = ucwords(mb_strtolower($ciudad['name'])); 
            $filtro_ciudad['filtro'] = $ciudad['filtro']; 
          }
        ?>
      <?php } ?>
      </ul>
      <li><a class="option-select" id="ciudad-<?php print $provincia['tid']; ?>_select" <?php if(empty($filtro_ciudad)) print 'style="display:none;"'; ?> rel="noindex"><?php if(!empty($filtro_ciudad['name'])) print $filtro_ciudad['name']; ?></a></li>
    </div>
    <?php } ?>
    <input type="hidden" name="ciudad" id="ciudad_link" value="<?php if(!empty($filtro_ciudad['filtro'])) print $filtro_ciudad['filtro']; ?>" />
    
    <!-- Barrio -->
    <?php 
      $filtro_barrio = array();
    ?>
    <?php foreach($campos_provincia as $provincia){ ?>
      <?php foreach($provincia['child_data'] as $ciudad){ ?>
        <?php if(!empty($ciudad['child_data'])){ ?>
          <div id="contenedor-barrio-<?php print $ciudad['tid']; ?>" class="barrio" <?php if(empty($filtro_ciudad) || $ciudad['tid'] != $filtro_ciudad['tid']) print 'style="display:none;"'; ?>>
            <a onclick="toggle_menu('barrio-<?php print $ciudad['tid']; ?>'); ocultar_select('barrio-<?php print $ciudad['tid']; ?>', 'barrio', '');" href="javascript:;" rel="noindex">Barrio</a>
            <ul class="sub" id="barrio-<?php print $ciudad['tid']; ?>" style="display: none;">
            <?php foreach($ciudad['child_data'] as $barrio){ ?>
              <li><a onclick="seleccion_opcion('barrio-<?php print $ciudad['tid']; ?>', '<?php echo ucwords(mb_strtolower($barrio['name'])); ?>', '<?php echo str_replace('"', '-', $barrio['filtro']); ?>', 'barrio');" style="cursor:pointer;" rel="noindex"><?php echo ucwords(mb_strtolower($barrio['name'])); ?></a></li>
              <?php
                if(in_array($barrio['tid'], $filtros_nombres)){
                  $filtro_barrio['name'] = ucwords(mb_strtolower($barrio['name'])); 
                  $filtro_barrio['filtro'] = $barrio['filtro']; 
                }
              ?>
            <?php } ?>
            </ul>
            <li><a class="option-select" id="barrio-<?php print $ciudad['tid']; ?>_select" <?php if(empty($filtro_barrio)) print 'style="display:none;"'; ?> rel="noindex"><?php if(!empty($filtro_barrio['name'])) print $filtro_barrio['name']; ?></a></li>
          </div>        
        <?php } ?>
      <?php } ?>
    <?php } ?>
    <input type="hidden" name="barrio" id="barrio_link" value="<?php if(!empty($filtro_barrio['filtro'])) print $filtro_barrio['filtro']; ?>" />
    
    <?php
      foreach($filtros_nombres as $filtro_texto){
        if(strpos($filtro_texto, 'Precio') !== false){
          $precio_desde_hasta = explode(' ', $filtro_texto);
          $precio_desde = $precio_desde_hasta[1];
          $precio_hasta = $precio_desde_hasta[3];
        }
        if($filtro_texto == '$')
          $moneda_peso_check = 1;
        if($filtro_texto == 'U$S')
          $moneda_dolar_check = 1;
        if(strpos($filtro_texto, 'Km') !== false){
          $km_desde_hasta = explode(' ', $filtro_texto);
          $km_desde = $km_desde_hasta[1];
          $km_hasta = $km_desde_hasta[3];
        }
        if(strpos($filtro_texto, 'Año') !== false){
          $anio_desde_hasta = explode(' ', $filtro_texto);
          $anio_desde = $anio_desde_hasta[1];
          $anio_hasta = $anio_desde_hasta[3];
        }
      }
    ?>
    <a class="no-select" href="javascript:;" rel="noindex">Precio</a>
    <ul class="sub" id="precio">
      <li>
        <div class="campo texto left"><input name="field_precio_desde" placeholder="Desde" type="text" value="<?php if(isset($precio_desde)) print $precio_desde; ?>" /></div>
        <div class="campo texto right"><input name="field_precio_hasta" placeholder="Hasta" type="text" value="<?php if(isset($precio_hasta)) print $precio_hasta; ?>" /></div>
      </li>
    </ul>
    <a class="no-select" href="javascript:;" rel="noindex">Moneda</a>
    <ul class="sub" id="moneda">
      <li>
        <div class="campo moneda">
          <label for="radio_moneda_peso">$</label><input type="radio" name="field_moneda" id="radio_moneda_peso" value="1" <?php if(isset($moneda_peso_check) && $moneda_peso_check == 1) print 'checked="checked"'; ?>/>
          <label for="radio_moneda_dolar">U$S</label><input type="radio" name="field_moneda" id="radio_moneda_dolar" value="2" <?php if(isset($moneda_dolar_check) && $moneda_dolar_check == 1) print 'checked="checked"'; ?>/>
        </div>
      </li>
    </ul>
    
    <div class="centrar">
      <div class="contactar">
        <a class="botonC" onClick="envio_form();" href="javascript:;" title="Filtrar" alt="Filtrar" rel="noindex">Aplicar</a>
      </div>
    </div>
    
  </nav>
  
  <?php
  foreach($filtros_ocultos as $filtro) {
  ?>
    <input type="hidden" name="<?php echo $filtro['parametro']; ?>" id="<?php echo $filtro['parametro']; ?>" value="<?php print $filtro['valor']; ?>" />
  <?php
  }
  ?>
  
  <input type="hidden" name="url" value="search/map/" />
  <input type="hidden" name="latitud" id="map-latitud" value=""/>
  <input type="hidden" name="longitud" id="map-longitud" value=""/>
  
  
  
  </form>
</section>
</div>
<script type="text/javascript">
  var latitud = -31.39893;
  var longitud = -64.182129;
  document.addEventListener('DOMContentLoaded', localizame, false);
  function localizame() {
    document.getElementById('link-map').href = document.getElementById('link-map').href + '/latitud=' + latitud + '/longitud=' + longitud;
    document.getElementById('map-latitud').value = latitud;
    document.getElementById('map-longitud').value = longitud;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(coordenadas);
    }
  }
  function coordenadas(position) {
    latitud = position.coords.latitude;
    longitud = position.coords.longitude;
    document.getElementById('link-map').href = document.getElementById('link-map').href + '/latitud=' + latitud + '/longitud=' + longitud;
    document.getElementById('map-latitud').value = latitud;
    document.getElementById('map-longitud').value = longitud;
  }
  
  function seleccion_opcion(contenedor, valor, filtro, content_link){
    
    //si viene con mas de un filtro tid, tomo solo el ultimo
    if(filtro.indexOf(" tid") != -1){
      var filtros_tids = filtro.split(' tid');
      filtro = 'tid' + filtros_tids[filtros_tids.length-1];
    }
    
    var el = document.getElementById(contenedor);
    var select = document.getElementById(contenedor + '_select');
    if(content_link != ''){
      var link = document.getElementById(content_link + '_link');
    } else {
      var link = document.getElementById(contenedor + '_link');
    }
    //Agregar valores
    select.innerHTML = valor;
    select.style.display = 'block';
    link.value = filtro;
    //Ocutar nav
    el.style.display = 'none';
  }
  
  function ocultar_select(select, content_link, content_link_sub){
    var el = document.getElementById(select + '_select');
    if(content_link != ''){
      var link = document.getElementById(content_link + '_link');
    } else {
      var link = document.getElementById(select + '_link');
    }
    //Elminar y ocultar sub opcion
    if(content_link_sub != ''){
      var sublink = document.getElementById(content_link_sub + '_link');
      sublink.value = '';
      var del = document.getElementsByClassName(content_link_sub);
      for(var i=0;i<del.length;i++){
        del[i].style.display = 'none';
      }
      //Si es provincia elimino barrio
      if(select == 'provincia'){
        var barriolink = document.getElementById('barrio_link');
        barriolink.value = '';
        var barriodel = document.getElementsByClassName('barrio');
        for(var i=0;i<barriodel.length;i++){
          barriodel[i].style.display = 'none';
        }
      }
    }
    el.style.display = 'none';
    link.value = '';
  }
  
  function mostrar_ciudad(id_provincia){
    var del = document.getElementsByClassName('ciudad');
    var del2 = document.getElementsByClassName('barrio');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    for(var i=0;i<del2.length;i++){
      del2[i].style.display = 'none';
    }
    document.getElementById('barrio_link').value = '';
    var el = document.getElementById('contenedor-ciudad-' + id_provincia);
    el.style.display = 'block';
  }
  
  function mostrar_barrio(id_ciudad){
    var del = document.getElementsByClassName('barrio');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-barrio-' + id_ciudad);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function mostrar_modelo(id_marca){
    var del = document.getElementsByClassName('modelo');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-modelo-' + id_marca);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function mostrar_subrubro(id_rubo){
    var del = document.getElementsByClassName('subsubrubro');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-subsubrubro-' + id_rubo);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function mostrar_marca(id_rubo){
    var del = document.getElementsByClassName('marca');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-marca-' + id_rubo);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function mostrar_inmueble(id_rubo){
    var del = document.getElementsByClassName('inmueble');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-inmueble-' + id_rubo);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function envio_form(){
    document.forms["formFiltro"].submit();
  }
</script>