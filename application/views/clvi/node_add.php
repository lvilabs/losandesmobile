<div id="content">
	<article class="aviso">
    <div class="container">
      <div class="row"> 
        <div class="cajaTitulo">
          <div class="titulo">
            <h1>
            <?php if(isset($node)) :?>
              Edición de Aviso <?php print $rubro_name; ?>
            <?php else: ?>
              Creación de Aviso <?php print $rubro_name; ?>
            <?php endif; ?>
            </h1>
          </div>
        </div>
      </div>
    </div>
    <div class="message" <?php if(empty($mensaje)) print 'style="display:none;"'; ?>><?php print $mensaje; ?></div>
    <div class="container">
      <div class="row">
      
        <?php if(!isset($rubro) || $rubro == '') { ?>
        
        <div class="content cuerpo">
          <div class="modal-publicacion-rubros">
            <h4 class="title">¿Qué deseás publicar?</h4>
            <a href="javascript:void(0);" onClick="cambioRubroModal('aviso_auto');" title="Publicá tu vehículo" class="first">
              <i class="fa fa-car"></i>
              <h5>Vehículos</h5>
            </a>
            <a href="javascript:void(0);" onClick="cambioRubroModal('aviso_casa');" title="Publicá tu casa o departamento">
              <i class="fa fa-home"></i>
              <h5 class="home">Inmuebles</h5>
            </a>
            <!-- <a href="http://empleos.clasificadoslavoz.com.ar/" title="Busca y publicá empleos">
              <i class="fa fa-briefcase"></i>
              <h5>Empleos</h5>
            </a> -->
            <a href="javascript:void(0);" onClick="cambioRubroModal('aviso_producto');" title="Publicá tu producto" class="first">
              <i class="fa fa-gift"></i>
              <h5>Productos</h5>
            </a>
            <a href="javascript:void(0);" onClick="cambioRubroModal('aviso_servicio');" title="Publicá tu servicio">
              <i class="fa fa-wrench"></i>
              <h5>Servicios</h5>
            </a>
            <a href="javascript:void(0);" onClick="cambioRubroModal('aviso_rural');" title="Publicá tu aviso rural" class="first">
              <img alt="Publicacion de avisos rurales" src="/public/clvi/img/tractor.png">
              <h5>Rurales</h5>
            </a>
          </div>
        </div>
        <form name="formRubros" id="formSelectorRubro" class="formAviso" action="/node_add" method="post">
          <input id="rubroModal" type="hidden" name="selectRubro" value="" />
        </form>
        
        <?php } else { ?>
      
        <div class="twelvecol">
          <div class="content cuerpo">
            <form name="formRubros" id="formSelectorRubro" class="formAviso" action="/node_add" method="post">
              <?php
              if(empty($node)):
              ?>
              <h2>Selecciona el Rubro</h2>
              <div class="campo">
                <select name="selectRubro" onChange="cambioRubro(this);">
                  <option value=""></option>
                  <optgroup label="Inmuebles">
                  <option value="aviso_campo" <?php if($rubro == "aviso_campo") print 'selected="selected"';?>>Campos</option>
                  <option value="aviso_casa" <?php if($rubro == "aviso_casa") print 'selected="selected"';?>>Casas</option>
                  <option value="aviso_cochera" <?php if($rubro == "aviso_cochera") print 'selected="selected"';?>>Cocheras</option>
                  <option value="aviso_departamento" <?php if($rubro == "aviso_departamento") print 'selected="selected"';?>>Departamentos</option>
                  <option value="aviso_galpon_deposito" <?php if($rubro == "aviso_galpon_deposito") print 'selected="selected"';?>>Galpones y Depósitos</option>
                  <option value="aviso_local" <?php if($rubro == "aviso_local") print 'selected="selected"';?>>Local</option>
                  <option value="aviso_oficina" <?php if($rubro == "aviso_oficina") print 'selected="selected"';?>>Oficinas</option>
                  <option value="aviso_terreno_lote" <?php if($rubro == "aviso_terreno_lote") print 'selected="selected"';?>>Terrenos y Lotes</option>
                  </optgroup>
                  <optgroup label="Autos">
                  <option value="aviso_4x4" <?php if($rubro == "aviso_4x4") print 'selected="selected"';?>>4x4, 4x2 y todo terreno</option>
                  <option value="aviso_auto" <?php if($rubro == "aviso_auto") print 'selected="selected"';?>>Autos</option>
                  <option value="aviso_plan_ahorro" <?php if($rubro == "aviso_plan_ahorro") print 'selected="selected"';?>>Planes de Ahorro</option>
                  <option value="aviso_utilitario" <?php if($rubro == "aviso_utilitario") print 'selected="selected"';?>>Utilitarios, pick-up y camiones</option>
                  <option value="aviso_moto" <?php if($rubro == "aviso_moto") print 'selected="selected"';?>>Motos, cuadriciclos y náutica</option>
                  <option value="aviso_repuesto" <?php if($rubro == "aviso_repuesto") print 'selected="selected"';?>>Accesorios y Repuestos</option>
                  </optgroup>
                  <option value="aviso_producto" <?php if($rubro == "aviso_producto") print 'selected="selected"';?>>Productos</option>
                  <option value="aviso_servicio" <?php if($rubro == "aviso_servicio") print 'selected="selected"';?>>Servicios</option>
                  <option value="aviso_rural" <?php if($rubro == "aviso_rural") print 'selected="selected"';?>>Rurales</option>
                </select>
              </div>
              <?php
              else:
              ?>

              <?php
              endif;
              ?>
            </form>
            <?php
            // Campos simples
            $campos = array('title' => 'field_aviso_titulo', 'body' => 'field_aviso_descripcion');
            $seleccionado = array();
            foreach($campos as $key => $campo) {
              $seleccionado[$campo] = '';
              if(isset($_POST[$campo])) {
                $seleccionado[$campo] = $_POST[$campo];
              } else if(isset($node[$key])) {
                $seleccionado[$campo] = $node[$key];
              }
            }
            
            // Campos tipo [field_name][0]->value
            $campos = array();
            $campos['field_aviso_operacion']        = 'field_aviso_operacion';
            $campos['field_aviso_usado']            = 'field_aviso_usado';
            $campos['field_aviso_tel_vendedor']     = 'field_aviso_telefono'; // Son diferentes nombres.
            $campos['field_aviso_ocultar_tel']      = 'field_aviso_ocultar_tel';
            $campos['field_aviso_mail_vendedor']    = 'field_aviso_mail'; // Son diferentes nombres.
            $campos['field_aviso_ocultar_mail']     = 'field_aviso_ocultar_mail';
            $campos['field_aviso_precio']           = 'field_aviso_precio';
            $campos['field_aviso_moneda']           = 'field_aviso_moneda';
            $campos['field_aviso_ocultar_precio']   = 'field_aviso_ocultar_precio';
            $campos['field_aviso_forma_pago']       = 'field_aviso_forma_pago';
            $campos['field_aviso_calle']            = 'field_aviso_calle';
            $campos['field_aviso_calle_altura']     = 'field_aviso_altura'; // Son diferentes nombres.
            $campos['field_aviso_zona']             = 'field_aviso_zona';
            $campos['field_aviso_confidencial']     = 'field_aviso_confidencial';
            $campos['field_aviso_recibe_menor']     = 'field_aviso_recibe_menor';
            $campos['field_aviso_forma_pago']       = 'field_aviso_forma_pago';
            // Campos de autos
            $campos['field_aviso_usado_anio']       = 'field_aviso_usado_anio';
            $campos['field_aviso_kilometros']       = 'field_aviso_kilometros';
            $campos['field_aviso_combustible']      = 'field_aviso_combustible';
            $campos['segmento_autos']               = 'segmento_autos';
            $campos['field_aviso_version']          = 'field_aviso_version';
            $campos['field_aviso_cilindrada']       = 'field_aviso_cilindrada';
            $campos['field_aviso_color']            = 'color'; // Son diferentes nombres.
            $campos['field_aviso_transmision']      = 'field_aviso_transmision';
            $campos['field_aviso_cant_puertas']     = 'field_aviso_cant_puertas';
            $campos['field_aviso_tipo_unidad_util'] = 'field_aviso_tipo_unidad_util';
            // Campos de inmuebles
            $campos['field_aviso_tipo_unidad']      = 'field_aviso_tipo_unidad';
            $campos['field_aviso_tipo_campo']       = 'field_aviso_tipo_campo';
            $campos['field_aviso_tipo_unidad_dpto'] = 'field_aviso_tipo_unidad_dpto';
            $campos['field_aviso_tipo_terreno']     = 'field_aviso_tipo_terreno';
            $campos['field_aviso_tipo_plan']        = 'field_aviso_tipo_plan';
            $campos['field_aviso_tipo_barrio']      = 'field_aviso_tipo_barrio';
            $campos['field_aviso_num_manzana']      = 'field_aviso_num_manzana';
            $campos['field_aviso_num_lote']         = 'field_aviso_num_lote';
            $campos['field_aviso_apto_escritura']   = 'field_aviso_apto_escritura';
            $campos['field_aviso_apto_credito']     = 'field_aviso_apto_credito';
            $campos['field_aviso_antiguedad']       = 'field_aviso_antiguedad';
            $campos['field_aviso_estrenar']         = 'field_aviso_estrenar';
            $campos['field_aviso_etapa_construccion'] = 'field_aviso_etapa_construccion';
            $campos['field_aviso_cantidad_privados'] = 'field_aviso_cantidad_privados';
            $campos['field_aviso_ubicacion']        = 'field_aviso_ubicacion';
            $campos['field_aviso_piso_numero']      = 'field_aviso_piso_numero';
            $campos['field_aviso_cantidad_dormitorios'] = 'field_aviso_cantidad_dormitorios';
            $campos['field_aviso_cantidad_plantas'] = 'field_aviso_cantidad_plantas';
            $campos['field_aviso_cantidad_banios']  = 'field_aviso_cantidad_banios';
            $campos['field_aviso_tipo_banio']       = 'field_aviso_tipo_banio';
            $campos['field_aviso_tipo_techo']       = 'field_aviso_tipo_techo';
            $campos['field_aviso_tipo_porton']      = 'field_aviso_tipo_porton';
            $campos['field_aviso_cantidad_cocheras'] = 'field_aviso_cantidad_cocheras';
            $campos['field_aviso_cobertura_cochera'] = 'field_aviso_cobertura_cochera';
            $campos['field_aviso_tipo_cochera']     = 'field_aviso_tipo_cochera';
            $campos['field_aviso_superficie_total'] = 'field_aviso_sup_total'; // Son diferentes nombres.
            $campos['field_aviso_superficie_cubierta'] = 'field_aviso_sup_cubierta'; // Son diferentes nombres.
            $campos['field_aviso_distancia_pavimento'] = 'field_aviso_distancia_pavimento';
            $campos['field_aviso_cantidad_hectarias']  = 'field_aviso_cantidad_hectarias';
            $campos['field_aviso_ancho_entrada']    = 'field_aviso_ancho_entrada';
            $campos['field_aviso_alto_entrada']     = 'field_aviso_alto_entrada';
            $campos['field_aviso_cantidad_columnas'] = 'field_aviso_cantidad_columnas';
            $campos['field_aviso_cantidad_naves']   = 'field_aviso_cantidad_naves';
            $campos['field_aviso_patron_municipalidad'] = 'field_aviso_patron_municipalidad';
            $campos['field_aviso_superficie_terreno'] = 'field_aviso_superficie_terreno';
            $campos['field_aviso_metros_frente']    = 'field_aviso_metros_frente';
            $campos['field_aviso_metros_fondo']     = 'field_aviso_metros_fondo';
            $campos['field_aviso_superficie_construib'] = 'field_aviso_superficie_construib';
            $campos['field_aviso_expensas']         = 'field_aviso_expensas';
            $campos['field_aviso_ganaderia']        = 'field_aviso_ganaderia';
            $campos['field_aviso_agricultura']      = 'field_aviso_agricultura';
            $campos['field_aviso_casa_principal']   = 'field_aviso_casa_principal';
            $campos['field_aviso_casa_casero']      = 'field_aviso_casa_casero';
            $campos['field_aviso_en_edificio']      = 'field_aviso_en_edificio';
            $campos['field_aviso_galeria_shopping'] = 'field_aviso_galeria_shopping';
            $campos['field_aviso_vigilancia']       = 'field_aviso_vigilancia';
            $campos['field_aviso_baulera']          = 'field_aviso_baulera';
            $campos['field_aviso_propiedad_ocupada'] = 'field_aviso_propiedad_ocupada';
            $campos['field_aviso_ultima_actividad'] = 'field_aviso_ultima_actividad';
            $campos['field_aviso_tipo_edificio']    = 'field_aviso_tipo_edificio';
            $campos['field_aviso_condicion']        = 'field_aviso_condicion';
            $campos['field_aviso_dpto_piso']        = 'field_aviso_dpto_piso';
            $campos['field_aviso_cantidad_pisos']   = 'field_aviso_cantidad_pisos';
            $campos['field_aviso_valor_cuota']      = 'field_aviso_valor_cuota';
            $campos['field_aviso_valor_cuota_promedio'] = 'field_aviso_valor_cuota_promedio';
            $campos['field_aviso_valor_cuota_suscrip'] = 'field_aviso_valor_cuota_suscrip';
            $campos['field_aviso_tipo_emprendimiento'] = 'field_aviso_tipo_emprendimiento';
            $campos['field_aviso_tiempo_entrega']   = 'field_aviso_tiempo_entrega';
            $campos['field_aviso_superficie']       = 'field_aviso_superficie';
            $campos['field_aviso_constructora']     = 'field_aviso_constructora';
            $campos['field_aviso_financiadora']     = 'field_aviso_financiadora';
            $campos['field_aviso_precio_hasta']     = 'field_aviso_precio_hasta';
            $campos['field_aviso_web']              = 'field_aviso_web';
            $campos['field_aviso_numero_matricula'] = 'field_aviso_numero_matricula';
            $campos['field_aviso_precio_uva']       = 'field_aviso_precio_uva';
            
            foreach($campos as $key => $campo) {
              $seleccionado[$campo] = '';
              if(isset($_POST[$campo])) {
                $seleccionado[$campo] = $_POST[$campo];
              } else if(isset($node[$key])) {
                $seleccionado[$campo] = $node[$key][0]->value;
              }
            }
            if(!empty($seleccionado['field_aviso_usado_anio'])) {
              $seleccionado['field_aviso_usado_anio'] = substr($seleccionado['field_aviso_usado_anio'], 0, 4);
            }
            // Taxonomias
            // Si se hace un post de algun campo comun se toman los datos de ahi sino del node.
            $taxonomias = array();
            if(isset($_POST['field_aviso_titulo'])) {
              $campos_taxonomias = array();
              $campos_taxonomias[] = 'marca_modelo_4x4';
              $campos_taxonomias[] = 'marca_modelo_autos';
              $campos_taxonomias[] = 'marca_modelo_planes_de_ahorro';
              $campos_taxonomias[] = 'marca_modelo_utilitarios';
              $campos_taxonomias[] = 'marca_motos';
              $campos_taxonomias[] = 'subrubro_repuestos';
              $campos_taxonomias[] = 'rubros_producto';
              $campos_taxonomias[] = 'rubros_servicio';
              $campos_taxonomias[] = 'rubros_rural';
              $campos_taxonomias[] = 'ambientes_casas';
              $campos_taxonomias[] = 'ambientes_departamentos';
              $campos_taxonomias[] = 'ambientes_locales';
              $campos_taxonomias[] = 'ambientes_oficinas';
              $campos_taxonomias[] = 'instalaciones_casas_y_departamentos';
              $campos_taxonomias[] = 'instalaciones_campos';
              $campos_taxonomias[] = 'instalaciones_galpones_y_depositos';
              $campos_taxonomias[] = 'instalaciones_locales';
              $campos_taxonomias[] = 'instalaciones_oficinas';
              $campos_taxonomias[] = 'amenities_departamentos';
              $campos_taxonomias[] = 'servicios_casas';
              $campos_taxonomias[] = 'servicios_campos';
              $campos_taxonomias[] = 'servicios_departamentos';
              $campos_taxonomias[] = 'servicios_galpones_depositos';
              $campos_taxonomias[] = 'servicios_locales';
              $campos_taxonomias[] = 'servicios_oficinas';
              $campos_taxonomias[] = 'confort';
              $campos_taxonomias[] = 'seguridad';
              foreach($campos_taxonomias as $nombre) {
                if(isset($options[$nombre]) && isset($_POST[$options[$nombre]['key']])) {
                  $valor = $_POST[$options[$nombre]['key']];
                  if(is_array($valor)) {
                    // Hay variables que son array como los servicios, amenities etc.
                    $taxonomias = array_merge($taxonomias, $valor);
                  } else {
                    $taxonomias[] = $valor;
                  }
                }
              }
              if(isset($_POST['provincia'])) {
                $taxonomias[] = $_POST['provincia'];
                if(isset($_POST['ciudad-'.$_POST['provincia']])) {
                  $taxonomias[] = $_POST['ciudad-'.$_POST['provincia']];
                  $ciudad_tid = $_POST['ciudad-'.$_POST['provincia']];
                  if(isset($_POST['barrio-'.$ciudad_tid])) {
                    $taxonomias[] = $_POST['barrio-'.$ciudad_tid];
                  }
                }
              }
            } elseif(!empty($node)) {
              foreach($node['taxonomy'] as $tid=>$term) {
                $taxonomias[] = $tid;
              }
            }//print_r($taxonomias);
            ?>
            <?php if(isset($rubro) && $rubro != '') { ?>
              <form name="formAviso" id="formCreacionAviso" class="formAviso" action="/node_add/submit_form" enctype="multipart/form-data" method="post">
                <h2>Campos Requeridos</h2>
                <input name="field_aviso_categoria" type="hidden" value="<?php print $rubro; ?>"/>
                <?php if(array_key_exists('field_aviso_tipo_unidad', $options)) { ?>
                  <!-- Tipo unidad casa -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_tipo_unidad', $campos_error)) print 'campo-error'; ?>" ><?php if($options['field_aviso_tipo_unidad']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['field_aviso_tipo_unidad']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_tipo_unidad']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_tipo_unidad']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_unidad']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_tipo_campo', $options)) { ?>
                  <!-- Tipo campo -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_tipo_campo', $campos_error)) print 'campo-error'; ?>"><?php if($options['field_aviso_tipo_campo']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['field_aviso_tipo_campo']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_tipo_campo']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_tipo_campo']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_campo']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_tipo_unidad_dpto', $options)) { ?>
                  <!-- Tipo unidad dpto -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_tipo_unidad_dpto', $campos_error)) print 'campo-error'; ?>"><?php if($options['field_aviso_tipo_unidad_dpto']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['field_aviso_tipo_unidad_dpto']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_tipo_unidad_dpto']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_tipo_unidad_dpto']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_unidad_dpto']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_tipo_terreno', $options)) { ?>
                   <!-- Tipo terreno -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_tipo_terreno', $campos_error)) print 'campo-error'; ?>"><?php if($options['field_aviso_tipo_terreno']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['field_aviso_tipo_terreno']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_tipo_terreno']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_tipo_terreno']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_terreno']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('marca_modelo_4x4', $options)) { ?>
                  <!-- Marca Modelo 4x4 -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('marca_modelo_4x4', $campos_error)) print 'campo-error'; ?>"><?php if($options['marca_modelo_4x4']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['marca_modelo_4x4']['key']; ?>">
                    <option value="" label="- <?php print $options['marca_modelo_4x4']['name']; ?> -"></option>
                    <?php foreach($options['marca_modelo_4x4']['opciones'] as $option){ ?>
                      <?php if(!empty($option['data_child'])){ ?>
                        <optgroup label="<?php print $option['name']; ?>">
                      <?php } else { ?>
                        <optgroup label="<?php print $option['name']; ?>">
                        <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                      <?php foreach($option['data_child'] as $option2){ ?>
                            <option value="<?php print $option2['key']; ?>" <?php if(in_array($option2['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option2['name']; ?></option>
                      <?php      
                        }
                      } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('marca_modelo_autos', $options)) { ?>
                  <!-- Marca Modelo Auto -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('marca_modelo_autos', $campos_error)) print 'campo-error'; ?>"><?php if($options['marca_modelo_autos']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['marca_modelo_autos']['key']; ?>">
                    <option value="" label="- <?php print $options['marca_modelo_autos']['name']; ?> -"></option>
                    <?php foreach($options['marca_modelo_autos']['opciones'] as $option){ ?>
                      <?php if(!empty($option['data_child'])){ ?>
                        <optgroup label="<?php print $option['name']; ?>">
                      <?php } else { ?>
                        <optgroup label="<?php print $option['name']; ?>">
                        <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                      <?php foreach($option['data_child'] as $option2){ ?>
                            <option value="<?php print $option2['key']; ?>" <?php if(in_array($option2['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option2['name']; ?></option>
                      <?php      
                        }
                      } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('marca_modelo_planes_de_ahorro', $options)) { ?>
                  <!-- Marca Modelo Plan Ahorro -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('marca_modelo_planes_de_ahorro', $campos_error)) print 'campo-error'; ?>"><?php if($options['marca_modelo_planes_de_ahorro']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['marca_modelo_planes_de_ahorro']['key']; ?>">
                    <option value="" label="- <?php print $options['marca_modelo_planes_de_ahorro']['name']; ?> -"></option>
                    <?php foreach($options['marca_modelo_planes_de_ahorro']['opciones'] as $option){ ?>
                      <?php if(!empty($option['data_child'])){ ?>
                        <optgroup label="<?php print $option['name']; ?>">
                      <?php } else { ?>
                        <optgroup label="<?php print $option['name']; ?>">
                        <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                      <?php foreach($option['data_child'] as $option2){ ?>
                            <option value="<?php print $option2['key']; ?>" <?php if(in_array($option2['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option2['name']; ?></option>
                      <?php      
                        }
                      } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('marca_modelo_utilitarios', $options)) { ?>
                  <!-- Marca Modelo utilitario -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('marca_modelo_utilitarios', $campos_error)) print 'campo-error'; ?>"><?php if($options['marca_modelo_utilitarios']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['marca_modelo_utilitarios']['key']; ?>">
                    <option value="" label="- <?php print $options['marca_modelo_utilitarios']['name']; ?> -"></option>
                    <?php foreach($options['marca_modelo_utilitarios']['opciones'] as $option){ ?>
                      <?php if(!empty($option['data_child'])){ ?>
                        <optgroup label="<?php print $option['name']; ?>">
                      <?php } else { ?>
                        <optgroup label="<?php print $option['name']; ?>">
                        <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                      <?php foreach($option['data_child'] as $option2){ ?>
                            <option value="<?php print $option2['key']; ?>" <?php if(in_array($option2['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option2['name']; ?></option>
                      <?php      
                        }
                      } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('marca_motos', $options)) { ?>
                  <!-- Marca moto -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('marca_motos', $campos_error)) print 'campo-error'; ?>"><?php if($options['marca_motos']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['marca_motos']['key']; ?>">
                    <option value="" label="- <?php print $options['marca_motos']['name']; ?> -"></option>
                    <?php foreach($options['marca_motos']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('subrubro_repuestos', $options)) { ?>
                  <!-- Subrubro repuesto -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('subrubro_repuestos', $campos_error)) print 'campo-error'; ?>"><?php if($options['subrubro_repuestos']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['subrubro_repuestos']['key']; ?>">
                    <option value="" label="- <?php print $options['subrubro_repuestos']['name']; ?> -"></option>
                    <?php foreach($options['subrubro_repuestos']['opciones'] as $option){ ?>
                      <?php if(!empty($option['data_child'])){ ?>
                        <optgroup label="<?php print $option['name']; ?>">
                      <?php } else { ?>
                        <optgroup label="<?php print $option['name']; ?>">
                        <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                      <?php foreach($option['data_child'] as $option2){ ?>
                            <option value="<?php print $option2['key']; ?>" <?php if(in_array($option2['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option2['name']; ?></option>
                      <?php      
                        }
                      } ?>
                  </select></div>
                <?php } ?>
                
                <?php if(array_key_exists('field_aviso_tipo_unidad_util', $options)) { ?>
                  <!-- Tipo unidad utilitario -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_tipo_unidad_util', $campos_error)) print 'campo-error'; ?>"><?php if($options['field_aviso_tipo_unidad_util']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['field_aviso_tipo_unidad_util']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_tipo_unidad_util']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_tipo_unidad_util']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_unidad_util']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                
                
                <?php if(array_key_exists('rubros_producto', $options)) { ?>
                  <!-- Rubro Producto -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('rubros_producto', $campos_error)) print 'campo-error'; ?>"><?php if($options['rubros_producto']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['rubros_producto']['key']; ?>">
                    <option value="" label="- <?php print $options['rubros_producto']['name']; ?> -"></option>
                    <?php foreach($options['rubros_producto']['opciones'] as $option){ ?>
                      <?php if(!empty($option['data_child'])){ ?>
                        <optgroup label="<?php print $option['name']; ?>">
                      <?php } else { ?>
                        <optgroup label="<?php print $option['name']; ?>">
                        <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                      <?php foreach($option['data_child'] as $option2){ ?>
                          <option value="<?php print $option2['key']; ?>" <?php if(in_array($option2['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option2['name']; ?></option>
                          <?php foreach($option2['data_child'] as $option3){ ?>
                            <option value="<?php print $option3['key']; ?>" <?php if(in_array($option3['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option3['name']; ?></option>
                          <?php 
                          }      
                        }
                      } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('rubros_servicio', $options)) { ?>
                  <!-- Rubro Servicio -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('rubros_servicio', $campos_error)) print 'campo-error'; ?>"><?php if($options['rubros_servicio']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['rubros_servicio']['key']; ?>" id="subrubros-servicios">
                    <option value="" label="- <?php print $options['rubros_servicio']['name']; ?> -"></option>
                    <?php foreach($options['rubros_servicio']['opciones'] as $option){ ?>
                      <?php if(!empty($option['data_child'])){ ?>
                        <optgroup label="<?php print $option['name']; ?>">
                      <?php } else { ?>
                        <optgroup label="<?php print $option['name']; ?>">
                        <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                      <?php foreach($option['data_child'] as $option2){ ?>
                         <option value="<?php print $option2['key']; ?>" <?php if(in_array($option2['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option2['name']; ?></option>
                         <?php foreach($option2['data_child'] as $option3){ ?>
                            <option value="<?php print $option3['key']; ?>" <?php if(in_array($option3['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option3['name']; ?></option>
                          <?php 
                          }      
                        }
                      } ?>
                  </select></div>
                  
                  <div id="matricula-profesional" class="DN">
                    <div class="msg-alojamientos-form">
                      <span class="red">Evite rechazos en sus aviso</span>
                      <br>
                      <strong>Estimado usuario:</strong> Solicitamos a quienes sean <strong>"Profesionales"</strong> y deseen cargar su aviso en el sub- rubro de <strong>"Profesionales y Oficios"</strong> que deben ingresar su concreta identificación <strong>nombre y apellido</strong>, en el campo descripción, y <strong>número de matrícula</strong> al cargar su aviso, sino el mismo quedará rechazado en nuestro proceso de moderación sin ser publicado en nuestro sitio web.
                    </div>
                    <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_numero_matricula', $campos_error)) print 'campo-error'; ?>"><input name="field_aviso_numero_matricula" placeholder="Número de Matrícula" type="text" value="<?php print $seleccionado['field_aviso_numero_matricula']; ?>" maxlength="80" /></div>
                  </div>  
                  
                <?php } ?>
                <?php if(array_key_exists('rubros_rural', $options)) { ?>
                  <!-- Rubro Rural -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('rubros_rural', $campos_error)) print 'campo-error'; ?>"><?php if($options['rubros_rural']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['rubros_rural']['key']; ?>">
                    <option value="" label="- <?php print $options['rubros_rural']['name']; ?> -"></option>
                    <?php foreach($options['rubros_rural']['opciones'] as $option){ ?>
                      <?php if(!empty($option['data_child'])){ ?>
                        <optgroup label="<?php print $option['name']; ?>">
                      <?php } else { ?>
                        <optgroup label="<?php print $option['name']; ?>">
                        <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                      <?php foreach($option['data_child'] as $option2){ ?>
                         <option value="<?php print $option2['key']; ?>" <?php if(in_array($option2['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option2['name']; ?></option>
                      <?php       
                        }
                      } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_operacion', $options) && in_array($rubro, array('aviso_casa', 'aviso_campo', 'aviso_cochera', 'aviso_departamento', 'aviso_galpon_deposito', 'aviso_local', 'aviso_oficina', 'aviso_terreno_lote', 'aviso_producto', 'aviso_rural'))) { 
                  // Deshabilitamos operacion "alquiler temporario" para casas y departementos
                  if(in_array($rubro, array('aviso_casa', 'aviso_departamento'))) {
                    foreach($options['field_aviso_operacion']['opciones'] as $key => $option){
                      if($option['key'] == 5)
                        unset($options['field_aviso_operacion']['opciones'][$key]);
                    }
                  }
                ?>
                  <!-- Operacion -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_operacion', $campos_error)) print 'campo-error'; ?>"><?php if($options['field_aviso_operacion']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select id="aviso-operaciones" name="<?php print $options['field_aviso_operacion']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_operacion']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_operacion']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado['field_aviso_operacion']) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_tipo_emprendimiento', $options) && in_array($rubro, array('aviso_emprendimiento'))) { ?>
                  <!-- tipo de emprendimiento -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_tipo_emprendimiento', $campos_error)) print 'campo-error'; ?>"><?php if($options['field_aviso_tipo_emprendimiento']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['field_aviso_tipo_emprendimiento']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_tipo_emprendimiento']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_tipo_emprendimiento']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado['field_aviso_tipo_emprendimiento']) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                <!-- Titulo -->
                <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_titulo', $campos_error)) print 'campo-error'; ?>"><span class="requerido">*</span> <input name="field_aviso_titulo" placeholder="Título del Aviso" type="text" value="<?php print $seleccionado['field_aviso_titulo']; ?>" maxlength="80" /></div>
                <span class="detalle">Máximo de 80 caracteres.</span>
                <!-- Descripcion -->
                <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_descripcion', $campos_error)) print 'campo-error'; ?>"><span class="requerido">*</span> <textarea rows="5" placeholder="Descripción" name="field_aviso_descripcion"><?php print $seleccionado['field_aviso_descripcion']; ?></textarea></div>
                <?php if(array_key_exists('field_aviso_usado', $options)) { ?>
                  <!-- Uso -->
                  <div><strong><?php if($options['field_aviso_usado']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_usado']['name']; ?>:</strong></div>
                  <?php foreach($options['field_aviso_usado']['opciones'] as $option){ ?>
                    <div class="campo"><label for="radio_estado_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="radio" name="<?php print $options['field_aviso_usado']['key']; ?>" id="radio_estado_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_usado']['key']]) print 'checked="checked"'; ?> onClick="mostrarKilometros(this, 'campo-kilometro');" /></div>
                  <?php } ?>
                <?php } ?>
                <?php if(in_array($rubro, array('aviso_emprendimiento'))) { ?>
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_superficie', $campos_error)) print 'campo-error'; ?>" id="campo-superficie"><span class="requerido">*</span> <input name="field_aviso_superficie" placeholder="Superficie desde-hasta" type="text" value="<?php print $seleccionado['field_aviso_superficie']; ?>" /></div>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_tiempo_entrega', $options)) { ?>
                  <!-- Forma de Pago -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_tiempo_entrega', $campos_error)) print 'campo-error'; ?>"><?php if($options['field_aviso_tiempo_entrega']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['field_aviso_tiempo_entrega']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_tiempo_entrega']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_tiempo_entrega']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tiempo_entrega']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                <?php if(in_array($rubro, array('aviso_4x4', 'aviso_auto', 'aviso_moto', 'aviso_plan_ahorro', 'aviso_utilitario'))){ ?>
                  <!-- Anio -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_usado_anio', $campos_error)) print 'campo-error'; ?>"><span class="requerido">*</span> <input name="field_aviso_usado_anio" placeholder="Año" type="text" value="<?php print $seleccionado['field_aviso_usado_anio']; ?>" /></div>
                  <span class="detalle">Sólo números y formato de año YYYY. Mayor a 1960 y menor o igual al actual.</span>
                  <!-- Kilometros -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_kilometros', $campos_error)) print 'campo-error'; ?>" id="campo-kilometro" <?php if($seleccionado['field_aviso_usado'] != 1) print 'style="display:none;"'; ?>><span class="requerido">*</span> <input name="field_aviso_kilometros" placeholder="Kilómetros" type="text" value="<?php print $seleccionado['field_aviso_kilometros']; ?>" /></div>
                <?php } ?>
                
                <!-- Telefono -->
                <?php if(empty($seleccionado['field_aviso_telefono'])){ ?>
                  <div class="campo telefonoContacto <?php if(!empty($campos_error) && in_array('field_aviso_telefono', $campos_error)) print 'campo-error'; ?>">
                    <span class="requerido">*</span> 
                    <input name="field_aviso_tel_vendedor_1" placeholder="Teléfono 1" type="text" id="tel_vendedor_1" value="<?php print $tel_1; ?>"/> /
                    <input name="field_aviso_tel_vendedor_2" placeholder="Teléfono 2" type="text" id="tel_vendedor_2" value="<?php print $tel_2; ?>"/> /
                    <input name="field_aviso_tel_vendedor_3" placeholder="Teléfono 3" type="text" id="tel_vendedor_3" value="<?php print $tel_3; ?>"/>
                    <label class="placeholder">Teléfono Vendedor</label>
                  </div>
                  <span class="detalle">Puedes ingresar hasta 3 teléfonos distintos.</span>
                <?php } else { ?>
                  <?php 
                    $tel_1 = '';
                    $tel_2 = '';
                    $tel_3 = '';
                    $array_tel = explode('/', $seleccionado['field_aviso_telefono']);
                    foreach($array_tel as $key => $num_tel) {
                      switch($key) {
                        case 0:
                          $tel_1 = trim($num_tel);
                          break;
                        case 1:
                          $tel_2 = trim($num_tel);
                          break;
                        case 2:
                          $tel_3 = trim($num_tel);
                          break;
                      }
                    }
                  ?>
                  <div class="campo telefonoContacto <?php if(!empty($campos_error) && in_array('field_aviso_telefono', $campos_error)) print 'campo-error'; ?>">
                    <span class="requerido">*</span> 
                    <input name="field_aviso_tel_vendedor_1" placeholder="Teléfono 1" type="text" id="tel_vendedor_1" value="<?php print $tel_1; ?>"/> /
                    <input name="field_aviso_tel_vendedor_2" placeholder="Teléfono 2" type="text" id="tel_vendedor_2" value="<?php print $tel_2; ?>"/> /
                    <input name="field_aviso_tel_vendedor_3" placeholder="Teléfono 3" type="text" id="tel_vendedor_3" value="<?php print $tel_3; ?>"/>
                    <label class="placeholder">Teléfono Vendedor</label>
                  </div>
                  <span class="detalle">Puedes ingresar hasta 3 teléfonos distintos.</span>
                <?php } ?>
                <input name="field_aviso_telefono" id="edit-field-aviso-tel-vendedor-value" placeholder="Teléfono Vendedor" type="hidden" value="<?php print $seleccionado['field_aviso_telefono']; ?>" />
                
                
                <?php if(array_key_exists('field_aviso_ocultar_tel', $options)) { ?>
                  <!-- Ocultar Telefono -->
                  <div class="campo"><label for="checkbox_ocultar_telefono"><?php if($options['field_aviso_ocultar_tel']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_ocultar_tel']['name']; ?></label>
                  <?php foreach($options['field_aviso_ocultar_tel']['opciones'] as $option){ ?>
                    <input type="checkbox" id="checkbox_ocultar_telefono" name="<?php print $options['field_aviso_ocultar_tel']['key']; ?>" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_ocultar_tel']['key']]) print 'checked="checked"'; ?> />
                  <?php } ?>
                  </div>
                <?php } ?>
                
                <?php $check_quitar = false; ?>
                <div class="whatsapp avisos">
                  <label for="edit-profile-telefono-whatsapp">Teléfono para WhatsApp: </label>
                  <?php if(empty($node['field_aviso_telefono_whatsapp'][0]->value)){ ?>
                    <div class="whatsapp_bandera"></div><span class="whatsapp_prefijo"> +549 0</span> 
                    <?php if(isset($post['telefono_whatsapp']) && !empty($post['telefono_whatsapp'])) { ?>
                      <div class="campo solo_telefono">
                        <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="<?php print $post['telefono_whatsapp']; ?>" data-hidden="<?php print $post['telefono_whatsapp']; ?>">
                      </div>
                      <?php $check_quitar = true; ?>
                    <?php } else { ?>
                      <div class="campo area_whatsapp">
                        <input type="text" name="codigo_area_whatsapp" id="codigo_area_whatsapp" maxlength="4" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="">
                      </div>
                      <div class="sub_whatsapp">15 </div>
                      <div class="campo">
                        <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="7" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="">
                      </div>
                    <?php } ?>
                  <?php } else { ?>
                    <?php $whatsapp = explode('+549', $node['field_aviso_telefono_whatsapp'][0]->value); ?>
                    <div class="whatsapp_bandera"></div><span class="whatsapp_prefijo"> +549 0</span>
                    <div class="campo solo_telefono">
                      <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="10" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="<?php print $whatsapp[1]; ?>" data-hidden="<?php print $whatsapp[1]; ?>">
                    </div>
                    <?php $check_quitar = true; ?>
                  <?php } ?>
                  <label>Para recibir contactos por WhatsApp complete este campo.</label>
                </div>
                
                <!-- Ocultar WhatsApp -->
                <?php if($check_quitar) { ?>
                  <div class="campo"><label for="checkbox_ocultar_whatsapp">Quitar teléfono WhatsApp</label>
                    <input type="checkbox" id="checkbox_ocultar_whatsapp" name="field_aviso_ocultar_whatsapp" value="1" <?php if($option['key'] == $seleccionado[$options['field_aviso_ocultar_tel']['key']]) print 'checked="checked"'; ?> />
                  </div>
                <?php } ?>
                
                <!-- Mail -->
                <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_mail', $campos_error)) print 'campo-error'; ?>"><span class="requerido">*</span> <input name="field_aviso_mail" placeholder="E-mail Vendedor" type="text" value="<?php print $seleccionado['field_aviso_mail']; ?>" /></div>
                <?php if(array_key_exists('field_aviso_ocultar_mail', $options)) { ?>
                  <!-- Ocultar Mail -->
                  <div class="campo"><label for="checkbox_ocultar_mail"><?php if($options['field_aviso_ocultar_mail']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_ocultar_mail']['name']; ?></label>
                  <?php foreach($options['field_aviso_ocultar_mail']['opciones'] as $option){ ?>
                    <input type="checkbox" id="checkbox_ocultar_mail" name="<?php print $options['field_aviso_ocultar_mail']['key']; ?>" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_ocultar_mail']['key']]) print 'checked="checked"'; ?> />
                  <?php } ?>
                  </div>
                <?php } ?>
                <?php if(in_array($rubro, array('aviso_emprendimiento'))) { ?>
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_web', $campos_error)) print 'campo-error'; ?>" id="campo-web"><input name="field_aviso_web" placeholder="Sitio web" type="text" value="<?php print $seleccionado['field_aviso_web']; ?>" /></div>
                <?php } ?>
                <?php if(in_array($rubro, array('aviso_emprendimiento'))) { ?>
                  <h2>Firmas Responsables</h2>
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_constructora', $campos_error)) print 'campo-error'; ?>" id="campo-constructora"><input name="field_aviso_constructora" placeholder="Constructora" type="text" value="<?php print $seleccionado['field_aviso_constructora']; ?>" /></div>
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_financiadora', $campos_error)) print 'campo-error'; ?>" id="campo-financiadora"><input name="field_aviso_financiadora" placeholder="Financia" type="text" value="<?php print $seleccionado['field_aviso_financiadora']; ?>" /></div>
                <?php } ?>
                <!-- Precio -->
                <h2>Precio</h2>
                <?php
                $label_precio = 'Precio';
                if(in_array($rubro, array('aviso_emprendimiento'))) { 
                  $label_precio = 'Precio Desde';
                }
                ?>
                <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_precio', $campos_error)) print 'campo-error'; ?>"><span class="requerido">*</span> <input name="field_aviso_precio" placeholder="<?php echo $label_precio; ?>" type="text" value="<?php print $seleccionado['field_aviso_precio']; ?>" /></div>
                <span class="detalle">Sólo números y punto.</span>
                <?php if(in_array($rubro, array('aviso_emprendimiento'))) { ?>
                <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_precio_hasta', $campos_error)) print 'campo-error'; ?>"><span class="requerido">*</span> <input name="field_aviso_precio_hasta" placeholder="Precio Hasta" type="text" value="<?php print $seleccionado['field_aviso_precio_hasta']; ?>" /></div>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_moneda', $options)) { ?>
                  <!-- Moneda -->
                  <div><strong><?php print $options['field_aviso_moneda']['name']; ?>:</strong></div>
                  <?php foreach($options['field_aviso_moneda']['opciones'] as $option){ ?>
                    <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_moneda', $campos_error)) print 'campo-error'; ?>"><span class="requerido">*</span><label for="radio_moneda_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="radio" name="<?php print $options['field_aviso_moneda']['key']; ?>" id="radio_moneda_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_moneda']['key']]) print 'checked="checked"'; ?> /></div>
                  <?php } ?>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_ocultar_precio', $options)) { ?>
                  <!-- Ocultar Precio -->
                  <div class="campo"><label for="checkbox_ocultar_precio"><?php if($options['field_aviso_ocultar_precio']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_ocultar_precio']['name']; ?></label>
                  <?php foreach($options['field_aviso_ocultar_precio']['opciones'] as $option){ ?>
                    <input type="checkbox" id="checkbox_ocultar_precio" name="<?php print $options['field_aviso_ocultar_precio']['key']; ?>" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_ocultar_precio']['key']]) print 'checked="checked"'; ?> />
                  <?php } ?>
                  </div>
                <?php } ?>
                
                <?php if(array_key_exists('field_aviso_precio_uva', $options)) { ?>
                  <!-- Mostrar Precio UVA -->
                  <div class="cotizacion-uva"><strong>Valor UVA <?php print date('d-m-Y'); ?></strong> <span><?php print $cotizacion_uva; ?></span></div>
                  <div class="campo form-precio-uva"><label for="checkbox_precio_uva"><?php if($options['field_aviso_precio_uva']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_precio_uva']['name']; ?></label>
                  <?php foreach($options['field_aviso_precio_uva']['opciones'] as $option){ ?>
                    <input type="checkbox" id="checkbox_precio_uva" name="<?php print $options['field_aviso_precio_uva']['key']; ?>" value="<?php print $option['key']; ?>" checked="<?php if($option['key'] == $seleccionado[$options['field_aviso_precio_uva']['key']]) print 'checked'; ?>" />
                  <?php } ?>
                  </div>
                  <div class="mont-convertido-uva"></div>
                <?php } ?>
                
                <?php if(array_key_exists('field_aviso_recibe_menor', $options)) { ?>
                  <!-- Recibe menor -->
                  <div><strong><?php if($options['field_aviso_recibe_menor']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_recibe_menor']['name']; ?>:</strong><div>
                  <?php foreach($options['field_aviso_recibe_menor']['opciones'] as $option){ ?>
                    <div class="campo"><label for="radio_menor_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="radio" name="<?php print $options['field_aviso_recibe_menor']['key']; ?>" id="radio_menor_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_recibe_menor']['key']]) print 'checked="checked"'; ?> /></div>
                  <?php } ?>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_forma_pago', $options)) { ?>
                  <!-- Forma de Pago -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_forma_pago', $campos_error)) print 'campo-error'; ?>"><?php if($options['field_aviso_forma_pago']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['field_aviso_forma_pago']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_forma_pago']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_forma_pago']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_forma_pago']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                <?php if(array_key_exists('field_aviso_tipo_plan', $options)) { ?>
                  <!-- Tipo plan -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_tipo_plan', $campos_error)) print 'campo-error'; ?>"><?php if($options['field_aviso_tipo_plan']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                  <select name="<?php print $options['field_aviso_tipo_plan']['key']; ?>">
                    <option value="" label="- <?php print $options['field_aviso_tipo_plan']['name']; ?> -"></option>
                    <?php foreach($options['field_aviso_tipo_plan']['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_plan']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                <?php } ?>
                
<!-- CAMPOS NO REQUERIDOS -->
                <!-- MULTIMEDIA -->
                <h2>Multimedia</h2>
                <!-- Foto -->
                <span class="detalle">Extensiones permitidas: gif, png, jpg, jpeg.</span>

                <?php if(isset($node['field_aviso_fotos'])): ?>
                  <?php foreach($node['field_aviso_fotos'] as $foto): ?>
                    <?php if(!empty($foto)): ?>
                    <div class="campo">
                      <img src="<?php echo $this->config->item('static_sitio').'/'.$foto->thumbnail; ?>">
                    </div>
                    <?php endif; ?>
                  <?php endforeach; ?>
                <?php endif; ?>
               
                <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_foto', $campos_error)) print 'campo-error'; ?>"><label>Foto 1</label><input type="file" name="field_aviso_foto" ></div>
                <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_foto1', $campos_error)) print 'campo-error'; ?>"><label>Foto 2</label><input type="file" name="field_aviso_foto1" ></div>
                <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_foto2', $campos_error)) print 'campo-error'; ?>"><label>Foto 3</label><input type="file" name="field_aviso_foto2" ></div>
                <!-- Video -->
                <!-- <div class="campo"><input name="field_aviso_video" placeholder="Video" type="text" value="<?php if(isset($seleccionado['field_aviso_video'])) print $seleccionado['field_aviso_video']; ?>" /></div>
                <span class="detalle">Sólo url de youtube.</span> -->
                
                <!-- UBICACION -->
                <h2>Ubicación</h2>
                <!-- id="ver-ubicacion" class="mas-campos" onClick="masContenido('content_ubicacion', 'ver-ubicacion');" -->
                <?php if(isset($options_ubicacion) && !empty($options_ubicacion)) { ?>
                  <!-- Provincia -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('provincia', $campos_error)) print 'campo-error'; ?>"><span class="requerido">*</span>
                  <select id="select-provincia" name="provincia" onChange="mostrarCiudad(this);">
                    <option value="" label="- Provincia -"></option>
                    <?php foreach($options_ubicacion['opciones'] as $option){ ?>
                      <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                    <?php } ?>
                  </select></div>
                  <!-- Ciudad -->
                  <?php foreach($options_ubicacion['opciones'] as $provincia){ ?>
                  <div id="ciudad-<?php print $provincia['key']; ?>" class="campo <?php if(!empty($campos_error) && in_array('ciudad', $campos_error)) print 'campo-error'; ?> ciudad" <?php if(!in_array($provincia['key'], $taxonomias)) print 'style="display:none;"'; ?>><span class="requerido">*</span>
                    <select id="select-ciudad-<?php print $provincia['key']; ?>" name="ciudad-<?php print $provincia['key']; ?>" onChange="mostrarBarrio(this);">
                      <option value="" label="- Ciudad -"></option>
                      <?php foreach($provincia['data_child'] as $ciudad){ ?>  
                        <option value="<?php print $ciudad['key']; ?>" <?php if(in_array($ciudad['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $ciudad['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <?php } ?>
                  <!-- Barrio -->
                  <?php foreach($options_ubicacion['opciones'] as $provincia){ ?>
                    <?php foreach($provincia['data_child'] as $ciudad){ ?>
                      <?php if(!empty($ciudad['data_child'])){ ?>
                        <div id="barrio-<?php print $ciudad['key']; ?>" class="campo <?php if(!empty($campos_error) && in_array('barrio', $campos_error)) print 'campo-error'; ?> barrio" <?php if(!in_array($ciudad['key'], $taxonomias)) print 'style="display:none;"'; ?>>
                          <select name="barrio-<?php print $ciudad['key']; ?>">
                            <option value="" label="- Barrio -"></option>
                            <?php foreach($ciudad['data_child'] as $barrio){ ?>
                              <option value="<?php print $barrio['key']; ?>" <?php if(in_array($barrio['key'], $taxonomias)) print 'selected="selecterd"'; ?>><?php print $barrio['name']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
                <div id="content_ubicacion" style="/*display:none;*/">
                  <?php if(array_key_exists('field_aviso_tipo_barrio', $options)) { ?>
                    <!-- Tipo Barrio -->
                    <div class="campo"><?php if($options['field_aviso_tipo_barrio']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                    <select name="<?php print $options['field_aviso_tipo_barrio']['key']; ?>">
                      <option value="" label="- <?php print $options['field_aviso_tipo_barrio']['name']; ?> -"></option>
                      <?php foreach($options['field_aviso_tipo_barrio']['opciones'] as $option){ ?>
                        <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_barrio']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                    </select></div>
                  <?php } ?>
                  <?php if(array_key_exists('field_aviso_zona', $options)) { ?>
                    <!-- Zona -->
                    <div class="campo"><?php if($options['field_aviso_zona']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                    <select name="<?php print $options['field_aviso_zona']['key']; ?>">
                      <option value="" label="- <?php print $options['field_aviso_zona']['name']; ?> -"></option>
                      <?php foreach($options['field_aviso_zona']['opciones'] as $option){ ?>
                        <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_zona']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                      <?php } ?>
                    </select>
                    </div>
                    <span class="detalle">Sólo para ciudad de Córdoba.</span>
                  <?php } ?>
                  <!-- Calle -->
                  <div class="campo"><input id="calle" name="field_aviso_calle" placeholder="Calle" type="text" value="<?php print $seleccionado['field_aviso_calle']; ?>" /></div>
                  <!-- Altura -->
                  <div class="campo <?php if(!empty($campos_error) && in_array('field_aviso_altura', $campos_error)) print 'campo-error'; ?>"><input id="altura" name="field_aviso_altura" placeholder="Altura" type="text" value="<?php print $seleccionado['field_aviso_altura']; ?>" /><br></div>
                  <span class="detalle">Sólo números.</span>
                  <?php if(array_key_exists('field_aviso_confidencial', $options)) { ?>
                    <!-- Confidencial -->
                    <div class="campo"><label for="checkbox_confidencial"><?php if($options['field_aviso_confidencial']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_confidencial']['name']; ?></label>
                    <?php foreach($options['field_aviso_confidencial']['opciones'] as $option){ ?>
                      <input id="checkbox_confidencial" type="checkbox" name="<?php print $options['field_aviso_confidencial']['key']; ?>" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_confidencial']['key']]) print 'checked="checked"'; ?> />
                    <?php } ?>
                    </div>
                  <?php } ?>
                  <?php if(in_array($rubro, array('aviso_campo', 'aviso_terreno_lote'))){ ?>
                    <!-- Num Manzana -->
                    <div class="campo"><input name="field_aviso_num_manzana" placeholder="Num. Manzana" type="text" value="<?php print $seleccionado['field_aviso_num_manzana']; ?>" /></div>
                    <!-- Num Lote -->
                    <div class="campo"><input name="field_aviso_num_lote" placeholder="Num. Lote" type="text" value="<?php print $seleccionado['field_aviso_num_lote']; ?>" /></div>
                  <?php } ?>
                  <!-- Mapa -->
                   <h2 class="mas-campos" onClick="generarMapa();">Marcar dirección en mapa</h2>
                  <div id="multimedia_mapas" class="Video" style="display:none;"></div>
                  <?php
                  $seleccionado['field_aviso_latitud'] = '';
                  $seleccionado['field_aviso_longitud'] = '';
                  if(isset($_POST['field_aviso_latitud'])) {
                    $seleccionado['field_aviso_latitud'] = $_POST['field_aviso_latitud'];
                    $seleccionado['field_aviso_longitud'] = $_POST['field_aviso_longitud'];
                  } elseif(isset($node['field_aviso_mapa'][0]->latitude)) {
                    $seleccionado['field_aviso_latitud'] = $node['field_aviso_mapa'][0]->latitude;
                    $seleccionado['field_aviso_longitud'] = $node['field_aviso_mapa'][0]->longitude;
                  }
                  ?>
                  <input id="map-latitud" name="field_aviso_latitud" type="hidden" value="<?php print $seleccionado['field_aviso_latitud']; ?>" />
                  <input id="map-longitud" name="field_aviso_longitud" type="hidden" value="<?php print $seleccionado['field_aviso_longitud']; ?>" />
                </div>
                
                <!-- CARACTERISTICAS -->
                <?php if(!in_array($rubro, array('aviso_producto', 'aviso_servicio', 'aviso_rural', 'aviso_repuesto', 'aviso_emprendimiento'))) { ?>
                  <h2 id="ver-caracteristicas" class="mas-campos" onClick="masContenido('content_caracteristicas', 'ver-caracteristicas');">Características</h2>
                  <div id="content_caracteristicas" style="display:none;">
                    <?php if(array_key_exists('field_aviso_apto_escritura', $options)) { ?>
                      <!-- Apto Escritura -->
                      <div class="campo"><?php if($options['field_aviso_apto_escritura']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_apto_escritura']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_apto_escritura']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_apto_escritura']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_apto_escritura']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_combustible', $options)) { ?>
                      <!-- Combustible -->
                      <div class="campo"><label><?php if($options['field_aviso_combustible']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?></label>
                      <select name="<?php print $options['field_aviso_combustible']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_combustible']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_combustible']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_combustible']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_apto_credito', $options)) { ?>
                      <!-- Apto Credito -->
                      <div class="campo"><label><?php if($options['field_aviso_apto_credito']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?></label>
                      <select name="<?php print $options['field_aviso_apto_credito']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_apto_credito']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_apto_credito']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_apto_credito']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_casa', 'aviso_cochera', 'aviso_departamento', 'aviso_galpon_deposito', 'aviso_local', 'aviso_oficina'))){ ?>
                      <!-- Antiguedad -->
                      <div class="campo"><input name="field_aviso_antiguedad" placeholder="Antigüedad" type="text" value="<?php print $seleccionado['field_aviso_antiguedad']; ?>" /></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_estrenar', $options)) { ?>
                      <!-- Estrenar -->
                      <div class="campo"><label for="checkbox_estrenar"><?php if($options['field_aviso_estrenar']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_estrenar']['name']; ?></label>
                      <?php foreach($options['field_aviso_estrenar']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_estrenar']['key']; ?>" id="checkbox_estrenar" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_estrenar']['key']]) print 'checked="checked"'; ?> onClick="mostrarEtapaConst(this, 'campo-etapa-const');" />
                      <?php } ?>
                      </div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_etapa_construccion', $options)) { ?>
                      <!-- Etapa Construccion -->
                      <div class="campo" id="campo-etapa-const" <?php if($seleccionado['field_aviso_estrenar'] != 1) print 'style="display:none;"'; ?>><?php if($options['field_aviso_etapa_construccion']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_etapa_construccion']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_etapa_construccion']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_etapa_construccion']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_etapa_construccion']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select>
                      </div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_oficina'))){ ?>
                      <!-- Cantidad privados -->
                      <div class="campo"><input name="field_aviso_cantidad_privados" placeholder="Cantidad de Privados" type="text" value="<?php print $seleccionado['field_aviso_cantidad_privados']; ?>" /></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_ubicacion', $options)) { ?>
                      <!-- Orientacion -->
                      <div class="campo"><?php if($options['field_aviso_ubicacion']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_ubicacion']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_ubicacion']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_ubicacion']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_ubicacion']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_departamento'))){ ?>
                      <!-- Piso numero -->
                      <div class="campo"><input name="field_aviso_piso_numero" placeholder="Piso Número" type="text" value="<?php print $seleccionado['field_aviso_piso_numero']; ?>" /></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_cantidad_dormitorios', $options)) { ?>
                      <!-- Cantidad Dormitorios -->
                      <div class="campo"><?php if($options['field_aviso_cantidad_dormitorios']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_cantidad_dormitorios']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_cantidad_dormitorios']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_cantidad_dormitorios']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_cantidad_dormitorios']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_cantidad_plantas', $options)) { ?>
                      <!-- Cantidad Plantas -->
                      <div class="campo"><?php if($options['field_aviso_cantidad_plantas']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_cantidad_plantas']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_cantidad_plantas']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_cantidad_plantas']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_cantidad_plantas']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_cantidad_banios', $options)) { ?>
                      <!-- Cantidad Banios -->
                      <div class="campo"><?php if($options['field_aviso_cantidad_banios']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_cantidad_banios']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_cantidad_banios']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_cantidad_banios']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_cantidad_banios']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_tipo_banio', $options)) { ?>
                      <!-- Tipo Banios -->
                      <div class="campo"><?php if($options['field_aviso_tipo_banio']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_tipo_banio']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_tipo_banio']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_tipo_banio']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_banio']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_tipo_techo', $options)) { ?>
                      <!-- Tipo techo industrial -->
                      <div class="campo"><?php if($options['field_aviso_tipo_techo']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_tipo_techo']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_tipo_techo']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_tipo_techo']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_techo']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_tipo_porton', $options)) { ?>
                      <!-- Tipo porton -->
                      <div class="campo"><?php if($options['field_aviso_tipo_porton']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_tipo_porton']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_tipo_porton']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_tipo_porton']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_porton']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_cantidad_cocheras', $options)) { ?>
                      <!-- Cantidad Cochera -->
                      <div class="campo"><?php if($options['field_aviso_cantidad_cocheras']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_cantidad_cocheras']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_cantidad_cocheras']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_cantidad_cocheras']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_cantidad_cocheras']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_cobertura_cochera', $options)) { ?>
                      <!-- Cobertura Cochera -->
                      <div class="campo"><?php if($options['field_aviso_cobertura_cochera']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_cobertura_cochera']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_cobertura_cochera']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_cobertura_cochera']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_cobertura_cochera']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_tipo_cochera', $options)) { ?>
                      <!-- Tipo Cochera -->
                      <div class="campo"><?php if($options['field_aviso_tipo_cochera']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_tipo_cochera']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_tipo_cochera']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_tipo_cochera']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_cochera']['key']]) print 'selected="selecterd"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_casa', 'aviso_galpon_deposito', 'aviso_local', 'aviso_oficina'))){ ?>
                      <!-- Superficie Total -->
                      <div class="campo"><input name="field_aviso_sup_total" placeholder="Superficie Total" type="text" value="<?php print $seleccionado['field_aviso_sup_total']; ?>" /></div>
                      <!-- Superfcie Cubierta -->
                      <div class="campo"><input name="field_aviso_sup_cubierta" placeholder="Superficie Cubierta" type="text" value="<?php print $seleccionado['field_aviso_sup_cubierta']; ?>" /></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_campo'))){ ?>
                      <!-- Distancia Pavimento -->
                      <div class="campo"><input name="field_aviso_distancia_pavimento" placeholder="Distancia del Pavimento" type="text" value="<?php print $seleccionado['field_aviso_distancia_pavimento']; ?>" /></div>
                      <!-- Cantidad Hectarias -->
                      <div class="campo"><input name="field_aviso_cantidad_hectarias" placeholder="Cantidad de Hectarias" type="text" value="<?php print $seleccionado['field_aviso_cantidad_hectarias']; ?>" /></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_galpon_deposito'))){ ?>
                      <!-- Ancho entrada -->
                      <div class="campo"><input name="field_aviso_ancho_entrada" placeholder="Ancho de la Entrada" type="text" value="<?php print $seleccionado['field_aviso_ancho_entrada']; ?>" /></div>
                      <!-- Alto entrada -->
                      <div class="campo"><input name="field_aviso_alto_entrada" placeholder="Alto de la Entrada" type="text" value="<?php print $seleccionado['field_aviso_alto_entrada']; ?>" /></div>
                      <!-- Cantidad columnas -->
                      <div class="campo"><input name="field_aviso_cantidad_columnas" placeholder="Cantidad de Columnas" type="text" value="<?php print $seleccionado['field_aviso_cantidad_columnas']; ?>" /></div>
                      <!-- Cantidad naves -->
                      <div class="campo"><input name="field_aviso_cantidad_naves" placeholder="Cantidad de Naves" type="text" value="<?php print $seleccionado['field_aviso_cantidad_naves']; ?>" /></div>
                      <!-- Patron municipal -->
                      <div class="campo"><input name="field_aviso_patron_municipalidad" placeholder="Patrón de Municipalidad Córdoba" type="text" value="<?php print $seleccionado['field_aviso_patron_municipalidad']; ?>" /></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_terreno_lote'))){ ?>
                      <!-- Superficie terreno -->
                      <div class="campo"><input name="field_aviso_superficie_terreno" placeholder="Superficie Terreno" type="text" value="<?php print $seleccionado['field_aviso_superficie_terreno']; ?>" /></div>
                      <!-- Metros frente -->
                      <div class="campo"><input name="field_aviso_metros_frente" placeholder="Metros de Frente" type="text" value="<?php print $seleccionado['field_aviso_metros_frente']; ?>" /></div>
                      <!-- Metros fondo -->
                      <div class="campo"><input name="field_aviso_metros_fondo" placeholder="Metros de Fondo" type="text" value="<?php print $seleccionado['field_aviso_metros_fondo']; ?>" /></div>
                      <!-- Superficie construible -->
                      <div class="campo"><input name="field_aviso_superficie_construib" placeholder="Superficie Construible" type="text" value="<?php print $seleccionado['field_aviso_superficie_construib']; ?>" /></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_cochera', 'aviso_departamento', 'aviso_local', 'aviso_oficina'))){ ?>
                      <!-- Expensas -->
                      <div class="campo"><input name="field_aviso_expensas" placeholder="Expensas" type="text" value="<?php print $seleccionado['field_aviso_expensas']; ?>" /></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_ganaderia', $options)) { ?>
                      <!-- Ganaderia -->
                      <div class="campo"><label for="checkbox_ganaderia"><?php if($options['field_aviso_ganaderia']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_ganaderia']['name']; ?></label>
                      <?php foreach($options['field_aviso_ganaderia']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_ganaderia']['key']; ?>" id="checkbox_ganaderia" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_ganaderia']['key']]) print 'checked="checked"'; ?> />
                      <?php } ?>
                      </div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_agricultura', $options)) { ?>
                      <!-- Agricultura -->
                      <div class="campo"><label for="checkbox_agricultura"><?php if($options['field_aviso_agricultura']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_agricultura']['name']; ?></label>
                      <?php foreach($options['field_aviso_agricultura']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_agricultura']['key']; ?>" id="checkbox_agricultura" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_agricultura']['key']]) print 'checked="checked"'; ?> />
                      <?php } ?>
                      </div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_casa_principal', $options)) { ?>
                      <!-- Casa Principal -->
                      <div class="campo"><label for="checkbox_casa"><?php if($options['field_aviso_casa_principal']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_casa_principal']['name']; ?></label>
                      <?php foreach($options['field_aviso_casa_principal']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_casa_principal']['key']; ?>" id="checkbox_casa" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_casa_principal']['key']]) print 'checked="checked"'; ?> />
                      <?php } ?>
                      </div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_casa_casero', $options)) { ?>
                      <!-- Casa de Casero -->
                      <div class="campo"><label for="checkbox_casero"><?php if($options['field_aviso_casa_casero']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_casa_casero']['name']; ?></label>
                      <?php foreach($options['field_aviso_casa_casero']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_casa_casero']['key']; ?>" id="checkbox_casero" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_casa_casero']['key']]) print 'checked="checked"'; ?> />
                      <?php } ?>
                      </div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_en_edificio', $options)) { ?>
                      <!-- En Edificio -->
                      <div class="campo"><label for="checkbox_en_edificio"><?php if($options['field_aviso_en_edificio']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_en_edificio']['name']; ?></label>
                      <?php foreach($options['field_aviso_en_edificio']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_en_edificio']['key']; ?>" id="checkbox_en_edificio" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_en_edificio']['key']]) print 'checked="checked"'; ?> />
                      <?php } ?>
                      </div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_galeria_shopping', $options)) { ?>
                      <!-- Galeria o Shopping -->
                      <div class="campo"><label for="checkbox_en_galeria"><?php if($options['field_aviso_galeria_shopping']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_galeria_shopping']['name']; ?></label>
                      <?php foreach($options['field_aviso_galeria_shopping']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_galeria_shopping']['key']; ?>" id="checkbox_en_galeria" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_galeria_shopping']['key']]) print 'checked="checked"'; ?> />
                      <?php } ?>
                      </div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_vigilancia', $options)) { ?>
                      <!-- Vigilania -->
                      <div class="campo"><label for="checkbox_vigilancia"><?php if($options['field_aviso_vigilancia']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_vigilancia']['name']; ?></label>
                      <?php foreach($options['field_aviso_vigilancia']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_vigilancia']['key']; ?>" id="checkbox_vigilancia" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_vigilancia']['key']]) print 'checked="checked"'; ?> />
                      <?php } ?>
                      </div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_baulera', $options)) { ?>
                      <!-- Baulera -->
                      <div class="campo"><label for="checkbox_baulera"><?php if($options['field_aviso_baulera']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_baulera']['name']; ?></label>
                      <?php foreach($options['field_aviso_baulera']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_baulera']['key']; ?>" id="checkbox_baulera" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_baulera']['key']]) print 'checked="checked"'; ?> />
                      <?php } ?>
                      </div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_propiedad_ocupada', $options)) { ?>
                      <!-- Propiedad Ocupada -->
                      <div class="campo"><label for="checkbox_ocupada"><?php if($options['field_aviso_propiedad_ocupada']['requerido'] == 1){ print '<span class="requerido">*</span> '; } print $options['field_aviso_propiedad_ocupada']['name']; ?></label>
                      <?php foreach($options['field_aviso_propiedad_ocupada']['opciones'] as $option){ ?>
                        <input type="checkbox" name="<?php print $options['field_aviso_propiedad_ocupada']['key']; ?>" id="checkbox_ocupada" value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_propiedad_ocupada']['key']]) print 'checked="checked"'; ?> />
                      <?php } ?>
                      </div>      
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_local'))){ ?>
                      <!-- Ultima actividad -->
                      <div class="campo"><input name="field_aviso_ultima_actividad" placeholder="Última Actividad" type="text" value="<?php print $seleccionado['field_aviso_ultima_actividad']; ?>" /></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_tipo_edificio', $options)) { ?>
                      <!-- Tipo Edificio -->
                      <div class="campo"><?php if($options['field_aviso_tipo_edificio']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_tipo_edificio']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_tipo_edificio']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_tipo_edificio']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_tipo_edificio']['key']]) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_condicion', $options)) { ?>
                      <!-- Condicion inmueble -->
                      <div class="campo"><?php if($options['field_aviso_condicion']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_condicion']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_condicion']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_condicion']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_condicion']['key']]) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_departamento'))){ ?>
                      <!-- Deptos por piso -->
                      <div class="campo"><input name="field_aviso_dpto_piso" placeholder="Departamentos por Piso" type="text" value="<?php print $seleccionado['field_aviso_dpto_piso']; ?>" /></div>
                      <!-- Cantidad pisos -->
                      <div class="campo"><input name="field_aviso_cantidad_pisos" placeholder="Cantidad de Pisos" type="text" value="<?php print $seleccionado['field_aviso_cantidad_pisos']; ?>" /></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_plan_ahorro'))){ ?>
                      <!-- Valor cuota -->
                      <div class="campo"><input name="field_aviso_valor_cuota" placeholder="Valor Cuota" type="text" value="<?php print $seleccionado['field_aviso_valor_cuota']; ?>" /></div>
                      <span class="detalle">Valor de la Cuota en Pesos.</span>
                      <!-- Valor cuota promedio -->
                      <div class="campo"><input name="field_aviso_valor_cuota_promedio" placeholder="Valor Cuota promedio" type="text" value="<?php print $seleccionado['field_aviso_valor_cuota_promedio']; ?>" /></div>
                      <span class="detalle">Valor de la Cuota promedio en Pesos.</span>
                      <!-- Valor cuota suscripcion -->
                      <div class="campo"><input name="field_aviso_valor_cuota_suscrip" placeholder="Valor cuota suscripción" type="text" value="<?php print $seleccionado['field_aviso_valor_cuota_suscrip']; ?>" /></div>
                      <span class="detalle">Valor de la Cuota suscripción en Pesos.</span>
                    <?php } ?>
                    <?php if(array_key_exists('segmento_autos', $options)) { ?>
                      <!-- Segmento Auto y plan -->
                      <div class="campo"><?php if($options['segmento_autos']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['segmento_autos']['key']; ?>">
                        <option value="" label="- <?php print $options['segmento_autos']['name']; ?> -"></option>
                        <?php foreach($options['segmento_autos']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_4x4', 'aviso_auto', 'aviso_plan_ahorro', 'aviso_utilitario'))){ ?>
                      <!-- Version -->
                      <div class="campo"><input name="field_aviso_version" placeholder="Versión" type="text" value="<?php print $seleccionado['field_aviso_version']; ?>" /></div>
                    <?php } ?>
                    <?php if(array_key_exists('segmento_motos', $options)) { ?>
                      <!-- Segmento moto -->
                      <div class="campo"><?php if($options['segmento_motos']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['segmento_motos']['key']; ?>">
                        <option value="" label="- <?php print $options['segmento_motos']['name']; ?> -"></option>
                        <?php foreach($options['segmento_motos']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['segmento_motos']['key']]) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(in_array($rubro, array('aviso_moto'))){ ?>
                      <!-- Cilindradas -->
                      <div class="campo"><input name="field_aviso_cilindrada" placeholder="Cilindrada" type="text" value="<?php print $seleccionado['field_aviso_cilindrada']; ?>" /></div>
                    <?php } ?>
                    <?php if(array_key_exists('color', $options)) { ?>
                      <!-- Color -->
                      <div class="campo"><?php if($options['color']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['color']['key']; ?>">
                        <option value="" label="- <?php print $options['color']['name']; ?> -"></option>
                        <?php foreach($options['color']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_transmision', $options)) { ?>
                      <!-- Transmision -->
                      <div class="campo"><?php if($options['field_aviso_transmision']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_transmision']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_transmision']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_transmision']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_transmision']['key']]) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                    <?php if(array_key_exists('field_aviso_cant_puertas', $options)) { ?>
                      <!-- Cantidad de puertas -->
                      <div class="campo"><?php if($options['field_aviso_cant_puertas']['requerido'] == 1){ print '<span class="requerido">*</span> '; } ?>
                      <select name="<?php print $options['field_aviso_cant_puertas']['key']; ?>">
                        <option value="" label="- <?php print $options['field_aviso_cant_puertas']['name']; ?> -"></option>
                        <?php foreach($options['field_aviso_cant_puertas']['opciones'] as $option){ ?>
                          <option value="<?php print $option['key']; ?>" <?php if($option['key'] == $seleccionado[$options['field_aviso_cant_puertas']['key']]) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
                        <?php } ?>
                      </select></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                
                <?php if(array_key_exists('ambientes_casas', $options)) { ?>
                  <!-- Ambientes Casa -->
                  <h2 id="ver-ambientes" class="mas-campos" onClick="masContenido('content_ambientes', 'ver-ambientes');"><?php print $options['ambientes_casas']['name']; ?></h2>
                  <div id="content_ambientes" style="display:none;">
                    <?php foreach($options['ambientes_casas']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_ambientes_casa_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['ambientes_casas']['key']; ?>[<?php print $option['key']; ?>]" id="check_ambientes_casa_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <?php if(array_key_exists('ambientes_departamentos', $options)) { ?>
                  <!-- Ambientes depto -->
                  <h2 id="ver-ambientes" class="mas-campos" onClick="masContenido('content_ambientes', 'ver-ambientes');"><?php print $options['ambientes_departamentos']['name']; ?></h2>
                  <div id="content_ambientes" style="display:none;">
                    <?php foreach($options['ambientes_departamentos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_ambientes_dpto_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['ambientes_departamentos']['key']; ?>[<?php print $option['key']; ?>]" id="check_ambientes_dpto_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <?php if(array_key_exists('ambientes_locales', $options)) { ?>
                  <!-- Ambientes local -->
                  <h2 id="ver-ambientes" class="mas-campos" onClick="masContenido('content_ambientes', 'ver-ambientes');"><?php print $options['ambientes_locales']['name']; ?></h2>
                  <div id="content_ambientes" style="display:none;">
                    <?php foreach($options['ambientes_locales']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_ambientes_local_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['ambientes_locales']['key']; ?>[<?php print $option['key']; ?>]" id="check_ambientes_local_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <?php if(array_key_exists('ambientes_oficinas', $options)) { ?>
                  <!-- Ambientes oficina -->
                  <h2 id="ver-ambientes" class="mas-campos" onClick="masContenido('content_ambientes', 'ver-ambientes');"><?php print $options['ambientes_oficinas']['name']; ?></h2>
                  <div id="content_ambientes" style="display:none;">
                    <?php foreach($options['ambientes_oficinas']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_ambientes_oficina_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['ambientes_oficinas']['key']; ?>[<?php print $option['key']; ?>]" id="check_ambientes_oficina_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <?php if(array_key_exists('instalaciones_casas_y_departamentos', $options)) { ?>
                  <!-- Instalaciones Casa y Depto -->
                  <h2 id="ver-instalaciones" class="mas-campos" onClick="masContenido('content_instalaciones', 'ver-instalaciones');"><?php print $options['instalaciones_casas_y_departamentos']['name']; ?></h2>
                  <div id="content_instalaciones" style="display:none;">
                    <?php foreach($options['instalaciones_casas_y_departamentos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_instalacioes_casa_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_casas_y_departamentos']['key']; ?>[<?php print $option['key']; ?>]" id="check_instalacioes_casa_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <?php if(array_key_exists('instalaciones_campos', $options)) { ?>
                  <!-- Instalaciones Campo -->
                  <h2 id="ver-instalaciones" class="mas-campos" onClick="masContenido('content_instalaciones', 'ver-instalaciones');"><?php print $options['instalaciones_campos']['name']; ?></h2>
                  <div id="content_instalaciones" style="display:none;">
                    <?php foreach($options['instalaciones_campos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_instalacioes_campo_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_campos']['key']; ?>[<?php print $option['key']; ?>]" id="check_instalacioes_campo_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Instalaciones galpon -->
                <?php if(array_key_exists('instalaciones_galpones_y_depositos', $options)) { ?>
                  <h2 id="ver-instalaciones" class="mas-campos" onClick="masContenido('content_instalaciones', 'ver-instalaciones');"><?php print $options['instalaciones_galpones_y_depositos']['name']; ?></h2>
                  <div id="content_instalaciones" style="display:none;">
                    <?php foreach($options['instalaciones_galpones_y_depositos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_instalacioes_galpon_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_galpones_y_depositos']['key']; ?>[<?php print $option['key']; ?>]" id="check_instalacioes_galpon_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Instalaciones local -->
                <?php if(array_key_exists('instalaciones_locales', $options)) { ?>
                  <h2 id="ver-instalaciones" class="mas-campos" onClick="masContenido('content_instalaciones', 'ver-instalaciones');"><?php print $options['instalaciones_locales']['name']; ?></h2>
                  <div id="content_instalaciones" style="display:none;">
                    <?php foreach($options['instalaciones_locales']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_instalacioes_local_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_locales']['key']; ?>[<?php print $option['key']; ?>]" id="check_instalacioes_local_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Instalaciones oficina -->
                <?php if(array_key_exists('instalaciones_oficinas', $options)) { ?>
                  <h2 id="ver-instalaciones" class="mas-campos" onClick="masContenido('content_instalaciones', 'ver-instalaciones');"><?php print $options['instalaciones_oficinas']['name']; ?></h2>
                  <div id="content_instalaciones" style="display:none;">
                    <?php foreach($options['instalaciones_oficinas']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_instalacioes_oficina_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['instalaciones_oficinas']['key']; ?>[<?php print $option['key']; ?>]" id="check_instalacioes_oficina_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Amenities Depto -->
                <?php if(array_key_exists('amenities_departamentos', $options)) { ?>
                  <h2 id="ver-amenities" class="mas-campos" onClick="masContenido('content_amenities', 'ver-amenities');"><?php print $options['amenities_departamentos']['name']; ?></h2>
                  <div id="content_amenities" style="display:none;">
                    <?php foreach($options['amenities_departamentos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_amenities_dpto_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['amenities_departamentos']['key']; ?>[<?php print $option['key']; ?>]" id="check_amenities_dpto_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Servicios Casa -->
                <?php if(array_key_exists('servicios_casas', $options)) { ?>
                  <h2 id="ver-servicios" class="mas-campos" onClick="masContenido('content_servicios', 'ver-servicios');"><?php print $options['servicios_casas']['name']; ?></h2>
                  <div id="content_servicios" style="display:none;">
                    <?php foreach($options['servicios_casas']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_servicio_casa_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_casas']['key']; ?>[<?php print $option['key']; ?>]" id="check_servicio_casa_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Servicios Campo -->
                <?php if(array_key_exists('servicios_campos', $options)) { ?>
                  <h2 id="ver-servicios" class="mas-campos" onClick="masContenido('content_servicios', 'ver-servicios');"><?php print $options['servicios_campos']['name']; ?></h2>
                  <div id="content_servicios" style="display:none;">
                    <?php foreach($options['servicios_campos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_servicio_campo_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_campos']['key']; ?>[<?php print $option['key']; ?>]" id="check_servicio_campo_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Servicios Depto -->
                <?php if(array_key_exists('servicios_departamentos', $options)) { ?>
                  <h2 id="ver-servicios" class="mas-campos" onClick="masContenido('content_servicios', 'ver-servicios');"><?php print $options['servicios_departamentos']['name']; ?></h2>
                  <div id="content_servicios" style="display:none;">
                    <?php foreach($options['servicios_departamentos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_servicio_dpto_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_departamentos']['key']; ?>[<?php print $option['key']; ?>]" id="check_servicio_dpto_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Servicios galpon -->
                <?php if(array_key_exists('servicios_galpones_depositos', $options)) { ?>
                  <h2 id="ver-servicios" class="mas-campos" onClick="masContenido('content_servicios', 'ver-servicios');"><?php print $options['servicios_galpones_depositos']['name']; ?></h2>
                  <div id="content_servicios" style="display:none;">
                    <?php foreach($options['servicios_galpones_depositos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_servicio_galpon_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_galpones_depositos']['key']; ?>[<?php print $option['key']; ?>]" id="check_servicio_galpon_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Servicios local -->
                <?php if(array_key_exists('servicios_locales', $options)) { ?>
                  <h2 id="ver-servicios" class="mas-campos" onClick="masContenido('content_servicios', 'ver-servicios');"><?php print $options['servicios_locales']['name']; ?></h2>
                  <div id="content_servicios" style="display:none;">
                    <?php foreach($options['servicios_locales']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_servcio_local_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_locales']['key']; ?>[<?php print $option['key']; ?>]" id="check_servcio_local_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Servicios oficina -->
                <?php if(array_key_exists('servicios_oficinas', $options)) { ?>
                  <h2 id="ver-servicios" class="mas-campos" onClick="masContenido('content_servicios', 'ver-servicios');"><?php print $options['servicios_oficinas']['name']; ?></h2>
                  <div id="content_servicios" style="display:none;">
                    <?php foreach($options['servicios_oficinas']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_servcio_oficina_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_oficinas']['key']; ?>[<?php print $option['key']; ?>]" id="check_servcio_oficina_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Confort -->
                <?php if(array_key_exists('confort', $options)) { ?>
                  <h2 id="ver-confort" class="mas-campos" onClick="masContenido('content_confort', 'ver-confort');"><?php print $options['confort']['name']; ?></h2>
                  <div id="content_confort" style="display:none;">
                    <?php foreach($options['confort']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_confort_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['confort']['key']; ?>[<?php print $option['key']; ?>]" id="check_confort_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Seguridad -->
                <?php if(array_key_exists('seguridad', $options)) { ?>
                  <h2 id="ver-seguiridad" class="mas-campos" onClick="masContenido('content_seguridad', 'ver-seguiridad');"><?php print $options['seguridad']['name']; ?></h2>
                  <div id="content_seguridad" style="display:none;">
                    <?php foreach($options['seguridad']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_seguridad_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['seguridad']['key']; ?>[<?php print $option['key']; ?>]" id="check_seguridad_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Amenities Emprendimiento -->
                <?php if(array_key_exists('amenities_emprendimientos', $options)) { ?>
                  <h2 id="ver-amenities" class="mas-campos" onClick="masContenido('content_amenities', 'ver-amenities');"><?php print $options['amenities_emprendimientos']['name']; ?></h2>
                  <div id="content_amenities" style="display:none;">
                    <?php foreach($options['amenities_emprendimientos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_amenities_emprendimientos_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['amenities_emprendimientos']['key']; ?>[<?php print $option['key']; ?>]" id="check_amenities_emprendimientos_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <!-- Servicios Emprendimiento -->
                <?php if(array_key_exists('servicios_emprendimientos', $options)) { ?>
                  <h2 id="ver-servicios" class="mas-campos" onClick="masContenido('content_servicios', 'ver-servicios');"><?php print $options['servicios_emprendimientos']['name']; ?></h2>
                  <div id="content_servicios" style="display:none;">
                    <?php foreach($options['servicios_emprendimientos']['opciones'] as $option){ ?>
                      <div class="campo"><label for="check_servicios_emprendimientos_<?php print $option['key']; ?>"><?php print $option['name']; ?></label><input type="checkbox" name="<?php print $options['servicios_emprendimientos']['key']; ?>[<?php print $option['key']; ?>]" id="check_servicios_emprendimientos_<?php print $option['key']; ?>" value="<?php print $option['key']; ?>" <?php if(in_array($option['key'], $taxonomias)) print 'checked="checked"'; ?> /></div>
                    <?php } ?>
                  </div>
                <?php } ?>
                <h2>Legales y promociones</h2>
                <!-- Terminos y condicioens -->
                <div class="campo"><label for="check_uso_servicio"><span class="requerido">*</span> Acepto las condiciones de utilización del servicio</label>
                  <input type="checkbox" name="field_aviso_terminos" id="check_uso_servicio" value="1" checked="checked" />
                </div>
                <!-- Recibir mails -->
                <div class="campo"><label for="check_promociones">Aceptación recepción de ofertas y promociones</label>
                  <input type="checkbox" name="field_aviso_promociones" id="check_promociones" value="1" checked="checked" />
                </div>
                <input type="hidden" name="nid" value="<?php if(isset($node['nid'])) print $node['nid']; ?>">
                <input id="submit-aviso" value="<?php if(isset($node) && $node!=FALSE) echo 'Actualizar Aviso'; else echo 'Crear Aviso'; ?>" type="submit" onClick="javascript:bloquearSubmit();return false;"/>
              </form>
            <?php } ?>
          </div>	
        </div>
    
    <?php } ?>
      </div>	
    </div>
    
    <div class="message-legal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Está expresamente prohibido ofrecer o publicar material que quebrante cualquier legislación vigente (ejemplo Decreto Nacional 936/11 y Decreto Provincial 365/12). Tampoco está permitido publicar material inmoral, obsceno, pornográfico, discriminatorio, injurioso o cualquier otro contenido que afecte la privacidad de las personas, la propiedad intelectual o que sea contrario a las buenas costumbres. </div>
    
  </article>
</div>
<script src="http://maps.google.com/maps/api/js?key=<?php print $this->config->item('gmap_api'); ?>&amp;sensor=false" type="text/javascript"></script>
<?php
if(isset($node['field_aviso_fotos']) && count($node['field_aviso_fotos'])>1){ ?>
<script type="text/javascript" src="/public/js/swipe.js"></script>
<script type="text/javascript">
  window.slider = new Swipe(document.getElementById('slider'),0);
</script>
<?php } ?>