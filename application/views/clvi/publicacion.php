<div id="content" class="admin publicacion">
<div class="container">
  <div class="row">
    <div class="twelvecol subtitulo">
      <span><?php echo $publicacion->titulo; ?></span>
    </div>
    <div class="twelvecol desc">
      <span class="aplicado"></span>Espacio y mejoras ya aplicados al aviso.
    </div>
    <div class="twelvecol desc clear">
      <span class="espera"></span>Espacio y mejoras ya selecc. y en espera de pago.
    </div>
    <?php if(!empty($mensaje)) : ?>
    <div class="twelvecol mensaje clear">
      <span class="error"><?php echo $mensaje; ?></span><br>
      <span class="error">
      <?php
      if(!empty($errores)) {
        foreach($errores as $error) {
          echo $error.'.<br>';
        }
      }
      ?>
      </span>
    </div>
    <?php endif; ?>
    <form method="post" id="form-publicacion">
    
    <nav id="confirm_publicacion" class="menu ">
      <p class="mensaje">Al editar esta publicación perderá el espacio que aplicaba sobre el aviso. ¿Desea continuar de todos modos?</p>
      <a href="javascript:void(0);" class="link-aceptar">Aceptar</a>
      <a href="javascript:void(0);" class="link-registro">Cancelar</a>
    </nav>
    
    <div class="twelvecol campo <?php echo (isset($errores['espacio'])?'error':''); ?>">
      <h2>Destaque</h2>
      <ul>
<?php
$espacio_seleccion = $publicacion->espacios->seleccionado;
if(isset($form->espacio)) {
  $espacio_seleccion = $form->espacio;
}
foreach($publicacion->espacios->opciones as $key => $opcion) {
  $extra = '$'.$opcion->precio;
  $precio = $opcion->precio;
  if($opcion->disponibles>0) {
    $extra = 'disponibles '.$opcion->disponibles.' de '.$opcion->adquiridos;
    $precio = 0;
  }
  if($publicacion->espacios->seleccionado==$key) {
    $extra = '';
    $precio = 0;
  }
  if($key==6) {
    $extra = 'Ilimitado';
  }
  print '<li class="'.(($publicacion->espacios->seleccionado==$key)?"seleccionado":"").'"><input type="radio" name="espacio" id="'.$key.'" value="'.$key.'" '.(($espacio_seleccion==$key)?"checked":"").'><label for="'.$key.'">'.$opcion->nombre.'</label> <span class="desc">'.$extra.'</span><input type="hidden" id="precio_espacio_'.$key.'" name="precio_espacio_'.$key.'" value="'.$precio.'"></li>';
}
?>
      </ul>
    </div>
    <div class="twelvecol campo">
      <h2>Mejoras</h2>
      <ul>
<?php
$papel_seleccionado = false;
$papel_aplicado = false;
$creditos_papel = array('credito_7597', 'credito_1029997', 'credito_1030068', 'credito_2387500');
foreach($publicacion->creditos as $key => $mejora) {
  $mejora_seleccionada = isset($mejora->checked)?$mejora->checked:false;
  if($mejora_seleccionada=='checked' && in_array($key, $creditos_papel)) {
    $papel_seleccionado = true;
    $papel_aplicado = true;
  }
  if(isset($form->mejoras)) {
    if(array_key_exists($key, $form->mejoras)) {
      $mejora_seleccionada = 'checked';
      if(in_array($key, $creditos_papel)) {
        $papel_seleccionado = true;
      }
    }
  }
  $extra = '$'.$mejora->precio;
  $precio = $mejora->precio;
  if(isset($mejora->disponibles) && $mejora->disponibles>0) {
    $extra = 'disponibles '.$mejora->disponibles.' de '.$mejora->adquiridos;
    $precio = 0;
  }
  if(isset($mejora->checked) && $mejora->checked=='checked') {
    $extra = '';
    $precio = 0;
  }
  print '<li class="'.((isset($mejora->checked) && $mejora->checked=='checked')?"seleccionado":"").(isset($errores[$key])?' error':'').'"><input type="checkbox" name="'.$key.'" id="'.$key.'" '.(($mejora_seleccionada=='checked')?"checked":"").'><label for="'.$key.'">'.$mejora->nombre.'</label> ';
  if(isset($mejora->select)) {
    $opcion_seleccionada = $mejora->select->seleccionado;
    if(isset($form->select_credito_banderola)) {
      $opcion_seleccionada = $form->select_credito_banderola;
    }
    print '<select name="select_'.$key.'">';
    foreach($mejora->select->opciones as $clave => $opcion) {
      print '<option value="'.$clave.'" '.($opcion_seleccionada==$clave?'selected':'').'>'.$opcion.'</option>';
    }
    print '</select> ';
  }
  print '<span class="desc">'.$extra.'</span><input type="hidden" id="precio_'.$key.'" name="precio_'.$key.'" value="'.$precio.'">';
  print '</li>';
}
?>
      </ul>
    </div>
<?php
if(isset($publicacion->web_papel->texto)) :
$texto = $publicacion->web_papel->texto;
if(isset($form->web_papel)) {
  $texto = $form->web_papel;
}
$class = '';
if(isset($errores['credito_7597']) || isset($errores['credito_1029997']) || isset($errores['credito_1030068']) || isset($errores['credito_2387500'])) {
  $class = 'error';
}
?>
    <div class="twelvecol campo <?php echo $papel_seleccionado?'':'DN'; ?> <?php echo $class; ?>" id="div_web_papel">
      <h2>Publicar en Papel</h2>
      <textarea name="web_papel" id="web_papel" cols="28" rows="3" <?php echo $papel_aplicado?'disabled':''; ?>><?php echo $texto; ?></textarea>
      <div class="papel-previsualizar" style="display: block;">
        <input type="button" class="preview <?php echo $papel_aplicado?'DN':''; ?>" value="Previsualizar">
        <div class="simulacion-papel"></div>
      </div>
      <div class="fechas">
        Fechas de publicación<br>
        <table class="tabla_dias">
          <tbody>
            <tr>
              <th>Dom <?php echo $publicacion->web_papel->dias[0]; ?></th>
              <th>Lun <?php echo $publicacion->web_papel->dias[1]; ?></th>
              <th>Mar <?php echo $publicacion->web_papel->dias[2]; ?></th>
              <th>Mié <?php echo $publicacion->web_papel->dias[3]; ?></th>
              <th>Jue <?php echo $publicacion->web_papel->dias[4]; ?></th>
              <th>Vie <?php echo $publicacion->web_papel->dias[5]; ?></th>
              <th>Sáb <?php echo $publicacion->web_papel->dias[6]; ?></th>
            </tr>
            <tr>
              <?php 
              for($i=0; $i<7; $i++) {
              ?>
              <td><?php echo (in_array($i, $publicacion->web_papel->dias_publicacion)==true)?'X':'' ?></td>
              <?php
              }
              ?>
            </tr>
          </tbody>
        </table><br>
        <span class="importante">Las fechas pueden variar dependiendo de la acreditación del pago</span>
      </div>
    </div>
<?php
endif;
if(isset($publicacion->sitios_externos)) :
?>
    <div class="twelvecol campo">
      <h2>Exportar a sitios externos</h2>
      <ul>
<?php
foreach($publicacion->sitios_externos->opciones as $key => $sitio) {
  if(isset($publicacion->sitios_externos->seleccionados->$key)) {
    print '<li class="checked"><input type="checkbox" id="'.$key.'" checked="checked" name="sitios_externos['.$key.']" value="'.$key.'"><label for="'.$key.'">'.$sitio.'</label></li>';
  } else {
    print '<li><input type="checkbox" id="'.$key.'" name="sitios_externos['.$key.']" value="'.$key.'"><label for="'.$key.'">'.$sitio.'</label></li>';
  }
}
?>
      </ul>
    </div>
<?php
endif;
?>
    <div class="twelvecol">
      <div class="total">Total: $<span id="precio-total">0.00</span></div>
      <input type="hidden" name="nid" value="<?php echo $nid; ?>">
      <input type="hidden" name="publicado" value="<?php echo $publicacion->publicado; ?>">
      <input type="hidden" name="espacio-aplicado" value="<?php echo $publicacion->espacios->seleccionado; ?>">
      <input type="submit" name="submit" value="Publicar" class="">
    </div>
    </form>
  </div>
</div>
</div>