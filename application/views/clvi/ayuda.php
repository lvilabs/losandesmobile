<?php //print_r($node); ?>
<div id="content">
	<article class="aviso">
    <div class="container">
      <div class="row"> 
        <div class="cajaTitulo">
          <div class="titulo"><h1>Ayuda</h1></div>    
        </div>
      </div>
    </div>
    
    <div class="message-ayuda ">Aquí podrás encontrar toda la información necesaria para que conozcas como utilizar el Sitio Clasificados Los Andes.</div>
    
  <div class="container">
    <div class="row">
      <div class="twelvecol">
        <div class="content cuerpo ayuda">
          
          <?php foreach($listado as $ayudas_agrupadas) { ?>
            <h2><?php print $ayudas_agrupadas[0]->taxonomy; ?></h2>
            <?php foreach($ayudas_agrupadas as $ayuda) { ?>
              <a class="link-pregunta close" href="javascript:void(0);" data-id="<?php print $ayuda->nid; ?>"><?php print $ayuda->pregunta; ?></a>
              <div class="texto respuesta" id="respuesta-<?php print $ayuda->nid; ?>"><?php print $ayuda->respuesta; ?></div>
            <?php } ?>
          <?php } ?>
          
        </div>
      </div>
    </div>
  </div>

</article>
</div>

<script type="text/javascript"> 
  jQuery(document).ready(function ($) {
    $('.link-pregunta').click(function() {
      var nid = $(this).attr('data-id');
      var clases = $(this).attr('class');
      if(clases.indexOf('close') == -1) {
        $(this).removeClass('opened');
        $(this).addClass('close');
        $('#respuesta-'+nid).hide('');
      } else {
        $(this).removeClass('close');
        $(this).addClass('opened');
        $('#respuesta-'+nid).show('');
      }
    });
  });
</script>