<div id="content">
<header class="barraInfo">
  <h1><?php if($listado->titulo == '') echo 'Resultado de búsqueda'; else echo $listado->titulo; ?></h1>
  <span class="numeroAvisos"><?php echo $listado->total; ?> avisos</span>
<?php if($key != ''){ ?>
  <span class="filtro map"><a id="link-map" href="/search/map/<?php print $key; ?>/<?php print $tids; ?>" rel="search">Map</a></span>
<?php } else { ?>
  <span class="filtro map"><a id="link-map" href="/search/map/<?php print $tids; ?>" rel="search">Map</a></span>
<?php } ?>
</header>

<?php if(!empty($filtros)){ ?>
<div><ul class="filtroActivo">	
<?php foreach($filtros as $filtro){ ?>
  <li><a href="<?php print $filtro['url']; ?>" rel="search"><?php print $filtro['nombre']; ?><span></span></a></li>
<?php   } ?>
</ul></div>
<?php } ?>

<?php
if($listado->total == 0){
?>
<!-- sin resultados -->
<section class="noticias">
  <div class="noticias sinResultados">
    <div class="fondoResaltado">
      <h2>Su búsqueda no produjo resultados</h2>
      <p>Compruebe si la ortografía es correcta, o trate de quitar los filtros.
Elimine las comillas que definen frases para buscar cada palabra individualmente. "Auto azul" encuentra menos resultados que Auto azul.</p>
    </div>
  </div>
</section>
<!-- fin resultados -->
<?php } else { ?>
<div id="infinity">
<?php  
  $count = 1;
  foreach($listado as $key => $item){
    if(isset($item->nid)){
      
      switch($item->type){
        case 'Motos, cuadriciclos y náutica':
          $item->type = 'Motos';
          break;
        case '4x4, 4x2 y todo terreno':
          $item->type = '4x4';
          break;
        case 'Utilitarios, pick-up y camiones':
          $item->type = 'Utilitarios';
          break;
        case 'Accesorios y Repuestos':
          $item->type = 'Repuestos';
          break;
      }
      
      if($count == 3){ 
        if(isset($adslots)) {
          foreach($adslots as $adslot) {
            if($adslot['adunit'] == 'dfp-rectangle_1') {
              print ads_get_banner($adslot['adunit']);
            }
          }
        }
      }
      if($count == 7){ 
        if(isset($adslots)) {
          foreach($adslots as $adslot) {
            if($adslot['adunit'] == 'dfp-rectangle_2') {
              print ads_get_banner($adslot['adunit']);
            }
          }
        }
      }
?>
  
  <div class="search-main">
    <div class="search-item">
      
      <input type="checkbox" class="favourite" name="favourite_<?php echo $item->nid ?>" id="favourite_<?php echo $item->nid ?>" data-nid="<?php echo $item->nid ?>">
      <label for="favourite_<?php echo $item->nid ?>" class="favourite-result transparent">
        <span class="fa">
          <i class="fa-star far"></i>
          <i class="fa-star fas"></i>
          <i class="fa-star fx"></i>
        </span>
      </label>
            
      <a href="/<?php echo $item->url_amigable ?>" alt="<?php echo $item->title ?>">  
        <div class="sc-img">
        
          <?php if($item->type == 'Productos') { ?>
            <img alt="<?php echo $item->subrubro.' '.$item->title.' a '.$item->field_aviso_moneda_value.$item->field_aviso_precio_value; ?>" src="<?php echo urldecode($item->filepath_imagen_producto_sc); ?>" />
          <?php } else { ?>
            <img alt="<?php echo $item->subrubro.' '.$item->title.' a '.$item->field_aviso_moneda_value.$item->field_aviso_precio_value; ?>" src="<?php echo urldecode($item->filepath_app); ?>" />
          <?php } ?>
          
          <div class="sc-price">
            <div><?php if($item->field_aviso_moneda_value != '') echo $item->field_aviso_moneda_value; else echo $item->field_aviso_precio_value; ?><?php if($item->field_aviso_precio_value != 'consultar') echo $item->field_aviso_precio_value; ?></div>
            
            <?php if(isset($item->precio_uva) && $item->precio_uva) { ?>
              <div class="search-precio-uva <?php print $item->nid; ?>" data-precio="<?php print str_replace('.', '', $item->field_aviso_precio_value); ?>" data-moneda="<?php print $item->field_aviso_moneda_value; ?>" data-aviso="<?php print $item->nid; ?>"></div>
            <?php } ?>
            
            <?php if($item->disponible_venta) { ?>
              <p class="mensajePago">Precio en 1 pago</p>
            <?php } ?>
            
          </div>
          
          <?php if(!$item->disponible_venta) { ?>
            <a class="link-search-contacto" href="#modal-contactar-buscador" data-nid="<?php print $item->nid; ?>" data-title="<?php print $item->title; ?>">Contactar</a>
          <?php } ?>
          
        </div>
        <div class="sc-content">
          <?php if($count <= 4) { ?>
            <h2 class="title-result"><?php echo $item->title; ?></h2>
          <?php } elseif($count <= 8) { ?>
            <h3 class="title-result"><?php echo $item->title; ?></h3>
          <?php } else { ?>
            <h4 class="title-result"><?php echo $item->title; ?></h4>
          <?php } ?>
          <div class="sc-features">
            <?php if(!empty($item->descuento)) { ?>
              <span class="descuento"><?php print '<strong>'.$item->descuento->porcentaje.'%</strong> OFF'; ?></span>
            <?php } ?>
            <?php if($item->disponible_venta) { ?>
              <a title="Comprar el producto" href="/<?php echo $item->url_amigable ?>#modal-compra-datos" class="btn-comprar"><span></span>Comprar</a>
            <?php } else { ?>
              <p class="sizemed"><a href="/search/result/<?php echo str_replace('"', '%22', $item->filtro_subrubro); ?>"><i class="fa fa-circle"></i><?php echo $item->type.' - '.$item->subrubro ?></a></p>
            <?php } ?>
          </div>
        </div>
      </a>
    </div>
  </div>
  
<?php
    }
    $count++;
  }
?>
</div>
<?php
  $aviso_pagina = $listado->page * $listado->page_result;
  if($aviso_pagina < $listado->total){
    $url_explode = explode('/', $_SERVER['REQUEST_URI']);
    $url_implode = '';
    $pagina_actual = 1;
    foreach($url_explode as $url){
      if(strpos($url,'page=')===false && $url != '')
        $url_implode .= '/'.$url;
      if(strpos($url,'page=')!==false) {
        $page_explode = explode('page=', $url);
        $pagina_actual = $page_explode[1];
      }
    }  
    $url_implode = str_replace('/search/result', '', $url_implode);
?>
  <input type="hidden" id="filtros" name="filtros" value="<?php print $url_implode; ?>" />
  <input type="hidden" id="pagina_inicio" name="pagina_inicio" value="<?php print $pagina_actual; ?>" />
  <input type="hidden" id="pagina_fin" name="pagina_fin" value="<?php print $pagina_actual; ?>" />
  <input type="hidden" id="ga_categoria" name="ga_categoria" value="<?php echo $listado->cxense->gclCategories; ?>" />
  <!--
  <section class="noticias">  
    <a class="mas-noticias" href="<?php print $url_implode.'/page='.$listado->page; ?>" rel="next">Ver más avisos</a>
  </section> 
  -->
  <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
  </div>
   
  <a href="#"” class="back-to-top">
    <i class="fa fa-arrow-circle-up"></i>
  </a>
  
<?php
  }
}
?>
</div>

<div id="modal-contactar-buscador" class="white-popup mfp-hide clearfix">
  <div class="contacto-header">Consultar por <span id="contacto-titulo"></span></div>
  <form action="" accept-charset="UTF-8" method="post" id="contactar-vendedor-formulario">
    <div class="messages" style="display: none;"></div>
    <div class="form-item">
     <input type="text" maxlength="128" name="contactar_vendedor_nombre" id="edit-contactar-vendedor-nombre" size="60" value="<?php if(!empty($contacto)) print $contacto['contacto_nombre']; ?>" placeholder="Nombre" class="form-text" required>
    </div>
    <div class="form-item">
     <input type="text" maxlength="128" name="contactar_vendedor_telefono" id="edit-contactar-vendedor-telefono" size="60" value="<?php if(!empty($contacto)) print $contacto['contacto_telefono']; ?>" placeholder="Teléfono" class="form-text">
    </div>
    <div class="form-item">
     <input type="text" maxlength="128" name="contactar_vendedor_mail" id="edit-contactar-vendedor-mail" size="60" value="<?php if(!empty($contacto)) print $contacto['contacto_mail']; ?>" placeholder="E-mail" class="form-text">
    </div>
    <div class="form-item">
     <textarea cols="60" rows="5" name="contactar_vendedor_consulta" id="edit-contactar-vendedor-consulta" placeholder="Escriba su consulta aquí..." class="form-textarea resizable" required><?php if(!empty($contacto)) print $contacto['contacto_consulta']; ?></textarea>
    </div>
    <div class="g-recaptcha" id="RecaptchaField1"></div>
    <input type="hidden" name="contactar_vendedor_estado" id="edit-contactar-vendedor-estado" value="1">
    <input type="hidden" name="contactar_vendedor_aviso_nid" id="edit-contactar-vendedor-aviso" value="">
    <input type="hidden" name="contactar_vendedor_buscador" id="edit-contactar-vendedor-buscador" value="1">
    <input type="submit" name="op" id="edit-contactar-vendedor-submit" value="Consultar" class="form-submit">
  </form>
</div>

<div class="blockbkg" id="bkg" style="visibility: hidden;"></div>
<div id="popover" class="popover Resultado" style="top: 20%; left: 5%;">
  <div class="arrow"></div>
  <h3 class="popover-title">Nueva Funcionalidad</h3>
  <div class="popover-content">¡Ahora podés marcar tus avisos como favoritos! <br>Seleccioná la estrella ubicada en el margen superior de cada aviso y guardalo para visualizarlo después desde el botón Estrella del menú de cabecera.</div>
</div>

<script type="text/javascript">
  var latitud = -32.88937;
  var longitud = -68.844343;
  document.addEventListener('DOMContentLoaded', localizame, false);
  function localizame() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(coordenadas, errorcoordenadas);
    } else {
      document.getElementById('link-map').href = document.getElementById('link-map').href + '/latitud=' + latitud + '/longitud=' + longitud;
    }
  }
  function coordenadas(position) {
    latitud = position.coords.latitude;
    longitud = position.coords.longitude;
    document.getElementById('link-map').href = document.getElementById('link-map').href + '/latitud=' + latitud + '/longitud=' + longitud;
  }
  function errorcoordenadas(position) {
    document.getElementById('link-map').href = document.getElementById('link-map').href + '/latitud=' + latitud + '/longitud=' + longitud;
  }
</script>