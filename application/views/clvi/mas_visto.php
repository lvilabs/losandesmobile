<section class="noticias mas-visto">
<h1>Más visto</h1>
<?php
foreach($mas_visto as $seccion => $list){
    foreach($list as $key => $item){
?>
    <article class="widget">
        <h2><a href="/node/<?php echo $item->nid;?>"><?php echo $item->titulo;?></a></h2>
        <footer>Hace <?php echo convertir_fecha($item->timestamp);?> en <a href="/taxonomy/term/<?php echo $item->tag_duro->tid;?>"><?php echo $item->tag_duro->name;?></a></footer>
    </article>
<?php
    }
}
?>
</section>
