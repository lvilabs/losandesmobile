<?php $parametros_filtro = str_replace('%3A', ':', $parametros_filtro); ?>

<div id="content">

<header class="barraInfo">

<?php
  if(in_array(6330, $rubros_principales)) {
    if(in_array(6338, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Campos</span>';
    } elseif(in_array(6331, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Casas</span>';
    } elseif(in_array(6339, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Cocheras</span>';
    } elseif(in_array(6334, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Departamentos</span>';
    } elseif(in_array(6332, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Emprendimientos</span>';
    } elseif(in_array(6337, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Galpones y Depósitos</span>';
    } elseif(in_array(6335, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Locales</span>';
    } elseif(in_array(6336, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Oficinas</span>';
    } elseif(in_array(6333, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Terrenos y Lotes</span>';
    } else {
      print '<span class="filtro rubroPadre"><i class="fa fa-home"></i> Inmuebles</span>';
    }
  } elseif(in_array(6323, $rubros_principales)) {
    if(in_array(6325, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-car"></i> 4x4</span>';
    } elseif(in_array(6329, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-car"></i> Repuestos</span>';
    } elseif(in_array(6324, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-car"></i> Autos</span>';
    } elseif(in_array(6326, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-car"></i> Motos</span>';
    } elseif(in_array(6328, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-car"></i> Palnes de Ahorro</span>';
    } elseif(in_array(6327, $rubros_principales)) {
      print '<span class="filtro rubroPadre"><i class="fa fa-car"></i> Utilitarios</span>';
    } else {
      print '<span class="filtro rubroPadre"><i class="fa fa-car"></i> Vehículos</span>';
    }
  } elseif(in_array(6106, $rubros_principales)) {
    print '<span class="filtro rubroPadre"><i class="fa fa-gift"></i> Productos</span>';
  } elseif(in_array(6017, $rubros_principales)) {
    print '<span class="filtro rubroPadre"><i class="fa fa-wrench"></i> Servicios</span>';
  } elseif(in_array(6000, $rubros_principales)) {
    print '<span class="filtro rubroPadre"><i class="fa fa-tag"></i> Rurales</span>';
  }
?>
  
<?php if($key != ''){ ?>
  <span class="filtro map"><a id="link-map" href="/search/map/<?php print $key; ?>/<?php print $parametros_filtro; ?>" rel="search">Map</a></span>
<?php } else { ?>
  <span class="filtro map"><a id="link-map" href="/search/map/<?php print $parametros_filtro; ?>" rel="search">Map</a></span>
<?php } ?>
  <span class="filtro listado"><a href="/search/result/<?php print $key; ?>/<?php print $parametros_filtro; ?>" rel="search"></a></span>
</header>

<?php if(!empty($filtros)){ ?>
<ul class="filtroActivo">	
<?php foreach($filtros as $filtro){ ?>
  <li><a href="<?php print str_replace(':', '%3A', $filtro['url']); ?>" rel="search"><?php print $filtro['nombre']; ?><span></span></a></li>
<?php   } ?>
</ul>
<?php } ?>

<div class="filter-mob">
  <form name="formFiltro" class="formFiltro" action="/filtros/submit_form/<?php print $parametros_filtro; ?>" method="post">
  <?php
    $existe_principal = array_intersect($rubros_principales, array(6323, 6330, 6106, 6000, 6017));
    $existe_secundario = array_intersect($rubros_principales, array(6325, 6329, 6324, 6326, 6328, 6327, 6338, 6331, 6339, 6334, 6332, 6337, 6335, 6336, 6333));     
    $existe_modelo = array_intersect($rubros_principales, array(6324, 6325, 6328, 6327)); 
    $existe_subrubro = array_intersect($rubros_principales, array(6329, 6106, 6000, 6017));
    $existe_tipo_vehiculo = array_intersect($rubros_principales, array(6323));
    $existe_inmueble = array_intersect($rubros_principales, array(6330));
    $inmuebles_tipo = array(6331, 6334, 6333);
  ?>
  
  <?php if(!empty($key)){ ?>
    <input type="hidden" name="key" value="<?php print $key.'/'; ?>"/>
  <?php } ?>
  
  <?php if(!empty($existe_principal)){ ?>
    <input type="hidden" name="raiz" value="<?php print 'tid:'.current($existe_principal); ?>"/>
    <?php if(!empty($existe_secundario)){ ?>
      <input type="hidden" name="sub-raiz" value="<?php print 'tid:'.current($existe_secundario); ?>"/>
    <?php } ?>
  <?php } ?>
  
  <?php
    //print_r($campos_filtros);
  ?>
  
  <div class="content-selects">
  
  <?php if(!isset($campos_filtros[0]['label'])){ ?>
    <div class="select-group">
      <i class="fa fa-bars"></i>
      <select name="subrubro" class="form-select" id="subrubro_link">
        <option value="">Rubro</option>
        <?php foreach($campos_filtros as $subrubro){
            if($subrubro['filtro'] == "tid:8688") continue; ?>
          <option value="<?php print $subrubro['filtro']; ?>" <?php if(strpos($parametros_filtro, $subrubro['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $subrubro['name']; ?></option>
        <?php } ?>
      </select>
    </div>
  <?php } ?>
  
  <?php $count_filtros = 0; ?>
  <?php foreach($campos_filtros as $campo){ ?>
    
    <!-- Tipo de Vehiculo -->
    <?php if(trim($campo['name']) == 'Vehículos' || in_array(trim($campo['label']), array('Tipo de Vehículo', 'Marca y Modelo', 'Subrubro', 'Marca'))){ ?>
      <?php if(in_array(trim($campo['label']), array('Tipo de Vehículo', 'Marca y Modelo', 'Subrubro', 'Marca'))) $campo['child_data'] = $campos_filtros; ?>
      <?php foreach($campo['child_data'] as $tipo_vehiculo){ ?>
        <?php if($tipo_vehiculo['label'] == 'Tipo de Vehículo' && !empty($tipo_vehiculo['child_data'])){ ?>
          <div class="select-group subrubros <?php if(trim($campo['label']) != 'Tipo de Vehículo') print 'hidden'; ?>" data-padre="<?php print $campo['filtro']; ?>">
            <i class="fa fa-car"></i>
            <select name="tipo_vehiculo" class="form-select" id="tipo_vehiculo_link">
              <option value=""><?php print $tipo_vehiculo['label']; ?></option>
              <?php foreach($tipo_vehiculo['child_data'] as $datos){ ?>
                <option value="<?php print $datos['filtro']; ?>" <?php if(strpos($parametros_filtro, $datos['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $datos['name']; ?></option>
              <?php } ?>
            </select>
          </div>
        <?php } ?>
        <?php if(in_array(trim($campo['label']), array('Marca y Modelo', 'Subrubro', 'Marca'))) $tipo_vehiculo = $campo; ?>
        <!-- Modelo de Vehiculo -->
        <?php foreach($tipo_vehiculo['child_data'] as $datos){ ?>
          <?php if(in_array(trim($campo['label']), array('Marca y Modelo', 'Subrubro', 'Marca'))) $datos = $campo; ?>
          <?php if(!empty($datos['child_data'])){ ?>
            <?php foreach($datos['child_data'] as $marcas){ ?>
              <?php if(in_array($marcas['label'], array('Marca y Modelo', 'Subrubro', 'Marca')) && !empty($marcas['child_data'])){ ?>
                <div class="select-group marcas <?php if(!in_array(trim($campo['label']), array('Marca y Modelo', 'Subrubro', 'Marca'))) print 'hidden'; ?>" data-padre="<?php print $datos['filtro']; ?>">
                  <i class="fas fa-tachometer-alt"></i>
                  <select name="marcas_<?php print $datos['tid']; ?>" class="form-select select-marcas" id="marcas_<?php print $datos['tid']; ?>_link">
                    <option value=""><?php if(in_array($marcas['label'], array('Marca y Modelo', 'Marca'))) print 'Marca'; else print 'Rubro repuestos'; ?></option>
                    <?php foreach($marcas['child_data'] as $datos_marca){ ?>
                      <option value="<?php print $datos_marca['filtro']; ?>" <?php if(strpos($parametros_filtro, $datos_marca['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $datos_marca['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <!-- Marca de Vehiculo -->
                <?php foreach($marcas['child_data'] as $datos_modelo){ ?>
                  <?php if(!empty($datos_modelo['child_data'])){ ?>
                    <div class="select-group modelos hidden" data-padre="<?php print $datos_modelo['filtro']; ?>">
                      <i class="fas fa-tachometer-alt"></i>
                      <select name="modelos_<?php print $datos_modelo['tid']; ?>" class="form-select" id="modelos_<?php print $datos_modelo['tid']; ?>_link">
                        <option value=""><?php if(in_array($marcas['label'], array('Marca y Modelo'))) print 'Modelo'; else print 'Sub-Rubro repuestos'; ?></option>
                        <?php foreach($datos_modelo['child_data'] as $modelo){ ?>
                          <option value="tid:<?php print $modelo['tid']; ?>" <?php if(strpos($parametros_filtro, $modelo['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $modelo['name']; ?></option>
                        <?php } ?>
                      </select>
                    </div>          
                  <?php } ?>
                <?php } ?>
              <?php } ?>
              <?php if(in_array(trim($campo['label']), array('Marca y Modelo', 'Subrubro', 'Marca'))) break; ?>
            <?php } ?>
          <?php } ?>
          <?php if(in_array(trim($campo['label']), array('Marca y Modelo', 'Subrubro', 'Marca'))) break; ?>
        <?php } ?>
        <!-- Otros filtros Vehiculos -->
        <?php foreach($campos_filtros as $filtros_vehiculo){ ?>
          <?php if(!empty($filtros_vehiculo['child_data'])){ ?>
            <?php if(in_array($filtros_vehiculo['label'], array('Tipo de Vendedor', 'Estado', 'Tipo de Combustible')) && !empty($filtros_vehiculo['child_data'])){ ?>
              <div class="select-group filtrosVehiculos" data-padre="<?php print $datos['filtro']; ?>">
                <i class="fa <?php if($filtros_vehiculo['label'] == 'Tipo de Vendedor') print 'fa-user'; if( $filtros_vehiculo['label'] == 'Estado') print 'fa-sort'; if($filtros_vehiculo['label'] == 'Tipo de Combustible') print 'fa-tint'; ?>"></i>
                <select name="<?php print texto_a_class($filtros_vehiculo['label']); ?>" class="form-select" id="<?php print texto_a_class($filtros_vehiculo['label']); ?>_link">
                  <option value=""><?php print $filtros_vehiculo['label']; ?></option>
                  <?php foreach($filtros_vehiculo['child_data'] as $datos_filtros_vehiculo){ ?>
                    <option value="<?php print str_replace('"', '-', $datos_filtros_vehiculo['filtro']); ?>" <?php if(strpos($parametros_filtro, str_replace(' ', '%20', str_replace('"', '%22', $datos_filtros_vehiculo['filtro']))) !== FALSE) print 'selected="selected"'; ?>><?php print $datos_filtros_vehiculo['name']; ?></option>
                  <?php } ?>
                </select>
              </div>          
            <?php } ?>
          <?php } ?>
        <?php } ?>
        <?php if(trim($campo['label']) == 'Tipo de Vehículo' || in_array(trim($campo['label']), array('Marca y Modelo', 'Subrubro', 'Marca'))) break; ?>
      <?php } ?>
      <?php if(trim($campo['label']) == 'Tipo de Vehículo' || in_array(trim($campo['label']), array('Marca y Modelo', 'Subrubro', 'Marca'))) break; ?>
    <?php } ?>
    
    <!-- Tipo de Inmueble -->
    <?php if((trim($campo['name']) == 'Inmuebles' || in_array(trim($campo['label']), array('Tipo de Inmueble', 'Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) && $count_filtros == 0){ ?>
      <?php if(in_array(trim($campo['label']), array('Tipo de Inmueble', 'Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) $campo['child_data'] = $campos_filtros; ?>
      <?php foreach($campo['child_data'] as $tipo_inmueble){ ?>
        <?php if($tipo_inmueble['label'] == 'Tipo de Inmueble' && !empty($tipo_inmueble['child_data'])){ ?>
          <div class="select-group subrubros <?php if(trim($campo['label']) != 'Tipo de Inmueble') print 'hidden'; ?>" data-padre="<?php print $campo['filtro']; ?>">
            <i class="fa fa-home"></i>
            <select name="tipo_inmueble" class="form-select" id="tipo_inmueble_link">
              <option value=""><?php print $tipo_inmueble['label']; ?></option>
              <?php foreach($tipo_inmueble['child_data'] as $datos){ ?>
                <option value="<?php print $datos['filtro']; ?>" <?php if(strpos($parametros_filtro, $datos['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $datos['name']; ?></option>
              <?php } ?>
            </select>
          </div>
        <?php } ?>  
        <?php if(in_array(trim($tipo_inmueble['label']), array('Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) $tipo_inmueble['child_data'] = $campos_filtros; ?>
        <!-- Filtros Inmuebles -->
        <?php foreach($tipo_inmueble['child_data'] as $datos){ ?>        
          <?php if(in_array(trim($tipo_inmueble['label']), array('Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) $datos['child_data'] = $campos_filtros; ?>
          <?php if(!empty($datos['child_data'])){ ?>
            <?php foreach($datos['child_data'] as $filtros_inmuebles){ ?>
              <?php if(($tipo_inmueble['label'] == 'Tipo de Inmueble' && in_array($filtros_inmuebles['label'], array('Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) || ($tipo_inmueble['label'] == 'Operación' && in_array($filtros_inmuebles['label'], array('Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios', 'Tipo de Vendedor', 'Tipo de Barrio', 'Apto Crédito', 'Apto Escritura', 'Estado'))) && !empty($filtros_inmuebles['child_data'])){ ?>
                
                <?php if(in_array($filtros_inmuebles['label'], array('Tipo de Barrio', 'Cantidad de Dormitorios'))) { ?>
                
                  <div class="filter-category select-group filtrosInmuebles multiple <?php if(!in_array(trim($tipo_inmueble['label']), array('Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) print 'hidden'; ?>" data-padre="<?php print $datos['filtro']; ?>">
                    <i class="fa <?php if($filtros_inmuebles['label'] == 'Cantidad de Dormitorios') print 'fa-bed'; if($filtros_inmuebles['label'] == 'Tipo de Barrio') print 'fa-home';?>"></i>
                    <h5><?php print $filtros_inmuebles['label']; ?></h5>
                    <div class="content-multiple">
                    <?php foreach($filtros_inmuebles['child_data'] as $datos_filtros_inmuebles) { ?>
                      <div class="clear-both">
                        <input name="<?php print texto_a_class($filtros_inmuebles['label']).'_'.$datos_filtros_inmuebles['tid']; ?>" type="checkbox" class="" value="<?php print str_replace('"', '-', $datos_filtros_inmuebles['filtro']); ?>" id="<?php print texto_a_class($filtros_inmuebles['label']).'_'.$datos_filtros_inmuebles['tid']; ?>" <?php if(strpos($parametros_filtro, str_replace(' ', '%20', str_replace('"', '%22', $datos_filtros_inmuebles['filtro']))) !== FALSE) print 'checked="checked"'; ?>>
                        <p class="fl-left"><label for="<?php print texto_a_class($filtros_inmuebles['label']).'_'.$datos_filtros_inmuebles['tid']; ?>"><?php print $datos_filtros_inmuebles['name']; ?></label></p>
                      </div>
                    <?php } ?>
                    </div>
                  </div>
                
                <?php } else { ?>
                
                  <div class="select-group filtrosInmuebles <?php if(!in_array(trim($tipo_inmueble['label']), array('Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) print 'hidden'; ?>" data-padre="<?php print $datos['filtro']; ?>">
                    <i class="fa <?php if($filtros_inmuebles['label'] == 'Operación') print 'fa-dollar-sign'; if($filtros_inmuebles['label'] == 'Tipo de Unidad') print 'fa-home'; if($filtros_inmuebles['label'] == 'Cantidad de Dormitorios') print 'fa-bed'; if($filtros_inmuebles['label'] == 'Tipo de Vendedor') print 'fa-user'; if($filtros_inmuebles['label'] == 'Apto Crédito') print 'fa-check'; if($filtros_inmuebles['label'] == 'Apto Escritura') print 'fa-check'; if($filtros_inmuebles['label'] == 'Estado') print 'fa-building'; ?>"></i>
                    <select name="<?php print texto_a_class($filtros_inmuebles['label']).'_'.$datos['tid']; ?>" class="form-select" id="<?php print texto_a_class($filtros_inmuebles['label']).'_'.$datos['tid']; ?>_link">
                      <option value=""><?php print $filtros_inmuebles['label']; ?></option>
                      <?php foreach($filtros_inmuebles['child_data'] as $datos_filtros_inmuebles){ ?>
                        <option value="<?php print str_replace('"', '-', $datos_filtros_inmuebles['filtro']); ?>" <?php if(strpos($parametros_filtro, str_replace(' ', '%20', str_replace('"', '%22', $datos_filtros_inmuebles['filtro']))) !== FALSE) print 'selected="selected"'; ?>><?php print $datos_filtros_inmuebles['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                
                <?php } ?>
                
              <?php } ?>
            <?php } ?>
          <?php } ?>
          <?php if(in_array(trim($tipo_inmueble['label']), array('Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) break; ?>
        <?php } ?>
        <?php if(in_array(trim($campo['label']), array('Tipo de Inmueble', 'Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) break; ?>
      <?php } ?>
      <?php // if(in_array(trim($campo['label']), array('Tipo de Inmueble', 'Operación', 'Tipo de Unidad', 'Cantidad de Dormitorios'))) break; ?>
    <?php } else { ?>
    <!-- Filtros generales Inmuebles -->
      <?php if(in_array(trim($campo['label']), array('Tipo de Vendedor', 'Apto Crédito', 'Apto Escritura')) && $campos_filtros[0]['label'] == 'Tipo de Inmueble') { ?>
        <div class="select-group filtrosInmuebles">
          <i class="fa <?php if($campo['label'] == 'Operación') print 'fa-dollar-sign'; if($campo['label'] == 'Tipo de Unidad') print 'fa-home'; if($campo['label'] == 'Cantidad de Dormitorios') print 'fa-bed'; if($campo['label'] == 'Tipo de Vendedor') print 'fa-user'; if($campo['label'] == 'Tipo de Barrio') print 'fa-home'; if($campo['label'] == 'Apto Crédito') print 'fa-check'; if($campo['label'] == 'Apto Escritura') print 'fa-check'; if($campo['label'] == 'Estado') print 'fa-building'; ?>"></i>
          <select name="<?php print texto_a_class($campo['label']).'_'.$datos['tid']; ?>" class="form-select" id="<?php print texto_a_class($campo['label']).'_'.$datos['tid']; ?>_link">
            <option value=""><?php print $campo['label']; ?></option>
            <?php foreach($campo['child_data'] as $datos_filtros_inmuebles){ ?>
              <option value="<?php print str_replace('"', '-', $datos_filtros_inmuebles['filtro']); ?>" <?php if(strpos($parametros_filtro, str_replace(' ', '%20', str_replace('"', '%22', $datos_filtros_inmuebles['filtro']))) !== FALSE) print 'selected="selected"'; ?>><?php print $datos_filtros_inmuebles['name']; ?></option>
            <?php } ?>
          </select>
        </div>
      <?php } ?>      
    <?php } ?>
    <?php $count_filtros++; ?>
    
    <!-- Rubros Productos -->
    <?php if(trim($campo['name']) == 'Productos' || (in_array(6106, $rubros_principales) && trim($campo['label']) == 'Rubros')){ ?>
      <?php foreach($campo['child_data'] as $rubro_productos){ ?>
        <?php if(trim($campo['label']) == 'Rubros') $rubro_productos = $campo; ?>
        <?php if($rubro_productos['label'] == 'Rubros' && !empty($rubro_productos['child_data'])){ ?>
          <div class="select-group filtrosVarios subrubros <?php if(trim($campo['label']) != 'Rubros') print 'hidden'; ?>" data-padre="<?php print $campo['filtro']; ?>">
            <i class="fa fa-gift"></i>
            <select name="rubro_productos" class="form-select" id="rubro_productos_link">
              <option value="">Rubros productos</option>
              <?php foreach($rubro_productos['child_data'] as $datos){ ?>
                <option value="<?php print $datos['filtro']; ?>" <?php if(strpos($parametros_filtro, $datos['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $datos['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <?php if(trim($campo['label']) == 'Rubros') { ?>
            <!-- Sub-rubros Productos -->
            <?php foreach($rubro_productos['child_data'] as $datos_subrubro){ ?>
              <?php if(!empty($datos_subrubro['child_data'])){ ?>
                <div class="select-group filtrosVarios subproductos hidden" data-padre="<?php print $datos_subrubro['filtro']; ?>">
                  <i class="fa fa-gift"></i>
                  <select name="subproductos_<?php print $datos_subrubro['tid']; ?>" class="form-select" id="subproductos_<?php print $datos_subrubro['tid']; ?>_link">
                    <option value="">Sub-Rubros productos</option>
                    <?php foreach($datos_subrubro['child_data'] as $subrubro){ ?>
                      <option value="<?php print $subrubro['filtro']; ?>" <?php if(strpos($parametros_filtro, $subrubro['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $subrubro['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>          
              <?php } ?>
            <?php } ?>
          <?php } ?>
        <?php } ?>
        <?php if(trim($campo['label']) == 'Rubros') break; ?>
      <?php } ?>
    <?php } ?>
    
    <!-- Rubros Rurales -->
    <?php if(trim($campo['name']) == 'Rurales' || (in_array(6000, $rubros_principales) && trim($campo['label']) == 'Rubros')){ ?>
      <?php foreach($campo['child_data'] as $rubro_rurales){ ?>
        <?php if(trim($campo['label']) == 'Rubros') $rubro_rurales = $campo; ?>
        <?php if($rubro_rurales['label'] == 'Rubros' && !empty($rubro_rurales['child_data'])){ ?>
          <div class="select-group filtrosVarios subrubros <?php if(trim($campo['label']) != 'Rubros') print 'hidden'; ?>" data-padre="<?php print $campo['filtro']; ?>">
            <i class="fa fa-tag"></i>
            <select name="rubro_rurales" class="form-select" id="rubro_rurales_link">
              <option value="">Rubros rurales</option>
              <?php foreach($rubro_rurales['child_data'] as $datos){ ?>
                <option value="<?php print $datos['filtro']; ?>" <?php if(strpos($parametros_filtro, $datos['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $datos['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <?php if(trim($campo['label']) == 'Rubros') { ?>
            <!-- Sub-rubros Rurales -->
            <?php foreach($rubro_rurales['child_data'] as $datos_subrubro){ ?>
              <?php if(!empty($datos_subrubro['child_data'])){ ?>
                <div class="select-group filtrosVarios subrurales hidden" data-padre="<?php print $datos_subrubro['filtro']; ?>">
                  <i class="fa fa-tag"></i>
                  <select name="subrurales_<?php print $datos_subrubro['tid']; ?>" class="form-select" id="subrurales_<?php print $datos_subrubro['tid']; ?>_link">
                    <option value="">Sub-Rubros rurales</option>
                    <?php foreach($datos_subrubro['child_data'] as $subrubro){ ?>
                      <option value="<?php print $subrubro['filtro']; ?>" <?php if(strpos($parametros_filtro, $subrubro['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $subrubro['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>          
              <?php } ?>
            <?php } ?>
          <?php } ?>
        <?php } ?>
        <?php if(trim($campo['label']) == 'Rubros') break; ?>
      <?php } ?>
    <?php } ?>
    
    <!-- Rubros Servicios -->
    <?php if(trim($campo['name']) == 'Servicios'  || (in_array(6017, $rubros_principales) && trim($campo['label']) == 'Rubros')){ ?>
      <?php foreach($campo['child_data'] as $rubro_servicios){ ?>
        <?php if(trim($campo['label']) == 'Rubros') $rubro_servicios = $campo; ?>
        <?php if($rubro_servicios['label'] == 'Rubros' && !empty($rubro_servicios['child_data'])){ ?>
          <div class="select-group filtrosVarios subrubros <?php if(trim($campo['label']) != 'Rubros') print 'hidden'; ?>" data-padre="<?php print $campo['filtro']; ?>">
            <i class="fa fa-wrench"></i>
            <select name="rubro_servicios" class="form-select" id="rubro_servicios_link">
              <option value="">Rubros servicios</option>
              <?php foreach($rubro_servicios['child_data'] as $datos){ ?>
                <option value="<?php print $datos['filtro']; ?>" <?php if(strpos($parametros_filtro, $datos['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $datos['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <?php if(trim($campo['label']) == 'Rubros') { ?>
            <!-- Sub-rubros Servicios -->
            <?php foreach($rubro_servicios['child_data'] as $datos_subrubro){ ?>
              <?php if(!empty($datos_subrubro['child_data'])){ ?>
                <div class="select-group filtrosVarios subservicios hidden" data-padre="<?php print $datos_subrubro['filtro']; ?>">
                  <i class="fa fa-wrench"></i>
                  <select name="subservicios_<?php print $datos_subrubro['tid']; ?>" class="form-select" id="subservicios_<?php print $datos_subrubro['tid']; ?>_link">
                    <option value="">Sub-Rubros servicios</option>
                    <?php foreach($datos_subrubro['child_data'] as $subrubro){ ?>
                      <option value="<?php print $subrubro['filtro']; ?>" <?php if(strpos($parametros_filtro, $subrubro['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $subrubro['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>          
              <?php } ?>
            <?php } ?>
          <?php } ?>
        <?php } ?>
        <?php if(trim($campo['label']) == 'Rubros') break; ?>
      <?php } ?>
    <?php } ?>
  <?php } ?>
  
  <!-- Provincia -->
  <?php 
    $filtro_provincia = array();
  ?>
  <?php if(isset($campos_provincia) && !empty($campos_provincia)){ ?>
    <div class="select-group filtrosUbicacion">
      <i class="fa fa-map-marker"></i>
      <select name="provincia" class="form-select" id="provincia_select">
        <option value="">Provincia</option>
        <?php foreach($campos_provincia as $opcion){ ?>
          <option value="<?php print $opcion['filtro']; ?>" <?php if(strpos($parametros_filtro, $opcion['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $opcion['name']; ?></option>
        <?php } ?>
      </select>
    </div>
  <?php } ?>
  
  <!-- Ciudad -->
  <?php 
    $filtro_ciudad = array();
  ?>
  <?php if(isset($campos_provincia) && !empty($campos_provincia)){ ?>
    <?php foreach($campos_provincia as $provincia){ ?>
      <div class="select-group filtrosUbicacion ciudades hidden" data-padre="<?php print $provincia['filtro']; ?>">
        <i class="fa fa-map-marker"></i>
        <select name="ciudad_<?php print $provincia['tid']; ?>" class="form-select select-ciudad" id="ciudad_<?php print $provincia['tid']; ?>_select">
          <option value="">Ciudad</option>
          <?php foreach($provincia['child_data'] as $ciudad){ ?>
            <option value="<?php print $ciudad['filtro']; ?>" <?php if(strpos($parametros_filtro, $ciudad['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $ciudad['name']; ?></option>
          <?php } ?>
        </select>
      </div>  
    <?php } ?>
  <?php } ?>
  
  <!-- Barrio -->
  <?php 
    $filtro_barrio = array();
  ?>
  <?php if(isset($campos_provincia) && !empty($campos_provincia)){ ?>
    <?php foreach($campos_provincia as $provincia){ ?>
      <?php foreach($provincia['child_data'] as $ciudad){ ?>
        <?php if(!empty($ciudad['child_data'])){ ?>
          
          <?php if($ciudad['tid']!=3194) { ?>
            <div class="select-group filtrosUbicacion barrios hidden" data-padre="<?php print $ciudad['filtro']; ?>">
              <i class="fa fa-map-marker"></i>
              <select name="barrio_<?php print $ciudad['tid']; ?>" class="form-select" id="barrio_<?php print $ciudad['tid']; ?>_select">
                <option value=""><?php echo 'Barrio'; ?></option>
                <?php foreach($ciudad['child_data'] as $barrio){ ?>
                  <option value="<?php print $barrio['filtro']; ?>" <?php if(strpos($parametros_filtro, $barrio['filtro']) !== FALSE) print 'selected="selected"'; ?>><?php print $barrio['name']; ?></option>
                <?php } ?>
              </select>
            </div>
          <?php } else { ?>
            <div class="filter-category select-group multiple barrios hidden" data-padre="<?php print $ciudad['filtro']; ?>">
              <i class="fa fa-map-marker"></i>
              <h5><?php print 'Barrios de Córdoba'; ?></h5>
              <div class="content-multiple">
              <?php foreach($ciudad['child_data'] as $barrio) { ?>
                <div class="clear-both">
                  <input name="field_check_barrio_cba_<?php print $barrio['tid']; ?>" type="checkbox" class="" value="<?php print $barrio['filtro']; ?>" id="tid_<?php print $barrio['tid']; ?>" <?php if(strpos($parametros_filtro, $barrio['filtro']) !== FALSE) print 'checked="checked"'; ?>>
                  <p class="fl-left"><label for="tid_<?php print $barrio['tid']; ?>"><?php print $barrio['name']; ?></label></p>
                </div>
              <?php } ?>
              </div>
            </div>
          <?php } ?>
          
        <?php } ?>
      <?php } ?>
    <?php } ?>
  <?php } ?>
  
  </div>
  
  <?php
    foreach($filtros_nombres as $filtro_texto){
      if(strpos($filtro_texto, 'Precio') !== false){
        $precio_desde_hasta = explode(' ', $filtro_texto);
        $precio_desde = $precio_desde_hasta[1];
        $precio_hasta = $precio_desde_hasta[3];
      }
      if($filtro_texto == '$')
        $moneda_peso_check = 1;
      if($filtro_texto == 'U$S')
        $moneda_dolar_check = 1;
      if($filtro_texto == 'UVA')
        $moneda_uva_check = 1;
      if(strpos($filtro_texto, 'Km') !== false){
        $km_desde_hasta = explode(' ', $filtro_texto);
        $km_desde = $km_desde_hasta[1];
        $km_hasta = $km_desde_hasta[3];
      }
      if(strpos($filtro_texto, 'Año') !== false){
        $anio_desde_hasta = explode(' ', $filtro_texto);
        $anio_desde = $anio_desde_hasta[1];
        $anio_hasta = $anio_desde_hasta[3];
      }
    }
  ?>
  
  <div class="main-wrapper">
    <div class="filter-category">
      <h5>Precio</h5>
      <input name="field_precio_desde" class="left" placeholder="Desde" type="text" value="<?php if(isset($precio_desde)) print $precio_desde; ?>" />
      <input name="field_precio_hasta" class="right" placeholder="Hasta" type="text" value="<?php if(isset($precio_hasta)) print $precio_hasta; ?>" />
    </div>
  
    <div class="filter-category">
      <h5>Moneda</h5>
      <div class="clear-both">
        <input type="radio" name="field_moneda" class="fl-left" value="1" <?php if(isset($moneda_peso_check) && $moneda_peso_check == 1) print 'checked="checked"'; ?> />
        <p class="sizemed fl-left">$</p>
      </div>
      <div class="clear-both">
        <input type="radio" name="field_moneda" class="fl-left" value="2" <?php if(isset($moneda_dolar_check) && $moneda_dolar_check == 1) print 'checked="checked"'; ?> />
        <p class="sizemed fl-left">U$S</p>
      </div>
      <?php if(in_array(6330, $rubros_principales)) { ?>
        <div class="clear-both">
          <input type="radio" name="field_moneda" class="fl-left" value="3" <?php if(isset($moneda_uva_check) && $moneda_uva_check == 1) print 'checked="checked"'; ?> />
          <p class="sizemed fl-left moneda-uva">UVA</p>
        </div>
      <?php } ?>
    </div>
  
  
  <?php if(!empty($existe_modelo) || !empty($existe_tipo_vehiculo)){ ?>
    <div class="filter-category">
      <h5>Kilómetros</h5>
      <input name="field_kilometro_desde" placeholder="Desde" type="text" value="<?php if(isset($km_desde)) print $km_desde; ?>" />
      <input name="field_kilometro_hasta" placeholder="Hasta" type="text" value="<?php if(isset($km_hasta)) print $km_hasta; ?>" />
    </div>
    <div class="filter-category">
      <h5>Año del modelo</h5>
      <input name="field_modelo_desde" placeholder="Desde" type="text" value="<?php if(isset($anio_desde)) print $anio_desde; ?>" />
      <input name="field_modelo_hasta" placeholder="Hasta" type="text" value="<?php if(isset($anio_hasta)) print $anio_hasta; ?>" />
    </div>
  <?php } ?>
  </div>
  
  <div class="filter-buttons">
    <a class="btn btn-primary fl-left" href="javascript:envio_form();">filtrar</a>
    <?php 
      $url_cancelar = $_SERVER['REQUEST_URI']; 
      $url_cancelar = str_replace('/filtros/avanzados/', '/search/result/', $url_cancelar);
    ?>
    <a class="btn btn-secondary fl-right" href="<?php print $url_cancelar; ?>">cancelar</a>
  </div>
  
  <?php
  foreach($filtros_ocultos as $filtro) {
  ?>
    <input type="hidden" name="<?php echo $filtro['parametro']; ?>" id="<?php echo $filtro['parametro']; ?>" value="<?php print $filtro['valor']; ?>" />
  <?php
  }
  ?>
  <input type="hidden" name="url" value="search/result/" />
  
  </form>
</div>
</div>