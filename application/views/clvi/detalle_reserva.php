<?php //print_r($resultado); ?>
<div id="content">
	<article class="aviso detalle-reserva">
    <div class="container">
      <div class="content cuerpo">
        <div class="texto-top">
          <h3>N° de Reserva: <?php echo $resultado->nid; ?></h3>
          <span class="<?php print strtolower($resultado->estado); ?>"><?php print $resultado->estado; ?></span>
<?php if($tipo_usuario == 'propietario' && $resultado->estado == 'Pendiente') {   
  $fecha_fin_reserva = $resultado->created + 24*60*60; ?>
        </div>
        <div class="texto reloj">
          <div class="countdown"></div><i class="fa fa-clock-o"></i>
<script>
  jQuery(document).ready(function($){
    var fechaFin = new Date(<?php print $fecha_fin_reserva; ?>*1000);
    $('.countdown').dsCountDown({
      endDate: new Date(fechaFin),
      titleDays: '',
      titleHours: '',
      titleMinutes: '',
      titleSeconds: ''
    });
    $('.ds-days').hide();
  });
</script>
<?php } ?>
        </div>
        <div class="texto"><span><i class="fa fa-home"></i>Alojamieto:</span></div>
        <div class="texto"><h2><?php echo $resultado->aviso->title; ?></h2></div>
      </div>
      <div id='slider'>
        <ul>
          <li><div class="imagen"><img src="<?php echo $resultado->aviso->foto; ?>" width="320" height="180" alt="<?php echo $resultado->aviso->title; ?>" /></div></li>
        </ul>
      </div>
    </div>
    <div class="container">
      <div class="content cuerpo">
          
        <div class="texto">
          <span><i class="fa fa-map-marker"></i>Ubicación:</span>
          <span></span>
        </div>
        <div class="texto oscuro">
          <span class="ancho-total margen-bottom"><?php print $resultado->aviso->ubicacion; ?></span>
        </div>
        <div class="texto">
          <span><i class="fa fa-calendar-check-o"></i>Check-in:</span>
          <span><i class="fa fa-calendar-times-o"></i>Check-out:</span>
        </div>
        <div class="texto oscuro">
          <span class="margen margen-bottom"><?php print $resultado->checkin; ?></span>
          <span class="margen margen-bottom"><?php print $resultado->checkout; ?></span>
        </div>
        <div class="texto">
          <span class="margen-bottom"><i class="fa fa-moon-o"></i><?php print $resultado->noches; ($resultado->noches == 1)? print ' Noche': print ' Noches';?></span>
          <span class="margen-bottom"><i class="fa fa-users"></i><?php print $resultado->aviso->cantidadPersonas; ($resultado->aviso->cantidadPersonas == 1)? print ' Huésped': print ' Huéspedes';?></span>
        </div>
        <div class="texto">
          <span class="margen ancho-total"><i class="fa fa-user"></i><?php ($tipo_usuario == 'propietario')? print 'Inquilino' : print 'Propietario'; ?>:</span>
        </div>
        <div class="texto">
          <span class="ancho-total datos-user"><i class="fa fa-user"></i><?php print $resultado->usuario->nombre.' '.$resultado->usuario->apellido; ?></span>
        </div>
        <?php if($tipo_usuario == 'propietario') { ?>
          <div class="texto">
            <span class="ancho-total datos-user"><i class="fa fa-envelope"></i><?php print $resultado->usuario->mail; ?></span>
          </div>
          <div class="texto">
            <span class="margen-bottom ancho-total datos-user"><i class="fa fa-phone"></i><?php print $resultado->usuario->telefono; ?></span>
          </div>
        <?php } elseif($resultado->estado == 'Aceptada') { ?>
          <div class="texto">
            <span class="ancho-total datos-user"><i class="fa fa-envelope"></i><?php print $resultado->usuario->mail; ?></span>
          </div>
          <div class="texto">
            <span class="margen-bottom ancho-total datos-user"><i class="fa fa-phone"></i><?php print $resultado->usuario->telefono; ?></span>
          </div>
        <?php } else { ?>
          <div class="texto"><span class="margen-bottom ancho-total"></span></div>
        <?php } ?>
        
        <div class="texto">
          <span class="margen ancho-total"><i class="fa fa-credit-card-alt"></i>Resumen:</span>
        </div>
        <div class="texto oscuro datos-precio">
          <span>Precio por noche:</span>
          <span><?php print $resultado->moneda.' '.$resultado->tarifaNoche; ?></span>
        </div>
        <div class="texto oscuro datos-precio">
          <span>Subtotal:</span>
          <span><?php print $resultado->moneda.' '.$resultado->tarifaReserva; ?></span>
        </div>
        <div class="texto oscuro datos-precio">
          <span>Depósito de garantía:</span>
          <span><?php print $resultado->moneda.' '.$resultado->tarifaGarantia; ?></span>
        </div>
        <div class="texto oscuro datos-precio">
          <span>Cargo por limpieza:</span>
          <span><?php print $resultado->moneda.' '.$resultado->tarifaLimpieza; ?></span>
        </div>
        <div class="texto oscuro datos-precio">
          <span>Desayuno:</span>
          <span><?php print $resultado->moneda.' '.$resultado->tarifaDesayuno; ?></span>
        </div>
        <div class="texto oscuro datos-precio">
          <span>Cochera:</span>
          <span><?php print $resultado->moneda.' '.$resultado->tarifaCochera; ?></span>
        </div>
        <div class="texto oscuro total">
          <span class="margen-bottom">Total:</span>
          <span class="margen-bottom"><?php print $resultado->moneda.' '.$resultado->tarifaTotal; ?></span>
        </div>
        
        <?php if($tipo_usuario == 'propietario' && $resultado->estado == 'Pendiente') { ?>
          <ul class="ui-tabs-nav">
            <li class="tab-confirm"><a href="javacript:void(0);" class="ui-tabs-anchor" id="ui-id-1">Aceptar</a></li>
            <li class="tab-reject"><a href="javacript:void(0);" class="ui-tabs-anchor" id="ui-id-2">Rechazar</a></li>
          </ul>
          <form name="formReservaAceptar" class="formReservaAceptada" action="/administrar/reservas_accion/aceptada" method="post">
            <input name="reserva_id" type="hidden" value="<?php print $resultado->nid; ?>" />
            <input name="btnAceptar" type="submit" value="Confirmar"/>
          </form>
          <form name="formReservaRechazar" class="formReservaRechazada" action="/administrar/reservas_accion/rechazada" method="post" onsubmit="return validar_form_reserva_rechazo();">
            <div class="campo">
              <label>¿Por que rechaza la reserva?</label>
              <select name="opciones" class="form-select" id="edit-opciones">
                <option value="1">Días no disponibles</option>
                <option value="2">Prefiero reservas de más días</option>
                <option value="3">No llego a un acuerdo con el inquilino</option>
              </select>
            </div>
            <div class="campo">
              <label>Mensaje:</label>
              <textarea cols="60" rows="5" name="mensaje" id="edit-mensaje" class="form-textarea resizable textarea-processed"></textarea>
            </div>
            <input name="reserva_id" type="hidden" value="<?php print $resultado->nid; ?>" />
            <input name="aviso_id" type="hidden" value="<?php print $resultado->aviso->nid; ?>" />
            <div id="message-form" style="display:none;"><div id="message-text" class="fondoResaltado"></div></div>
            <input id="submit-rechazo" name="btnRechazar" type="submit" value="Rechazar"/>
          </form>
        <?php } elseif($tipo_usuario == 'propietario' && $resultado->estado == 'Aceptada') { ?>
          <form name="formReservaCancelada" class="formReservaCancelada" action="/administrar/reservas_accion/cancelada" method="post">
            <input name="reserva_id" type="hidden" value="<?php print $resultado->nid; ?>" />
            <input name="aviso_id" type="hidden" value="<?php print $resultado->aviso->nid; ?>" />
            <input name="tipo_usuario" type="hidden" value="<?php print $tipo_usuario; ?>" />
            <input name="btnCancelar" type="submit" value="Cancelar"/>
          </form>
        <?php } elseif($tipo_usuario == 'inquilino' && $resultado->estado == 'Pendiente') { ?>
          <form name="formReservaCancelada" class="formReservaCancelada" action="/administrar/reservas_accion/cancelada" method="post">
            <input name="reserva_id" type="hidden" value="<?php print $resultado->nid; ?>" />
            <input name="aviso_id" type="hidden" value="<?php print $resultado->aviso->nid; ?>" />
            <input name="tipo_usuario" type="hidden" value="<?php print $tipo_usuario; ?>" />
            <input name="btnCancelar" type="submit" value="Cancelar"/>
          </form>
        <?php } ?>
        
      </div>
    </div>
  </article>
</div>

<script type="text/javascript"> 
  function onFocusTextForm(campo)
  {
    if(campo.value == 'Mensaje')
      campo.value = '';
  }
  function onBlurTextForm(campo)
  {
    if(campo.value == ''){
      if(campo.name == 'mensaje')
        campo.value = 'Mensaje';
    }
  }
  function validar_form_reserva_rechazo(){
    document.getElementById("submit-rechazo").style.display="none";
    document.getElementById("message-form").style.display="none";
    document.getElementById("submit-rechazo").disabled = true;
    document.getElementById("message-text").innerHTML= "";
    var Formulario = document.forms['formReservaRechazar'];
    var longitudFormulario = document.forms['formReservaRechazar'].length;
    for (var i=0;i<=longitudFormulario-1;i++){
      if(Formulario.elements[i].name == 'opciones' && Formulario.elements[i].value == '') {
        document.getElementById("message-form").style.display="block";
        document.getElementById("message-text").innerHTML='<p>Seleccione una razón de rechazo.</p>';
        document.getElementById("submit-rechazo").style.display="block";
        document.getElementById("submit-rechazo").disabled = false;
        return false;
      }
    }
    return true;
  }
</script>