
<?php if(isset($adslots)) { shuffle($adslots); ?>
<!-- 
<h2 class="title-oportunidades">¡Oportunidades para Mamá!</h2>
<div id="slider-fotos" class="swipe content-oportunidades" style="max-width:410px;">
  <div class='swipe-wrap'>
    <div>
  <?php $count = 0; foreach($adslots as $adslot) { 
    if(in_array($adslot['adunit'], array('dfp-oportunidad1','dfp-oportunidad2','dfp-oportunidad3','dfp-oportunidad4','dfp-oportunidad5','dfp-oportunidad6'))) { ?>
    
    <?php if($count == 3){ ?></div><div><?php } ?>
    
    <div class='banner'>
      <div id='<?php print $adslot['adunit']; ?>'>
        <script>
          googletag.cmd.push(function() { googletag.display('<?php print $adslot['adunit']; ?>'); });
        </script>
      </div>
    </div>
    
    <?php $count++; }
  } ?>
    </div>
  </div>
</div>
-->
<!-- 
<div id="content-slider-fotos" class="content-slider slide-oportunidades" >
  <button class="flickity-prev-next-button previous" onclick="slider.prev();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(15,0)"></path></svg></button>
  <button class="flickity-prev-next-button next" onclick="slider.next();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(85,100) rotate(180)"></path></svg></button>
</div>
-->
<script type="text/javascript" src="/public/js/swipe.js"></script>
<script>
// window.slider = new Swipe(document.getElementById('slider-fotos'),{continuous: true, speed: 800, auto: 5000});
</script>
<?php } ?>

<div id="content">
  <h1 class="DN">Avisos clasificados para comprar y vender en Córdoba</h1>
<div class="container">
	<div class="row">
    <section class="rubros-home">
      <div class="titDestacados titBuscar"><span class="DN">Buscá por Rubro</span></div>      	
      <ul>
        
        <a href="/inmuebles" title="Home de inmuebles en Clasificados Los Andes">
          <i class="fas fa-home"></i>
          <h5 class="home">Inmuebles</h5>
        </a>
        <a href="/autos" title="Home de vehículos en Clasificados Los Andes">
          <i class="fas fa-car-alt"></i>
          <h5>Vehículos</h5>
        </a>
        
        <a href="/productos" title="Home de productos en Clasificados Los Andes">
          <i class="fas fa-gift"></i>
          <h5>Productos</h5>
        </a>
        <a href="https://clasificados.losandes.com.ar/microsite/viewVarious/words/m/empleos" title="Sitio de empleos de Clasificados Los Andes">
          <i class="fas fa-briefcase"></i>
          <h5>Empleos</h5>
        </a>
        
        <a href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/educacionales" title="Sitio educacionales de Los Andes">
          <i class="fas fa-book"></i>
          <h5>Educacionales</h5>
        </a>
        <a href="https://clasificados.losandes.com.ar/microsite/viewVarious/words//m/edictos-judiciales" title="Sitio de legales de Los Andes">
          <i class="fas fa-balance-scale"></i>
          <h5>Legales</h5>
        </a>
        
        <a href="/search/result/filters=tid:6017%20" title="Resultado de búsqueda de servicios">
          <i class="far fa-handshake"></i>
          <h5>Servicios</h5>
        </a>
        <a class="btn-publicar" href="/node_add" title="Publicá tu aviso gratis" alt="Publicá tu aviso gratis">
          <h5>Publicá Gratis</h5>
        </a>
        
      </ul>
    </section>
  </div>
  <div class="row"></div>   