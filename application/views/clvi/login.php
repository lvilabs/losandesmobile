<div id="content">
  
  <div class="aviso users-forms">
    <div class="cajaTitulo">
      <div class="titulo">
        <h1>Iniciar sesión</h1>
      </div>
    </div>
  
    <div class="message">
      <?php echo validation_errors(); ?>
      <?php if(!empty($mensaje)) echo $mensaje; ?>
    </div>

    <div class="content cuerpo">
      <form name="formLogin" class="formAviso login" action="/user/login" method="post">
        <hr />
        <input type="hidden" name="token" value="<?php print $token; ?>" />
        <div class="campo"><input type="text" name="username" placeholder="Usuario" value="" /></div>
        <div class="campo"><input type="password" name="password" placeholder="Contraseña" value="" /></div>
        <a class="link-password" href="/user/password">Olvidé mi contraseña</a>
        <input type="submit" name="submit" value="Iniciar sesión" class="boton" />
        <a class="link-registro" href="/registro-particular">Registrarse</a>
      </form>
    </div>
  </div>

</div>