<?php
  $con_video = 0;
  if(!empty($nota['video_genoa']) || !empty($nota['video_youtube']))
    $con_video = 1;
?>
<section class="nodo nota_periodistica">
  <div class="container"> 
    <article class="clearfix">
      <div class="nodeInfo <?php if($con_video) print 'video'; ?>"> 
        <header class="<?php if(empty($nota['fotos'])) print 'sinFoto'; ?>">
          <div class="panel-body panel-slider">
            <div class="contenido">
              <div class="contenido-titulo">
                <h1><?php echo $nota['title']; ?></h1>
                <div class="footer-card">
                  <div class="data"> 
                    <span class="nodeData clearfix"> 
                      <time><?php echo $nota['fecha_nota']; ?></time> &nbsp;•&nbsp; <?php echo $nota['categoria']; ?>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <?php if($con_video){ ?>
              <?php if(!empty($nota['video_genoa'])){ ?>
                <?php print $nota['video_genoa']; ?>
              <?php } elseif(!empty($nota['video_youtube'])){ ?>
                <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="100%" height="240" type="text/html" src="<?php print $nota['video_youtube']; ?>?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&origin=https://youtubeembedcode.com"></iframe>
              <?php } ?>
            <?php } else { ?>
              <?php if(count($nota['fotos'])>1){ ?>
                <div class="paginacion">
                  <a href="#" class="anterior" onclick="slider.prev();return false;"><img width="41" height="41" class="Anterior pngfix BtnCA1" src="/public/img/sprite_slider_izq.png" style="float: left;opacity: 10;z-index: 1000;" title="Anterior"></a>
                  <a href="#" class="siguiente" onclick="slider.next();return false;"><img width="41" height="41" class="Siguiente pngfix BtnCA1" src="/public/img/sprite_slider_der.png" style="float: right;opacity: 10;z-index: 1000;" title="Siguiente"></a>
                </div>
              <?php } ?>
              <?php if(!empty($nota['fotos'])){ ?>
                <div id="slider" style="width:360px;">
                  <ul>
                    <?php foreach($nota['fotos'] as $kimg => $vimg){ ?>
                      <li <?php echo ($kimg==0)?"style='display:block;'":"style='display:none;'"; ?>><div class="imagen"><img alt="Nota <?php echo $vimg['texto']; ?>" src="<?php echo $vimg['path']; ?>" width="100%" height="180" /></div></li>
                    <?php } ?>
                  </ul>
                </div>
              <?php } ?>
              <!-- <div class="caption">
                <p ng-bind-html="caption" class="ng-binding">La basura acumulada en Alta Córdoba.</p>
              </div> -->
            <?php } ?>
          </div> 
        </header> 
      </div>
      <div class="nodeFull clearfix">
        <div> 
          <?php if(!empty($nota['autor'])) { ?>
            <div class="author-in-content">
              <div class="arriba">
                <span class="author-name">Por <span><?php echo $nota['autor']; ?></span></span>
              </div>
            </div>
          <?php } ?>
          <main> 
            <div count="shareCount.facebook">
              <ul class="user-share">
                <li class="sfb ng-scope">
                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php print urlencode($this->config->item('sitio_origen').$_SERVER['REQUEST_URI']); ?>" target="_blank" class="share-item ng-isolate-scope">
                    <div class="sprite-compartir sprite-facebook">
                      <i class="fab fa-facebook-f"></i>
                    </div>
                  </a>
                </li>
                <li class="stw ng-scope">
                  <a href="http://twitter.com/share?text=<?php print $nota['title']; ?>&url=<?php print urlencode($this->config->item('sitio_origen').$_SERVER['REQUEST_URI']); ?>" target="_blank" class="share-item" >
                    <div class="sprite-compartir sprite-twitter">
                      <i class="fab fa-twitter"></i>
                    </div>
                  </a>
                </li>
              </ul>
            </div>
            <div class="teaser">
              <?php echo $nota['resumen']; ?>
            </div>
            <div class="body">
              <?php echo $nota['body']; ?>
            </div>
            <div count="shareCount.facebook">
              <ul class="user-share">
                <li class="sfb ng-scope">
                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php print urlencode($this->config->item('sitio_origen').$_SERVER['REQUEST_URI']) ?>" target="_blank" class="share-item ng-isolate-scope">
                    <div class="sprite-compartir sprite-facebook">
                      <i class="fab fa-facebook-f"></i>
                    </div>
                  </a>
                </li>
                <li class="stw ng-scope">
                  <a href="http://twitter.com/share?text=<?php print $nota['title']; ?>&url=<?php print urlencode($this->config->item('sitio_origen').$_SERVER['REQUEST_URI']) ?>" target="_blank" class="share-item" >
                    <div class="sprite-compartir sprite-twitter">
                      <i class="fab fa-twitter"></i>
                    </div>
                  </a>
                </li>
              </ul>
            </div> 
          </main>
        </div>
      </div>
    </article>
  </div> 
</section>
<?php
if(count($nota['fotos'])>1){ ?>
<script type="text/javascript" src="/public/js/swipe.js"></script>
<script>
window.slider = new Swipe(document.getElementById('slider'),0);
</script>
<?php } ?>