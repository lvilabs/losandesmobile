<div id="content">
<h1 class="DN">Ofertas, promociones y descuentos en productos</h1>

<div class="container">
  <div class="row">
    <section class="noticias">
      <h2><span>Ofertas</span></h2>
    </section>
    <div class="cabecera">
      <?php
      if(isset($imagen_head) && !empty($imagen_head)) {
      ?>
      <img alt="Promociones de productos en Clasificados La Voz" src="<?php print $this->config->item('static_sitio').$imagen_head; ?>">
      <?php
      }
      ?>
    </div>
  </div>
</div>

<section class="noticias">
<div class="container ofertas">
  <div class="row">
    <div class="twelvecol">
<?php 
$count = 1;
foreach($avisos as $key => $item){
  ?>
  <article class="widget oferta-item rubro-<?php echo $item->rubro_tid; ?>" >
    <?php 
    if(!empty($item->foto)) {
    ?>
    <div class="thumb">
      <a href="<?php echo $item->path; ?>"><img alt="Oferta <?php echo $item->title.' a '.$item->precio_mostrar; ?>" title="<?php echo $item->title; ?>" src="<?php echo urldecode($item->foto) ?>" /></a>
    </div>
    <?php } ?>
    <div class="ficha">
      <?php if($count <= 3) { ?>
        <h2 class="ofertas"><a href="/<?php echo $item->path; ?>"><?php echo $item->title ?></a></h2>
      <?php } elseif($count <= 7) { ?>
        <h3 class="ofertas"><a href="/<?php echo $item->path; ?>"><?php echo $item->title ?></a></h3>
      <?php } else { ?>
        <h4 class="ofertas"><a href="/<?php echo $item->path; ?>"><?php echo $item->title ?></a></h4>
      <?php } ?>
      <?php if($item->disponible_venta) { ?>
        <a title="Comprar el producto" href="<?php echo $item->path ?>#modal-compra-datos" class="btn-comprar"><span></span>Comprar</a>
      <?php } ?>
      <span class="mensajePago ofertas"> Precio en 1 pago</span>
      <div class="precio-oferta"><?php echo $item->precio_mostrar; ?></div>
      <?php if(!empty($item->descuento)) { ?>
        <span class="descuento"><?php print '<strong>'.$item->descuento->porcentaje.'%</strong> OFF'; ?></span>
      <?php } ?>
    </div>
  </article>
  <?php
  $count++;
}
?>
    </div>
  </div>
</div>
</section>
</div>