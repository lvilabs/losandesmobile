<a class="favourite-top" href="/administrar/favoritos">
  <span class="fa">
    <?php if($_COOKIE["clviFavourites"] && !empty($_COOKIE["clviFavourites"])){ ?>
      <i class="fa-star fas"></i>
    <?php } else { ?>
      <i class="fa-star far"></i>
    <?php } ?>
  </span>
</a>
<!-- 
<a class="alertas-top" href="#">
  <span class="fa">
    <?php if($_COOKIE["clviFavourites"] && !empty($_COOKIE["clviFavourites"])){ ?>
      <i class="fas fa-bell"></i>
    <?php } else { ?>
      <i class="far fa-bell"></i>
    <?php } ?>
  </span>
</a>
-->
<?php if($_COOKIE["clviHistorial"] && !empty($_COOKIE["clviHistorial"])){ ?>
  <a class="historial-top" href="/administrar/historial">
    <span class="fa">
      <i class="fas fa-history"></i>
    </span>
  </a>
<?php } else { ?>
  <a class="historial-top" href="javascript:void(0);">
    <span class="fa apagado">
      <i class="fas fa-history"></i>
    </span>
  </a>
<?php } ?>
