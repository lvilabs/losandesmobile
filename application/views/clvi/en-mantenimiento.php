<div id="content">
  <div class="container">
    <div class="row"> 
      <div class="cajaTitulo" style="border-bottom: 1px solid #ccc; padding: 0px;   height: initial;">
        <div class="titulo"><h1><strong style="font-weight: bold;">Estimado Usuario:</strong></h1></div>
      </div>
    </div>
  </div> 
  <div class="container">
    <div class="row" style="background: #FFFFCC; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; padding: 10px; color: #666; font-size: 13px; margin-top: 10px;"> 
      <div>
        <p>Sólo por hoy, desde las <strong style="font-weight: bold;"><u>13 a las 15.30 hs.</u></strong> estaremos realizando tareas de mantenimiento en nuestros servidores para brindarle mayor rapidez al sitio.</p>
        <p>No podrás publicar nuevos avisos, pero los motores de búsqueda permanecen activos para que tus clientes puedan navegar, visualizar y consultar tus avisos ya publicados.</p>
        <br>
        <p>Lamentamos los inconvenientes que esto pueda ocasionarte.</p>
        <br>
        <p><strong style="font-weight: bold;">Equipo de Clasificados La Voz</strong></p>
      </div>
      <div style="margin-left: 33%;">
        <img style="width: 100px;" src="/public/img/mantenimiento.png" />
      </div>
      <div class="clear0">&nbsp;</div>
    </div>
    <div class="clear0">&nbsp;</div>
  </div>
</div>