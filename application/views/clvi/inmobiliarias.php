<div id="content" class="page-inmobiliarias">

<div class="container">
  <div class="row">
    <section class="noticias">
      <h2><span>Inmobiliarias</span></h2>
      <div class="container barra-buscador">
        <div class="twelvecol">
          <form id="formBusquedaInmobiliaria" name="formBusquedaInmobiliaria" class="ch-form ch-header-form" action="" method="GET">
            <div class="fondo-inmo">
              <input name="inmobiliaria" type="text" class="search" id="buscador_inmobiliarias" autocomplete="on" placeholder="Buscá una inmobiliaria" maxlength="60" data-sugnumber="6"
              value="<?php echo isset($args["comercio"])?$args["comercio"]:''; ?>">
              <span class="icono-buscar" onclick="envioBusquedaInmobiliarias();return false;" style="cursor:pointer;"></span>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
</div>

<section class="noticias">
<div class="container pages inmobiliarias">
  <div class="row">
    <div class="twelvecol">
<?php
foreach($usuarios as $key => $item) {
  ?>
  <article class="widget oferta-item" >
    <?php
    if(!empty($item->foto)) {
    ?>
    <div class="div_foto">
      <a href="/search/result/filters=is_uid:<?php echo $item->uid; ?>"><img alt="Logo de inmobiliaria <?php echo str_replace('"', '', $item->profile_nombre_comercial); ?>" title="<?php echo str_replace("'", "", $item->profile_nombre_comercial); ?>" src="<?php echo urldecode($item->foto); ?>" /></a>
    </div>
    <?php } ?>
    <div class="ficha">
      <h2><a href="/search/result/filters=is_uid:<?php echo $item->uid; ?>"><?php echo $item->profile_nombre_comercial; ?></a></h2>
      <h4><?php echo $item->profile_calle.' '.$item->profile_altura; ?></h4>
      <div><?php if($item->profile_telefono_principal != '') echo 'Tel: '.$item->profile_telefono_principal; ?></div>
      <div class="links"><a href="mailto:<?php echo $item->mail; ?>" title="Mail de la inmobiliaria"><?php echo $item->mail; ?></a></div>
      <?php if(!empty($item->profile_web)) { ?>
        <?php if(eregi("http://", $item->profile_web)) { ?>
          <div class="links"><a href="<?php echo $item->profile_web; ?>" title="Web de la inmobiliaria" target="_blank"><?php echo $item->profile_web; ?></a></div>
        <?php } else { ?>
          <div class="links"><a href="http://<?php echo $item->profile_web; ?>" title="Web de la inmobiliaria" target="_blank"><?php echo $item->profile_web; ?></a></div>
        <?php } ?>
      <?php } ?>
      <a href="/search/result/filters=is_uid:<?php echo $item->uid; ?>" class="boton" title="Avisos de la inmobiliaria">Ver avisos de esta inmobiliaria</a>
    </div>
  </article>
  <?php
}
?>
    </div>
    <div class="paginacion">
      <?php echo $pagination; ?>
    </div>
  </div>
</div>
</section>
</div>