<div class="container">
  <div class="row">
    <div class="store">
      <section class="container store confirmacion">
        <div class="currentBuy">
          <h3><?php print $mensaje; ?>:</h3>
          <div class="clearfix">
        <?php if($status == 'pending') { ?>
          <div class="col-md-12 alerta">El pedido se encuentra en espera de pago.</div>
        <?php } elseif($status == 'cancel') { ?>
          <div class="col-md-12 error">Su compra ha sido cancelada o ha existido algún error durante el proceso en la plataforma de pagos. </div>
        <?php } ?>
        <?php if($status == 'ok') { ?>
          <div class="col-md-12 mensajeOk">Gracias por su compra.<br> El vendedor procederá a preparar su pedido.</div>
        <?php } elseif($status == 'pending') { ?>
          <div class="col-md-12 mensajeOk">En cuanto el pago se acredite en la cuenta del vendedor, le llegará un email con esa notificación.</div>
        <?php } ?>
        </div>
        <div class="col-md-12 producto clearfix">
          <div class="col-lg-3 col-md-3 col-sm-3"><a href="/<?php print $nodo['url']; ?>"><img src="<?php print $nodo['foto_musa']; ?>" alt="" title="" width="100%" ></a>  </div>
            <div class="col-lg-9 col-md-9 col-sm-9 descripcion">  
              <h2><a href="/<?php print $nodo['url']; ?>"><?php print $nodo['title']; ?></a></h2>
              <div class="Precio"><?php echo $nodo['moneda'].' '.$nodo['precio']; ?></div>
              <img src="<?php print $nodo['logo_musa']; ?>" alt="" title="">
            </div>
          </div>
          <?php
          if(isset($codigo_conversion)) {
            print $codigo_conversion;
          }
          ?>
        </div>
      </section>
    </div>
  </div>
</div>