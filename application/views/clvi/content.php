<div id="content">

	<article class="aviso">
	<div class="container">
	<div class="row"> 
  
  <input type="checkbox" class="favourite" name="favourite_<?php echo $node['nid'] ?>" id="favourite_<?php echo $node['nid'] ?>" data-nid="<?php echo $node['nid'] ?>">
  <label for="favourite_<?php echo $node['nid'] ?>" class="transparent">
    <span class="fa">
      <i class="fa-star far"></i>
      <i class="fa-star fas"></i>
      <i class="fa-star fx"></i>
    </span>
  </label>

<?php 
  
  //$node['field_aviso_fotos_micrositios'] = $node['field_aviso_fotos_micrositios'][0];
  //$node['field_aviso_renders'] = $node['field_aviso_renders'][0];
  //$node['field_aviso_planos'] = $node['field_aviso_planos'][0];
  
  $cont_multimedia = 0;
  $first_multimedia = '';
  $last_multimedia = '';
  if(!empty($node['field_aviso_fotos_micrositios'][0])){ 
    $cont_multimedia++; 
    $last_multimedia = 'fotos'; 
    if($first_multimedia == '') $first_multimedia = 'fotos';
  }
  if(!empty($node['field_aviso_renders'][0])){ 
    $cont_multimedia++; 
    $last_multimedia = 'renders';
    if($first_multimedia == '') $first_multimedia = 'renders';    
  }
  if(!empty($node['field_aviso_planos'][0])){ 
    $cont_multimedia++; 
    $last_multimedia = 'planos'; 
    if($first_multimedia == '') $first_multimedia = 'planos';
  }
  
  if($node['con_precio_uva']) {
    $precio_uva = str_replace('.', '', $node['field_aviso_precio']);
    $precio_uva = str_replace(',', '.', $precio_uva);
    $moneda_uva = 1;
    if($node['field_aviso_moneda'] == 'U$S')
      $moneda_uva = 2;
  }
  
  if($cont_multimedia > 1) { ?>  
  <ul class="Solapas clearfix">
    <?php if(!empty($node['field_aviso_fotos_micrositios'][0])) { ?>
    <li data-multimedia="fotos" class="link-multimedia fotos <?php if($first_multimedia == 'fotos') print 'Act'; ?> <?php if($last_multimedia != 'fotos') print 'no-last'; ?>">
      <a rel="nofollow" title="Fotos" href="javascript:;">Fotos<span></span></a>
    </li>
    <?php } ?>
    <?php if(!empty($node['field_aviso_renders'][0])) { ?>
    <li data-multimedia="renders" class="link-multimedia renders <?php if($first_multimedia == 'renders') print 'Act'; ?> <?php if($last_multimedia != 'renders') print 'no-last'; ?>">
      <a rel="nofollow" title="Renders" href="javascript:;">Renders<span></span></a>
    </li>
    <?php } ?>
    <?php if(!empty($node['field_aviso_planos'][0])) { ?>
    <li data-multimedia="planos" class="link-multimedia planos <?php if($first_multimedia == 'planos') print 'Act'; ?> <?php if($last_multimedia != 'planos') print 'no-last'; ?>">
      <a rel="nofollow" title="Planos" href="javascript:;">Planos<span></span></a>
    </li>
    <?php } ?>
  </ul>
<?php } ?>
  
<?php if(!empty($node['field_aviso_fotos_micrositios'][0])){ ?>
  <?php if(count($node['field_aviso_fotos_micrositios'])>1){ ?>
<div id="slider-fotos" class="swipe multimedia-ficha" style="max-width:410px;">
	<div class='swipe-wrap'>
		<?php if(!$node['publicado']){ ?>	
      <div class="finalizado">
        <div class="leyenda">AVISO FINALIZADO</div>
      </div>
    <?php } ?> 
    <?php foreach($node['field_aviso_fotos_micrositios'] as $kimg => $vimg){ 
      if(isset($node['argenprop']) && $node['argenprop'] == 1){
        if(strpos($vimg,'RB_noimagen.jpg')===false)
          $foto = $vimg;
        else
          $foto = urldecode($vimg);
      } else {
        $foto = urldecode($vimg);
      }
    ?>
		<div><img alt="<?php echo $node['comscore']['parametro1'].' '.$node['comscore']['parametro2'].' '.$node['title'].' a '.$node['field_aviso_moneda'].$node['field_aviso_precio']; ?>" title="<?php echo $node['title'] ?>" src="<?php print $foto ?>" width="100%" height="160"/></div>
		<?php } ?>
	</div>
</div>
<div id="content-slider-fotos" class="content-slider <?php if($first_multimedia != 'fotos') print 'ocultar'; ?>" >
  <button class="flickity-prev-next-button previous" onclick="slider.prev();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(15,0)"></path></svg></button>
  <button class="flickity-prev-next-button next" onclick="slider.next();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(85,100) rotate(180)"></path></svg></button>
</div>
  <?php } else { ?>
    <?php if(!$node['publicado']){ ?>	
      <div class="finalizado">
        <div class="leyenda">AVISO FINALIZADO</div>
      </div>
    <?php } ?> 
    <div id="slider-fotos" class="multimedia-ficha">
      <?php if($node['type'] == 'aviso_producto') { ?>
        <img alt="<?php echo $node['comscore']['parametro1'].' '.$node['comscore']['parametro2'].' '.$node['title'].' a '.$node['field_aviso_moneda'].$node['field_aviso_precio']; ?>" title="<?php echo $node['title'] ?>" src="<?php print urldecode($node['field_aviso_fotos_sc'][0]); ?>" width="100%" height="160"/>
      <?php } else { ?>
        <img alt="<?php echo $node['comscore']['parametro1'].' '.$node['comscore']['parametro2'].' '.$node['title'].' a '.$node['field_aviso_moneda'].$node['field_aviso_precio']; ?>" title="<?php echo $node['title'] ?>" src="<?php print urldecode($node['field_aviso_fotos_micrositios'][0]); ?>" width="100%" height="160"/>
      <?php } ?>
    </div>
  <?php } ?>
<?php } ?>


<?php if(!empty($node['field_aviso_renders'][0]) && $node['publicado']){ ?>
  <?php if(isset($node['field_aviso_renders']) && count($node['field_aviso_renders'])>1){ ?>
<div id="slider-renders" class="swipe multimedia-ficha <?php if($first_multimedia != 'renders') print 'ocultar'; ?>">
	<div class='swipe-wrap'>
		<?php foreach($node['field_aviso_renders'] as $kimg => $vimg){ 
      $render = urldecode($vimg);
    ?>
			<div><img alt="<?php echo $node['comscore']['parametro1'].' '.$node['comscore']['parametro2'].' '.$node['title'].' a '.$node['field_aviso_moneda'].$node['field_aviso_precio']; ?>" title="<?php echo $node['title'] ?>" src="<?php echo $render; ?>" /></div>
		<?php } ?>
	</div>
</div>
<div id="content-slider-renders" class="content-slider <?php if($first_multimedia != 'renders') print 'ocultar'; ?>" >
  <button class="flickity-prev-next-button previous" onclick="sliderRenders.prev();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(15,0)"></path></svg></button>
  <button class="flickity-prev-next-button next" onclick="sliderRenders.next();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(85,100) rotate(180)"></path></svg></button>
</div>
  <?php } else { ?>
    <div id="slider-renders" class="multimedia-ficha <?php if($first_multimedia != 'renders') print 'ocultar'; ?>"><img alt="<?php echo $node['comscore']['parametro1'].' '.$node['comscore']['parametro2'].' '.$node['title'].' a '.$node['field_aviso_moneda'].$node['field_aviso_precio']; ?>" title="<?php echo $node['title'] ?>" src="<?php echo urldecode($node['field_aviso_renders'][0]); ?>" /></div>
  <?php } ?>
<?php } ?>

<?php if(!empty($node['field_aviso_planos'][0]) && $node['publicado']){ ?>
  <?php if(isset($node['field_aviso_planos']) && count($node['field_aviso_planos'])>1){ ?>
<div id="slider-planos" class="swipe multimedia-ficha <?php if($first_multimedia != 'planos') print 'ocultar'; ?>">
	<div class='swipe-wrap'>
		<?php foreach($node['field_aviso_planos'] as $kimg => $vimg){ 
      $plano = urldecode($vimg);
    ?>
			<div><img alt="<?php echo $node['comscore']['parametro1'].' '.$node['comscore']['parametro2'].' '.$node['title'].' a '.$node['field_aviso_moneda'].$node['field_aviso_precio']; ?>" title="<?php echo $node['title'] ?>" src="<?php echo $plano; ?>" /></div>
		<?php } ?>
	</div>
</div>
<div id="content-slider-planos" class="content-slider <?php if($first_multimedia != 'planos') print 'ocultar'; ?>" >
  <button class="flickity-prev-next-button previous" onclick="sliderPlanos.prev();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(15,0)"></path></svg></button>
  <button class="flickity-prev-next-button next" onclick="sliderPlanos.next();return false;" type="button"><svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(85,100) rotate(180)"></path></svg></button>
</div>
  <?php } else { ?>
    <div id="slider-planos" class="multimedia-ficha <?php if($first_multimedia != 'planos') print 'ocultar'; ?>"><img alt="<?php echo $node['comscore']['parametro1'].' '.$node['comscore']['parametro2'].' '.$node['title'].' a '.$node['field_aviso_moneda'].$node['field_aviso_precio']; ?>" title="<?php echo $node['title'] ?>" src="<?php echo urldecode($node['field_aviso_planos'][0]); ?>" /></div>
  <?php } ?>
<?php } ?>

<div class="cajaTitulo">
<!-- <div class="titulo"> -->
  <h1 style="display: none;"><?php echo $node['title'] ?></h1>
<!--  </div> -->
<div class="precio <?php if(isset($node['datos_venta']) && !empty($node['datos_venta'])) print 'tiendas'; ?> <?php if($node['con_precio_uva']) print 'uvas'; ?>">
  <?php if(isset($node['datos_venta']) && !empty($node['datos_venta'])) { ?>
    <span class="mensajePago">Precio en 1 pago</span>
  <?php } ?>
  <span><?php echo $node['field_aviso_moneda'] ?><div class="numero"><?php echo $node['field_aviso_precio'] ?></div></span>
  <?php if(isset($node['datos_venta']) && !empty($node['datos_venta']) && !empty($node['datos_venta']['precio_anterior']) && $node['datos_venta']['precio_anterior'] != '0.00') { ?>
    <span class="PrecioAnterior"><?php print $node['field_aviso_moneda'].$node['datos_venta']['precio_anterior']; ?></span>
    <?php if(!empty($node['datos_venta']['descuento']) && $node['datos_venta']['descuento']['porcentaje'] != 0) { ?>
      <span class="descuento"><?php print '<strong>'.$node['datos_venta']['descuento']['porcentaje'].'%</strong> OFF'; ?></span>
    <?php } ?>
  <?php } ?>
  <?php if($node['con_precio_uva']) { ?>
    <div class="precio-uva" data-precio="<?php print $precio_uva; ?>" data-moneda="<?php print $moneda_uva; ?>"></div>
  <?php } ?>
</div>
</div>
<?php if(!empty($node['field_aviso_videos'])){ ?>
<a class="video" href="<?php echo $node['field_aviso_videos'][0]; ?>">Click para ver video</a>
<?php } ?>
<?php if(isset($node['mapas']) && !empty($node['mapas']) && $node['comscore']['rubro_raiz'] != 'vehiculos'){ ?>
<a class="video" href="#img_mapa" rel="noindex">Ver mapa</a>
<?php } ?>
<div class="centrar">
<?php if($node['publicado']){ ?>

<?php if(isset($node['datos_venta']['disponible']) && !empty($node['datos_venta']['disponible'])) { ?>
<div class="line price">
  <?php if($node['datos_venta']['vendedor']['dineromail'] || $node['datos_venta']['vendedor']['mercadopago']
    || $node['datos_venta']['vendedor']['todopago']) { ?>
    <!-- <div class="cuotas">
      6 cuotas de <?php print $node['datos_venta']['precio_cuotas']; ?>
    </div> -->
    <?php if($node['datos_venta']['vendedor']['mercadopago']) { ?>
      <div class="MercadoPago">
        <span class="Logo1" style="display: block;width: 40%;float: left;"></span>
        <span class="Logo2" style="display: block;width: 30%;float: left;"></span>
        <span class="Logo3" style="display: block;width: 30%;float: left;"></span>
        <span class="Logo4" style="display: block;width: 35%;float: left;"></span>
        <span class="Link" style="display: block;width: 65%;float: left;"><a class="mercadopago_promociones" href="https://www.mercadopago.com/mla/credit_card_promos.html" target="_blank" rel="follow">Ver promociones</a></span>
      </div>
    <?php } ?>
    <?php if($node['datos_venta']['vendedor']['dineromail']) { ?>
      <div class="DineroMail">
        <span class="Logo1" style="display: block;width: 40%;float: left;"></span>
        <span class="Logo2" style="display: block;width: 20%;float: left;"></span>
        <span class="Logo3" style="display: block;width: 40%;float: left;"></span>
      </div>
    <?php } ?>
    <?php if($node['datos_venta']['vendedor']['todopago']) { ?>
      <div class="TodoPago">
        <span class="Logo1" style="display: block;width: 40%;float: left;"></span>
        <span class="Logo2" style="display: block;width: 30%;float: left;"></span>
        <span class="Logo3" style="display: block;width: 30%;float: left;"></span>
        <span class="Logo4" style="display: block;width: 35%;float: left;"></span>
      </div>
    <?php } ?>
  <?php } ?>
  <div class="comprar"><a href="#modal-compra-datos" id="inicio-compra" class="open-popup-link" rel="noindex">Comprar</a></div>
  <?php if($node['datos_venta']['vendedor']['pago_domicilio']) { ?>
    <div class="compraAccion PagoDomicilio clearfix">
      <span></span><p>También podés pagar cuando recibas el producto.</p>
    </div>
  <?php } ?>
  <div class="compraAccion Envio clearfix">
    <span></span><a href="#modal-forma-envio" id="forma-envio" class="open-popup-link" rel="noindex">Ver formas de envío</a>
  </div>
  <div class="compraAccion Disponibilidad clearfix">
    <?php
    if(isset($node['stock_talles'])) {
    ?>
      <span> </span>
      <p>
      Talles:
        <?php
        $talles = array();
        foreach($node['stock_talles'] as $stock) {
          $talles[] = $stock['talle'];
        }
        print implode(', ', $talles);
        ?>
      </p>
    <?php
    } else {
    ?>
    <span></span><p>Cantidad disponible: <span class="Cantidad"> <?php (is_numeric($node['datos_venta']['cantidad']))? print $node['datos_venta']['cantidad'].' items' : print $node['datos_venta']['cantidad'] ?> </p>
    <?php
    }
    ?>
  </div>
  <div class="compraAccion Seguridad clearfix">
    <span></span><p>Comprar en Productos Clasificados es seguro.</p>
  </div>
  <div class="compraAccion VerMas clearfix">
    <a href="#modal-saber-mas" class="open-popup-link" rel="noindex">Saber más</a>
  </div>
</div>
<?php } ?>

<?php if($node['publicado']){ ?>	
<div class="container">
	<div class="row">
		<div class="twelvecol">
			<div class="content cuerpo">
        <h2 class="descripcion">Descripción</h2>
        <div class="texto <?php /*if($node['cxense']['gcl-tipo-aviso'] == 'producto')*/ print 'acortado' ?>">
          <p>
            <?php echo str_replace('?q=','',$node['body']); ?>
          </p>
        </div>
        <?php //if($node['cxense']['gcl-tipo-aviso'] == 'producto') { ?>
          <div class="ver-mas" style="display:none;">Ver más</div>
        <?php //} ?>
      </div>
		</div>
	</div>
</div>
<?php } ?>	

<div class="contactar">
	<a class="botonC " href="#formulario_contacto" title="Contactar al vendedor" alt="Contactar al vendedor" rel="noindex">Contactar al vendedor</a>
  <?php if($node['cxense']['gcl-tipo-aviso'] != 'producto') { ?>
    
    
    <?php if(!empty($node['field_aviso_tel_vendedor'])){ ?>
      <?php $array_tel = explode('/', $node['field_aviso_tel_vendedor']); ?>
      <?php if(count($array_tel) == 1) { ?>
        <a class="botonC llamar" href="tel:<?php echo preg_replace("/[^0-9]/","", $node['field_aviso_tel_vendedor']); ?>" rel="noindex"><span><b class="DN">Llamar</b><span></a>
      <?php } else { ?>
        <a class="botonC llamar" href="javascript:void(0);" rel="noindex"><span><b class="DN">Llamar</b><span></a>
        <div class="llamarMultiple">
          <div class="llamarMultipleClose">Cerrar</div>
        <?php foreach($array_tel as $num_tel) { ?>
          <?php if(strlen($num_tel)>7 && strlen($num_tel)<15) { ?>
          <a href="tel:<?php echo preg_replace("/[^0-9]/","", $num_tel); ?>" rel="noindex"><?php echo preg_replace("/[^0-9]/","", $num_tel); ?></a>
          <?php } ?>
        <?php } ?>
        </div>
      <?php } ?>
    <?php } ?>
    
    
    <?php if(!empty($node['field_aviso_mail_vendedor'])){ ?>
    <a class="botonC enviar" href="mailto:<?php echo $node['field_aviso_mail_vendedor']; ?>" title="Enviar formulario" alt="Enviar formulario" rel="noindex"><span><b class="DN">Enviar formulario</b></span></a>
    <?php } ?>
    
    
    <?php if(!empty($node['field_aviso_tel_vendedor'])){ ?>
      <?php $array_tel = explode('/', $node['field_aviso_tel_vendedor']); ?>
      <?php if(count($array_tel) == 1) { ?>
        <a class="botonC sms" href="sms:<?php echo preg_replace("/[^0-9]/","", $node['field_aviso_tel_vendedor']); ?>" title="Enviar sms" alt="Enviar sms" rel="noindex"><span><b class="DN">Enviar sms</b></span></a>
      <?php } else { ?>
        <a class="botonC sms" href="javascript:void(0);" rel="noindex"><span><b class="DN">Enviar sms</b></span></a>
        <div class="smsMultiple">
          <div class="smsMultipleClose">Cerrar</div>
        <?php foreach($array_tel as $num_tel) { ?>
          <?php if(strlen($num_tel)>7 && strlen($num_tel)<15) { ?>
          <a href="sms:<?php echo preg_replace("/[^0-9]/","", $num_tel); ?>" rel="noindex"><?php echo preg_replace("/[^0-9]/","", $num_tel); ?></a>
          <?php } ?>
        <?php } ?>
        </div>
      <?php } ?>
    <?php } ?>
    
    <?php if(!empty($node['profile_telefono_whatsapp']) && strlen($node['profile_telefono_whatsapp'])>7 && strlen($node['profile_telefono_whatsapp'])<15){ ?>
    <a class="botonC whatsapp" href="https://api.whatsapp.com/send?phone=<?php echo preg_replace("/[^0-9]/","", $node['profile_telefono_whatsapp']); ?>&text=Estoy%20interesado%20en%20el%20aviso%20<?php echo urlencode($this->config->item('sitio_host').$_SERVER['REQUEST_URI']); ?>" title="Contactar vía WhatsApp" alt="Contactar vía WhatsApp" rel="noindex"><span><b class="DN"></b></span></a>
    <?php } ?>
  <?php } ?>
</div>
<?php } ?>
</div>
</div>
</div>

<div class="container">
	<div class="row">
		<div class="twelvecol">
			<div class="content cuerpo">
        <div class="caracteristicas <?php if($node['cxense']['gcl-tipo-aviso'] == 'producto') print "desplegable"; ?>">
          <div class="texto">
          <?php foreach($node['caracteristicas_b'] as $carct_b){ ?>
            <h2><?php print $carct_b['title']; ?></h2><ul>
              <?php foreach($carct_b['data'] as $data){ ?>
                <?php if(isset($data['label'])){ ?>
                  <li><span><?php print $data['label']; ?>:</span> <?php print $data['value']; ?></li>
                <?php } else { ?>
                  <li><?php print $data['value']; ?></li>
                <?php } ?>
              <?php } ?>
            </ul>
          <?php } ?>
          </div>
        </div>

<?php if(isset($node['datos_comercio']) && !empty($node['datos_comercio'])){ ?>        
        <section class="datosVendedores">
          <h3>Datos del vendedor </h3>
<?php if(!empty($node['datos_comercio']['comercio_imagen'])){ ?>          
          <div class="thumb">
            <a class="ver_mas" href="/search/result/filters=is_uid:<?php echo $node['uid']; ?>%20" rel="search">
              <img alt="<?php print 'Logo de '.$node['datos_comercio']['comercio_tipo'].' '.$node['datos_comercio']['comercio_nombre']; ?>" title="<?php echo $node['datos_comercio']['comercio_nombre']; ?>" src="<?php print $node['datos_comercio']['comercio_imagen']; ?>" class="imagecache imagecache-logo_250_200" >
            </a>
          </div>
<?php } ?>          
          <div class="ficha"><p><?php print $node['datos_comercio']['comercio_tipo']; ?>: <strong><?php print $node['datos_comercio']['comercio_nombre']; ?></strong></p></div>
          
          <?php if(!empty($node['datos_venta']['vendedor']['texto_info'])) { ?> 
            <div class="info"><?php print $node['datos_venta']['vendedor']['texto_info']; ?></div>
          <?php } ?>
          <div class="mas_avisos">
            <a class="ver_mas" href="/search/result/filters=is_uid:<?php echo $node['uid']; ?>%20" rel="search">Ver más avisos del vendedor</a>
          </div>
        </section>
<?php } else { ?>
        <a class="ver_mas" href="/search/result/filters=is_uid:<?php echo $node['uid']; ?>%20" rel="search">Ver más avisos del vendedor</a>
<?php } ?>

<?php if(isset($node['datos_venta']['disponible']) && !empty($node['datos_venta']['disponible'])) { ?>
        <div class="comprar"><a href="#modal-compra-datos" id="inicio-compra" class="open-popup-link" rel="noindex">Comprar</a></div>
<?php } ?>
        
<?php if(isset($node['mapas']) && !empty($node['mapas']) && $node['comscore']['rubro_raiz'] != 'vehiculos'){ ?>
        <a name="img_mapa" rel="noindex"></a>
        <h3>Ubicación</h3>
      </div>
      <figure class="mapa">
        <a href="http://maps.google.com/maps?hl=es&amp;q=<?php print $node['mapas']['latitude']; ?>,<?php print $node['mapas']['longitude']; ?>" rel="nofollow">
          <!-- <img src="http://maps.google.com/maps/api/staticmap?center=<?php print $node['mapas']['latitude']; ?>,<?php print $node['mapas']['longitude']; ?>&zoom=17&size=462x253&maptype=roadmap&markers=color:green|<?php print $node['mapas']['latitude']; ?>,<?php print $node['mapas']['longitude']; ?>&key=<?php print $this->config->item('gmap_api'); ?>" /> -->
          
          <div id="multimedia_mapas"></div>
          
          <script>
            map = new OpenLayers.Map("multimedia_mapas");
            map.addLayer(new OpenLayers.Layer.OSM());
            var lonLat = new OpenLayers.LonLat( <?php print $node['mapas']['longitude']; ?> ,<?php print $node['mapas']['latitude']; ?> )
                  .transform(
                    new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                    map.getProjectionObject() // to Spherical Mercator Projection
                  );
            var zoom = 15;
            var markers = new OpenLayers.Layer.Markers( "Markers" );
            map.addLayer(markers);
            markers.addMarker(new OpenLayers.Marker(lonLat));
            map.setCenter (lonLat, zoom);
          </script>
          
          <!-- 
          <div class="boton-como-llegar">
            <span class="texto-como-llegar">Cómo llegar</span>
            <span class="logo-como-llegar"></span>
          </div>
          -->
        </a>
      </figure>
<?php } ?>      
      <div class="content cuerpo">
        <h3 class="<?php if($node['cxense']['gcl-tipo-aviso'] == 'producto') print "desplegable asc"; ?>">Ficha</h3>
        <div class="caracteristicas <?php if($node['cxense']['gcl-tipo-aviso'] == 'producto') print "desplegable"; ?>">
          <div class="texto">
            <ul class="tabla">
            <?php $count = 1; foreach($node['caracteristicas_a'] as $carct_a){ ?>
              <?php if($carct_a['filter'] != ''){ ?>
                <li class="<?php if($count%2 != 0) print 'stripe'; ?> clearfix"><span class="field"><?php print $carct_a['label']; ?>:</span><span class="caract"><a href="/search/result/<?php print str_replace('"', '%22', $carct_a['filter']); ?>" rel="search"><?php print $carct_a['value']; ?></a></span></li>
              <?php } else { ?>
                <li class="<?php if($count%2 != 0) print 'stripe'; ?> clearfix"><span class="field"><?php print $carct_a['label']; ?>:</span><span class="caract"><?php print $carct_a['value']; ?></span></li>
              <?php } $count++; ?>
            <?php } ?>
            </ul>
          </div>
        </div>
      </div>
      
      <div class="content cuerpo">
<?php if($node['publicado']){ ?>
        <h4>Contacto</h4>
        
        <?php if(!empty($node['field_aviso_numero_matricula'])){ ?>
          <h5><span class="label">N° Matrícula:</span> <?php print $node['field_aviso_numero_matricula']; ?></h5>
        <?php } ?>
        
        <?php if(!empty($node['field_aviso_tel_vendedor'])){ ?>
          <h5><span class="label">Teléfono:</span> <?php print $node['field_aviso_tel_vendedor']; ?></h5>
        <?php } ?>
        <p>Completá tus datos para contactar al vendedor</p>
        <a name="formulario_contacto" rel="noindex"></a>  
        <form name="formContacto" class="formContacto" action="/contacto" method="post" onsubmit="return validar_form_contacto();">
          <div class="campo"><input name="contactar_vendedor_nombre" type="text" value="<?php if(!empty($contacto)) print $contacto['contacto_nombre']; ?>" placeholder="Nombre"/></div>
          <div class="campo"><input name="contactar_vendedor_telefono" type="text" value="<?php if(!empty($contacto)) print $contacto['contacto_telefono']; ?>" placeholder="Teléfono"/></div>
          <div class="campo"><input name="contactar_vendedor_mail" type="text" value="<?php if(!empty($contacto)) print $contacto['contacto_mail']; ?>" placeholder="E-mail"/></div>
          <div class="campo"><textarea name="contactar_vendedor_consulta" rows="5" placeholder="Escriba su consulta aquí"><?php if(!empty($contacto)) print $contacto['contacto_consulta']; ?></textarea></div>
          <input name="contactar_vendedor_aviso" type="hidden" value="<?php print $node['nid']; ?>" />
          <div class="g-recaptcha" id="RecaptchaField1"></div>
          <div id="message-form" style="display:none;"><div id="message-text" class="fondoResaltado"></div></div>
          <input id="submit-contacto" name="" type="submit"/>
          <div id="ajax-loading" style="display:none"><img src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/ajax-loader.gif" /></div>
        </form>
<?php } ?>
			</div>
      
<?php if($node['publicado']){ ?>
      <div class="denuncia-content cuerpo">
        <h4>Denunciar Aviso</h4>
        <div class="message-legal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Ayudanos a mejorar. </strong>Denunciá el aviso si el mismo ofrece algún producto o servicio que quebrante cualquier legislación vigente (ejemplo Decreto Nacional 936/11 y Decreto Provincial 365/12) o incluye contenido inmoral, obsceno, pornográfico, discriminatorio, injurioso o cualquier otro contenido que afecte la privacidad de las personas, la propiedad intelectual o que sea contrario a las buenas costumbres.  </div>
        <div class="denuncia-boton" onclick="toggle_menu('denuncia-content-form');">Denunciar</div>
        <div id="denuncia-content-form" style="display: none;">
          <form name="formDenuncia" class="formDenuncia" action="/denuncia" method="post" onsubmit="return validar_form_denuncia();">            
            <div class="campo"><span class="requerido">*</span><select name="denuncia_motivo" id="edit-motivo" class="form-select required" ><option value="0">- Motivo -</option><option value="1">Lenguaje obseno</option><option value="2">Temas de adultos</option><option value="3">Lenguaje racista o sexista</option><option value="4">Contiene información privada</option><option value="5">Otro</option></select></div>
            <div class="campo"><span class="requerido">*</span><textarea name="denuncia_descripcion" rows="5" placeholder="Escribe tu denuncia aquí"></textarea></div>
            <div class="campo"><span class="requerido">*</span><input name="denuncia_mail" type="text" value="" placeholder="E-mail"/></div>
            <input name="denuncia_robot" type="hidden" value="0" />
            <input name="denuncia_aviso" type="hidden" value="<?php print $node['nid']; ?>" />
            <div id="message-form-denuncia" style="display:none;"><div id="message-text-denuncia" class="fondoResaltado"></div></div>
            <input id="submit-denuncia" name="" type="submit"/>
            <div id="ajax-loading-denuncia" style="display:none"><img src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/ajax-loader.gif" /></div>
          </form>
        </div>
        <div id="message-form-denuncia-ok" style="display:none;"><div id="message-text-denuncia-ok" class="fondoResaltado"></div></div>
      </div>
<?php } ?>
     
		</div>
	</div>
</div>

<div class="container">
  <div class="row">
    <div class="content cuerpo">
      <div class="content cuerpo share">
      <div class="facebook"><a href="http://m.facebook.com/sharer.php?u=<?php echo urlencode($this->config->item('sitio_host').$_SERVER['REQUEST_URI']); ?>&amp;t=<?php echo urlencode($node['title']); ?>" target="_blank" rel="nofollow"></a></div>
      <div class="twitter"><a href="http://twitter.com/share?text=Estoy%20viendo%20el%20aviso%20<?php echo urlencode($node['title']); ?>%20en%20Clasificados%20La%20Voz%20M%C3%B3vil&amp;url=<?php echo urlencode($this->config->item('sitio_host').$_SERVER['REQUEST_URI']); ?>" target="_blank" rel="nofollow"></a></div>
      <div class="whatsapp"><a href="whatsapp://send?text=<?php echo urlencode($node['title'].' - '.$this->config->item('sitio_host').$_SERVER['REQUEST_URI']); ?>" data-text="" data-action="share/whatsapp/share" rel="nofollow"></a></div>
      <div class="mail"><a href='mailto:?subject=Te%20enviaron%20una%20aviso%20desde%20Clasificados%20La%20Voz%20Mobile &amp;body=Te%20enviaron%20una%20aviso%20desde%20Clasificados%20La%20Vos%20Mobile%0A%0A<?php echo strip_tags($node['title']); ?>%0A%0A<?php echo strip_tags($node['body']); ?>%0A%0A%0A%0A<?php echo urlencode($this->config->item('sitio_host').$_SERVER['REQUEST_URI']); ?>' rel="noindex"></a></div>
      </div>
    </div>
  </div>
</div>

</article>
</div>
<?php
if(count($node['field_aviso_fotos_micrositios'])>1){ ?>
<script type="text/javascript" src="/public/js/swipe.js"></script>
<script>
window.slider = new Swipe(document.getElementById('slider-fotos'),{continuous: true});
window.sliderRenders = new Swipe(document.getElementById('slider-renders'),0);
window.sliderPlanos = new Swipe(document.getElementById('slider-planos'),0);
</script>
<?php } ?>
<?php if(isset($node['mapas']) && !empty($node['mapas'])){ ?>
<script type="text/javascript">
  var latitud;
  var longitud;
  $(document).ready(function() {
    localizame();
  });
  function localizame() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(coordenadas);
    }
  }
  function coordenadas(position) {
    latitud = position.coords.latitude;
    longitud = position.coords.longitude;
    $('.mapa a').attr('href', 'https://maps.google.com/maps?saddr=' + latitud + ',' + longitud + '&daddr=<?php print $node['mapas']['latitude']; ?>,<?php print $node['mapas']['longitude']; ?>&output=classic&dg=ntvb');
  }
</script>
<?php } ?>


<!-- MODALES -->
<?php if(isset($node['datos_venta']['disponible']) && !empty($node['datos_venta']['disponible']) && $node['publicado']) { ?>
<div id="modal-forma-envio" class="white-popup mfp-hide">
  <h2>Formas de Envío</h2>
  <?php if($node['datos_venta']['vendedor']['entrega_domicilio']) { ?>
  <div class="ContentCalcular">
    <h3>Envío a domicilio</h3>
      <p>Ingresá tu dirección para calcular el costo de envío </p>
      <form name="getDireccion" id="compraCalculoDireccion" method="post">
        <input type="text" name="entrega_direccion" id="direccion_costo_envio" title="Ej: San Martín 578, Córdoba, Córdoba" placeholder="Ej: San Martín 578, Córdoba, Córdoba">
        <input type="hidden" name="vendedor_uid" id="vendedor_uid_costo_envio" value="<?php print $node['datos_venta']['vendedor']['id_vendedor']; ?>">
        <input type="button" value="Calcular" class="form-submit calcular_costo_envio">
        <div class="loader-envio" style="display: none;"><img src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/ajax-loader-tienda.gif" /></div>
        <div class="help-block with-errors-envio" style="display: none;"></div>     
      </form>
  </div>
  <?php } ?>
  <?php if($node['datos_venta']['vendedor']['entrega_sucursal']) { ?>
  <div class="ContentSucursales">
    <h3>Retirar en sucursal</h3>
      <div class="FondoSucursales">
        <div class="Direcciones">
          <?php foreach($node['datos_venta']['vendedor']['sucursales'] as $sucursal) { ?>
            <a href="#" class="Links lugar_entrega" data-latitud="<?php print $sucursal['latitud']; ?>" data-longitud="<?php print $sucursal['longitud']; ?>" data-nombre="<?php print $sucursal['nombre']; ?>" data-direccion="<?php print $sucursal['direccion']; ?>" rel="noindex"><?php print $sucursal['direccion']; ?></a>
          <?php } ?>
        </div>
        <div class="Mapa" id="mapa_envio" style="width: 100%; height: 315px;"></div>
      </div>
  </div>
  <?php } ?>
</div>
<div id="modal-compra-datos" class="white-popup mfp-hide clearfix">
  <div>
    <div class="currentBuy">
      <h3>Estás comprando:</h3>
      <div class="row producto clearfix">
        <div class="col-lg-4 col-md-4 col-sm-4"><img src="<?php print $node['fotos_tienda_musa_compra']; ?>" alt="" title="" width="100%"></div>    
        <div class="col-lg-8 col-md-8 col-sm-8 descripcion">  
          <h2><?php print $node['title']; ?></h2>
          <div class="mensajePago">Precio en 1 pago</div>
          <div class="Precio"><?php echo $node['field_aviso_moneda'].' '.$node['field_aviso_precio']; ?></div>
          <?php if($node['datos_venta']['precio_anterior']>0) { ?>
            <span class="PrecioAnterior"><?php print $node['field_aviso_moneda'].$node['datos_venta']['precio_anterior']; ?></span>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="clearfix avance">
      <div id="paso-uno" class="item activo"><span class="numero">1</span> Datos de contacto</div>
      <div id="paso-dos" class="item"><span class="numero">2</span> Datos de entrega</div>
      <div id="paso-tres" class="item"><span class="numero">3</span> Confirmación</div>
    </div>
    <div id="content-pasos-compra">
      <div class="consigna">Complete los datos para continuar con el proceso de compra</div>
      <form name="sentMessage" id="compraDatosForm" class="has-validation-callback" method="post">
        <div class="control-group form-group">
          <div class="controls">
            <label class="sr-only">Nombre:</label>
            <input type="text" name="compra_nombre" class="form-control" placeholder="Nombre:" required>
            <p class="help-block"></p>
          </div>
        </div>
        <div class="control-group form-group">
          <div class="controls">
            <label class="sr-only">Apellido:</label>
            <input type="text" name="compra_apellido" class="form-control" placeholder="Apellido:" required>
            <p class="help-block"></p>
          </div>
        </div>
        <div class="control-group form-group">
          <div class="controls">
            <label class="sr-only">Teléfono:</label>
            <input type="text" name="compra_telefono" class="form-control" placeholder="Teléfono:" required>
          </div>
        </div>
        <div class="control-group form-group">
          <div class="controls">
            <label class="sr-only">Email:</label>
            <input type="email" name="compra_email" class="form-control" placeholder="Email:" required>
          </div>
        </div>
        <?php if($node['datos_venta']['vendedor']['envio_andreani']) { ?>
        <div class="control-group form-group">
          <div class="controls">
            <label class="sr-only">Documento:</label>
            <input type="text" name="compra_documento" class="form-control" placeholder="Documento:" required>
          </div>
        </div>
        <?php } ?>
        <div class="control-group form-group">
          <div class="controls">
            <label class="sr-only">Comentarios:</label>
            <textarea rows="5" cols="100" name="compra_comentarios" class="form-control" placeholder="Comentarios:" maxlength="999" style="resize:none"></textarea>
          </div>
        </div>
        <div class="g-recaptcha modal" id="RecaptchaField2"></div>
        <input type="hidden" name="robots" id="robots" value="">
        <input type="hidden" name="compra_nid" value="<?php print $node['nid']; ?>">
        <input type="hidden" name="compra_vendedor_id" value="<?php print $node['datos_venta']['vendedor']['id_vendedor']; ?>">
        <input type="hidden" name="compra_precio" value="<?php print $node['field_aviso_precio']; ?>">
        <div class="help-block with-errors" style="display: none;"></div>
        <!-- For success/fail messages -->
        <button type="submit" class="btn btn-primary pull-right" id="btn-compra-datos">Continuar</button>
        <div class="loader" style="display: none;"><img class="" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/ajax-loader-tienda.gif" /></div>
      </form>
    </div>
  </div>    
</div>
<div id="modal-saber-mas" class="white-popup mfp-hide">
  <h2>Compra segura</h2>
  <p>Todos los productos ofrecidos a través de las tiendas virtuales de <strong>Clasificados La Voz</strong> pertenecen a comercios previamente autorizados y verificados. <br />
  Publicitan sus productos de manera oficial a través de nuestras tiendas. Puede comprar con seguridad y tranquilidad. </p>
  <p>El proceso de compra es muy simple y fácil: Abona el producto, le llegará un correo confirmado y luego se comunicarán del comercio para ajustar detalles de entrega y/o retiro.<br />
  La facturación, logística, entrega y/o distribución es responsabilidad del comercio que publicita el producto, ante cualquier consulta sobre el producto puede comunicarse con el comercio en forma directa a través del formulario de contacto ubicado en cada aviso. </p>
  <p> Para conocer más sobre el servicio, sugerencias u observaciones puede escribirnos a <a href="mailto:clasificadosweb@lavozdelinterior.com.ar" rel="noindex"><strong>clasificadosweb@lavozdelinterior.com.ar</strong></a> </p>
</div>
<?php } ?>
<!-- FIN MODALES -->



<script type="text/javascript"> 
  function validar_form_contacto(){
    document.getElementById("submit-contacto").style.display="none";
    document.getElementById("ajax-loading").style.display="block";
    document.getElementById("message-form").style.display="none";
    document.getElementById("submit-contacto").disabled = true;
    document.getElementById("message-text").innerHTML= "";
    var Formulario = document.forms['formContacto'];
    var longitudFormulario = document.forms['formContacto'].length;
    var cadenaFormulario = "";
    for (var i=0;i<=longitudFormulario-1;i++){
      var valorPost = "";
      if(Formulario.elements[i].name == 'contactar_vendedor_nombre' && Formulario.elements[i].value != 'Nombre')
        valorPost = Formulario.elements[i].value;
      if(Formulario.elements[i].name == 'contactar_vendedor_telefono' && Formulario.elements[i].value != 'Teléfono')
        valorPost = Formulario.elements[i].value;
      if(Formulario.elements[i].name == 'contactar_vendedor_mail' && Formulario.elements[i].value != 'E-mail')
        valorPost = Formulario.elements[i].value;
      if(Formulario.elements[i].name == 'contactar_vendedor_consulta' && Formulario.elements[i].value != 'Escriba su consulta aquí')
        valorPost = Formulario.elements[i].value;
      if(Formulario.elements[i].name == 'contactar_vendedor_aviso')
        valorPost = Formulario.elements[i].value;
      
      if(Formulario.elements[i].name != "" && Formulario.elements[i].name != 'g-recaptcha-response'){
        if(cadenaFormulario == "")
          cadenaFormulario+=Formulario.elements[i].name+'='+encodeURI(valorPost);
        else
          cadenaFormulario+="&"+Formulario.elements[i].name+'='+encodeURI(valorPost);
      }
      if(Formulario.elements[i].name == 'g-recaptcha-response')
        cadenaFormulario += "&"+Formulario.elements[i].name+'='+Formulario.elements[i].value;
    }
    var xmlhttp;
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function(){
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
        var valores = xmlhttp.responseText;
        var json = JSON.parse(valores);
        var status = json['status'];
        var mensaje = json['mensaje'];
        document.getElementById("message-form").className = status;
        document.getElementById("message-form").style.display="block";
        document.getElementById("message-text").innerHTML='<p>'+mensaje+'</p>';
        document.getElementById("submit-contacto").style.display="block";
        document.getElementById("submit-contacto").disabled = false;
        document.getElementById("ajax-loading").style.display="none";
        // Ejecutamos el javascript que viene dentro de mensaje, si es que lo hay.
        var element = document.getElementById("message-text");
        var scripts = element.getElementsByTagName('script');
        for(var i=0;i<scripts.length;i++) {
          eval(scripts[i].innerHTML);
        }
        // Evento contacto ga
        // send_event_ga('contacto', 'formulario', 'mobile');
      } 
    }
    addContactoCookies(cadenaFormulario);
    xmlhttp.open("GET","/contacto/contacto_vendedor?"+cadenaFormulario,true);
    xmlhttp.send();
    return false;
  }
  function validar_form_denuncia(){
    document.getElementById("submit-denuncia").style.display="none";
    document.getElementById("ajax-loading-denuncia").style.display="block";
    document.getElementById("message-form-denuncia").style.display="none";
    document.getElementById("message-form-denuncia-ok").style.display="none";
    document.getElementById("submit-denuncia").disabled = true;
    document.getElementById("message-text-denuncia").innerHTML= "";
    document.getElementById("message-text-denuncia-ok").innerHTML= "";
    var Formulario = document.forms['formDenuncia'];
    var longitudFormulario = document.forms['formDenuncia'].length;
    var cadenaFormulario = "";
    for (var i=0;i<=longitudFormulario-1;i++){
      var valorPost = "";
      if(Formulario.elements[i].name == 'denuncia_motivo' && Formulario.elements[i].value != 0)
        valorPost = Formulario.elements[i].value;
      if(Formulario.elements[i].name == 'denuncia_descripcion' && Formulario.elements[i].value != 'Escribe tu denuncia aquí')
        valorPost = Formulario.elements[i].value;
      if(Formulario.elements[i].name == 'denuncia_mail' && Formulario.elements[i].value != 'E-mail')
        valorPost = Formulario.elements[i].value;
      if(Formulario.elements[i].name == 'denuncia_robot' && Formulario.elements[i].value != '')
        valorPost = Formulario.elements[i].value;
      if(Formulario.elements[i].name == 'denuncia_aviso')
        valorPost = Formulario.elements[i].value;
      
      if(Formulario.elements[i].name != ""){
        if(cadenaFormulario == "")
          cadenaFormulario+=Formulario.elements[i].name+'='+encodeURI(valorPost);
        else
          cadenaFormulario+="&"+Formulario.elements[i].name+'='+encodeURI(valorPost);
      }
    }
    var xmlhttp2;
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp2=new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp2.onreadystatechange=function(){
      if (xmlhttp2.readyState==4 && xmlhttp2.status==200){
        var valores = xmlhttp2.responseText;
        var valor = valores.split(',');
        var status = valor[0].split('=');
        var mensaje = valor[1].split('=');
        document.getElementById("submit-denuncia").style.display="block";
        document.getElementById("submit-denuncia").disabled = false;
        document.getElementById("ajax-loading-denuncia").style.display="none";
        if(status[1] == 'ok'){
          document.getElementById("message-form-denuncia-ok").className = status[1];
          document.getElementById("message-form-denuncia-ok").style.display="block";
          document.getElementById("message-text-denuncia-ok").innerHTML='<p>'+mensaje[1]+'</p>';
          document.getElementById("denuncia-content-form").style.display="none";
        } else {
          document.getElementById("message-form-denuncia").className = status[1];
          document.getElementById("message-form-denuncia").style.display="block";
          document.getElementById("message-text-denuncia").innerHTML='<p>'+mensaje[1]+'</p>';
        }
      } 
    }
    xmlhttp2.open("GET","/denuncia/denuncia_aviso?"+cadenaFormulario,true);
    xmlhttp2.send();
    return false;
  }
  
  jQuery(document).ready(function ($) {
    if($(".cuerpo .acortado").length > 0 && $(".cuerpo .acortado").height() > 150){
      $(".cuerpo .acortado").css({'max-height':'150px','overflow':'hidden','background':'linear-gradient(#FFF 70%, #333)'});
      $(".cuerpo .ver-mas").css('display','block');
      $('.cuerpo .ver-mas').click(function() {
        $(".cuerpo .acortado").css({'max-height':'initial','background':'none'});
        $(".cuerpo .ver-mas").css('display','none');
      });
    }
    
    if($("h2.desplegable").length > 0){
      $('h2.desplegable').on('click',function(){
        $('.caracteristicas.desplegable').toggle('slow');
        if($("h2.desplegable").hasClass("asc")){ 
          $("h2.desplegable").addClass("desc");
          $("h2.desplegable").removeClass("asc");
        } else {
          if($("h2.desplegable").hasClass("desc")){ 
            $("h2.desplegable").addClass("asc");
            $("h2.desplegable").removeClass("desc");
          }
        }
      });
    }
    
    $('.ocultar').hide('');
    $('.link-multimedia').click(function() {
      var multimedia = $(this).attr('data-multimedia');
      $('.multimedia-ficha').hide('');
      $('.content-slider').hide('');
      $('.link-multimedia').removeClass('Act');
      $('#slider-'+multimedia).show('');
      $('#content-slider-'+multimedia).show('');
      $(this).addClass('Act');
    });
    
  });
</script>