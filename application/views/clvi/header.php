<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php 
  $user_session = array();
  if(isset($this->session)) {
    $user_session = $this->session->userdata('logged_in'); 
  }
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php $title = $this->config->item('header_title'); if(isset($head_title) && $head_title != '') $title = $head_title; ?>
<title><?php echo $title; ?></title>
<meta name="description" content="<?php echo (isset($head_description) && $head_description != '')? $head_description : $this->config->item('header_description'); ?>">
<meta property="og:title" content="<?php echo $title; ?>" />
<meta name="keywords" content="<?php echo $this->config->item('head_keywords') ?>" />
<meta name="news_keywords" content="<?php echo $this->config->item('head_keywords') ?>" />
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="icon" type="image/png" href="/public/clvi/img/favicon.png" sizes="192x192">
<meta name="theme-color" content="#008acf">
<?php if((!isset($tipo) || (isset($tipo) && $tipo!='admin')) && 
  $this->uri->segment(1)!='node_add' && ($this->uri->segment(1)!='node' && $this->uri->segment(3)!='edit')) {
?>
<meta http-equiv="refresh" content="600" />
<?php } ?>

<?php
  if(!isset($node)) $node = array();
  meta_og($node, $head_title, $head_description);
?>
<meta property="fb:pages" content="" />

<link href="/public/css/reset.css" rel="stylesheet" type="text/css" />
<link href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/global.css?rand=4" rel="stylesheet" type="text/css" />
<?php if(isset($canonical)) : ?>
<link rel="canonical" href="<?php echo $this->config->item('sitio_origen').'/'.$canonical; ?>" />
<?php endif; ?>

<!-- 1140px Grid styles for IE -->
<!--[if lte IE 9]><link rel="stylesheet" href="/public/css/ie.css" type="text/css" media="screen" /><![endif]-->
<!-- The 1140px Grid - http://cssgrid.net/ -->
<link rel="stylesheet" href="/public/css/1140.css" type="text/css" media="screen" />
<!-- Your styles -->
<link rel="stylesheet" href="/public/css/styles.css" type="text/css" media="screen" />

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<link href="/public/fonts/stylesheet.css" rel="stylesheet" type="text/css" />

<?php if(isset($tipo) && ($tipo=='tienda' || $tipo=='buscador')){ ?>
<link rel="stylesheet" href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/tienda.css" type="text/css" media="screen">
<link rel="stylesheet" href="/public/css/magnific-popup.css" type="text/css" media="screen">
<?php } ?>
<?php if(isset($tipo) && $tipo=='admin') { ?>
<link href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/admin.css?rand=10" rel="stylesheet" type="text/css" />
<?php } ?>

<?php if(isset($calendario_backend) && $calendario_backend) { ?>
<link href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/booking/jquery.dop.Select.css" rel="stylesheet" type="text/css" />
<link href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/booking/jquery.dop.BackendBookingCalendarPRO.css" rel="stylesheet" type="text/css" />
<link href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/booking/jquery.dop.FrontendBookingCalendarPRO.css" rel="stylesheet" type="text/css" />
<?php } ?>

<?php if(isset($calendario_front) && $calendario_front) { ?>
<link href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/booking/jquery.dop.Select.css" rel="stylesheet" type="text/css" />
<link href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/booking/jquery.dop.FrontendBookingCalendarPRO.css" rel="stylesheet" type="text/css" />
<link href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/booking/style.css" rel="stylesheet" type="text/css" />
<link href="/public/css/fivestar.css" rel="stylesheet" type="text/css" />
<?php } ?>

<?php if(isset($es_buscador_alojamiento) && $es_buscador_alojamiento) { ?>
<link href="/public/css/dhtmlx.css" rel="stylesheet" type="text/css" />
<link href="/public/css/ui.datepicker.css" rel="stylesheet" type="text/css" />
<link href="/public/css/fivestar.css" rel="stylesheet" type="text/css" />
<?php } ?>

<?php if(isset($es_registro) && $es_registro) { ?>
<link href="/public/css/ui.datepicker.css" rel="stylesheet" type="text/css" />
<?php } ?>

<?php if((isset($es_buscador_alojamiento) && $es_buscador_alojamiento) || (isset($calendario_front) && $calendario_front) || (isset($es_prehome) && $es_prehome) || $tipo == 'filtro') { ?>
<link href="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/css/styles-new.css" rel="stylesheet" type="text/css" />
<?php } ?>
<?php if(empty($user_session)) { 
  if((isset($head_tipo) && ($head_tipo=='aviso' || $head_tipo=='buscador'))) { 
    if((isset($tipo) && $tipo=='tienda')) { ?>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallbackFichaTienda&render=explicit" async defer></script>
  <?php } else { ?>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallbackFicha&render=explicit" async defer></script>
  <?php }
  }
  if($this->uri->segment(1)=='contact') { ?>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallbackContacto&render=explicit" async defer></script>  
  <?php }
} ?>

<!--css3-mediaqueries-js - http://code.google.com/p/css3-mediaqueries-js/ - Enables media queries in some unsupported browsers-->
<script type="text/javascript" src="/public/js/css3-mediaqueries.js"></script>
<!--[if lte IE 8]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="/public/js/jquery.js" ></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/base.js" ></script>
<?php if(isset($tipo) && $tipo=='admin') { ?>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/admin.js?rand=13"></script>
<?php } ?>

<?php if(isset($calendario_backend) && $calendario_backend) { ?>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/booking/jquery-ui-1.11.1.min.js" ></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/booking/jquery.dop.Select.js" ></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/booking/dop-prototypes.js" ></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/booking/jquery.dop.BackendBookingCalendarPRO.js" ></script>
<?php } ?>

<?php if(isset($calendario_front) && $calendario_front) { ?>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/booking/jquery-ui-1.11.1.min.js" ></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/booking/jquery.dop.Select.js" ></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/booking/dop-prototypes.js" ></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/booking/jquery.dop.FrontendBookingCalendarPRO.js" ></script>
<script type="text/javascript" src="/public/js/fivestar.js" ></script>
<?php } ?>

<?php if(isset($es_prehome) && $es_prehome) { ?>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/buscador-prehome.js" ></script>
<?php } ?>

<?php if(isset($es_buscador_alojamiento) && $es_buscador_alojamiento) { ?>
<script type="text/javascript" src="/public/js/ui.datepicker.min.js" ></script>
<script type="text/javascript" src="/public/js/dhtmlx.js" ></script>
<script type="text/javascript" src="/public/js/markerclusterer.js" ></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/buscador-alojamiento.js" ></script>
<script type="text/javascript" src="/public/js/fivestar.js" ></script>
<?php } ?>

<?php if(isset($es_registro) && $es_registro) { ?>
<script type="text/javascript" src="/public/js/ui.datepicker.min.js" ></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/admin.js" ></script>
<?php } ?>

<?php if($this->uri->segment(1)=='node_add' || ($this->uri->segment(1)=='node' && $this->uri->segment(3)=='edit')) { ?>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/edicion.js"></script>
<script type="text/javascript" src="/public/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinymce.init({
  selector: 'textarea',
  theme : "simple",
});
</script>
<?php } ?>

<?php if($head_tipo = 'aviso') { ?>
<script src="http://www.openlayers.org/api/OpenLayers.js"></script>
<?php } ?>

<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>

<script type='text/javascript'>
var SITIO_ORIGEN = '<?php echo $this->config->item('sitio_origen'); ?>';
</script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
  <?php if(isset($scripts)) print $scripts; ?>
</script>

<?php if(isset($adslots)) { ?>
<script>
  var ads = { styles: {}, light:true, loadGPT:true };
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>
<script async='async' src='https://cdn.jsdelivr.net/gh/adcase/adcase.js@2/dist/adcase.js'></script>
<script>
  googletag.cmd.push(function() {
    <?php foreach($adslots as $adslot) { ?>
      <?php if(isset($adslot['adblock'])) { ?>
      googletag.defineSlot('<?php print $adslot['adblock']; ?>', <?php print $adslot['adsizes']; ?>, '<?php print $adslot['adunit']; ?>')
      <?php if (isset($adslots['target']) && !empty($adslots['target'])){ ?>
        <?php foreach($adslots['target'] as $target_slot) { ?>
        .setTargeting('<?php print $target_slot['key']; ?>', ['<?php print $target_slot['value']; ?>'])
        <?php } ?>
      <?php } ?>
      .addService(googletag.pubads());
      <?php } ?>
    <?php } ?>
    googletag.pubads().setTargeting('adcase', ads.logData);
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs(true, true);
    googletag.pubads().setCentering(true);
    googletag.pubads().addEventListener('slotRenderEnded', function(event) {
        ads.slotRendered(event);
    });
    googletag.enableServices();
  });
</script>
<?php } ?>

</head>

<body class="<?php if($tipo=='buscador') print 'body-result'; ?>">

<div id="wrapper" class="section-<?php if(!empty($head_tipo)) echo $head_tipo; else if(isset($tipo)) echo $tipo; ?>">
<A NAME="TOP" rel="noindex"></A>

<?php if($tipo=='buscador'){ ?>
<div class="header-fijo">
<?php } ?>

<header>
	<div class="container">
		<div class="row">
			<div class="twelvecol">
				<div class="logo"><a href="/" rel="index">Clasificados La Voz</a></div>
          <div id="datos_cuenta" class="cuenta"></div>       
			</div>
		</div>
	</div>
</header>
<?php
$url = $_SERVER["REQUEST_URI"];
$url_explode = explode('/', $url);
$url_array = array();
$url_map_array = array();
foreach($url_explode as $url_part){
  if($url_part != '') {
    if(strpos($url_part,'filters=')===false && strpos($url_part,'page=')===false && strpos($url_part,'latitud=')===false && strpos($url_part,'longitud=')===false)
      $url_array[] = $url_part;
    if(strpos($url_part,'latitud=')!==false || strpos($url_part,'longitud=')!==false)
      $url_map_array[] = $url_part;
  }
}
$url_final = implode('/', $url_array);
if(strpos($url_final,'search')===false || $url_final == '')
  $url_final = 'search/result/';
$url_map = implode('/', $url_map_array);
?>
<nav id="main" class="main" style="display: none;">
<?php
if(isset($rubros)) {
  foreach($rubros as $krubro => $vrubro) {
    if(is_object($vrubro)) {
      foreach($vrubro as $kr => $vr){
        if($kr=='name'){
	?>
	<a onclick="toggle_menu('<?php echo mb_strtolower($vrubro->name) ?>');" href="javascript:;" rel="noindex"><?php echo $vrubro->name ?></a>
	<ul class="sub" id="<?php echo mb_strtolower($vrubro->name) ?>" style="display: none;">
	<?php
        } elseif(is_numeric($kr)){
		?>
		<li><a href="/<?php print $url_final; ?><?php if($vr->filtro != '') print '/'.$vr->filtro; ?><?php if($url_map != '') print '/'.$url_map; ?>" rel="search"><?php echo $vr->name ?></a></li>
	<?php
        }
      }
    }
	?>
	</ul>
	<?php
  }
}
?> 
</nav>
<nav id="menu_admin" class="main DN">
  <a class="no-select" href="/administrar/mis-avisos.html">Mis Avisos</a>
  <a class="no-select" href="/node_add">Publicar Aviso</a>
  <a class="no-select" href="/contactos_recibidos">Consultas</a>
  <a class="no-select" href="/estado_cuenta">Estado de cuenta</a>
  <?php if(!empty($user_session)) { ?>
  <a class="no-select" href="/user/<?php print $user_session->uid; ?>/edit">Editar Perfil</a>
  <?php } ?>
</nav>
<script type="text/javascript">
    var sugerencias_historial = new Array();
    var xmlhttp;
    
    var typewatch = function(){
      var timer = 0;
      return function(callback, ms){
          clearTimeout (timer);
          timer = setTimeout(callback, ms);
      }
    }();
    
    function toggle_menu(id) {
      var e = document.getElementById(id);
      if(e.style.display == 'block') {
        e.style.display = 'none';
      } else {
        e.style.display = 'block';
      }
    }
    function buscar(){
      var filtro = document.getElementById('query').value;
      var oculto = document.getElementsByClassName('buscador-hidde').value;
      if(filtro != 'Encontrá lo que quieras' && filtro != '' && $('.buscador-hidde').css('display') == 'block'){
        window.location = '/search/result/' + document.getElementById('query').value + '<?php if($url_map != '') print '/'.$url_map; ?>';
      }
    }
    function searchKeyPress(e)
    {
        // look for window.event in case event isn't passed in
        if (typeof e == 'undefined' && window.event) { e = window.event; }
        if (e.keyCode == 13){
          registrar_sugerencia(document.getElementById('query').value);
          setTimeout(buscar,500);
          //buscar();
        }
    }
    function envioBusqueda()
    {
        registrar_sugerencia(document.getElementById('query').value);
        setTimeout(buscar,500);
        //buscar();
    }
    function envioSugerencia(filtro){
        registrar_sugerencia(filtro);
        setTimeout("window.location='/search/result/" + filtro + "<?php if($url_map != '') print '/'.$url_map; ?>'",500);
    }
    function onFocusText()
    {
      if(document.getElementById('query').value == 'Encontrá lo que quieras')
        document.getElementById('query').value = '';
    }
    function onBlurText()
    {
      if(document.getElementById('query').value == '')
        document.getElementById('query').value = 'Encontrá lo que quieras';
    }
    function obtener_sugerencias(texto){
      if(xmlhttp!=undefined){
        xmlhttp.abort();
      }
      typewatch(function(){obtener_sugerencias_ajax(texto);}, 380 );
    }
    
    function obtener_sugerencias_ajax(texto)
    {
        var texto = texto;
        if(texto.length > 2) {
          if(sugerencias_historial[texto]!=undefined) {
            var obj = {'texto': texto};
            var html = armarHTML(sugerencias_historial[texto], obj);
            document.getElementById('main-sug').innerHTML='<ul class="sub">' + html + '</ul>';
            document.getElementById('main-sug').style.display = 'block';
            return false;
          }
          for(i=1; i<=texto.length-3; i++) {
            var texto_parcial = texto.substring(0, texto.length-i);
            if(sugerencias_historial[texto_parcial]!=undefined) {
              if(sugerencias_historial[texto_parcial].length<31) {
                var obj = {'texto': texto};
                var html = armarHTML(sugerencias_historial[texto_parcial], obj);
                document.getElementById('main-sug').innerHTML='<ul class="sub">' + html + '</ul>';
                document.getElementById('main-sug').style.display = 'block';
                return false;
              }
              break;
            }
          }
          if(xmlhttp!=undefined){
            xmlhttp.abort();
          }
          if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
          } else { // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange=function(){
            if (xmlhttp.readyState==4 && xmlhttp.status==200){
              var valores = xmlhttp.responseText;
              if(valores != ''){
                var array_sugerencias = false;
                if (typeof JSON != 'undefined')
                  array_sugerencias = JSON.parse(valores)
                else 
                  array_sugerencias = json_parse(valores);
                var html = armarHTML(array_sugerencias, false);
                document.getElementById('main-sug').innerHTML='<ul class="sub">' + html + '</ul>';
                document.getElementById('main-sug').style.display = 'block';
                sugerencias_historial[texto] = array_sugerencias;
                sugerencias_historial.length++;
                if(sugerencias_historial.length>10) {
                  for (key in sugerencias_historial) {
                    delete sugerencias_historial[key];
                    sugerencias_historial.length--;
                    break;
                  }
                }
              } else {
                document.getElementById('main-sug').innerHTML='';
                document.getElementById('main-sug').style.display = 'none';
              }
            } 
          }
          xmlhttp.open("GET","/search/get_sugerencias/"+texto,true);
          xmlhttp.send();
        } else {
          document.getElementById('main-sug').innerHTML='';
          document.getElementById('main-sug').style.display = 'none';
        }
    }
    // Armamos un conjunto de <li> en html a partir de un array de sugerencias.
    function armarHTML(array_sugerencias, texto)
    {
      if(texto!=false)
        array_sugerencias = array_sugerencias.filter(filtrarSugerencias, texto);
      var html = ""
      var i = 0;
      for (index in array_sugerencias) {
        if(index=='filter')
          break; // Si el navegador no soporta filter nativo se le agrega como propiedad, con esto evitamos mostrarla.
        html += '<li><a onclick="envioSugerencia(\''+array_sugerencias[index]+'\');return false;" href="#">'+array_sugerencias[index]+'</a></li>';
        i++;
        if(i>5)
          break; // Mostramos solamente hasta 6 items.
      }
      return html;
    }
    var filtrarSugerencias = function(value) {
      if(value.indexOf(this.texto)>=0)
        return true;
      return false;
    }
    function registrar_sugerencia(texto) {
      //return false;
      var texto = texto;
      if(texto.length > 0 && texto != 'Encontrá lo que quieras') {
        var xmlhttp;
        if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            var valores = xmlhttp.responseText;
          } 
        }
        xmlhttp.open("GET","/search/set_sugerencias/"+texto,true);
        xmlhttp.send();
      }
    }
    
    function toggleByClass(className, status){
      var elements = document.getElementsByClassName(className)
      for (var i = 0; i < elements.length; i++){
          elements[i].style.display = status;
      }
    }
    
    function mostrar_ofertas(tid, rubro) {
      document.getElementById('rubro_seleccionado').innerHTML = rubro;
      document.getElementById('rubros_ofertas').style.display = 'none';
      toggleByClass('oferta-item', 'none');
      toggleByClass('rubro-'+tid, '');
    }
    
    function envioBusquedaTiendas() {
      document.formBusquedaTienda.submit();
    }
     
    function loadScript(url, callback){
    
      var script = document.createElement("script")
      script.type = "text/javascript";
      
      if (script.readyState){  //IE
        script.onreadystatechange = function(){
          if (script.readyState == "loaded" ||
              script.readyState == "complete"){
            script.onreadystatechange = null;
            callback();
          }
        };
      } else {  //Others
        script.onload = function(){
          callback();
        };
      }
      script.src = url;
      document.getElementsByTagName("head")[0].appendChild(script);
    }
    if (typeof JSON == 'undefined'){
      loadScript("<?php echo $this->config->item('sitio_host'); ?>/public/js/json_parse_min.js", function(){
      });
    }
    
    if (!Array.prototype.filter) {
      Array.prototype.filter = function(fun /*, thisp*/) {
        var len = this.length >>> 0;
        if (typeof fun != "function") {
            throw new TypeError();
        }
        var res = [];
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this) {
                var val = this[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, this)) {
                    res.push(val);
                }
            }
        }
        return res;
      };
    }

var popup = false;
$(document).ready(function() {
  // Interceptamos el boton volver, y cerramos el popup abierto si es que hay.
  $(window).on('hashchange', function() {
    if(location.hash=='') {
      cerrar_div(popup, false);
    }
  });
});    
</script>
<?php
if((!isset($tipo) || $tipo!="admin") && (!isset($es_buscador_alojamiento) || !$es_buscador_alojamiento) && (!isset($es_prehome) || !$es_prehome)) {
?>
<section class="barra-buscador">
  <div class="container">
		<div class="row">
			<div class="twelvecol">
        <div id="formBusqueda" class="ch-form ch-header-form" action="" method="GET" role="search">
          <div class="fondo">
            <div class="buscador-hidde">
              <span class="search-close"></span>
              <input name="as_word" type="text" class="search <?php if($key != '') print 'key-active'; ?>" id="query" autocomplete="off" onkeypress="searchKeyPress(event);" onkeyup="obtener_sugerencias(this.value);" value="Encontrá lo que quieras" maxlength="60" data-sugnumber="6" onfocus="onFocusText();" onblur="onBlurText();">
            </div>
            <span class="icono-buscar" onclick="envioBusqueda();return false" style="cursor:pointer;"></span>
            
            <?php if($key != ''){ ?>
              <a class="filtro avanzados" href="/filtros/avanzados/<?php print $key; ?>/<?php print $tids; ?>"><span class="filtro-span"></span></a>
            <?php } else { ?>
              <a class="filtro avanzados" href="/filtros/avanzados/<?php print $tids; ?>"><span class="filtro-span"></span></a>
            <?php } ?>
            
            <div id="datos_menu" class="menu"></div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<nav id="main-sug" class="main" style="display:none;"></nav>

<?php if($tipo=='buscador'){ ?>
</div>
<?php } ?>

<?php
}
?>

<div id="mensaje-favoritos-nologin">
  <span class="fa">
    <i class="fa-star fx"></i>
  </span>
  <span>Si quieres mantener guardado tu historial de favoritos, recomendamos <a href="/user">loguearte</a> o <a href="/registro-particular">crear una cuenta</a>.</span>
</div>

<script type="text/javascript">
  var url = window.location.pathname.split("/");
  if(url[1] == 'search'){
    if (typeof url[3] != "undefined"){
      if(url[3].indexOf('filters')==-1 && url[3].indexOf('page')==-1 && url[3].indexOf('lat')==-1 && url[3].indexOf('long')==-1) {
        document.getElementById('query').value = decodeURI(url[3]);
      } else {
        if (typeof url[4] != "undefined"){
          if(url[4].indexOf('filters')==-1 && url[4].indexOf('page')==-1 && url[4].indexOf('lat')==-1 && url[4].indexOf('long')==-1) 
            document.getElementById('query').value = decodeURI(url[4]);
        }
      }
    } 
  }
  if(url[1] == 'filtros' && url[2] == 'avanzados'){
    if (typeof url[3] != "undefined"){
      if(url[3].indexOf('filters')==-1) {
        document.getElementById('query').value = decodeURI(url[3]);
      }
    } 
  }
</script>
<?php
// Para la seccion de administracion mostramos menu admin.
if(isset($tipo) && $tipo=='admin') {
  $data2['title'] = $title;
  $this->load->view($this->config->item('carpeta_sitio').'/menu_admin', $data2);
}
?>

<?php if(isset($adslots)) {
  foreach($adslots as $adslot) {
    if($adslot['adunit'] == 'dfp-anuncio_previo') {
      print ads_get_banner($adslot['adunit']);
    }
    if($adslot['adunit'] == 'dfp-topsite') {
      print ads_get_banner($adslot['adunit']);
    }
    if($adslot['adunit'] == 'dfp-zocalo') {
      print ads_get_banner($adslot['adunit']);
    }
  }
}
?>
