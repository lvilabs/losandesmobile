<div id="content" class="pages page-error">
  <div class="container">
    <div class="row">
      <div class="noticias">
        <h1>Página no encontrada</h1>
        <h2>ERROR 404</h2>
        <p>
            La página a la que intentas ingresar no está disponible. Asegurate de haber puesto bien la dirección.
        </p>
      </div>
    </div>
  </div>
</div>
