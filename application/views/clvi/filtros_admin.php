<div id="content-filtros">
<?php
$tids = array();
if(isset($respuesta->filtros_aplicados->tids)) {
  $tids = explode(' ', trim($respuesta->filtros_aplicados->tids));
}
?>
<?php if(!empty($filtros_aplicados)) { ?>
<ul class="filtroActivo">	
<?php foreach($filtros as $filtro){ ?>
  <li><a href="<?php print str_replace(':', '%3A', $filtro['url']); ?>"><?php print $filtro['nombre']; ?><span></span></a></li>
<?php   } ?>
</ul>
<?php } ?>

<section class="filtros">
  <form name="formFiltro" class="formFiltro" action="/administrar/mis-avisos.html<?php if(isset($parametros_filtro)) print $parametros_filtro; ?>" method="post">

  <nav id="main-sub" class="main-sub">
    <?php if(!empty($key)){ ?>
      <input type="hidden" name="key" value="<?php print $key.'/'; ?>"/>
    <?php } ?>

    <a onclick="toggle_menu('rubro'); ocultar_select('rubro', '', '');" href="javascript:;">Rubro</a>
    <ul class="sub" id="rubro" style="display: none;">
      <?php
      $rubros = (array)$respuesta->filtros->filtro_term_rubros;
      asort($rubros);
      $rubro_tid_aplicado = '';
      $rubro_nombre_aplicado = '';
      foreach($rubros as $tid => $rubro) :
        if($tid == 8688) continue;
        if(in_array($tid, $tids)) {
          $rubro_tid_aplicado = $tid;
          $rubro_nombre_aplicado = $rubro;
        }
      ?>
      <li><a onclick="seleccion_opcion('rubro', '<?php echo $rubro; ?>', '<?php echo $tid; ?>', '');" style="cursor:pointer;"><?php echo $rubro; ?></a></li>
      <?php
      endforeach;
      ?>
    </ul>
    <li><a class="option-select" id="rubro_select" <?php if(empty($rubro_tid_aplicado)) print 'style="display:none;"'; ?>><?php print $rubro_nombre_aplicado; ?></a></li>
    <input type="hidden" name="rubro" id="rubro_link" value="<?php print $rubro_tid_aplicado; ?>" />
    <?php
      if(isset($respuesta->filtros_aplicados->field_precio_desde)) {
        $precio_desde = $respuesta->filtros_aplicados->field_precio_desde;
      }
      if(isset($respuesta->filtros_aplicados->field_precio_hasta)) {
        $precio_hasta = $respuesta->filtros_aplicados->field_precio_hasta;
      }
      if(!empty($filtros_nombres)) {
        foreach($filtros_nombres as $filtro_texto){
          if(strpos($filtro_texto, 'Precio') !== false){
            $precio_desde_hasta = explode(' ', $filtro_texto);
            $precio_desde = $precio_desde_hasta[1];
            $precio_hasta = $precio_desde_hasta[3];
          }
          if($filtro_texto == '$')
            $moneda_peso_check = 1;
          if($filtro_texto == 'U$S')
            $moneda_dolar_check = 1;
          if(strpos($filtro_texto, 'Km') !== false){
            $km_desde_hasta = explode(' ', $filtro_texto);
            $km_desde = $km_desde_hasta[1];
            $km_hasta = $km_desde_hasta[3];
          }
          if(strpos($filtro_texto, 'Año') !== false){
            $anio_desde_hasta = explode(' ', $filtro_texto);
            $anio_desde = $anio_desde_hasta[1];
            $anio_hasta = $anio_desde_hasta[3];
          }
        }
      }
    ?>
    <a class="no-select" href="javascript:;">Precio</a>
    <ul class="sub" id="precio">
      <li>
        <div class="campo texto left"><input name="field_precio_desde" placeholder="Desde" type="text" value="<?php if(isset($precio_desde)) print $precio_desde; ?>" /></div>
        <div class="campo texto right"><input name="field_precio_hasta" placeholder="Hasta" type="text" value="<?php if(isset($precio_hasta)) print $precio_hasta; ?>" /></div>
      </li>
    </ul>
    <!--
    <a class="no-select" href="javascript:;">Moneda</a>
    <ul class="sub" id="moneda">
      <li>
        <div class="campo moneda">
          <label for="radio_moneda_peso">$</label><input type="radio" name="field_moneda" id="radio_moneda_peso" value="1" <?php if(isset($moneda_peso_check) && $moneda_peso_check == 1) print 'checked="checked"'; ?>/>
          <label for="radio_moneda_dolar">U$S</label><input type="radio" name="field_moneda" id="radio_moneda_dolar" value="2" <?php if(isset($moneda_dolar_check) && $moneda_dolar_check == 1) print 'checked="checked"'; ?>/>
        </div>
      </li>
    </ul>
    -->
    <?php if(!empty($existe_modelo) || !empty($existe_tipo_vehiculo)){ ?>
    <a class="no-select" href="javascript:;">Kilómetros</a>
    <ul class="sub" id="kilometros">
      <li>
        <div class="campo texto left"><input name="field_kilometro_desde" placeholder="Desde" type="text" value="<?php if(isset($km_desde)) print $km_desde; ?>" /></div>
        <div class="campo texto right"><input name="field_kilometro_hasta" placeholder="Hasta" type="text" value="<?php if(isset($km_hasta)) print $km_hasta; ?>" /></div>
      </li>
    </ul>
    <a class="no-select" href="javascript:;">Año del modelo</a>
    <ul class="sub" id="anio_modelo">
      <li>
        <div class="campo texto left"><input name="field_modelo_desde" placeholder="Desde" type="text" value="<?php if(isset($anio_desde)) print $anio_desde; ?>" /></div>
        <div class="campo texto right"><input name="field_modelo_hasta" placeholder="Hasta" type="text" value="<?php if(isset($anio_hasta)) print $anio_hasta; ?>" /></div>
      </li>
    </ul>
    <?php } ?>
    <a onclick="toggle_menu('operacion'); ocultar_select('operacion', '', 'ciudad');" href="javascript:;">Operación</a>
    <ul class="sub" id="operacion" style="display: none;">
      <?php
      $operacion_oid_aplicado = '';
      $operacion_nombre_aplicado = '';
      foreach($respuesta->filtros->filtro_operacion as $oid => $operacion) :
        if(isset($respuesta->filtros_aplicados->operacion) && $respuesta->filtros_aplicados->operacion==$oid) {
          $operacion_oid_aplicado = $oid;
          $operacion_nombre_aplicado = $operacion;
        }
      ?>
      <li><a onclick="seleccion_opcion('operacion', '<?php echo $operacion; ?>', '<?php echo $oid; ?>', '');" style="cursor:pointer;"><?php echo $operacion; ?></a></li>
      <?php
      endforeach;
      ?>
    </ul>
    <li><a class="option-select" id="operacion_select" <?php if(empty($operacion_oid_aplicado)) print 'style="display:none;"'; ?>><?php print $operacion_nombre_aplicado; ?></a></li>
    <input type="hidden" name="operacion" id="operacion_link" value="<?php print $operacion_oid_aplicado; ?>" />
    <a onclick="toggle_menu('estado'); ocultar_select('estado', '', '');" href="javascript:;">Estado</a>
    <ul class="sub" id="estado" style="display: none;">
      <?php
      $estado_sid_aplicado = '';
      $estado_nombre_aplicado = '';
      foreach($respuesta->filtros->filtro_estado_wf as $sid => $estado) :
        if(isset($respuesta->filtros_aplicados->estado) && $respuesta->filtros_aplicados->estado==$sid) {
          $estado_sid_aplicado = $sid;
          $estado_nombre_aplicado = $estado;
        }
      ?>
      <li><a onclick="seleccion_opcion('estado', '<?php echo $estado; ?>', '<?php echo $sid; ?>', '');" style="cursor:pointer;"><?php echo $estado; ?></a></li>
      <?php
      endforeach;
      ?>
    </ul>
    <li><a class="option-select" id="estado_select" <?php if(empty($estado_sid_aplicado)) print 'style="display:none;"'; ?>><?php print $estado_nombre_aplicado; ?></a></li>
    <input type="hidden" name="estado" id="estado_link" value="<?php print $estado_sid_aplicado; ?>" />
    <a onclick="toggle_menu('destaque'); ocultar_select('destaque', '', '');" href="javascript:;">Destaque</a>
    <ul class="sub" id="destaque" style="display: none;">
      <?php
      $espacio_nid_aplicado = '';
      $espacio_nombre_aplicado = '';
      foreach($respuesta->filtros->filtro_espacio_nid as $nid => $espacio) :
        if(isset($respuesta->filtros_aplicados->destaque) && $respuesta->filtros_aplicados->destaque==$nid) {
          $espacio_nid_aplicado = $nid;
          $espacio_nombre_aplicado = $espacio;
        }
      ?>
      <li><a onclick="seleccion_opcion('destaque', '<?php echo $espacio; ?>', '<?php echo $nid; ?>', '');" style="cursor:pointer;"><?php echo $espacio; ?></a></li>
      <?php
      endforeach;
      ?>
    </ul>
    <li><a class="option-select" id="destaque_select" <?php if(empty($espacio_nid_aplicado)) print 'style="display:none;"'; ?>><?php print $espacio_nombre_aplicado; ?></a></li>
    <input type="hidden" name="destaque" id="destaque_link" value="<?php print $espacio_nid_aplicado; ?>" />
    <div class="centrar">
      <div class="contactar">
        <a class="botonC" onClick="envio_form();" href="javascript:;" title="Filtrar" alt="Filtrar">Aplicar</a>
        <div style="clear: both;"></div>
      </div>
    </div>
  </nav>
  
  <input type="hidden" name="url" value="administrar/mis-avisos.html?" />
  <input type="hidden" name="pagina" id="pagina" value="0" />
  
  </form>
</section>
</div>
<script type="text/javascript">
  function seleccion_opcion(contenedor, valor, filtro, content_link){
    
    //si viene con mas de un filtro tid, tomo solo el ultimo
    if(filtro.indexOf(" tid") != -1){
      var filtros_tids = filtro.split(' tid');
      filtro = 'tid' + filtros_tids[filtros_tids.length-1];
    }
    
    var el = document.getElementById(contenedor);
    var select = document.getElementById(contenedor + '_select');
    if(content_link != ''){
      var link = document.getElementById(content_link + '_link');
    } else {
      var link = document.getElementById(contenedor + '_link');
    }
    //Agregar valores
    select.innerHTML = valor;
    select.style.display = 'block';
    link.value = filtro;
    //Ocutar nav
    el.style.display = 'none';
  }
  
  function ocultar_select(select, content_link, content_link_sub){
    var el = document.getElementById(select + '_select');
    if(content_link != ''){
      var link = document.getElementById(content_link + '_link');
    } else {
      var link = document.getElementById(select + '_link');
    }
    //Elminar y ocultar sub opcion
    if(content_link_sub != ''){
      var sublink = document.getElementById(content_link_sub + '_link');
      sublink.value = '';
      var del = document.getElementsByClassName(content_link_sub);
      for(var i=0;i<del.length;i++){
        del[i].style.display = 'none';
      }
      //Si es provincia elimino barrio
      if(select == 'provincia'){
        var barriolink = document.getElementById('barrio_link');
        barriolink.value = '';
        var barriodel = document.getElementsByClassName('barrio');
        for(var i=0;i<barriodel.length;i++){
          barriodel[i].style.display = 'none';
        }
      }
    }
    el.style.display = 'none';
    link.value = '';
  }
  
  function mostrar_ciudad(id_provincia){
    var del = document.getElementsByClassName('ciudad');
    var del2 = document.getElementsByClassName('barrio');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    for(var i=0;i<del2.length;i++){
      del2[i].style.display = 'none';
    }
    document.getElementById('barrio_link').value = '';
    var el = document.getElementById('contenedor-ciudad-' + id_provincia);
    el.style.display = 'block';
  }
  
  function mostrar_barrio(id_ciudad){
    var del = document.getElementsByClassName('barrio');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-barrio-' + id_ciudad);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function mostrar_modelo(id_marca){
    var del = document.getElementsByClassName('modelo');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-modelo-' + id_marca);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function mostrar_subrubro(id_rubo){
    var del = document.getElementsByClassName('subsubrubro');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-subsubrubro-' + id_rubo);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function mostrar_marca(id_rubo){
    var del = document.getElementsByClassName('marca');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-marca-' + id_rubo);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function mostrar_inmueble(id_rubo){
    var del = document.getElementsByClassName('inmueble');
    for(var i=0;i<del.length;i++){
      del[i].style.display = 'none';
    }
    var el = document.getElementById('contenedor-inmueble-' + id_rubo);
    if(el != undefined){
      el.style.display = 'block';
    }
  }
  
  function envio_form(){
    document.forms["formFiltro"].submit();
  }
  
  function paginacion(pagina) {
    $('#pagina').val(pagina);
    document.forms["formFiltro"].submit();
  }
</script>