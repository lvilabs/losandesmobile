<div id="content" class="admin">
<div class="container">
  
  <div class="acciones-masivas">
    <input type="checkbox" id="check-multiselect" name="admin-multiselect" value="1">
    <a href="javascript:void(0)" class="boton boton-delete no-active" data-accion="eliminar" onclick="accionesMasivas(this);">Eliminar</a>
    <a href="javascript:void(0)" class="boton boton-desp no-active" data-accion="despublicar" onclick="accionesMasivas(this);">Despublicar</a>
  </div>
  
  <section class="filtros menuEstados">
    <form name="formFiltroMenu" class="formFiltroMenu" action="/administrar/mis-avisos.html" method="post">
      <ul class="menu-estados-reserva">
        <li class="<?php if(isset($respuesta->filtros_aplicados->estado) && $respuesta->filtros_aplicados->estado == 3) print 'selected'; ?>"><a href="javascript:void(0);" onclick="seleccion_estado(3);">Publicados</a></li>
        <li class="<?php if(isset($respuesta->filtros_aplicados->estado) && $respuesta->filtros_aplicados->estado == 4) print 'selected'; ?>"><a href="javascript:void(0);" onclick="seleccion_estado(4);">No Publicados</a></li>
      </ul>
      <input type="hidden" name="estado" id="estado_menu" value="<?php if(isset($estado_sid_aplicado)) print $estado_sid_aplicado; ?>" />
    </form>
  </section> 

  <div class="row" id="avisos">
    <?php if(!empty($mensaje)) : ?>
      <?php if(isset($mensaje->status)) : ?>
      <div class="twelvecol mensaje clear">
        <span class="<?php echo $mensaje->status; ?>"><?php echo $mensaje->mensaje; ?></span>
      </div>
      <?php else : ?>
      <div class="twelvecol mensaje clear">
        <span class="error"><?php echo $mensaje; ?></span>
      </div>
      <?php endif; ?>
    <?php endif; ?>
    
    <div class="twelvecol">
<?php
foreach($respuesta->avisos as $key => $item) {
  $class_status_aviso = '';
  if($item->_workflow == WORKFLOW_AVISOS_ESTADO_PENDIENTE){
    $class_status_aviso = 'aviso_moderado';
  } elseif($item->_workflow == WORKFLOW_AVISOS_ESTADO_RECHAZADO){
    $class_status_aviso = 'aviso_rechazado';
  }
?>
  <article class="aviso clearfix <?php echo $item->visual_aviso->tipo_espacio_css_class; ?> <?php echo $class_status_aviso; ?>" id="aviso-<?php echo $item->nid; ?>">
    <input type="checkbox" class="aviso-check" data-id="<?php echo $item->nid; ?>" data-title="<?php print mb_substr($item->title, 0, 30); if(strlen($item->title)>30) print '...'; ?>" value="1">
    <div class="titulo"><?php print mb_substr($item->title, 0, 30); if(strlen($item->title)>30) print '...'; ?></div>
    <?php if($item->type == 'aviso_alquiler_temporario' && !empty($item->reservas_pendientes)): ?>
      <?php
        $fecha_proxima = 0;
        foreach($item->reservas_pendientes as $reserva) {
          if($fecha_proxima == 0 || $reserva->created < $fecha_proxima)
            $fecha_proxima = $reserva->created;
        }
        $fecha_fin_reserva = $fecha_proxima + 24*60*60;
      ?>
      <i class="fa fa-clock-o"></i>
      <span id="countdown-<?php print $item->nid; ?>"></span>
<script>
  jQuery(document).ready(function($){
    var fechaFin = new Date(<?php print $fecha_fin_reserva; ?>*1000);
    $('#countdown-<?php  print $item->nid; ?>').dsCountDown({
      endDate: new Date(fechaFin),
      titleDays: '',
      titleHours: '',
      titleMinutes: '',
      titleSeconds: ''
    });
    $('.ds-days').hide();
  });
</script>    
    <?php endif; ?>
    <div class="thumb-datos">
      <div class="thumb">
        <img src="<?php echo urldecode($this->config->item('sitio_origen').'/'.$item->visual_aviso->first_foto->filepath); ?>" width="94" alt="<?php echo $item->title; ?>" />
        <?php if(isset($item->disponible_venta) && $item->disponible_venta) { ?>
          <span class="DisponibleVenta"></span>
        <?php } ?>
      </div>
      <div class="datos">
        <div>
          <i class="fa fa-list"></i><span class="text-datos"><?php echo $item->visual_aviso->pie_information->rubro; ?></span>
          <span class="text-datos precio"><?php echo $item->precio; ?></span>
          
          <?php if(isset($item->field_aviso_precio_uva) && $item->field_aviso_precio_uva[0]->value): ?>  
            <div class="search-precio-uva <?php print $item->nid; ?>" data-precio="<?php print $item->field_aviso_precio[0]->value; ?>" data-moneda="<?php print $item->field_aviso_moneda[0]->value; ?>" data-aviso="<?php print $item->nid; ?>"></div>
          <?php endif; ?>
          
        </div>   
        <?php if($item->type == 'aviso_alquiler_temporario'): ?>  
          <?php if(!empty($item->reservas_pendientes)): ?> 
            <?php $cantidad_reservas = count($item->reservas_pendientes); ?>
            <div class="red">
              <a class="link-reservas" href="/administrar/reservas/recibidas?status=1&title=<?php print $item->title; ?>"><i class="fa fa-circle"></i><span class="text-datos"><?php ($cantidad_reservas == 1)? print $cantidad_reservas.' Reserva pendiente': print $cantidad_reservas.' Reservas pendientes'; ?> </span></a>
            </div>
          <?php else: ?>
            <div ><i class="fa fa-circle-o"></i><span class="text-datos">0 Reservas pendientes</span></div>
          <?php endif; ?>
        <?php endif; ?>
        <?php
          $array_contactos = split('-', $item->visual_aviso->contactos);
          $visitas = 0;
          if(strpos($item->visual_aviso->visitas, '(') !== FALSE) {
            $array_visitas = explode(' (', $item->visual_aviso->visitas);
            $visitas = $array_visitas[0];
            $array_vistas = explode(' ', $array_visitas[1]);
          } else {
            $visitas = $item->visual_aviso->visitas;
          }
          $vistas_telefono = $item->visual_aviso->vistas_telefono;
          $vistas_whatsapp = $item->visual_aviso->vistas_whatsapp;
        ?>
        <div>
          <i class="fa fa-eye"></i><span class="text-datos"><?php echo $visitas; ?></span>
          <?php if($item->type != 'aviso_alquiler_temporario'): ?> 
            <span class="segundo"><i class="fa fa-phone"></i><span class="text-datos"><?php print $vistas_telefono; ?></span></span>
          <?php endif; ?>
        </div>
        <div>
          <i class="fa fa-envelope-o"></i><span class="text-datos"><?php print $array_contactos[0]; ?></span>
          <?php if($item->type == 'aviso_alquiler_temporario'): ?>
            <a href="javascript:void(0);" id="calendario-<?php print $item->nid; ?>" class="red"><span class="segundo"><i class="fa fa-calendar"></i><span class="text-datos"></span></span></a>
          <?php endif; ?>
          <?php if($item->type != 'aviso_alquiler_temporario'): ?> 
            <span class="segundo"><i class="fab fa-whatsapp"></i><span class="text-datos"><?php print $vistas_whatsapp; ?></span></span>
          <?php endif; ?>
          <div class="botones <?php if($item->type == 'aviso_alquiler_temporario') print 'btn-alojamientos'; ?>">
            <a onclick="toggle_menu_admin('menu_aviso', <?php echo $item->nid; ?>, '<?php echo $item->path; ?>');" href="javascript:void(0);" class="boton">Acciones</a>
          </div>
        </div>
      </div>
    </div>
    
    <div class="bloques">
      <div class="bloque"><span class="small">Estado</span><br><?php echo $item->visual_aviso->estado_workflow; ?></div>
      <div class="bloque"><span class="small">Inicio</span><br><?php echo date('d-m-Y', strtotime($item->field_aviso_fecha_inicio[0]->value)); ?></div>
      <div class="bloque"><span class="small">Fin</span><br><?php echo date('d-m-Y', strtotime($item->field_aviso_fecha_fin[0]->value)); ?></div>
      <div class="bloque"><span class="small">Destaque</span><br><?php echo $item->visual_aviso->destaque; ?></div>
    </div>
  </article>
  <?php if($item->type == 'aviso_alquiler_temporario'){ ?>
  <div id="masinfo-del-aviso-<?php print $item->nid; ?>" class="twelvecol ContactosDelAviso" style="height: 223px;display: none;">
    <div class="listadoAdminCalendar" id="calendario-frontend-<?php print $item->nid; ?>"></div>
<script>
$('#calendario-<?php print $item->nid; ?>').click(function(event) {  
  // if($('#masinfo-del-aviso-<?php print $item->nid; ?>').css('display') == 'none') {
    $('#masinfo-del-aviso-<?php print $item->nid; ?>').show('');
    $('#calendario-frontend-<?php print $item->nid; ?>').DOPFrontendBookingCalendarPRO({
      'loadURL': '/calendario/load_data/<?php print $item->nid; ?>', 
      'sendURL': '/calendario/save_data/<?php print $item->nid; ?>', 
      'calendar': {"data": {"bookingStop": 0,
                            "dateType": 1,
                            "language": "en",
                            "languages": [],
                            "view": true},
                   "text": {"addMonth": "Agregar mes",
                            "available": "disponible",
                            "availableMultiple": "disponible",
                            "booked": "reservado",
                            "nextMonth": "Mes siguiente",
                            "previousMonth": "Mes anterior",
                            "removeMonth": "Quitar mes",
                            "unavailable": "no disponible"}
                  }
    });
    $(this).removeClass('red');
  /* } else {
    $('#masinfo-del-aviso-<?php print $item->nid; ?>').hide('');
  } */
});
</script>
  </div>
  <?php } ?>
<?php
}
?>
      <div class="paginacion">
        <ul class="pager">
          <?php
          $pagina_actual = 1;
          $total = 1;
          if(isset($respuesta->pagina)) {
            $pagina_actual = $respuesta->pagina;
          }
          if(isset($respuesta->total)) {
            $total = $respuesta->total;
          }
          $paginas = ceil($total/20);
          $pag = 1;
          if($pagina_actual>3) {
            if(($pagina_actual+3)<$total) {
              $pag = $pagina_actual-2;
            }
          }
          if($pagina_actual>1) {
          ?>
          <li class="pager-first first"><a class="active" title="Ir a la primer página" onclick="javascript:paginacion(1);return false;" href="#">«</a></li>
          <li class="pager-previous"><a class="active" title="Ir a la página anterior" onclick="javascript:paginacion(<?php echo ($pagina_actual-1); ?>);return false;" href="#">‹</a></li>
          <?php
          }
          ?>
          <li class="pager-current">Página <?php echo ($pagina_actual); ?></li>
          <?php
          if($pagina_actual<$paginas) {
          ?>
          <li class="pager-next"><a class="active" title="Ir a la página siguiente" onclick="javascript:paginacion(<?php echo ($pagina_actual+1); ?>);return false;" href="#">›</a></li>
          <li class="pager-last last"><a class="active" title="Ir a la última página" onclick="javascript:paginacion(<?php echo $paginas; ?>);return false;" href="#">»</a></li>
          <?php
          }
          ?>
        </ul>
      </div>
      <div id="mas_info">
        <div class="contenido"></div>
        <div class=""><a onclick="cerrar_div('mas_info', true)" href="javascript:void(0);" id="cerrar_mas_info" class="boton">Volver</a></div>
      </div>
    </div>
  </div>
  <nav id="menu_aviso" class="main menu menu_aviso" style="display: none; ">
    <ul class="sub">
      
      <li><a href="javascript:mostrar_mas_info('aviso-0');" id="link_mas_info">+ Info</a></li>
      
      <li><a onclick="toggle_menu('menu_aviso', 0);" id="link_editar" href="/node/0/edit">Editar</a></li>
      <li><a onclick="toggle_menu('menu_aviso', 0);" id="link_publicar" href="/node/0/publicar">Publicar</a></li>
      <li><a onclick="toggle_menu('menu_aviso', 0);" id="link_despublicar" href="/node/0/despublicar">Despublicar</a></li>
      <!--<li><a onclick="toggle_menu('menu_aviso', 0);" id="link_delete" href="/node/0/delete">Eliminar</a></li>-->
      <!--<li><a onclick="toggle_menu('menu_aviso', 0);" id="link_archivar" href="/node/0/archivar">Archivar</a></li>-->
      <li><a onclick="toggle_menu('menu_aviso', 0);" id="link_ver" href="/node/0">Ver</a></li>
      <li><a onclick="toggle_menu_admin('menu_aviso', 0);" href="javascript:void(0);">Volver</a></li>
    </ul>
  </nav>
  <nav id="menu_filtro_admin" class="main menu_filtro_admin" style="display: none; ">
    <ul class="sub">
      <li><a onclick="toggle_menu('menu_filtro_admin', 0);" id="link_filtro_rubro" href="/filtro/rubro">Rubro</a></li>
      <li><a onclick="toggle_menu('menu_filtro_admin', 0);" id="link_filtro_publicar" href="/filtro/estado">Estado</a></li>
      <li><a onclick="toggle_menu_admin('menu_filtro_admin', 0);" href="javascript:void(0);">Volver</a></li>
    </ul>
  </nav>
  <nav id="confirmacion_avisos" class="main menu menu_aviso" style="display: none; ">
    <ul class="sub">
      <form id="formConfirmAccionesMasivas" action="/administrar/acciones_masivas_avisos/" method="post">
        <input type="hidden" id="listaNids" name="nids" value="" />
        <div class="confirmacion-title">Estás por eliminar los siguientes avisos:</div>
        <div class="confirmacion-content"></div>
        <a href="javascript:void(0)" id="confirmarAcciones" class="boton" onclick="ejecutarAccionesMasivas();">Confirmar</a>
        <a href="javascript:void(0)" class="boton" onclick="confirmacionOcultarModal();">Cancelar</a>
      </form>
    </ul>
  </nav>
  
<script type="text/javascript">

</script>
</div>

</div>