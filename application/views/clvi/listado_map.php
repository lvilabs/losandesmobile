<div id="content">
<header class="barraInfo">
<span class="numeroAvisos"><?php if(!empty($listado)) echo $listado->total; ?> avisos</span>

<?php if($key != ''){ ?>
  <span class="filtro lista"><a href="/search/result/<?php print $key; ?>/<?php print $tids; ?>" rel="search">Lista</a></span>
<?php } else { ?>
  <span class="filtro lista"><a href="/search/result/<?php print $tids; ?>" rel="search">Lista</a></span>
<?php } ?>

<?php if($key != ''){ ?>
  <span class="filtro"><a href="/filtros/avanzados_map/<?php print $key; ?>/<?php print $tids; ?>">filtro</a></span>
<?php } else { ?>
  <span class="filtro"><a href="/filtros/avanzados_map/<?php print $tids; ?>">filtro</a></span>
<?php } ?>
</header>

<?php if(!empty($filtros)){ ?>
<ul class="filtroActivo">	
<?php foreach($filtros as $filtro){ ?>
  <li><a href="<?php print $filtro['url']; ?>" rel="search"><?php print $filtro['nombre']; ?><span></span></a></li>
<?php   } ?>
</ul>
<?php } ?>

<div class="bannermiddle"></div>

<section class="noticias">
<?php
if(empty($listado)){
?>
<!-- sin resultados -->
<div class="noticias sinResultados">
<div class="fondoResaltado">
	<h2>Su búsqueda no produjo resultados</h2>
    <p>Compruebe si la ortografía es correcta, o trate de quitar los filtros.
Elimine las comillas que definen frases para buscar cada palabra individualmente. "Auto azul" encuentra menos resultados que Auto azul.</p>
</div></div>
<!-- fin resultados -->
<?php
} else {  
?>
  <div class="lista-mapa" id="map_canvas" style="width: 99%; height: 300px;"></div>
<?php
  $aviso_pagina = $listado->page * $listado->page_result;
  if($aviso_pagina < $listado->total){
    $url_explode = explode('/', $_SERVER['REQUEST_URI']);
    foreach($url_explode as $url){
      if(strpos($url,'page=')===false && $url != '')
        $url_implode .= '/'.$url;
    }
?>  
  <a class="mas-noticias" href="<?php print $url_implode.'/page='.$listado->page; ?>" rel="next">Ver más avisos</a>
<?php
  }
} 
?>
</section>
</div>

<div class="bannermiddle"></div>

<?php if(!empty($listado)){ ?>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=<?php print $this->config->item('gmap_api'); ?>"></script>
<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', initialize, false);
  var lista_avisos = <?php print json_encode($listado); ?>
  
  function initialize() {
    var mapOptions = {
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    
    var geoloc = {};
    var coordinates = [];
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        map.setCenter(pos);
        coordinates.push(pos);
        var datos = {latitude:position.coords.latitude, longitude:position.coords.longitude, type:'location'};
        geoloc = {latitude:position.coords.latitude, longitude:position.coords.longitude}
        //Geolocalizacion
        addMarker(map, datos, null);
        //Avisos
        for (index in lista_avisos) { 
          if(lista_avisos[index].type != undefined) {
            addMarker(map, lista_avisos[index], geoloc);
            var latlng = new google.maps.LatLng(lista_avisos[index].latitude, lista_avisos[index].longitude);
            coordinates.push(latlng);
          }
        }
        
        bounds = new google.maps.LatLngBounds();
        for (i = 0; i < coordinates.length; i++) {
          bounds.extend(coordinates[i]);
        }
        map.fitBounds(bounds);
        
      }, function(position) {
        for (index in lista_avisos) { 
          if(lista_avisos[index].type != undefined){
            addMarker(map, lista_avisos[index], geoloc);
            var latlng = new google.maps.LatLng(lista_avisos[index].latitude, lista_avisos[index].longitude);
            coordinates.push(latlng);
          }
        }
        bounds = new google.maps.LatLngBounds();
        for (i = 0; i < coordinates.length; i++) {
          bounds.extend(coordinates[i]);
        }
        map.fitBounds(bounds);
      });
    } else {
      for (index in lista_avisos) { 
        if(lista_avisos[index].type != undefined){
          addMarker(map, lista_avisos[index], geoloc);
          var latlng = new google.maps.LatLng(lista_avisos[index].latitude, lista_avisos[index].longitude);
          coordinates.push(latlng);
        }
      }
      bounds = new google.maps.LatLngBounds();
      for (i = 0; i < coordinates.length; i++) {
        bounds.extend(coordinates[i]);
      }
      map.fitBounds(bounds);
    }
  }
  
  function addMarker(map, data, geoloc) {
    var image = {
      url: addMarkerGetImg(data.type)
    };
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(data.latitude, data.longitude),
      map: map,
      icon: image,
      title: data.title
    });
    
    if(data.type != 'location') {
      var infowindow = new google.maps.InfoWindow;
      google.maps.event.addListener(marker, "click", function() {
        var img = '';
        if(data.filepath != undefined)
          img = '<img src="' + data.filepath + '">';
        var url = 'http://' + window.location.hostname + '/' + data.url_amigable;
        //if(geoloc !== '')
        //  url = 'https://maps.google.com/maps?saddr=' + geoloc.latitude + ',' + geoloc.longitude + '&daddr=' + data.latitude + ',' + data.longitude + '&output=classic&dg=ntvb';
        infowindow.setContent('<div id="content">' +
          '<a href="' + url + '">' +
          img +
          '<h2>' + data.title + '</h2>' +
          '<div id="bodyContent">' + data.moneda + ' ' + data.precio + '</div>' +
          '</a>' +
          '</div>');
        infowindow.open(map, marker);
      });
      /*google.maps.event.addListener(marker, "mouseout", function() {
        infowindow.close();
      });*/
    }
  }
  
  function addMarkerGetImg(type) {
    var url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/gifts.png'; 
    switch(type){
      case "location":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/icon-43.png';  
        break;
      case "aviso_4x4":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/fourbyfour.png';  
        break;
      case "aviso_repuesto":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/tires.png';  
        break;
      case "aviso_auto":
      case "aviso_plan_ahorro":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/car.png';  
        break;
      case "aviso_moto":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/motorcycle.png';  
        break;
      case "aviso_utilitario":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/van.png';  
        break;
      case "aviso_campo":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/field.png';  
        break;
      case "aviso_casa":
      case "aviso_cochera":
      case "aviso_terreno_lote":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/house.png';  
        break;
      case "aviso_departamento":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/moderntower.png';  
        break;
      case "aviso_emprendimiento":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/bigcity.png';  
        break;
      case "aviso_galpon_deposito":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/factory.png';  
        break;
      case "aviso_local":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/office-building.png';  
        break;
      case "aviso_producto":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/mall.png';  
        break;
      case "aviso_rural":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/rice.png';  
        break;
      case "aviso_servicio":
        url_img = '/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/img-map/workshop.png';  
        break;
    }
        
    return url_img;
  }
  
</script>
<?php } ?>