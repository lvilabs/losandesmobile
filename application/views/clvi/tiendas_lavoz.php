<div id="content" class="page-tiendas">

<div class="container">
<section class="noticias">
<div class="container">
  <div class="row">
    <section><h2 class="">Tiendas Online</h2></section>
  </div>
</div>
<div class="container barra-buscador">
  <div class="row">
    <div class="twelvecol">
      <form id="formBusquedaTienda" name="formBusquedaTienda" class="ch-form ch-header-form" action="" method="GET">
        <div class="fondo">
          <input name="tienda" type="text" class="search" id="buscador_tiendas" autocomplete="on" value="<?php if(!empty($_GET['tienda'])) echo $_GET['tienda']; ?>" placeholder="Buscá una tienda" maxlength="60" data-sugnumber="6">
          <span class="icono-buscar" onclick="envioBusquedaTiendas();return false;" style="cursor:pointer;"></span>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="twelvecol">
<?php
  $i=0;
  $ultimo_rubro = '';
  foreach($tiendas as $key => $item) {
    if($item->subrubro==0) {
      continue;
    }
    if(!empty($_GET['tienda']) && stripos($item->user->profile_nombre_comercial, $_GET['tienda'])===FALSE) {
      continue;
    }
    if($ultimo_rubro=='' || $ultimo_rubro!=$item->subrubro) {
      print '<div class="rubro"><h4>'.$item->name.'</h4></div>';
      $ultimo_rubro = $item->subrubro;
    }
  ?>
  <article class="widget">
    <div class="thumb">
      <?php if(!empty($item->user->picture)) { ?>
      <a href="/search/result/filters=is_uid:<?php echo $item->uid; ?>%20"><img alt="Comercio <?php echo $item->user->profile_nombre_comercial; ?>" src="<?php echo urldecode($item->user->logo); ?>"></a>
      <?php } ?>
    </div>
    <div class="ficha">
      <h2><a href="/search/result/filters=is_uid:<?php echo $item->uid; ?>%20"><?php echo $item->user->profile_nombre_comercial; ?></a></h2>
      <?php if($item->entrega_domicilio==1) { ?>
      <h4>Envío a domicilio</h4>
      <?php } ?>
      <?php if($item->entrega_sucursal==1) { ?>
      <h4>Retiro en sucursal</h4>
      <?php } ?>
      <?php if($item->descuento_clublavoz>0) { ?>
      <h4>Descuento Club La Voz <?php echo $item->descuento_clublavoz; ?>%</h4>
      <?php } ?>
    </div>
  </article>
  <?php
  }
?>
    </div>
  </div>
</div>
</section>
</div>
</div>