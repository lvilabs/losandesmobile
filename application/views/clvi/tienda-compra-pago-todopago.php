<?php

if($mensaje['StatusCode']==-1) {
  print 'Redireccionando a Todo Pago...';
  print '<script type="text/javascript">window.location.href = "'.$mensaje['URL_Request'].'";</script>';
} else {
  print json_encode(array('status' => 'error', 'mensaje' => 'Error al realizar la compra. '.$mensaje['Message']));
}
