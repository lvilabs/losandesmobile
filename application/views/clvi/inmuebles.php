<h1 class="DN">Alquiler y venta de inmuebles, departamentos y casas en Córdoba</h1>
<div class="clf-mob-home inmuebles">
  <div class="clf-mob-img inmuebles DN"></div>
  <div class="clf-mob-content">
    <h2>ENCONTRÁ TU INMUEBLE</h2>
  </div>

  <div class="clf-mob-searchbox home inmuebles">
      
    <form name="formFiltro" class="formFiltro" action="/search/result" method="post">
    
    <?php 
      $tid_inmuebles = $this->config->item('tid_inmuebles');
    ?>
    <?php if(isset($rubros->$tid_inmuebles)) { ?>
      
      <input type="hidden" name="tidPadre" value="<?php print $tid_inmuebles; ?>" />
      <div class="select-group">
        <i class="fa fa-home"></i>
        <select name="buscador_tipo_inmueble" class="form-select" id="edit-buscador-tipo-inmueble">
          <option value="">INMUEBLE </option>
          <?php foreach($rubros->$tid_inmuebles as $rubro) { ?>
            <?php if(is_object($rubro)) { ?>
              <option value="<?php print $rubro->filtro; ?>" data-tid="<?php print $rubro->tid; ?>"><?php print $rubro->name; ?></option>
            <?php } ?>
          <?php } ?>
        </select>
      </div>

      <div id="trans" class="select-group transacciones">
        <i class="fa fa-dollar-sign"></i>
        <select name="buscador_transaccion" class="form-select" id="edit-buscador-transaccion">
          <option value="">TRANSACCIÓN </option>
          <option value="filters=tid:6330 ss_operacion:Venta">Venta</option>
          <option value="filters=tid:6330 ss_operacion:Alquileres">Alquileres</option>
        </select>
      </div>
      
      <?php foreach($rubros->$tid_inmuebles as $key => $rubro) { ?>
        <?php if(is_object($rubro)) { ?>
          <div id="trans-<?php print $key; ?>" class="select-group transacciones" style="display:none;">
            <i class="fa fa-dollar-sign"></i>
            <select name="buscador_transaccion" class="form-select" id="edit-buscador-transaccion-<?php print $key; ?>">
              <option value="">TRANSACCIÓN </option>
              <?php foreach($rubro as $transaccion) { ?>  
                <?php if(is_object($transaccion)) { ?>
                  <?php $trans_filtro = str_replace('"', '', $transaccion->filtro); ?>
                  <option value="<?php print $trans_filtro; ?>" ><?php print $transaccion->name; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </div>
        <?php } ?>
      <?php } ?>
      
      <a class="btn btn-primary btn-full" href="javascript:envioFiltro();" title="Buscar" alt="Buscar" rel="noindex">buscar</a>
      
    <?php } ?>
    </form>
  </div>

  <div class="clf-mob-searchbox home inmobiliarias inmuebles">
    <a class="btn btn-primary btn-full" href="/inmobiliarias" title="Buscador de inmobiliairas" alt="Buscar inmobiliarias de clasificados" rel="search">Inmobiliarias</a>
  </div>
</div>