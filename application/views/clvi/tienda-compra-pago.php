<div class="clearfix">
  <div class="total clearfix">
    <div class="itemT"><?php print $node['title']; ?></div>
    <div class="itemT"><span class="priceT">$ <?php print $node['field_aviso_precio']; ?></span> x <?php print $mensaje['venta']['cantidad']; ?></div>
    <?php if($mensaje['venta']['descuento_porcentaje']>0) { ?>
    <div class="itemT">Descuento</div>
    <div class="itemT">-$ <?php print $mensaje['venta']['descuento']; ?> (<?php print $mensaje['venta']['descuento_porcentaje']; ?>%)</div>
    <?php } ?>
    <div class="itemT">Costo de envío</div>
    <div class="priceT">$ <?php print $mensaje['venta']['costo_envio']; ?></div>
    <div class="itemT destacado">Total</div>
    <div class="priceT destacado">$ <?php print $mensaje['venta']['compra_total']; ?></div>
  </div>
  <?php if($node['datos_venta']['vendedor']['dineromail'] || $node['datos_venta']['vendedor']['mercadopago']
          || $node['datos_venta']['vendedor']['todopago']) { ?>
    <div class="alerta">Al confirmar la compra se dirigirá a la plataforma de pagos para que haga efectiva la compra. Recomendamos seleccionar la opción de pago con tarjeta para agilizar el proceso de pago.</div>
  <?php } ?>
  <div class="form-item">
    <form method="post" id="compraPagosForm" class="box">
      <div class="help-block with-errors" style="display: none;"></div>
      <?php if($node['datos_venta']['vendedor']['dineromail']) { ?>
        <button type="submit" class="btn btn-primary pull-right" id="btn-compra-dineromail">Dineromail</button>
      <?php } ?>
      <?php if($node['datos_venta']['vendedor']['mercadopago']) { ?>
        <button type="submit" class="btn btn-primary pull-right" id="btn-compra-mercadopago">MercadoPago</button>
      <?php } ?>
      <?php if($node['datos_venta']['vendedor']['todopago']) { ?>
        <button type="submit" class="btn btn-primary pull-right" id="btn-compra-todopago">Todo Pago</button>
      <?php } ?>
      <?php if($node['datos_venta']['vendedor']['pago_domicilio'] && $mensaje['venta']['lugar_id']==0) { ?>
        <button type="submit" class="btn btn-primary pull-right" id="btn-compra-domicilio">Efectivo en Domicilio</button>
      <?php } ?>
      <div class="loader" style="display: none;"><img class="" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/ajax-loader-tienda.gif" /></div>
      <input type="hidden" name="venta_id" id="edit-venta-id" value="<?php print $mensaje['venta']['venta_id']; ?>">
    </form>
  </div>
</div>