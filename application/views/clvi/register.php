<div id="content">
  <?php
    $post = $_POST;
    if(isset($data_user) && !empty($data_user) && empty($post)) {
      $post = $data_user;
    } else {
      if(isset($_POST['provincia']) && !empty($_POST['ciudad-'.$_POST['provincia']])) {
        $post['ciudad'] = $_POST['ciudad-'.$_POST['provincia']];
        if(!empty($_POST['barrio-'.$post['ciudad']]))
          $post['barrio'] = $_POST['barrio-'.$post['ciudad']];
      }
      if(isset($_POST['provinciaFiscal']) && !empty($_POST['ciudadFiscal-'.$_POST['provinciaFiscal']])) {
        $post['ciudadFiscal'] = $_POST['ciudadFiscal-'.$_POST['provinciaFiscal']];
        if(!empty($_POST['barrioFiscal-'.$post['ciudadFiscal']]))
          $post['barrioFiscal'] = $_POST['barrioFiscal-'.$post['ciudadFiscal']];
      }
    }
    $whatsapp = '';
    if(isset($post['telefono_whatsapp']) && !empty($post['telefono_whatsapp'])) {
      $whatsapp = explode('+549', $post['telefono_whatsapp']);
      $whatsapp = $whatsapp[1];
    }
  ?>
  <div class="aviso users-forms">
    <div class="cajaTitulo">
      
      <?php if(isset($user_session) && $user_session->status) { ?>
        <div class="titulo">       
          <h1>Editar perfil</h1>
        </div>
      <?php } else { ?>
        <?php if($tipo_usuario == 'comercio') { ?>
          <div class="titulo">       
            <h1 style="padding-left: 5px;padding-top: 3px">Registro de Comercio</h1>
          </div>
          <a href="/registro-particular" style="color:#9eabbc;">Registro Particular</a>
        <?php } else { ?>
          <div class="titulo">       
            <h1> Registro Particular</h1>
          </div>
          <a href="/registro-comercio" style="color:#9eabbc;>Registro de Comercio</a>
        <?php } ?>
      <?php } ?>
      
    </div>
    <div class="message" <?php if(empty($mensaje)) print 'style="display:none;"'; ?>><?php print $mensaje; ?></div>
    <div class="content cuerpo">
      <form name="formLogin" class="formAviso" action="/user/send_register" method="post">
        <?php if(isset($user_session) && $user_session->status) { ?>
          <input type="hidden" name="uid" value="<?php print $user_session->uid; ?>" />
          <input type="hidden" name="accion" value="edit" />
        <?php } else { ?>
          <input type="hidden" name="accion" value="register" />
        <?php } ?>
        <input type="hidden" name="token" value="<?php print $token; ?>" />
        <input type="hidden" name="tipo_usuario" value="<?php print $tipo_usuario; ?>" />
        <h2>Datos de Usuario</h2>
        <div class="campo <?php if(!empty($campos_error) && in_array('username', $campos_error)) print 'campo-error'; ?>">
          <span class="requerido">*</span>
          <input type="email" name="username" placeholder="E-mail" value="<?php if(isset($post['username'])) print $post['username']; ?>" required/>
        </div>
        <?php if(isset($user_session) && $user_session->status) { ?>
          <div class="campo <?php if(!empty($campos_error) && in_array('password', $campos_error)) print 'campo-error'; ?>">
            <input type="password" name="password" placeholder="Contraseña" value="" />
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('repassword', $campos_error)) print 'campo-error'; ?>">
            <input type="password" name="repassword" placeholder="Repetir Contraseña" value="" />
          </div>
        <?php } else { ?>
          <div class="campo <?php if(!empty($campos_error) && in_array('password', $campos_error)) print 'campo-error'; ?>">
           <span class="requerido">*</span>
             <input type="password" name="password" placeholder="Contraseña" value="" required />
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('repassword', $campos_error)) print 'campo-error'; ?>">
           <span class="requerido">*</span>
             <input type="password" name="repassword" placeholder="Repetir Contraseña" value="" required />
          </div>
        <?php } ?>
        <div class="campo">
          <input type="text" name="telefono" placeholder="Teléfono de Contacto" value="<?php if(isset($post['telefono'])) print $post['telefono']; ?>" />
        </div>
        <div class="whatsapp">
          <label for="edit-profile-telefono-whatsapp">Teléfono para WhatsApp: </label>
          <div class="whatsapp_bandera"></div><span class="whatsapp_prefijo"> +549 0</span> 
          <?php if(isset($post['telefono_whatsapp']) && !empty($post['telefono_whatsapp'])) { ?>
            <div class="campo solo_telefono">
              <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="7" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="<?php print $whatsapp; ?>">
            </div>
          <?php } else { ?>
            <div class="campo area_whatsapp">
              <input type="text" name="codigo_area_whatsapp" id="codigo_area_whatsapp" maxlength="4" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="">
            </div>
            <div class="sub_whatsapp">15 </div>
            <div class="campo">
              <input type="text" name="telefono_whatsapp" id="telefono_whatsapp" maxlength="7" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="">
            </div>
          <?php } ?>
          <label>Para recibir contactos por WhatsApp complete este campo.</label>
        </div>
        <?php if($tipo_usuario == 'comercio') { ?>
          <h2>Datos Comerciales</h2>
          <?php if(isset($user_session) && $user_session->status) { ?>
          <div class="campo <?php if(!empty($campos_error) && in_array('nombreComercial', $campos_error)) print 'campo-error'; ?>">
            <input type="text" name="nombreComercial" placeholder="Nombre Comercial" value="<?php if(isset($post['nombreComercial'])) print $post['nombreComercial']; ?>" readonly="readonly"/>
          </div>
          <?php } ?>
          <div><strong><span class="requerido">*</span>Sucursal:</strong></div>
          <div class="campo"><label>No</label><input type="radio" name="sucursal" id="radio_sucursal_0" value="0" <?php if(isset($post['sucursal']) && $post['sucursal'] == 0) print 'checked="checked"'; ?> onClick="mostrarSucursal(this, 'nombre-sucursal');" /></div>
          <div class="campo"><label>Si</label><input type="radio" name="sucursal" id="radio_sucursal_1" value="1" <?php if(isset($post['sucursal']) && $post['sucursal'] == 'Si') print 'checked="checked"'; ?> onClick="mostrarSucursal(this, 'nombre-sucursal');" /></div>
          
          <div class="campo <?php if(!empty($campos_error) && in_array('nombreSucursal', $campos_error)) print 'campo-error'; ?>" id="nombre-sucursal" <?php if($post['sucursal'] != 'Si') print 'style="display:none;"'; ?>>
            <span class="requerido">*</span>
            <input type="text" name="nombreSucursal" placeholder="Nombre Sucursal" value="<?php if(isset($post['nombreSucursal'])) print $post['nombreSucursal']; ?>"/>
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('calle', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="calle" placeholder="Calle" value="<?php print $post['calle']; ?>" required/>
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('altura', $campos_error)) print 'cam o-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="altura" placeholder="Altura" value="<?php print $post['altura']; ?>" required/>
          </div>
          
          <div class="campo">
            <input type="text" name="piso" placeholder="Piso" value="<?php print $post['piso']; ?>" />
          </div>
          <div class="campo">
            <input type="text" name="departamento" placeholder="Dto" value="<?php print $post['departamento']; ?>" />
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('codigoPostal', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="codigoPostal" placeholder="Código Postal" value="<?php print $post['codigoPostal']; ?>" required/>
          </div>
          
          <!-- Provincia -->
          <div class="campo <?php if(!empty($campos_error) && in_array('provincia', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <select id="select-provincia" name="provincia" onChange="mostrarCiudad(this);">
              <option value="" label="- Provincia -"></option>
              <?php foreach($options['opciones'] as $option){ ?>
                <option value="<?php print $option['key']; ?>" <?php if($post['provincia'] == $option['key']) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <!-- Ciudad -->
          <?php foreach($options['opciones'] as $provincia){ ?>
          <div id="ciudad-<?php print $provincia['key']; ?>" class="campo <?php if(!empty($campos_error) && in_array('ciudad', $campos_error)) print 'campo-error'; ?> ciudad" <?php if($provincia['key'] != $post['provincia']) print 'style="display:none;"'; ?>><span class="requerido">*</span>
            <select id="select-ciudad-<?php print $provincia['key']; ?>" name="ciudad-<?php print $provincia['key']; ?>" onChange="mostrarBarrio(this);">
              <option value="" label="- Ciudad -"></option>
              <?php foreach($provincia['data_child'] as $ciudad){ ?>  
                <option value="<?php print $ciudad['key']; ?>" <?php if($post['ciudad'] == $ciudad['key'] || $post['ciudad'] == $ciudad['key']) print 'selected="selected"'; ?>><?php print $ciudad['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <?php } ?>
          <!-- Barrio -->
          <?php foreach($options['opciones'] as $provincia){ ?>
            <?php foreach($provincia['data_child'] as $ciudad){ ?>
              <?php if(!empty($ciudad['data_child'])){ ?>
                <div id="barrio-<?php print $ciudad['key']; ?>" class="campo <?php if(!empty($campos_error) && in_array('barrio', $campos_error)) print 'campo-error'; ?> barrio" <?php if($ciudad['key'] != $post['ciudad']) print 'style="display:none;"'; ?>>
                  <select name="barrio-<?php print $ciudad['key']; ?>">
                    <option value="" label="- Barrio -"></option>
                    <?php foreach($ciudad['data_child'] as $barrio){ ?>
                      <option value="<?php print $barrio['key']; ?>" <?php if(isset($post['barrio']) && $post['barrio'] == $barrio['key']) print 'selected="selected"'; ?>><?php print $barrio['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              <?php } ?>
            <?php } ?>
          <?php } ?>
          
          <div class="campo <?php if(!empty($campos_error) && in_array('tipoComercio', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <select name="tipoComercio" required>
              <option value="" label="- Tipo de Comercio -"></option>
              <option value="Comercio" <?php if($post['tipoComercio'] == 'Comercio') print 'selected="selected"'; ?>>Comercio</option>
              <option value="Concesionaria" <?php if($post['tipoComercio'] == 'Concesionaria') print 'selected="selected"'; ?>>Concesionaria</option>
              <option value="Inmobiliaria" <?php if($post['tipoComercio'] == 'Inmobiliaria') print 'selected="selected"'; ?>>Inmobiliaria</option>
              <option value="Casa de repuestos y accesorios" <?php if($post['tipoComercio'] == 'Casa de repuestos y accesorios') print 'selected="selected"'; ?>>Casa de repuestos y accesorios</option>
            </select>
          </div>
          
          <div class="campo">
            <input type="text" name="web" placeholder="Web" value="<?php print $post['web']; ?>" />
          </div>
          
          <h2>Datos de Contacto</h2>
          <div class="campo <?php if(!empty($campos_error) && in_array('nombre', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="nombre" placeholder="Nombre Responsable" value="<?php print $post['nombre']; ?>" required/>
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('apellido', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="apellido" placeholder="Apellido Responsable" value="<?php print $post['apellido']; ?>" required/>
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('telefono2', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="telefono2" placeholder="Teléfono Responsable" value="<?php print $post['telefono2']; ?>" required/>
          </div>
          
          <h2>Datos Fiscales</h2>
          <div class="campo <?php if(!empty($campos_error) && in_array('razonSocial', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="razonSocial" placeholder="Razón Social" value="<?php print $post['razonSocial']; ?>" required/>
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('cuit', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="cuit" placeholder="CUIT Formato: 11111111111" value="<?php print $post['cuit']; ?>" required/>
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('iva', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <select name="iva" required>
              <option value="" label="- Condición IVA -"></option>
              <option value="Monotributo" <?php if($post['iva'] == 'Monotributo') print 'selected="selected"'; ?>>Monotributo</option>
              <option value="Responsable Inscripto" <?php if($post['iva'] == 'Responsable Inscripto') print 'selected="selected"'; ?>>Responsable Inscripto</option>
              <option value="Responsable No Inscripto" <?php if($post['iva'] == 'Responsable No Inscripto') print 'selected="selected"'; ?>>Responsable No Inscripto</option>
              <option value="No Responsable" <?php if($post['iva'] == 'No Responsable') print 'selected="selected"'; ?>>No Responsable</option>
              <option value="Consumidor Final" <?php if($post['iva'] == 'Consumidor Final') print 'selected="selected"'; ?>>Consumidor Final</option>
              <option value="Sujeto Exento" <?php if($post['iva'] == 'Sujeto Exento') print 'selected="selected"'; ?>>Sujeto Exento</option>
              <option value="Sujeto no categorizado" <?php if($post['iva'] == 'Sujeto no categorizado') print 'selected="selected"'; ?>>Sujeto no categorizado</option>
              <option value="Cliente del Exterior" <?php if($post['iva'] == 'Cliente del Exterior') print 'selected="selected"'; ?>>Cliente del Exterior</option>
            </select>
          </div>
          
          <div><strong>Domicilio:</strong></div>
          <div class="campo"><label>Usar datos comerciales</label>
          <input type="checkbox" name="domicilioFiscalComercial" value="1" <?php if($post['domicilioFiscalComercial'] == 1) print 'checked="checked"'; ?> onClick="ocultarDomicilio(this);" />
          </div>
          
          <div class="campo <?php if(!empty($campos_error) && in_array('calleFiscal', $campos_error)) print 'campo-error'; ?>" id="calle-fiscal" <?php if($post['domicilioFiscalComercial'] == 1) print 'style="display:none;"'; ?>>
            <span class="requerido">*</span>
            <input type="text" name="calleFiscal" placeholder="Calle" value="<?php print $post['calleFiscal']; ?>" />
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('alturaFiscal', $campos_error)) print 'campo-error'; ?>" id="altura-fiscal" <?php if($post['domicilioFiscalComercial'] == 1) print 'style="display:none;"'; ?>>
            <span class="requerido">*</span>
            <input type="text" name="alturaFiscal" placeholder="Altura" value="<?php print $post['alturaFiscal']; ?>" />
          </div>
          
          <div class="campo" id="piso-fiscal" <?php if($post['domicilioFiscalComercial'] == 1) print 'style="display:none;"'; ?>>
            <input type="text" name="pisoFiscal" placeholder="Piso" value="<?php print $post['pisoFiscal']; ?>" />
          </div>
          <div class="campo" id="dpto-fiscal" <?php if($post['domicilioFiscalComercial'] == 1) print 'style="display:none;"'; ?>>
            <input type="text" name="departamentoFiscal" placeholder="Dto" value="<?php print $post['departamentoFiscal']; ?>" />
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('codigoPostalFiscal', $campos_error)) print 'campo-error'; ?>" id="cp-fiscal" <?php if($post['domicilioFiscalComercial'] == 1) print 'style="display:none;"'; ?>>
            <span class="requerido">*</span>
            <input type="text" name="codigoPostalFiscal" placeholder="Código Postal" value="<?php print $post['codigoPostalFiscal']; ?>" />
          </div>
          
          <!-- Provincia -->
          <div class="campo <?php if(!empty($campos_error) && in_array('provinciaFiscal', $campos_error)) print 'campo-error'; ?>" id="provincia-fiscal" <?php if($post['domicilioFiscalComercial'] == 1) print 'style="display:none;"'; ?>>
            <span class="requerido">*</span>
            <select id="select-provincia" name="provinciaFiscal" onChange="mostrarCiudadFiscal(this);">
              <option value="" label="- Provincia -"></option>
              <?php foreach($options['opciones'] as $option){ ?>
                <option value="<?php print $option['key']; ?>" <?php if($post['provinciaFiscal'] == $option['key']) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <!-- Ciudad -->
          <?php foreach($options['opciones'] as $provincia){ ?>
          <div id="ciudad-fiscal-<?php print $provincia['key']; ?>" class="campo <?php if(!empty($campos_error) && in_array('ciudadFiscal', $campos_error)) print 'campo-error'; ?> ciudad-fiscal" <?php if($provincia['key'] != $post['provinciaFiscal'] || $post['domicilioFiscalComercial'] == 1) print 'style="display:none;"'; ?>><span class="requerido">*</span>
            <select id="select-ciudad-<?php print $provincia['key']; ?>" name="ciudadFiscal-<?php print $provincia['key']; ?>" onChange="mostrarBarrioFiscal(this);">
              <option value="" label="- Ciudad -"></option>
              <?php foreach($provincia['data_child'] as $ciudad){ ?>  
                <option value="<?php print $ciudad['key']; ?>" <?php if(isset($post['ciudadFiscal']) && $post['ciudadFiscal'] == $ciudad['key']) print 'selected="selected"'; ?>><?php print $ciudad['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <?php } ?>
          <!-- Barrio -->
          <?php foreach($options['opciones'] as $provincia){ ?>
            <?php foreach($provincia['data_child'] as $ciudad){ ?>
              <?php if(!empty($ciudad['data_child'])){ ?>
                <div id="barrio-fiscal-<?php print $ciudad['key']; ?>" class="campo <?php if(!empty($campos_error) && in_array('barrioFiscal', $campos_error)) print 'campo-error'; ?> barrio-fiscal" <?php if((isset($post['ciudadFiscal']) && $ciudad['key'] != $post['ciudadFiscal'])  || $post['domicilioFiscalComercial'] == 1) print 'style="display:none;"'; ?>>
                  <select name="barrioFiscal-<?php print $ciudad['key']; ?>">
                    <option value="" label="- Barrio -"></option>
                    <?php foreach($ciudad['data_child'] as $barrio){ ?>
                      <option value="<?php print $barrio['key']; ?>" <?php if(isset($post['barrioFiscal']) && $post['barrioFiscal'] == $barrio['key']) print 'selected="selected"'; ?>><?php print $barrio['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              <?php } ?>
            <?php } ?>
          <?php } ?>
          
        <?php } else { ?>  
          <h2>Datos Personales</h2>
          <div class="campo <?php if(!empty($campos_error) && in_array('nombre', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="nombre" placeholder="Nombre" value="<?php if(isset($post['nombre'])) print $post['nombre']; ?>" required/>
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('apellido', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <input type="text" name="apellido" placeholder="Apellido" value="<?php if(isset($post['apellido'])) print $post['apellido']; ?>" required/>
          </div>
          <div class="campo <?php if(!empty($campos_error) && in_array('sexo', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <select name="sexo" required>
              <option value="" label="- Sexo -"></option>
              <option value="Masculino" <?php if(isset($post['sexo']) && $post['sexo'] == 'Masculino') print 'selected="selected"'; ?>>Masculino</option>
              <option value="Femenino" <?php if(isset($post['sexo']) && $post['sexo'] == 'Femenino') print 'selected="selected"'; ?>>Femenino</option>
            </select>
          </div>
          
          <!-- Provincia -->
          <div class="campo <?php if(!empty($campos_error) && in_array('provincia', $campos_error)) print 'campo-error'; ?>">
            <span class="requerido">*</span>
            <select id="select-provincia" name="provincia" onChange="mostrarCiudad(this);">
              <option value="" label="- Provincia -"></option>
              <?php foreach($options['opciones'] as $option){ ?>
                <option value="<?php print $option['key']; ?>" <?php if(isset($post['provincia']) && $post['provincia'] == $option['key']) print 'selected="selected"'; ?>><?php print $option['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <!-- Ciudad -->
          <?php foreach($options['opciones'] as $provincia){ ?>
          <div id="ciudad-<?php print $provincia['key']; ?>" class="campo <?php if(!empty($campos_error) && in_array('ciudad', $campos_error)) print 'campo-error'; ?> ciudad" <?php if(!isset($post['provincia']) || $provincia['key'] != $post['provincia']) print 'style="display:none;"'; ?>><span class="requerido">*</span>
            <select id="select-ciudad-<?php print $provincia['key']; ?>" name="ciudad-<?php print $provincia['key']; ?>" onChange="mostrarBarrio(this);">
              <option value="" label="- Ciudad -"></option>
              <?php foreach($provincia['data_child'] as $ciudad){ ?>  
                <option value="<?php print $ciudad['key']; ?>" <?php if(isset($post['ciudad']) && $post['ciudad'] == $ciudad['key']) print 'selected="selected"'; ?>><?php print $ciudad['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <?php } ?>
          <!-- Barrio -->
          <?php foreach($options['opciones'] as $provincia){ ?>
            <?php foreach($provincia['data_child'] as $ciudad){ ?>
              <?php if(!empty($ciudad['data_child'])){ ?>
                <div id="barrio-<?php print $ciudad['key']; ?>" class="campo <?php if(!empty($campos_error) && in_array('barrio', $campos_error)) print 'campo-error'; ?> barrio" <?php if(!isset($post['ciudad']) || $ciudad['key'] != $post['ciudad']) print 'style="display:none;"'; ?>>
                  <select name="barrio-<?php print $ciudad['key']; ?>">
                    <option value="" label="- Barrio -"></option>
                    <?php foreach($ciudad['data_child'] as $barrio){ ?>
                      <option value="<?php print $barrio['key']; ?>" <?php if(isset($post['barrio']) && $post['barrio'] == $barrio['key']) print 'selected="selected"'; ?>><?php print $barrio['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              <?php } ?>
            <?php } ?>
          <?php } ?>
          
          <div class="campo">
            <input type="text" maxlength="30" name="nacimiento" id="nacimiento-datepicker-popup" size="20" value="<?php if(isset($nacimiento)) print $post['nacimiento']; ?>" placeholder="Fecha Nacimiento" >
          </div>
        <?php } ?>
        
        <div class="campo <?php if(!empty($campos_error) && in_array('condiciones', $campos_error)) print 'campo-error'; ?>">
          <span class="requerido">*</span>
          <label>Acepto las condiciones de utilización del servicio.</label>
          <input id="checkbox_condiciones" type="checkbox" name="condiciones" value="1" <?php if($post['condiciones'] == 1) print 'checked'; ?> required>
        </div>
        <div class="campo">
          <label>Aceptación recepción de ofertas y promociones.</label>
          <input id="checkbox_promociones" type="checkbox" name="promociones" value="1" <?php if($post['promociones'] == 1) print 'checked'; ?>>
        </div>
        
        <?php if(isset($user_session) && $user_session->status) { ?>
          <input type="submit" name="submit" value="Guardar" class="boton" />
        <?php } else { ?>
          <input type="submit" name="submit" value="Registrarme" class="boton" />
        <?php } ?>
      </form>
    </div>
  </div>

</div>