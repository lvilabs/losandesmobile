<?php  
  foreach($listado as $key => $item){
    if(isset($item->nid)){
      
      switch($item->type){
        case 'Motos, cuadriciclos y náutica':
          $item->type = 'Motos';
          break;
        case '4x4, 4x2 y todo terreno':
          $item->type = '4x4';
          break;
        case 'Utilitarios, pick-up y camiones':
          $item->type = 'Utilitarios';
          break;
        case 'Accesorios y Repuestos':
          $item->type = 'Repuestos';
          break;
      }
?>
  
  <div class="search-main">
    <div class="search-item">
    
      <input type="checkbox" class="favourite" name="favourite_<?php echo $item->nid ?>" id="favourite_<?php echo $item->nid ?>" data-nid="<?php echo $item->nid ?>">
      <label for="favourite_<?php echo $item->nid ?>" class="transparent">
        <span class="fa">
          <i class="fa-star far"></i>
          <i class="fa-star fas"></i>
          <i class="fa-star fx"></i>
        </span>
      </label>
    
      <a href="/<?php echo $item->url_amigable ?>" alt="<?php echo $item->title ?>">  
        <div class="sc-img">
        
          <?php if($item->type == 'Productos') { ?>
            <img alt="<?php echo $item->subrubro.' '.$item->title.' a '.$item->field_aviso_moneda_value.$item->field_aviso_precio_value; ?>" src="<?php echo urldecode($item->filepath_imagen_producto_sc); ?>" />
          <?php } else { ?>
            <img alt="<?php echo $item->subrubro.' '.$item->title.' a '.$item->field_aviso_moneda_value.$item->field_aviso_precio_value; ?>" src="<?php echo urldecode($item->filepath_app); ?>" />
          <?php } ?>
          
          <div class="sc-price">
            <div><?php if($item->field_aviso_moneda_value != '') echo $item->field_aviso_moneda_value; else echo $item->field_aviso_precio_value; ?><?php if($item->field_aviso_precio_value != 'consultar') echo $item->field_aviso_precio_value; ?></div>
            
            <?php if(isset($item->precio_uva) && $item->precio_uva) { ?>
              <div class="search-precio-uva <?php print $item->nid; ?>" data-precio="<?php print str_replace('.', '', $item->field_aviso_precio_value); ?>" data-moneda="<?php print $item->field_aviso_moneda_value; ?>" data-aviso="<?php print $item->nid; ?>"></div>
            <?php } ?>
            
            <?php if($item->disponible_venta) { ?>
              <p class="mensajePago">Precio en 1 pago</p>
            <?php } ?>
          </div>
        </div>
        <div class="sc-content">
          <h4 class="title-result"><?php echo $item->title; ?></h4>
          <div class="sc-features">
            <?php if(!empty($item->descuento)) { ?>
              <span class="descuento"><?php print '<strong>'.$item->descuento->porcentaje.'%</strong> OFF'; ?></span>
            <?php } ?>
            <?php if($item->disponible_venta) { ?>
              <a title="Comprar el producto" href="/<?php echo $item->url_amigable ?>#modal-compra-datos" class="btn-comprar"><span></span>Comprar</a>
            <?php } else { ?>
              <p class="sizemed"><a href="/search/result/<?php echo str_replace('"', '%22', $item->filtro_subrubro); ?>"><i class="fa fa-circle"></i><?php echo $item->type.' - '.$item->subrubro ?></a></p>
            <?php } ?>
          </div>
        </div>
      </a>
    </div>
  </div>
<?php }
  } 
?>    