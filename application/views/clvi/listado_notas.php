<?php //print_r($listado); ?>
<?php
  $get = $_GET;
  $pagina = 0;
  if(isset($get['page'])) $pagina = $get['page'];
?>
<div class="section listado-notas">
  <div class="clearfix region_b">
    <section class="b2 clearfix">
      <div class="container"> 
        <header class="tit-seccion"> Noticias </header>
        <div class="panel-instanciado clearfix ">
          <?php foreach($listado->notas as $nkey => $nota) { ?>         
            <?php if($nkey == 0 && $pagina == 0) { ?>
              <div class="panel nota principal ">
                <div class="panel-body"> 
                  <article class="node-teaser node-teaser-card">
                    <?php if(!empty($nota->imagen)) { ?>
                      <div class="foto"> 
                        <?php if($nota->con_video) { ?>
                          <div class="iconsHome"><div class="sprite-indicadores sprite-video"></div></div>
                        <?php } ?>
                        <a href="/<?php print $nota->url_amigable; ?>" target="_self"> 
                          <img src="<?php print $nota->imagen_grande; ?>" alt="" width="100%"> 
                        </a>
                      </div>
                    <?php } ?>
                    <div class="contenido">
                      <?php if(!empty($nota->autor)) { ?>
                        <div class="panelAuthor" rel="author">
                          <span class="byAuthor">por</span> <?php print $nota->autor; ?> 
                        </div>
                      <?php } ?>
                      <h1> <a href="/<?php print $nota->url_amigable; ?>" target="_self"><?php print $nota->titulo; ?></a></h1>
                      <div class="footer-card">
                        <div class="data"> 
                          <span class="nodeData"> 
                            <time><?php print $nota->fecha; ?></time>&nbsp;•&nbsp; <?php print $nota->categoria; ?> 
                          </span>
                        </div>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            <?php } ?>
            <?php if($nkey > 0 && $nkey < 3  && $pagina == 0) { ?>  
              <div class="panel nota comun <?php if(empty($nota->imagen)) print 'sin-foto'; ?>">
                <div class="panel-body"> 
                  <article class="node-teaser node-teaser-card">
                    <?php if(!empty($nota->imagen)) { ?>
                      <div class="foto"> 
                        <?php if($nota->con_video) { ?>
                          <div class="iconsHome"><div class="sprite-indicadores sprite-video"></div></div>
                        <?php } ?>
                        <?php if(!empty($nota->autor)) { ?>
                          <div class="gradiente"></div>
                        <?php } ?>
                        <a href="/<?php print $nota->url_amigable; ?>" target="_self"> 
                          <img src="<?php print $nota->imagen; ?>" alt="" width="100%"> 
                        </a>
                      </div>
                    <?php } ?>
                    <div class="contenido">
                      <?php if(!empty($nota->imagen) && !empty($nota->autor)) { ?>
                        <div class="panelAuthor" rel="author">
                          <span class="byAuthor">por</span> <?php print $nota->autor; ?> 
                        </div>
                      <?php } ?>
                      <h2> <a href="/<?php print $nota->url_amigable; ?>" target="_self"><?php print $nota->titulo; ?></a></h2>
                      <?php if(empty($nota->imagen)) { ?>
                        <div class="bajada"><?php print $nota->resumen; ?></div>
                      <?php } ?>
                      <?php if(empty($nota->imagen) && !empty($nota->autor)) { ?>
                        <div class="panelAuthor" rel="author">
                          <span class="byAuthor">por</span> <?php print $nota->autor; ?> 
                        </div>
                      <?php } ?>
                      <div class="footer-card">
                        <div class="data"> 
                          <span class="nodeData"> 
                            <time><?php print $nota->fecha; ?></time>&nbsp;•&nbsp; <?php print $nota->categoria; ?>
                          </span>
                        </div>
                      </div>
                    </div> 
                  </article>
                </div>
              </div>
            <?php } ?>  
            <?php if($nkey >= 3 || $pagina > 0) { ?>
              <article class="node-teaser node-teaser-list ">
                <div class="nodo-contenido"> 
                  <main class="nota_periodistica image">
                    <?php if(!empty($nota->imagen)) { ?>
                      <div class="foto"> 
                        <?php if($nota->con_video) { ?>
                          <div class="iconsHome"><div class="sprite-indicadores sprite-video"></div></div>
                        <?php } ?>
                        <a href="/<?php print $nota->url_amigable; ?>" target="_self"> 
                          <img src="<?php print $nota->imagen_chica; ?>"> 
                        </a>
                      </div>
                    <?php } ?>
                    <div class="contenido"> 
                      <header>
                        <h2> <a href="/<?php print $nota->url_amigable; ?>" target="_self"><?php print $nota->titulo; ?></a></h2> 
                      </header>
                      <?php if(!empty($nota->autor)) { ?>
                        <div class="panelAuthor" rel="author">
                          <span class="byAuthor">por</span> <?php print $nota->autor; ?>
                        </div>
                      <?php } ?>
                      <div class="footer-card">
                        <div class="data"> 
                          <span class="nodeData"> 
                            <time><?php print $nota->fecha; ?></time>&nbsp; • &nbsp; <?php print $nota->categoria; ?> 
                          </span>
                        </div>
                      </div>
                    </div> 
                  </main>
                </div> 
              </article>
            <?php } ?>
          <?php } ?>
        </div>
      </div> 
    </section>
  </div>
  
<?php
  $aviso_pagina = $listado->pagina * $listado->pagina_result;
  if($aviso_pagina < $listado->pagina_total){
?>
  <a class="mas-noticias" href="<?php print '/noticias?page='.$listado->pagina; ?>" rel="next">Ver más noticias</a>
<?php } ?>

  
</div>