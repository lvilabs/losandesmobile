<?php if(!empty($notas)) { ?>
<div class="container notas">
  <div class="columnas">
    <header class="tit-modulo"><a href="/noticias"> Noticias </a></header> 
    <section class="b6 clearfix">
      <?php foreach($notas as $nota) { ?>
        <div class="panel nota comun ">
          <div class="panel-body"> 
            <article class="node-teaser <?php if(empty($nota->imagen)) print 'node-teaser-list'; else print 'node-teaser-card'; ?>">
              <?php if(!empty($nota->imagen)) { ?>
                <div class="foto">
                  <?php if($nota->con_video) { ?>
                    <div class="iconsHome"><div class="sprite-indicadores sprite-video"></div></div>
                  <?php } ?>
                  <a href="/<?php print $nota->url_amigable; ?>" target="_self">
                    <div class="lazy">
                      <img src="<?php print $nota->imagen; ?>">
                    </div> 
                  </a>
                  <?php if(!empty($nota->autor)) { ?>
                    <div class="gradiente"></div>
                  <?php } ?>
                </div>
              <?php } ?>
              <div class="contenido">
                <?php if(empty($nota->imagen)) { ?>
                  <header>
                    <h2> <a href="/<?php print $nota->url_amigable; ?>" target="_self"><?php print $nota->titulo; ?></a></h2> 
                  </header>
                <?php } ?>
                <?php if(!empty($nota->autor)) { ?>
                  <div class="panelAuthor" rel="author"> 
                    <!-- <a href="/users/wgiannoni" target="_self"> 
                      <img src="http://wpc.72C72.betacdn.net/8072C72/lvi-images/sites/default/files/styles/box_100_100/public/pictures/picture-58611-1507649690.jpg">  -->
                      <span class="byAuthor">por</span> <?php print $nota->autor; ?> 
                    <!-- </a> -->
                  </div>
                <?php } ?>
                <?php if(!empty($nota->imagen)) { ?>
                  <h2> 
                    <a href="/<?php print $nota->url_amigable; ?>" target="_self"><?php print $nota->titulo; ?></a>
                  </h2>
                <?php } ?>
                <div class="footer-card">
                  <div class="data"> 
                    <span class="nodeData"> 
                      <time datetime="2018-09-26 12:09"><?php print $nota->fecha; ?></time>&nbsp;•&nbsp; 
                      <?php print $nota->categoria; ?>
                    </span>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </div>
      <?php } ?>
    </section> 
  </div>
</div>
<?php } ?>