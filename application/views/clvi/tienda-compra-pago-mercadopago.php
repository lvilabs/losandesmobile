<?php
require_once realpath('.').'/application/libraries/mp_sdk/lib/mercadopago.php';
$mp = new MP(servicio_decrypt($mensaje['mp_client_id'], 'cript_mp'), servicio_decrypt($mensaje['mp_client_secret'], 'cript_mp'));
if($mensaje['mercadopago_sandbox'] == 'sandbox') {
  $mp->sandbox_mode(TRUE);
} else {
  $mp->sandbox_mode(FALSE);
}
$preference_data = array(
  "items" => $mensaje['items'],
  'back_urls' => array(
    'success' => 'http://'.$_SERVER['SERVER_NAME'].'/tienda/back_url/'.$mensaje['venta_id'].'/ok/mercadopago',
    'failure' => 'http://'.$_SERVER['SERVER_NAME'].'/tienda/back_url/'.$mensaje['venta_id'].'/cancel/mercadopago',
    'pending' => 'http://'.$_SERVER['SERVER_NAME'].'/tienda/back_url/'.$mensaje['venta_id'].'/pending/mercadopago'
  ),
  'payer' => $mensaje['payer'],
  'external_reference' => $mensaje['external_reference']
);
try {
  $preference = $mp->create_preference($preference_data);
} catch (Exception $e) {
  print_r('Error: '.$e->getMessage());
  exit();
}
$init_point = $preference["response"]["init_point"];
if($mensaje['mercadopago_sandbox']  == 'sandbox') {
  $init_point = $preference["response"]["sandbox_init_point"];
}
$html = '<!doctype html>';
$html .= '<html>';
$html .= '  <head>';
$html .= '    <title>Redireccionando a MercadoPago</title>';
$html .= '  </head>';
$html .= '  <body>';
$html .= '    <a href="'.$init_point.'" id="MP-Checkout" name="MP-Checkout" mp-mode="redirect" class="orange-ar-m-sq-arall">Pay</a>';
$html .= '    <script type="text/javascript" src="http://mp-tools.mlstatic.com/buttons/render.js"></script>';
$html .= '    <script type="text/javascript">';
$html .= '      var n = window.document.getElementById("MP-Checkout");';
$html .= '      n.click();';
$html .= '    </script>';
$html .= '  </body>';
$html .= '</html>';
print $html;
