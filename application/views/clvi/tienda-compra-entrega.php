<div>
  <div class="consigna">
    <?php if($node['datos_venta']['vendedor']['entrega_domicilio'] && $node['datos_venta']['vendedor']['entrega_sucursal']) { ?>
      Seleccione el lugar de entrega o complete la dirección a donde debe entregarse el pedido.
    <?php } elseif($vendedor->entrega_sucursal) { ?>
      Seleccione el lugar de entrega donde retirará el pedido.
    <?php } else { ?>
      Complete la dirección a donde debe entregarse el pedido.
    <?php } ?>
  </div>
  <form name="sentMessage" id="compraEntregasForm" class="has-validation-callback" method="post">
    <div class="clearfix">
      <div class="seleccion_lugar">
        <?php if($node['datos_venta']['vendedor']['entrega_sucursal']) { ?>
          <div class="radio">
            <label>
              <input type="radio" id="radio-sucursal" name="entrega_lugar" class="" value="sucursal" checked="checked">
              Retirar en sucursal/punto de venta/depósito
            </label>
          </div>
        <?php } ?>
        <?php if($node['datos_venta']['vendedor']['entrega_domicilio']) { ?>
          <div class="radio">
            <label>
              <input type="radio" id="radio-domicilio" name="entrega_lugar" class="" value="domicilio" <?php print (!$node['datos_venta']['vendedor']['entrega_sucursal'])?'checked="checked"':''; ?>>
              Envío a domicilio
            </label>
          </div>
        <?php } ?>
        <?php if($node['datos_venta']['vendedor']['envio_capitalbox']) { ?>
          <div class="radio">
            <label>
              <input type="radio" id="radio-capitalbox_express" name="entrega_lugar" class="" value="capitalbox_express" <?php print (!$node['datos_venta']['vendedor']['entrega_sucursal'])?'checked="checked"':''; ?>>
              Envío a domicilio Capital Box Express(6 hs.)
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" id="radio-capitalbox_24hs" name="entrega_lugar" class="" value="capitalbox_24hs" <?php print (!$node['datos_venta']['vendedor']['entrega_sucursal'])?'checked="checked"':''; ?>>
              Envío a domicilio Capital Box 24hs
            </label>
          </div>
        <?php } ?>
        <?php if($node['datos_venta']['vendedor']['envio_andreani']) { ?>
          <div class="radio">
            <label>
              <input type="radio" id="radio-capitalbox_express" name="entrega_lugar" class="" value="andreani" <?php print (!$node['datos_venta']['vendedor']['entrega_sucursal'])?'checked="checked"':''; ?>>
              Envío a domicilio por Andreani
            </label>
          </div>
        <?php } ?>
      </div>
      <?php if($node['datos_venta']['vendedor']['entrega_sucursal']) { ?>
        <div id="datos_lugares">
          <div class="control-group form-group">
            <div class="controls">
              <label>Seleccionar sucursal:</label>
              <select name="lugar" class="form-select" id="edit-lugar">
                <option value="0" selected="selected"></option>
                <?php foreach($node['datos_venta']['vendedor']['sucursales'] as $key => $sucursal) { ?>
                  <option value="<?php print $key; ?>"><?php print $sucursal['direccion']; ?></option>
                <?php } ?>
              </select>
              <p class="help-block"></p>
            </div>
          </div>
        </div>
      <?php } ?>
      <?php if($node['datos_venta']['vendedor']['entrega_domicilio'] || $node['datos_venta']['vendedor']['capitalbox_express'] || $node['datos_venta']['vendedor']['capitalbox_24hs'] || $node['datos_venta']['vendedor']['envio_andreani']) { ?>
        <div id="datos_direccion" <?php print ($node['datos_venta']['vendedor']['entrega_sucursal'])?'style="display: none;"':''; ?>>
          <?php if($node['datos_venta']['vendedor']['envio_andreani']) { ?>
            <div class="control-group form-group">
              <div class="controls">
                <label class="sr-only">Calle:</label>
                <input type="text" name="entrega_direccion" class="form-control" placeholder="Calle:">
                <p class="help-block"></p>
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label class="sr-only">Número:</label>
                <input type="text" name="entrega_calle_numero" class="form-control" placeholder="Número:">
                <p class="help-block"></p>
              </div>
            </div>
          <?php } else { ?>
            <div class="control-group form-group">
              <div class="controls">
                <label class="sr-only">Dirección:</label>
                <input type="text" name="entrega_direccion" class="form-control" placeholder="Dirección:">
                <p class="help-block"></p>
              </div>
            </div>
          <?php } ?>
          <div class="control-group form-group">
            <div class="controls">
              <label class="sr-only">Ciudad:</label>
              <input type="text" name="entrega_ciudad" class="form-control" placeholder="Ciudad:">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label class="sr-only">Provincia:</label>
              <input type="text" name="entrega_provincia" class="form-control" placeholder="Provincia:">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label class="sr-only">CP.:</label>
              <input type="text" name="entrega_cp" class="form-control" placeholder="CP.:">
              <p class="help-block"></p>
            </div>
          </div>
        </div>
      <?php } ?>
      <div class="control-group form-group">
        <div class="controls">
          <label class="">Cantidad:</label>
          <input type="text" name="cantidad" class="form-control" placeholder="Cantidad" value="1">
          <p class="help-block"></p><?php print_r($node['tids']); ?>
        </div>
      </div>
      <?php if(isset($node['taxonomy'][6275])) { ?>
      <div class="control-group form-group">
        <div class="controls">
          <label class="">Talle:</label>
          <select name="talle" class="form-select" id="edit-talle">
            <option value="" selected="selected"></option>
            <?php foreach($node['stock_talles'] as $key => $stock) { ?>
              <option value="<?php print $stock['talle']; ?>"><?php print $stock['talle']; ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <?php } ?>
      <?php if($node['datos_venta']['vendedor']['descuento_clublavoz']>0) { ?>
      <div class="control-group form-group">
        <div class="controls">
          <label class="">Descuento Club La Voz:</label>
          <input type="text" name="cupon_descuento" class="form-control" placeholder="Número Tarjeta Club La Voz" value="">
          <input type="hidden" name="descuento_porcentaje" value="<?php print $node['datos_venta']['vendedor']['descuento_clublavoz']; ?>">
          <p class="help-block"></p>
        </div>
      </div>
      <?php } ?>
      <div class="help-block with-errors" style="display: none;"></div>
      <button type="submit" class="btn btn-primary pull-right" id="btn-compra-envios">Continuar</button>
      <div class="loader" style="display: none;"><img class="" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/img/ajax-loader-tienda.gif" /></div>
    </div>
    <input type="hidden" name="compra_nid" value="<?php print $node['nid']; ?>">
    <input type="hidden" name="venta_id" id="edit-venta-id" value="<?php print $mensaje['venta_id']; ?>">
    <input type="hidden" name="vendedor_uid" id="edit-vendedor-uid" value="<?php print $node['datos_venta']['vendedor']['id_vendedor']; ?>">
    <input type="hidden" name="precio" id="edit-precio" value="<?php print $node['field_aviso_precio']; ?>">
    <input type="hidden" name="zona_id" id="edit-zona-id" value="">
    <input type="hidden" name="venta_sitio" id="edit-sitio-venta" value="">
  </form>
</div>