<div id="content" class="pages page-error">
  <div class="container">
    <div class="row">
      <div class="noticias">
        <h1>Error desconocido</h1>
        <h2>ERROR <?php echo $status_code; ?></h2>
        <p>
            Ha ocurrido un error, intente nuevamente.
            <?php echo $message; ?>
        </p>
      </div>
    </div>
  </div>
</div>