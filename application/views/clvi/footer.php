<?php
  $user_session = array();
  if(isset($this->session)) {
    $user_session = $this->session->userdata('logged_in'); 
  }
?>

<?php if(isset($adslots)) {
  foreach($adslots as $adslot) {
    if($adslot['adunit'] == 'dfp-rectangle_1' && $tipo!='buscador') {
      print ads_get_banner($adslot['adunit']);
    }
    if($adslot['adunit'] == 'dfp-rectangle' && $tipo!='buscador') {
      print ads_get_banner($adslot['adunit']);
    }
  }
}
?>

<footer class="bottom">
<div class="sub">
<div class="menu">
<a href="/" class="current" rel="index">Movil</a> | <a style="white-space: nowrap;" class="publicar" href="/node_add">Publicá Gratis</a> | <a href="/faq" rel="help">Ayuda</a> | <a href="/contact">Contáctanos</a>
</div><a href="#TOP" rel="noindex"><div class="top"></div></a>
<div class="menu">
  <a href="http://www.losandes.com.ar" rel="follow">Los Andes</a>
</div>
<p>Copyright Los Andes 1995 - <?php print date('Y');?></p>
</div>
</footer>

</div>
<?php
    $taxonomia = null;
    if(isset($node['comscore'])){
      $taxonomia = $node['comscore'];
    }
    if(isset($listado)){
      $taxonomia = (array)$listado->comscore;
    }
    comscore($_SERVER['REQUEST_URI'], $taxonomia);
    
    analytics();
    
    if(isset($cxense_events))
      print $cxense_events;
    
?>

<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion_async.js" charset="utf-8"></script>
<?php if(isset($tipo) && $tipo=='tienda'){ ?>
<!-- AIzaSyCu13LeojIQytXQ4eur-wuq5sVAbMiRcno -->
<script src="http://maps.google.com/maps/api/js?key=<?php print $this->config->item('gmap_api'); ?>&amp;sensor=false" type="text/javascript"></script>	
<script type="text/javascript" src="/public/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/tienda.js"></script>
<?php } elseif(isset($tipo) && $tipo=='buscador') { ?>
<script type="text/javascript" src="/public/js/jquery.magnific-popup.min.js"></script>
<?php } elseif(isset($tipo) && $tipo=='admin') { ?>
<script type="text/javascript" src="/public/js/dscountdown.min.js"></script>
<?php } elseif(isset($tipo) && $tipo=='filtro') { ?>
<script type="text/javascript" src="/public/<?php echo $this->config->item('carpeta_sitio'); ?>/js/filtros.js"></script>
<?php } ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js"></script>
</body>
</html>
