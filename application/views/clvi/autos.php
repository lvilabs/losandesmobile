<h1 class="DN">Venta de autos, motos, 4x4, repuestos y accesorios en Córdoba</h1>
<div class="clf-mob-home autos">
  <div class="clf-mob-img autos DN"></div>
  <div class="clf-mob-content">
    <h2>ENCONTRÁ TU VEHÍCULO</h2>
  </div>

  <div class="clf-mob-searchbox-wrapper">
    <div class="clf-mob-searchbox home">
    <form name="formFiltro" class="formFiltro" action="/search/result" method="post">
    <?php $tid_autos = $this->config->item('tid_autos'); ?>
    <?php if(isset($rubros->$tid_autos)) { ?>
      <input type="hidden" name="tidPadre" value="<?php print $tid_autos; ?>" />
      <div class="select-group">
        <i class="fa fa-car"></i>
        <select name="buscador_tipo_auto" class="form-select" id="edit-buscador-tipo-auto">
          <option value="">TIPO </option>
          <?php foreach($rubros->$tid_autos as $rubro) { ?>
            <?php if(is_object($rubro)) { ?>
              <option value="<?php print $rubro->filtro; ?>" data-tid="<?php print $rubro->tid; ?>"><?php print $rubro->name; ?></option>
            <?php } ?>
          <?php } ?>
        </select>
      </div>
      
      <div id="marca" class="select-group marca">
        <i class="fas fa-tachometer-alt"></i>
        <select name="buscador_marca" class="form-select" id="edit-buscador-marca">
          <option value="">MARCA </option>
        </select>
      </div>
      
      <?php foreach($rubros->$tid_autos as $key => $rubro) { ?>
        <?php if(is_object($rubro)) { ?>
          <div id="marca-<?php print $key; ?>" class="select-group marca" style="display:none;">
            <i class="fas fa-tachometer-alt"></i>
            <select name="buscador_marca" class="form-select" id="edit-buscador-marca-<?php print $key; ?>">
              <?php if($rubro->name == 'Accesorios y Repuestos') { ?>
                <option value="">RUBRO </option>
              <?php } else { ?>
                <option value="">MARCA </option>
              <?php } ?>
              <?php foreach($rubro as $marca) { ?>  
                <?php if(is_object($marca)) { ?>
                  <option value="<?php print $marca->filtro; ?>" data-tid="<?php print $marca->tid; ?>"><?php print $marca->name; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </div>
        <?php } ?>
      <?php } ?>
      
      <div id="modelo" class="select-group modelo">
        <i class="fas fa-tachometer-alt"></i>
        <select name="buscador_modelo" class="form-select" id="edit-buscador-modelo">
          <option value="">MODELO </option>
        </select>
      </div>
      
      <?php foreach($rubros->$tid_autos as $rubro) { ?>
        <?php if(is_object($rubro)) { ?>
          <?php foreach($rubro as $key => $marca) { ?>
            <?php if(is_object($marca)) { ?>
              <div id="modelo-<?php print $key; ?>" class="select-group modelo" style="display:none;">
                <i class="fas fa-tachometer-alt"></i>
                <select name="buscador_modelo" class="form-select" id="edit-buscador-modelo-<?php print $key; ?>">
                  <?php if($rubro->name == 'Accesorios y Repuestos') { ?>
                    <option value="">SUB-RUBRO </option>
                  <?php } else { ?>
                    <option value="">MODELO </option>
                  <?php } ?>
                  <?php foreach($marca as $modelo) { ?>  
                    <?php if(is_object($modelo)) { ?>
                      <option value="<?php print $modelo->filtro; ?>" ><?php print $modelo->name; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
            <?php } ?>
          <?php } ?>
        <?php } ?>
      <?php } ?>
      
      <a class="btn btn-primary btn-full" href="javascript:envioFiltro();" title="Buscar" alt="Buscar" rel="nofollow">buscar</a>
      
    <?php } ?>
    </form>
    </div>
  </div>

  <div class="clf-mob-searchbox home concesionarias">
    <a class="btn btn-primary btn-full" href="/concesionarias" title="Buscador de concesionarias" alt="Buscar concesionarias de clasificados" rel="search">COncesionarias</a>
  </div>
</div>