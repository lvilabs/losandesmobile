<div class="usuario">
  <div class="subtitulo">
    Para poder continuar con su compra se requieren algunos datos
    <?php if(!empty($mensaje) || !empty($errores) ) : ?>
    <div class="twelvecol mensaje clear">
      <span class="<?php $respuesta->status!=0?'error':''; ?>"><?php echo $mensaje; ?></span><br>
      <?php if(!empty($errores)) : ?>
      <span class="error">
      <?php
        foreach($errores as $error) {
          echo $error.'.<br>';
        }
      ?>
      </span>
      <?php endif; ?>
    </div>
    <?php endif; ?>
  </div>
  <form name="completar_datos" id="completar_datos_form" class="has-validation-callback" method="post" action="">
    <div class="clearfix">
      <div class="campo clearfix">
        <label class="placeholder">Nombre</label>
        <input type="text" name="profile_nombre" maxlength="64" class="" value="<?php print $respuesta->usuario->profile_nombre; ?>">
      </div>
      <div class="campo clearfix">
        <label class="placeholder">Apellido</label>
        <input type="text" name="profile_apellido" maxlength="64" class="" value="<?php print $respuesta->usuario->profile_apellido; ?>">
      </div>
      <div class="campo clearfix">
        <label class="placeholder">Calle</label>
        <input type="text" name="profile_calle" maxlength="64" class="" value="<?php print $respuesta->usuario->profile_calle; ?>">
      </div>
      <div class="campo clearfix">
        <label class="placeholder">Altura</label>
        <input type="text" name="profile_altura" maxlength="10" class="" value="<?php print $respuesta->usuario->profile_altura; ?>">
      </div>
      <div class="campo clearfix">
        <label class="placeholder">Piso</label>
        <input type="text" name="profile_piso" maxlength="10" class="" value="<?php print $respuesta->usuario->profile_piso; ?>">
      </div>
      <div class="campo clearfix">
        <label class="placeholder">Provincia</label>
        <select name="profile_provincia">
            <?php foreach($respuesta->provincias as $tid => $nombre) { ?>
            <option value="<?php print $tid; ?>" <?php print ($tid==$respuesta->usuario->profile_provincia)?'selected':''; ?>><?php print $nombre; ?></option>
            <?php } ?>
        </select>
      </div>
      <div class="campo clearfix" style="<?php print($respuesta->usuario->profile_provincia!=3173)?'display:none':''; ?>" id="usuario_ciudad_wrapper">
        <label class="placeholder">Ciudad</label>
        <select name="profile_ciudad">
            <?php foreach($respuesta->ciudades as $tid => $nombre) { ?>
            <option value="<?php print $tid; ?>" <?php print ($tid==$respuesta->usuario->profile_ciudad)?'selected':''; ?>><?php print $nombre; ?></option>
            <?php } ?>
        </select>
      </div>
      <div class="campo clearfix">
        <label class="placeholder">CP</label>
        <input type="text" name="profile_cp" maxlength="10" class="" value="<?php print $respuesta->usuario->profile_cp; ?>">
      </div>
      <div class="campo clearfix">
        <label class="placeholder">Condición ante el IVA</label>
        <select name="profile_condicion_iva">
            <?php foreach($respuesta->condiciones_iva as $tid => $nombre) { ?>
            <option value="<?php print $tid; ?>" <?php print ($tid==$respuesta->usuario->profile_condicion_iva)?'selected':''; ?>><?php print $nombre; ?></option>
            <?php } ?>
        </select>
      </div>
      <div class="campo clearfix">
        <label class="placeholder">CUIT</label>
        <input type="text" name="profile_cuit" maxlength="11" class="" value="<?php print $respuesta->usuario->profile_cuit; ?>">
      </div>
      <div class="campo clearfix">
        <label class="placeholder">DNI</label>
        <input type="text" name="profile_dni" maxlength="11" class="" value="<?php print $respuesta->usuario->profile_dni; ?>">
      </div>
      <div class="campo clearfix">
        <input type="hidden" name="compra_id" value="<?php print $compra_id; ?>">
        <input type="submit" name="submit" class="continuar" value="Continuar">
      </div>
    </div>
  </form>
</div>
<?php
//print_r($respuesta->usuario);
