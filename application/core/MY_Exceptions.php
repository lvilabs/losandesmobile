<?php

class MY_Exceptions extends CI_Exceptions {

  public function __construct() {
    parent::__construct();
  }

  function show_404($page='') {
    $this->config =& get_config();
    $data = array();
    $data_head['head_title'] = 'Página no encontrada en '.$this->config['nombre_sitio'];
    $data_head['titulo'] = 'Página no encontrada en '.$this->config['nombre_sitio'];
    $data_head['tipo'] = 'error';
    set_status_header(404);
    // Use this section if your view file is under views directory
    $CI = &get_instance();
    $CI->load->view($this->config['carpeta_sitio'].'/header', $data_head);
    $CI->load->view($this->config['carpeta_sitio'].'/error_404', $data);
    $CI->load->view($this->config['carpeta_sitio'].'/footer', $data);
    echo $CI->output->get_output();
    exit;
  }

  function show_error($heading, $message, $template = 'error_general', $status_code = 500) {
    $this->config =& get_config();
    $data = array();
    $data_head['head_title'] = 'Ha ocurrudo un error en '.$this->config['nombre_sitio'];
    $data_head['titulo'] = 'Ha ocurrudo un error en '.$this->config['nombre_sitio'];
    $data_head['tipo'] = 'error';
    $data['status_code'] = $status_code;
    $data['message'] = $message;
    set_status_header($status_code);
    // Use this section if your view file is under views directory
    $CI = &get_instance();
    $CI->load->view($this->config['carpeta_sitio'].'/header', $data_head);
    $CI->load->view($this->config['carpeta_sitio'].'/error', $data);
    $CI->load->view($this->config['carpeta_sitio'].'/footer', $data);
    echo $CI->output->get_output();
    exit;
  }
}