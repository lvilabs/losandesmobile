<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filtros extends CI_Controller {

	function __construct() {
    parent::__construct();
    $this->load->helper('url');
    $this->load->helper('Functions_lvi');
    $this->load->model('avisos');
    $this->load->model('admin');
    $this->data['canonical'] = obtener_url_web($this->uri->uri_string()); //URL Canonical
  }
  
  /**
	 * Filtros for this controller.
	 *
	 */
	public function avanzados($param1 = NULL, $param2 = NULL)
	{
    
    // $this->output->cache(15); // 15 min
		
    //Tipos de avisos
    $array_principales = array(6323,6330,6106,6000,6017,6260);
    $array_princ_sec = array(6323,6330,6106,6000,6017,6260,6325,6329,6324,6326,6328,6327,6338,6331,6339,6334,6332,6337,6335,6336,6333);
    
    //Filtro rubros
    $data['rubros']=$this->avisos->get_lista_rubros();
    
    //Parametros filtro
    $filtros = '';
    $data['key'] = '';
    if(strpos($param1,'filters=')!==false)
      $filtros = $param1;
    else
      $data['key'] = $param1;
    if(strpos($param2,'filters=')!==false)
      $filtros = $param2;
    $data['parametros_filtro']=$filtros;
    
    //Subfiltro
    $array_tids = obtener_tids_url($filtros);
    
    //Campos de filtros
    $filtros_todos = $this->avisos->get_filtros_avanzados();
    if(!empty($array_tids) && array_intersect($array_tids, $array_princ_sec)){
      $data['campos_filtros'] = obtener_filtros_busqueda($array_tids, $filtros_todos);
    } else {
      $data['campos_filtros'] = $filtros_todos;
    }
    
    //dejar solo el ultimo tid de filtro
    foreach($data['campos_filtros'] as $k => $campo){
      if(isset($campo['filtro'])){
        $data['campos_filtros'][$k]['filtro'] = obtener_ultimo_tid($campo['filtro']);
      }
      if(isset($campo['child_data'])){
        foreach($campo['child_data'] as $k2 => $campo2){
          if(isset($campo2['filtro'])){
            $data['campos_filtros'][$k]['child_data'][$k2]['filtro'] = obtener_ultimo_tid($campo2['filtro']);
          }
          if(isset($campo2['child_data'])){
            foreach($campo2['child_data'] as $k3 => $campo3){
              if(isset($campo3['filtro'])){
                $data['campos_filtros'][$k]['child_data'][$k2]['child_data'][$k3]['filtro'] = obtener_ultimo_tid($campo3['filtro']);
              }
              if(isset($campo3['child_data'])){
                foreach($campo3['child_data'] as $k4 => $campo4){
                  if(isset($campo4['filtro'])){
                    $data['campos_filtros'][$k]['child_data'][$k2]['child_data'][$k3]['child_data'][$k4]['filtro'] = obtener_ultimo_tid($campo4['filtro']);
                  }
                  if(isset($campo4['child_data'])){
                    foreach($campo4['child_data'] as $k5 => $campo5){
                      if(isset($campo5['filtro'])){
                        $data['campos_filtros'][$k]['child_data'][$k2]['child_data'][$k3]['child_data'][$k4]['child_data'][$k5]['filtro'] = obtener_ultimo_tid($campo5['filtro']);
                      }
                    }
                  }
                }
              }
            }     
          }
        }
      }
    }
    
    //Provincias y ciudades
    $data['campos_provincia'] = $this->avisos->get_filtros_avanzados_provincia();
    
    //Filtros aplicados
    $parametros = array($param1, $param2);
    $rubros_principales = array();
    $data['filtros_nombres'] = array();
    foreach($array_tids as $tid){
      if($tid != ''){
        if(is_numeric($tid)){
          $rubros_principales[] = $tid;
          if(!in_array($tid, $array_principales)){
            $term_filter = $this->avisos->get_taxonomy($tid);
            $url = quitar_filtro_url($tid, $parametros, '/filtros/avanzados/');
            $data['filtros'][] = array('tid' => $term_filter['tid'], 'nombre' => $term_filter['name'], 'url' => $url);
            $data['filtros_nombres'][] = $term_filter['tid'];
          }
        } else {
          $url = quitar_filtro_url($tid, $parametros, '/filtros/avanzados/');
          $data['filtros'][] = array('tid' => $tid, 'nombre' => $tid, 'url' => $url);
          $data['filtros_nombres'][] = $tid;
        }
      }
    }
    
    $data['filtros_ocultos'] = array();
    $pattern = '/([^\s]*):("([^"]*)"|\[(.*)\]|([^\s]*))/';
    $f = $filtros;
    if(strpos($f, 'filters=')===0) {
      $f = substr($filtros, 8);
    }
    preg_match_all($pattern, urldecode($f), $matches); // Separamos por espacios que no esten entre comillas.
    $filters_array = $matches[0];
    foreach($filters_array as $filtro) {
      $data_filtro = explode(':', $filtro);
      if(strpos($data_filtro[0], 'is_uid') !== FALSE) {
        $data['filtros_ocultos'][] = array('parametro'=>$data_filtro[0], 'valor'=>$data_filtro[1]);
        $usuario = $this->admin->post_datos('datos_usuario', 'uid='.$data_filtro[1]);
        if(!empty($usuario->uid) && is_numeric($usuario->uid)) {
          $nombre = 'Vendedor';
          if(!empty($usuario->roles->{4})) {
            $nombre = $usuario->profile_nombre.' '.$usuario->profile_apellido;
          } else {
            if(!empty($usuario->profile_nombre_comercial)) {
              $nombre = $usuario->profile_nombre_comercial;
            }
          }
          for($i=0; $i<count($data['filtros']); $i++) {
            if($data['filtros'][$i]['nombre'] == 'Vendedor') {
              $data['filtros'][$i]['nombre'] = $nombre;
            }
          }
        }
      }
    }
    
    $data['rubros_principales'] = $rubros_principales;
    
    $data['head_title']='Filtros Avanzados - Clasificados La Voz';
    
    $data['tipo'] = 'filtro';
    $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
    $this->load->view($this->config->item('carpeta_sitio').'/filtros',$data);
		$this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
	}
  
  /**
	 * Filtros mapa for this controller.
	 *
	 */
	public function avanzados_map($param1 = NULL, $param2 = NULL)
	{
    
    $this->output->cache(15); // 15 min
		
    //Tipos de avisos
    $array_principales = array(6323,6330,6106,6000,6017,6260);
    $array_princ_sec = array(6323,6330,6106,6000,6017,6260,6325,6329,6324,6326,6328,6327,6338,6331,6339,6334,6332,6337,6335,6336,6333);
    
    //Filtro rubros
    $data['rubros']=$this->avisos->get_lista_rubros();
    
    //Parametros filtro
    $filtros = '';
    $data['key'] = '';
    if(strpos($param1,'filters=')!==false)
      $filtros = $param1;
    else
      $data['key'] = $param1;
    if(strpos($param2,'filters=')!==false)
      $filtros = $param2;
    $data['parametros_filtro']=$filtros;
    
    //Subfiltro
    $array_tids = obtener_tids_url($filtros);
        
    //Campos de filtros
    $filtros_todos = $this->avisos->get_filtros_avanzados();
    if(!empty($array_tids) && array_intersect($array_tids, $array_princ_sec)){
      $data['campos_filtros'] = obtener_filtros_busqueda($array_tids, $filtros_todos);
    } else {
      $data['campos_filtros'] = $filtros_todos;
    }
    
    //dejar solo el ultimo tid de filtro
    foreach($data['campos_filtros'] as $k => $campo){
      if(isset($campo['filtro'])){
        $data['campos_filtros'][$k]['filtro'] = obtener_ultimo_tid($campo['filtro']);
      }
      if(isset($campo['child_data'])){
        foreach($campo['child_data'] as $k2 => $campo2){
          if(isset($campo2['filtro'])){
            $data['campos_filtros'][$k]['child_data'][$k2]['filtro'] = obtener_ultimo_tid($campo2['filtro']);
          }
          if(isset($campo2['child_data'])){
            foreach($campo2['child_data'] as $k3 => $campo3){
              if(isset($campo3['filtro'])){
                $data['campos_filtros'][$k]['child_data'][$k2]['child_data'][$k3]['filtro'] = obtener_ultimo_tid($campo3['filtro']);
              }
              if(isset($campo3['child_data'])){
                foreach($campo3['child_data'] as $k4 => $campo4){
                  if(isset($campo4['filtro'])){
                    $data['campos_filtros'][$k]['child_data'][$k2]['child_data'][$k3]['child_data'][$k4]['filtro'] = obtener_ultimo_tid($campo4['filtro']);
                  }
                  if(isset($campo4['child_data'])){
                    foreach($campo4['child_data'] as $k5 => $campo5){
                      if(isset($campo5['filtro'])){
                        $data['campos_filtros'][$k]['child_data'][$k2]['child_data'][$k3]['child_data'][$k4]['child_data'][$k5]['filtro'] = obtener_ultimo_tid($campo5['filtro']);
                      }
                    }
                  }
                }
              }
            }     
          }
        }
      }
    }
                
    //Provincias y ciudades
    $data['campos_provincia'] = $this->avisos->get_filtros_avanzados_provincia();
    
    //Filtros aplicados
    $parametros = array($param1, $param2);
    $rubros_principales = array();
    $data['filtros_nombres'] = array();
    foreach($array_tids as $tid){
      if($tid != ''){
        if(is_numeric($tid)){
          $rubros_principales[] = $tid;
          if(!in_array($tid, $array_principales)){
            $term_filter = $this->avisos->get_taxonomy($tid);
            $url = quitar_filtro_url($tid, $parametros, '/filtros/avanzados_map/');
            $data['filtros'][] = array('tid' => $term_filter['tid'], 'nombre' => $term_filter['name'], 'url' => $url);
            $data['filtros_nombres'][] = $term_filter['tid'];
          }
        } else {
          $url = quitar_filtro_url($tid, $parametros, '/filtros/avanzados_map/');
          $data['filtros'][] = array('tid' => $tid, 'nombre' => $tid, 'url' => $url);
          $data['filtros_nombres'][] = $tid;
        }
      }
    }
    
    $data['filtros_ocultos'] = array();
    $pattern = '/([^\s]*):("([^"]*)"|\[(.*)\]|([^\s]*))/';
    $f = $filtros;
    if(strpos($f, 'filters=')===0) {
      $f = substr($filtros, 8);
    }
    preg_match_all($pattern, urldecode($f), $matches); // Separamos por espacios que no esten entre comillas.
    $filters_array = $matches[0];
    foreach($filters_array as $filtro) {
      $data_filtro = explode(':', $filtro);
      if(strpos($data_filtro[0], 'is_uid') !== FALSE) {
        $data['filtros_ocultos'][] = array('parametro'=>$data_filtro[0], 'valor'=>$data_filtro[1]);
        $usuario = $this->admin->post_datos('datos_usuario', 'uid='.$data_filtro[1]);
        if(!empty($usuario->uid) && is_numeric($usuario->uid)) {
          $nombre = 'Vendedor';
          if(!empty($usuario->roles->{4})) {
            $nombre = $usuario->profile_nombre.' '.$usuario->profile_apellido;
          } else {
            if(!empty($usuario->profile_nombre_comercial)) {
              $nombre = $usuario->profile_nombre_comercial;
            }
          }
          for($i=0; $i<count($data['filtros']); $i++) {
            if($data['filtros'][$i]['nombre'] == 'Vendedor') {
              $data['filtros'][$i]['nombre'] = $nombre;
            }
          }
        }
      }
    }
    
    $data['rubros_principales'] = $rubros_principales;
    $data['head_title']='Filtros Avanzados Mapa - Clasificados La Voz';
    
    $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
    $this->load->view($this->config->item('carpeta_sitio').'/filtros_map',$data);
		$this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
	}
  
  /**
	 * Submit formulario filtros avanzados.
	 *
	 */
	public function submit_form($param = NULL)
	{
    $post = $_POST;
    
    if(!empty($post)){
      
      $url_filtro = '';
      $key = '';
      $url = $post['url'];
      $lat = '';
      $long = '';
      unset($post['url']);
      foreach($post as $kfiltro => $filtro){
        
        if($kfiltro == 'key'){
          $key = $filtro;
        } elseif($kfiltro == 'latitud'){
          $lat = '/latitud='.$filtro;
        } elseif($kfiltro == 'longitud'){
          $long = '/longitud='.$filtro;
        } else {
          //si el filtro tiene ' ' le agrego '"'
          if(strpos($filtro, ' ') !== FALSE && strpos($filtro, ':') !== FALSE) {
            $filtro = str_replace('-', '', $filtro);
            $trim_filtro = explode(':', $filtro);
            if(isset($trim_filtro[1]))
              $filtro = $trim_filtro[0].':"'.$trim_filtro[1].'"';
          //si el filtro tiene '-' lo reemlazo por '"'
          } elseif(strpos($filtro, '-') !== FALSE) {
            $filtro = str_replace('-', '"', $filtro);
          }
          //filtro precio
          if($kfiltro == 'field_precio_desde'){
            if((isset($post['field_precio_desde']) && !empty($post['field_precio_desde']) || isset($post['field_precio_hasta']) && !empty($post['field_precio_hasta']))){
              $precio_desde = (empty($post['field_precio_desde']) || !is_numeric($post['field_precio_desde']))? 0 : $post['field_precio_desde'];
              $precio_hasta = (empty($post['field_precio_hasta']) || !is_numeric($post['field_precio_hasta']))? 1000000 : $post['field_precio_hasta'];
              $filtro = 'fs_precio%3A%5b'.$precio_desde.' TO '.$precio_hasta.'%5d';
            }
          }
          if($kfiltro == 'field_precio_hasta'){
            $filtro = '';
          }
          //filtro moneda
          if($kfiltro == 'field_moneda'){
             if(isset($post['field_moneda']) && !empty($post['field_moneda'])){
              $filtro = 'is_moneda_k%3A'.$filtro;
             }
          }
          //filtro kilometros
          if($kfiltro == 'field_kilometro_desde'){
            if((isset($post['field_kilometro_desde']) && !empty($post['field_kilometro_desde']) || isset($post['field_kilometro_hasta']) && !empty($post['field_kilometro_hasta']))){
              $kilometro_desde = (empty($post['field_kilometro_desde']) || !is_numeric($post['field_kilometro_desde']))? 0 : $post['field_kilometro_desde'];
              $kilometro_hasta = (empty($post['field_kilometro_hasta']) || !is_numeric($post['field_kilometro_hasta']))? 100000 : $post['field_kilometro_hasta'];
              $filtro = 'fs_kilometros%3A%5b'.$kilometro_desde.' TO '.$kilometro_hasta.'%5d';
            }
          }
          if($kfiltro == 'field_kilometro_hasta'){
            $filtro = '';
          }
          //filtro anio
          if($kfiltro == 'field_modelo_desde'){
            if((isset($post['field_modelo_desde']) && !empty($post['field_modelo_desde']) || isset($post['field_modelo_hasta']) && !empty($post['field_modelo_hasta']))){
              $modelo_desde = (empty($post['field_modelo_desde']) || $post['field_modelo_desde'] < 1970 || $post['field_modelo_desde'] > date("Y") || !is_numeric($post['field_modelo_desde']))? 1970 : $post['field_modelo_desde'];
              $modelo_hasta = (empty($post['field_modelo_hasta']) || $post['field_modelo_hasta'] < 1970 || $post['field_modelo_hasta'] > date("Y") || !is_numeric($post['field_modelo_hasta']))? date("Y") : $post['field_modelo_hasta'];
              $filtro = 'iss_anio%3A%5b'.$modelo_desde.' TO '.$modelo_hasta.'%5d';
            }
          }
          if($kfiltro == 'field_modelo_hasta'){
            $filtro = '';
          }
          //filtro por usuario (vendedor)
          if($kfiltro == 'is_uid'){
             if(isset($post['is_uid']) && !empty($post['is_uid'])){
              $filtro = 'is_uid%3A'.$filtro;
             }
          }
          
          if($filtro != ''){
            if($url_filtro == '')
              $url_filtro = 'filters='.$filtro;
            else
              $url_filtro .= '%20'.$filtro;
          }
        }
      }
      if(!empty($url_filtro)) {
        if(strpos($url_filtro, '%20')===FALSE) {
          $url_filtro .= '%20';
        }
      }
      
      redirect($url.$key.$url_filtro.$lat.$long, 'refresh');

    } else {
      $this->avanzados($param);
    }
	}
	
}

/* End of file filtros.php */
/* Location: ./application/controllers/filtros.php */