<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Node_add extends CI_Controller {
  
  var $data = array();
  
  function __construct() {
    parent::__construct();
    $this->load->helper('url');
    //$this->load->helper('form');
    $this->load->helper('Functions_lvi');
    $this->load->library('session');
    //$this->load->library('form_validation');
    $this->load->model('avisos');
    //$this->load->model('pages');
    $this->load->model('admin');
    $this->load->model('servicios');
    $this->data['token'] = $this->session->userdata('token');
    //$this->data['session'] = array('token'=>$this->session->userdata('token'));
    $this->data['canonical'] = obtener_url_web($this->uri->uri_string()); //URL Canonical
  }
	
  /**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
		$post = $_POST;
    if(!empty($post)){
      $this->rubro($post['selectRubro'], '', '');
    } else { 
      $data['rubros']=$this->avisos->get_lista_rubros();
      $data['node']=FALSE;
      $data['rubro_name'] = '';
      $data['mensaje'] = '';
      //Title Head
      $data['head_title']='Crear Aviso - Clasificados La Voz';
      $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
      $this->load->view($this->config->item('carpeta_sitio').'/node_add',$data);
      //$this->load->view($this->config->item('carpeta_sitio').'/en-mantenimiento',$data);
      $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
    }
	}
  
  /**
	 * Formulario por rubro.
	 *
	 */
	public function rubro($rubro, $mensaje, $campos_error, $node=FALSE)
	{
    
    if($node!=FALSE) {
      $data['node'] = $node;
    }
    
		$data['rubros'] = $this->avisos->get_lista_rubros();
    
    
    $options_ini = $this->avisos->get_options_form_avisos();
    $options = array();
    foreach($options_ini as $campo){
      $habilitado = 0;
      foreach($campo['rubros'] as $campo_rubro){
        if($campo_rubro['key'] == $rubro)
          $habilitado = 1;
      }
      if($habilitado){
        $options[$campo['key']] = $campo;
      }
    }
    
    $options_ubicacion = $this->avisos->get_options_ubicacion_form_avisos();
    //print_r($options_ubicacion);
    
    $data['rubro'] = $rubro;
    $data['mensaje'] = $mensaje;
    $data['campos_error'] = $campos_error;
    $data['rubro_name'] = obtener_aviso_rubro($rubro);
    $data['cotizacion_uva'] = $this->servicios->get_cotizacion_uva();
    $data['options'] = $options;
    
    $data['options_ubicacion'] = $options_ubicacion;
    
    //Title Head
    $data['head_title']='Crear Aviso '.$data['rubro_name'].' - Clasificados La Voz';
    if($node!=FALSE) {
      $data['head_title'] = 'Editar Aviso '.$data['rubro_name'].' - Clasificados La Voz';
    }
    $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
    $this->load->view($this->config->item('carpeta_sitio').'/node_add',$data);
    //$this->load->view($this->config->item('carpeta_sitio').'/en-mantenimiento',$data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
	}
  
  /**
	 * Submit formulario carga aviso.
	 *
	 */
	public function submit_form()
	{
    $this->load->model('admin');
    $post = $_POST;
    $files = $_FILES;
    if(!empty($post)){
      $status = validar_form_aviso($post, $files);
      if($status['validado']){
        $this->load->model('avisos');
        $mensaje = '';
        //Cargo imagenes al post
        if(!empty($files)) {
          foreach($files as $kfile => $vfile){
            if(!empty($vfile['tmp_name'])){
              $imagedata = base64_encode(file_get_contents($vfile['tmp_name']));
              $post[$kfile] = array('type' => $vfile['type'],
                                    'size' => $vfile['size'],
                                    'data' => $imagedata);
            }
          }
        }
        //Detectar provincia, ciudad y barrio
        if(isset($post['barrio-'.$post['ciudad-'.$post['provincia']]]) && !empty($post['barrio-'.$post['ciudad-'.$post['provincia']]])){
          $post['ubicacion'] = $post['barrio-'.$post['ciudad-'.$post['provincia']]];
        } else {
          $post['ubicacion'] = $post['ciudad-'.$post['provincia']];
        }
        if($this->admin->session_valida()==true) {
          $post['token'] = $this->session->userdata('token');
        }
        $data=$this->avisos->insert_aviso($post);
        foreach($data['mensaje'] as $msj){
          $mensaje .= $msj.'<br>';
        }
        if($data['status']==='ok') {
          // Borramos los datos solo si se registro correctamente, porque sino nos limpiaria el formulario.
          unset($_POST);
        }
        $node = FALSE;
        if(isset($post['nid']) && !empty($post['nid'])) {
          $token = $this->session->userdata('token');
          $respuesta = $this->admin->post_datos('admin/node_aviso', array('nid'=>$post['nid'], 'token'=>$token));
          if($respuesta->status!=1) {
            $mensaje .= $respuesta->mensaje;
          } else {
            $node = (array)$respuesta->nodo; // Si no convierto array luego no carga las vistas.  
          }
        }
        if($post['field_aviso_categoria'] == 'aviso_alquiler_temporario') {
          // Redirijimos al administrador
          //redirect('/administrar/mis-avisos.html', 'location');
          $this->alojamiento($mensaje, '', $node);
        } else {
          $this->rubro($post['field_aviso_categoria'], $mensaje, '', $node);
        }
      } else {
        $node = FALSE;
        if(isset($post['nid']) && !empty($post['nid'])) {
          $token = $this->session->userdata('token');
          $respuesta = $this->admin->post_datos('admin/node_aviso', array('nid'=>$post['nid'], 'token'=>$token));
          if($respuesta->status!=1) {
            $status['mensaje'] .= $respuesta->mensaje;
          } else {
            $node = (array)$respuesta->nodo; // Si no convierto array luego no carga las vistas.  
          }
        }
        if($post['field_aviso_categoria'] == 'aviso_alquiler_temporario')
          $this->alojamiento($status['mensaje'], $status['campos'], $node);
        else
          $this->rubro($post['field_aviso_categoria'], $status['mensaje'], $status['campos'], $node);
      }
    } else {
      $this->rubro('', $mensaje, '');
    }
	}
      
  public function edit($nid) {
    $this->load->model('admin');
    $mensaje = '';
    $node = FALSE;
    if($nid!=FALSE) {
      $token = $this->session->userdata('token');
      $respuesta = $this->admin->post_datos('admin/node_aviso', array('nid'=>$nid, 'token'=>$token));
      if($respuesta->status!=1) {
        $mensaje = $respuesta->mensaje;
      } else {  
        $node = (array)$respuesta->nodo; // Si no convierto array luego no carga las vistas.  
      }
    }
    if($node['type'] == 'aviso_alquiler_temporario')
      $this->alojamiento($mensaje, FALSE, $node);
    else
      $this->rubro($node['type'], $mensaje, FALSE, $node);
  }
 
}

/* End of file node_add.php */
/* Location: ./application/controllers/node_add.php */