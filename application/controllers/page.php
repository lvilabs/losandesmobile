<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

  var $data = array();

  function __construct() {
    parent::__construct();
    $this->load->helper('url');
    $this->load->helper('Functions_lvi');
    $this->load->model('avisos');
    $this->load->model('pages');
    $this->load->model('notas');
    $this->load->library('session');
    $this->data['tipo'] = 'page';
    $this->config->load('adslot_config', TRUE);
  }

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  public function index() {
  }
  
  public function inmuebles() {
    $data = $this->data;
    $data['canonical'] = 'inmuebles';
    $data['head_title'] = 'Inmuebles, compra, venta y alquileres - Clasificados La Voz';
    $data['head_description'] = 'Encontrar la casa, departamento o lote que buscas para comprar o alquilar. Conseguir la mayor oferta inmobiliaria de Córdoba con Clasificados La Voz';
    $this->output->cache(1); // 1 hora
    $data['rubros'] = $this->avisos->get_lista_rubros();
    $data['notas'] = $this->notas->get_destacados_nota('inmuebles', 3);
    $data['es_prehome'] = 1;
    $data['rubro_tid_seleccionado'] = 6330;
    $data['head_tipo'] = 'prehome';
    $data['adslots'] = $this->config->item('adslot_prehome','adslot_config');
    $data['adslots']['target'][] = array('key' => 'clasificados_tipo', 'value' => 'inmuebles');
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/inmuebles', $data);
    //$this->load->view($this->config->item('carpeta_sitio').'/nota_destacados',$data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function autos() {
    $data = $this->data;
    $data['canonical'] = 'vehiculos';
    $data['head_title'] = 'Autos, compra y venta de vehículos - Clasificados La Voz';
    $data['head_description'] = 'Encontrar el auto nuevo o usado que estás buscando. Comprar y vender en Córdoba con Clasificados La Voz';
    $this->output->cache(60); // 1 hora
    $data['rubros'] = $this->avisos->get_lista_rubros();
    $data['notas'] = $this->notas->get_destacados_nota('vehiculos', 3);
    $data['es_prehome'] = 1;
    $data['rubro_tid_seleccionado'] = 6323;
    $data['head_tipo'] = 'prehome';
    $data['adslots'] = $this->config->item('adslot_prehome','adslot_config');
    $data['adslots']['target'][] = array('key' => 'clasificados_tipo', 'value' => 'vehiculos');
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/autos', $data);
    //$this->load->view($this->config->item('carpeta_sitio').'/nota_destacados',$data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function productos() {
    $data_head = $this->data;
    $data_head['canonical'] = 'productos';
    $data_head['head_title'] = 'Productos y ofertas online - Clasificados La Voz';
    $data_head['head_description'] = 'Productos online en Córdoba y aprovechar las promociones y descuentos de Clasificados La Voz. Publicar aviso gratis y vender de todo';
    $this->output->cache(10); // 10 minutos
    $data_head['rubro_tid_seleccionado'] = 6106;
    $data_head['adslots'] = $this->config->item('adslot_prehome','adslot_config');
    $data_head['adslots']['target'][] = array('key' => 'clasificados_tipo', 'value' => 'productos');
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data_head);
    $data = $this->pages->get_page_content('productos');
    $data_notas['notas'] = $this->notas->get_destacados_nota('productos', 3);
    $data->banners = $this->data['banners'];
    $data->head_tipo = 'prehome';
    $this->load->view($this->config->item('carpeta_sitio').'/prehome-productos', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/nota_destacados',$data_notas);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function ofertas() {
    $data_head['tipo'] = 'page';
    $data_head['canonical'] = 'ofertas';
    $data_head['head_title'] = 'Ofertas de productos - Clasificados La Voz';
    $data_head['head_description'] = 'Oferta de productos online en Córdoba con descuentos de Clasificados La Voz. Publicar aviso gratis y vender de todo';
    $this->output->cache(60); // 1 hora
    $data_head['rubros'] = $this->avisos->get_lista_rubros();
    $data_head['rubro_tid_seleccionado'] = 6106;
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data_head);
    $data = $this->pages->get_page_content('ofertas');
    $this->load->view($this->config->item('carpeta_sitio').'/ofertas', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer');
  }
  public function tiendas_lavoz() {
    $data_head['tipo'] = 'page';
    $data_head['canonical'] = 'tiendas_lavoz';
    $this->output->cache(60); // 1 hora
    $data_head['rubros'] = $this->avisos->get_lista_rubros();
    $data_head['rubro_tid_seleccionado'] = 6106;
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data_head);
    $data = $this->pages->get_page_content('tiendas_lavoz');
    $this->load->view($this->config->item('carpeta_sitio').'/tiendas_lavoz', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer');
  }
  
  public function concesionarias() {
    $this->load->library('pagination');
    $data_head['head_title'] = 'Concesionarias que publican en '.$this->config->item('nombre_sitio');
    $data_head['titulo'] = 'Concesionarias que publican en '.$this->config->item('nombre_sitio');
    $data_head['tipo'] = 'page';
    $data_head['canonical'] = 'concesionarias';
    $data_head['rubros'] = $this->avisos->get_lista_rubros();
    $data_head['rubro_tid_seleccionado'] = 6323;
    $md5 = md5(json_encode($_GET));
    $data = array();
    $args = array();
    $nombre = $this->input->get('concesionaria', TRUE);
    if(!empty($nombre)) {
      $args['concesionaria'] = $nombre;
    }
    $page = $this->input->get('page', TRUE);
    if(!empty($page)) {
      $args['page'] = $page;
    }
    $ciudad = $this->input->get('ciudad', TRUE);
    if(!empty($ciudad)) {
      $args['ciudad'] = $ciudad;
    }
    $barrio = $this->input->get('barrio', TRUE);
    if(!empty($barrio)) {
      $args['barrio'] = $barrio;
    }
    if(!$json = $this->cache->file->get($this->config->item('token_sitio').'concesionarias_'.$md5)) {
      $data = $this->pages->get_page_content('concesionarias', FALSE, $args);
      $json = json_encode($data);
      $this->cache->file->save($this->config->item('token_sitio').'concesionarias_'.$md5, $json, 600); // 5min
    }
    $data = json_decode($json);
    $data->args = $args;

    $params = $_GET;
    if(isset($params['page'])) {
      unset($params['page']);
    }
    if(!empty($params)) {
      $config['base_url'] = $this->config->item('sitio_host').'/concesionarias?'.http_build_query($params);
    } else {
      $config['base_url'] = $this->config->item('sitio_host').'/concesionarias';
    }
    $config['first_url'] = $config['base_url'];
    // Para que los parametros coincidan con web de escritorio colocamos cantidad de paginas en rows y 1 en cant de rows por pagina.
    $config['total_rows'] = $data->pages;
    $config['per_page'] = 1;
    $config['page_query_string'] = TRUE;
    $config['query_string_segment'] = 'page';
    $config['first_link'] = 'Primera';
    $config['last_link'] = 'Última';
    $this->pagination->initialize($config);
    $data->pagination = $this->pagination->create_links();
    if(empty($params)) {
      $data->pagination = str_replace('&amp;page=', '?page=', $data->pagination);
    }
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data_head);
    $this->load->view($this->config->item('carpeta_sitio').'/concesionarias', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer');
  }

  public function inmobiliarias() {
    $this->load->library('pagination');
    $data_head['head_title'] = 'Inmobiliarias que publican en '.$this->config->item('nombre_sitio');
    $data_head['titulo'] = 'Inmobiliarias que publican en '.$this->config->item('nombre_sitio');
    $data_head['tipo'] = 'page';
    $data_head['canonical'] = 'inmobiliarias';
    $data_head['rubros'] = $this->avisos->get_lista_rubros();
    $data_head['rubro_tid_seleccionado'] = 6330;
    $md5 = md5(json_encode($_GET));
    $data = array();
    $args = array();
    $nombre = $this->input->get('inmobiliaria', TRUE);
    if(!empty($nombre)) {
      $args['comercio'] = $nombre;
    }
    $page = $this->input->get('page', TRUE);
    if(!empty($page)) {
      $args['page'] = $page;
    }
    $ciudad = $this->input->get('ciudad', TRUE);
    if(!empty($ciudad)) {
      $args['ciudad'] = $ciudad;
    }
    $barrio = $this->input->get('barrio', TRUE);
    if(!empty($barrio)) {
      $args['barrio'] = $barrio;
    }
    if(!$json = $this->cache->file->get($this->config->item('token_sitio').'inmobiliarias_'.$md5)) {
      $data = $this->pages->get_page_content('inmobiliarias', FALSE, $args);
      $json = json_encode($data);
      $this->cache->file->save($this->config->item('token_sitio').'inmobiliarias_'.$md5, $json, 600); // 5min
    }
    $data = json_decode($json);
    $data->args = $args;

    $params = $_GET;
    if(isset($params['page'])) {
      unset($params['page']);
    }
    if(!empty($params)) {
      $config['base_url'] = $this->config->item('sitio_host').'/inmobiliarias?'.http_build_query($params);
    } else {
      $config['base_url'] = $this->config->item('sitio_host').'/inmobiliarias';
    }
    $config['first_url'] = $config['base_url'];
    // Para que los parametros coincidan con web de escritorio colocamos cantidad de paginas en rows y 1 en cant de rows por pagina.
    $config['total_rows'] = $data->pages;
    $config['per_page'] = 1;
    $config['page_query_string'] = TRUE;
    $config['query_string_segment'] = 'page';
    $config['first_link'] = 'Primera';
    $config['last_link'] = 'Última';
    $this->pagination->initialize($config);
    $data->pagination = $this->pagination->create_links();
    if(empty($params)) {
      $data->pagination = str_replace('&amp;page=', '?page=', $data->pagination);
    }
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data_head);
    $this->load->view($this->config->item('carpeta_sitio').'/inmobiliarias', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer');
  }
  
  public function ayuda() {
    $data['tipo'] = 'page';
    $data['canonical'] = 'faq';
    $this->output->cache(60); // 1 hora
    $data['listado'] = $this->pages->get_page_content('faq');
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/ayuda', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer');
  }
  
  public function contact() {
    $data['tipo'] = 'page';
    $data['canonical'] = 'contact';
    //$this->output->cache(60); // 1 hora
    $data['mensaje'] = $mensaje;
    if( FALSE != ($mensaje = $this->session->flashdata('mensaje'))) {
      $data['mensaje'] = $mensaje;
    }
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/contact', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer');
  }
  
  public function send_contact() {
    $respuesta = $this->pages->post_datos('send_contacto_sitio', $_POST);
    $this->session->set_flashdata('mensaje', $respuesta);
    redirect('contact');
  }
  
}

/* End of file page.php */
/* Location: ./application/controllers/page.php */