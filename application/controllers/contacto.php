<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacto extends CI_Controller {

	/**
	 * Contacto for this controller.
	 *
	 */
	public function contacto_vendedor()
	{
    session_start();
    
    $get = $_GET;
    $this->load->helper('Functions_lvi');
		$this->load->model('avisos');
    $this->load->model('admin');
    
    //Validacion de campos
    $mensaje = '';
    $status = 'ok';
    $sin_telefono = false;
    $sin_mail = false;
    foreach($get as $kparm => $dparam){
      if($kparm == 'contactar_vendedor_nombre'){
        if($dparam == '' || $dparam == 'Nombre'){
          $mensaje .= 'Su nombre es requerido<br>';
          $status = 'error';
        }
      }
      if($kparm == 'contactar_vendedor_consulta'){
        if($dparam == '' || $dparam == 'Escriba su consulta aquí'){
          $mensaje .= 'Su consulta es requerida.<br>';
          $status = 'error';
        }
      }
      if($kparm == 'contactar_vendedor_telefono'){
        if($dparam == '' || $dparam == 'Teléfono')
          $sin_telefono = true;
      }
      if($kparm == 'contactar_vendedor_mail'){
        if($dparam == '' || $dparam == 'E-Mail')
          $sin_mail = true;
      }      
    }
    if($sin_telefono && $sin_mail){
      $mensaje .= 'Al menos un dato de contacto es requerido. Por favor ingrese su e-mail y/o teléfono.<br>';
      $status = 'error';
    }

    // Validacion captcha de google.
    if($this->admin->session_valida()!=true) {
      // Solo para usuarios no logueados.
      $json = verificar_recaptcha($get['g-recaptcha-response'], $this->config->item('api_key_recaptcha_secret'));
      if($json->success!==TRUE) {
        log_message('error', 'El captcha es incorrecto. '.print_r($json, TRUE));
        $mensaje .= "El captcha es incorrecto.";
        $status = 'error';
      }
    }
    
    if($status == 'ok'){
      
      foreach($get as $kparm => $dparam){
        if($kparm == 'contactar_vendedor_consulta')
          $_SESSION['contactar_vendedor_form_consulta'] = $dparam;
      }
      
      $data=$this->avisos->insert_contacto($get);
      if(isset($data['codigo_conversion'])) {
        $data['mensaje'] .= $data['codigo_conversion'];
      }
      print json_encode($data);
    } else {
      print json_encode(array('status'=>$status, 'mensaje'=>$mensaje));
    }
    
	}
}

/* End of file contacto.php */
/* Location: ./application/controllers/contacto.php */