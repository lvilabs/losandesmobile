<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redireccion extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function check()
	{
    $this->load->helper('Functions_lvi');
    
    //Redireccion buscador
    if(strpos($this->uri->uri_string(), 'search/apachesolr_search') !== false){
      $url_sections = urldecode(urldecode($_SERVER['REQUEST_URI']));
      $url_sections = explode('/', $url_sections);
      $get = $_GET;
      $key = '';
      $filters = array();
      $filtro_url = '';
      $page = '';
      if(isset($url_sections[4]) && $url_sections[4] != ''){
        $url_variables = explode('?', $url_sections[4]);
        $key = $url_sections[3];
      } elseif(isset($url_sections[3]) && $url_sections[3] != '') {
        $url_variables = explode('?', $url_sections[3]);
      } elseif(isset($url_sections[2]) && $url_sections[2] != '') {
        $url_variables = explode('?', $url_sections[2]);
      }
      
      //print_r($url_variables);
        
      if(is_array($url_variables)){ 
        
        if(!empty($url_variables[0]) && $url_variables[0] != 'apachesolr_search') {
          $key = $url_variables[0];
        }
        if(!empty($url_variables[1])) {
          
          $filtros = explode('&', $url_variables[1]);
          foreach($filtros as $filtro_valor) {
            $filtro = explode(':', $filtro_valor);
            $filtro_key = explode(']=', $filtro[0]);
            // Si es taxonomia cambia el filtro
            if(isset($filtro_key[1]) && strpos($filtro_key[1], 'im_taxonomy_vid_') !== FALSE) {
              $filters[] = 'tid:'.$filtro[1];
            // Filtros especiales
            } elseif(isset($filtro_key[1]) && in_array($filtro_key[1], array('fs_precio', 'is_moneda_k', 'fs_kilometros', 'fs_superficie_terreno', 'iss_anio'))) {
              // Si es desde - hasta
              if(strpos($filtro[1], '[') !== FALSE) {
                $filtro_to = str_replace('[', '%5B', $filtro[1]);
                $filtro_to = str_replace(']', '%5D', $filtro_to);
                $filters[] = $filtro_key[1].':'.$filtro_to;
              } else {
                $filters[] = $filtro_key[1].':'.$filtro[1];
              }
            // Filtro texto con espacio
            } elseif(isset($filtro_key[1]) && strpos($filtro[1], ' ') !== FALSE) {
              $filtro_espacio = '"'.$filtro[1].'"';
              $filters[] = $filtro_key[1].':'.$filtro_espacio;
            // Filtro normal
            } if(isset($filtro_key[1])) { 
              $filters[] = $filtro_key[1].':'.$filtro[1];
            } else {
              // No filtramos nada
            }
          }
          
        }
      } else {
        $key = $url_variables;
      }

      if(!empty($filters)){
        $filtro_url = 'filters=';
        foreach($filters as $filtro){
          if($filtro_url == 'filters=')
            $filtro_url .= $filtro;
          else
            $filtro_url .= ' '.$filtro;
        }
      }     
      
      //print $filtro_url;
      
      $search_final = '';
      if($key != '') $search_final .= $key;
      if($filtro_url != ''){ if($search_final == '') $search_final .= $filtro_url; else $search_final .= '/'.$filtro_url; }
      if($page != '') $search_final .= '/'.$page;
      
      //print $search_final; die;
      if(strpos($search_final, '%20')==FALSE && !empty($search_final)) {
        $search_final .= '%20';
      }
      $location = 'search/result/'.$search_final;
      header("HTTP/1.1 301 Moved Permanently");
      header('Location: /' . $location);
    //redireccion taxonomias
    } elseif(strpos($this->uri->uri_string(), 'taxonomy/term') !== false){
      $url = redireccion_taxonomy($this->uri->uri_string());
      header("HTTP/1.1 301 Moved Permanently");
      header('Location: /' . $url);
    //redireccion prehomes
    } elseif($this->uri->uri_string() == 'autos' || $this->uri->uri_string() == 'inmuebles'){
      $url = redireccion_prehomes($this->uri->uri_string());
      header("HTTP/1.1 301 Moved Permanently");
      header('Location: /' . $url);
    //redireccion creacion avisos
    } elseif(strpos($this->uri->uri_string(), 'node/add') !== false){
      $url = 'node_add';
      header("HTTP/1.1 301 Moved Permanently");
      header('Location: /' . $url);
    } else {
      $this->load->database();
      $query = $this->db->query('SELECT * FROM url_alias WHERE dst="'.str_replace('?q=','',$this->uri->uri_string()).'" LIMIT 1');	
      
      if($query->num_rows()!=0){
        foreach ($query->result() as $row)
        {
          $this->db->close();
          
          //redireccion taxonomias
          if(strpos($row->src, 'taxonomy/term') !== false){
            $url = redireccion_taxonomy($row->src);
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: /' . $url);
          } else {
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: /' . $row->src);
          }
        }
      } else {
        $this->db->close();
        show_404();
      }
    }
	}
	
	function full_web(){
		$this->load->model('servicios');
		$this->servicios->set_cookie();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */