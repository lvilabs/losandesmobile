<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Desarrollo extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->helper('form') ;
  }
  
  public function limpiar_cache() {
    echo form_open('desarrollo/limpiar_cache');
    echo form_label('Path:', 'name');
    $data= array(
      'name' => 'path',
    );
    echo form_input($data);
    $data= array(
      'name' => 'submit',
      'value' => 'Enviar',
    );
    echo form_submit($data);
    if(isset($_POST['path'])) {
      $uri_string = $_POST['path'];
      $CI =& get_instance();
      $path = $CI->config->item('cache_path');
      $path = rtrim($path, DIRECTORY_SEPARATOR);
      $cache_path = ($path == '') ? APPPATH.'cache/' : $path;
      $uri =  $CI->config->item('base_url').
            $CI->config->item('index_page').
            $uri_string;
            $md5 = md5($uri);
      $archivo = $_SERVER['DOCUMENT_ROOT'].'/application/cache/'.$md5;     
      if(file_exists($_SERVER['DOCUMENT_ROOT'].'/application/cache/'.$md5)) {
        echo '<br>Existe '.$md5;
        if(unlink($archivo)) {
          echo '<br>Eliminado';
        }
      } else {
        echo '<br>No existe '.$md5;
      }
    }
  }
}