<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

    function __construct() {
      parent::__construct();
      $this->load->helper('url');
      $this->load->helper('Functions_lvi');
      $this->load->library('session');
      $this->load->model('avisos');
      $this->load->model('admin');
      $this->data['token'] = $this->session->userdata('token');
      $this->data['session'] = array('token'=>$this->session->userdata('token'));
      $this->data['canonical'] = obtener_url_web($this->uri->uri_string()); //URL Canonical
      $this->config->load('adslot_config', TRUE);
    }
    
    /**
     * Apachesolr Search for this controller.
     *
     */
    public function result($param1 = NULL, $param2 = NULL, $param3 = NULL)
    {
      session_start();
      $this->output->cache(5); // 5 min
      
      if($param1==NULL && $param2==NULL & $param3==NULL) {
        $param = $_GET['filters'];
        if(!empty($param)) {
          $param2 = 'filters='.$param;
        }
        $param = $_GET['page'];
        if(!empty($param)) {
          $param3 = 'page='.$param;
        }
      }
      
      //Subfiltro
      $tids = '';
      if(strpos($param1,'filters=')!==false)
        $tids = $param1;
      if(strpos($param2,'filters=')!==false)
        $tids = $param2;
      $data['tids'] = $tids;
      $array_tids = obtener_tids_url($tids);
      $data['array_tids'] = $array_tids;
      
      //Filtro rubros
      $data['rubros']=$this->avisos->get_lista_rubros();
      
      //Filtros aplicados
      $parametros = array($param1, $param2, $param3);
      if(count($array_tids) > 0){
        foreach($array_tids as $tid){
          if($tid != ''){
            if(is_numeric($tid)){
              if(!in_array($tid, array(6323,6330,6106,6000,6017,6260))){
                $term_filter = $this->avisos->get_taxonomy($tid);
                $url = quitar_filtro_url($tid, $parametros, '/search/result/');
                $data['filtros'][] = array('tid' => $term_filter['tid'], 'nombre' => $term_filter['name'], 'url' => $url);
              }
            } else {
              $url = quitar_filtro_url($tid, $parametros, '/search/result/');
              $data['filtros'][] = array('tid' => $tid, 'nombre' => $tid, 'url' => $url);
            }
          }
        }
      }
      
      $filtros = '';
      if(strpos($tids, 'filters=')===0) {
        $filtros = substr($tids, 8);
      }
      $pattern = '/([^\s]*):("([^"]*)"|\[(.*)\]|([^\s]*))/';
      preg_match_all($pattern, urldecode($filtros), $matches); // Separamos por espacios que no esten entre comillas.
      $filters_array = $matches[0];
      
      foreach($filters_array as $filtro) {
        $data_filtro = explode(':', $filtro);
        if(strpos($data_filtro[0], 'is_uid') !== FALSE) {
          //$this->load->model('admin');
          $usuario = $this->admin->post_datos('datos_usuario', 'uid='.$data_filtro[1]);
          if(!empty($usuario->uid) && is_numeric($usuario->uid)) {
            $nombre = 'Vendedor';
            if(!empty($usuario->roles->{4})) {
              $nombre = $usuario->profile_nombre.' '.$usuario->profile_apellido;
            } else {
              if(!empty($usuario->profile_nombre_comercial)) {
                $nombre = $usuario->profile_nombre_comercial;
              }
            }
            for($i=0; $i<count($data['filtros']); $i++) {
              if($data['filtros'][$i]['nombre'] == 'Vendedor') {
                $data['filtros'][$i]['nombre'] = $nombre;
              }
            }
          }
        }
        
        // session fechas alojamientos
        unset($_SESSION['search-checkin']);
        unset($_SESSION['search-checkout']);
        if(strpos( $filtro, 'im_reserved:') !== FALSE) {
          $fechas = explode('im_reserved:[', $filtro);
          $fechas = explode('] im_available:', $fechas[1]);
          $fechas = explode(' TO ', $fechas[0]);
          $fecha_checkin = substr($fechas[0], 0, 4).'-'.substr($fechas[0], 4, 2).'-'.substr($fechas[0], 6, 2);
          $fecha_checkout = substr($fechas[1], 0, 4).'-'.substr($fechas[1], 4, 2).'-'.substr($fechas[1], 6, 2);
          $_SESSION['search-checkin'] = $fecha_checkin;
          $_SESSION['search-checkout'] = $fecha_checkout;
        }
        
      }
      
      //Title Head
      $texto_filtros = '';
      if(isset($data['filtros'])){
        foreach($data['filtros'] as $filtros){
          if($texto_filtros == '')
            $texto_filtros .= $filtros['nombre'];
          else
            $texto_filtros .= ', '.$filtros['nombre'];
        }
      }
      
      //Avisos
      $key = '';
      $filters = '';
      $page = '';
      if(strpos($param1,'filters=')===false){
        if($param1 != null) {
          if(strpos($param1,'page=')===false)
            $key = $param1;
          else
            $page = $param1;
        }
        if($param2 != null){
          if(strpos($param2,'filters=')===false)
            $page = $param2;
          else
            $filters = $param2;
        }
        if($param3 != null){
          if(strpos($param3,'filters=')===false)
            $page = $param3;
          else
            $filters = $param3;
        }
      } else {
        $filters = $param1;
        if($param2 != null){
          if(strpos($param2,'page=')===false)
            $key = $param2;
          else
            $page = $param2;
        }
        if($param3 != null){
          if(strpos($param3,'page=')===false)
            $key = $param3;
          else
            $page = $param3;
        }
      }
      $data['key']=$key;
      if(!empty($key)) {
        //Caso especial clasifotos. Si la busqueda es un numero de mas de 5 digitos redirige al aviso.
        $nid = trim($key);
        if(is_numeric($nid) && strlen($nid)>5) {
          $result = $this->avisos->get_alias($nid);
          if(!empty($result['alias'])) {
            redirect($result['alias']);
            return;
          }
        }
      }
      
      if($page != '') {
        $page = explode('page=', $page);
        $page[1] =  $page[1] - 1;
        $page = implode('page=', $page);
      }
      
      $key = array('key' => $key, 'filters' => $filters, 'page' => $page);
      $data['listado']=$this->avisos->get_listado_busqueda_filtros($key);
      if(isset($data['listado']->cxense->filtros)) {
        foreach($data['listado']->cxense->filtros as $filtros)
          $data['cxense_events'] .= $filtros; 
      }
      $data['head_title']='Resultado de '.$data['listado']->titulo;
      $data['head_description'] = 'Resultados de búsqueda de avisos de '.$data['listado']->titulo.' en Clasificados La Voz. Se publica de todo, se vende todo.';
      
      if(strpos($filters, 'tid:6323') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6323;
      } elseif(strpos($filters, 'tid:6330') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6330;
      } elseif(strpos($filters, 'tid:6106') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6106;
      } elseif(strpos($filters, 'tid:6000') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6000;
      } elseif(strpos($filters, 'tid:6017') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6017;
      }
      
      $data['usuario'] = $this->session->userdata('logged_in');
      $contacto = array();
      $contacto['contacto_consulta']  = isset($_SESSION['contactar_vendedor_form_consulta'])?$_SESSION['contactar_vendedor_form_consulta']:'';
      if(empty($contacto['contacto_consulta']))
        $contacto['contacto_consulta'] = 'Estoy interesado en este aviso que vi en Clasificados La Voz y quisiera que me contacten. Gracias.';
      if(!empty($data['usuario']) && $data['usuario']->uid) {
        $data_user = $this->admin->get_usuario_perfil($data['usuario']->uid);
        if(empty($contacto['contacto_nombre']))
          $contacto['contacto_nombre'] = $data_user['nombre'].' '.$data_user['apellido'];
        if(empty($contacto['contacto_telefono']))
          $contacto['contacto_telefono'] = $data_user['telefono'];
        if(empty($contacto['contacto_mail']))
          $contacto['contacto_mail'] = $data_user['username'];
      }
      $data['contacto'] = $contacto;
      
      $data['adslots'] = $this->config->item('adslot_resultado','adslot_config');
      $ads_keys = ads_get_keys_category(texto_a_class($data['listado']->cxense->gclCategories), $key['key']);
      $data['adslots']['target'] = $ads_keys;      
      
      if(!empty($data['es_buscador_alojamiento'])) {
        $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
        $this->load->view($this->config->item('carpeta_sitio').'/filtro_alojamientos',$data);
        $this->load->view($this->config->item('carpeta_sitio').'/listado_alojamientos',$data);
      } else {
        $data['head_tipo']='buscador';
        $data['tipo'] = 'buscador';
        $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
        $this->load->view($this->config->item('carpeta_sitio').'/listado',$data);
      }
      $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
    }
    
    public function infinity_result($param1 = NULL, $param2 = NULL, $param3 = NULL)
    {
      session_start();
      
      $this->output->cache(5); // 5 min
      
      if($param1==NULL && $param2==NULL & $param3==NULL) {
        $param = $_GET['filters'];
        if(!empty($param)) {
          $param2 = 'filters='.$param;
        }
        $param = $_GET['page'];
        if(!empty($param)) {
          $param3 = 'page='.$param;
        }
      }
      
      //Subfiltro
      $tids = '';
      if(strpos($param1,'filters=')!==false)
        $tids = $param1;
      if(strpos($param2,'filters=')!==false)
        $tids = $param2;
      $data['tids'] = $tids;
      $array_tids = obtener_tids_url($tids);
      $data['array_tids'] = $array_tids;
      
      //Filtro rubros
      //$data['rubros']=$this->avisos->get_lista_rubros();
      
      //Filtros aplicados
      $parametros = array($param1, $param2, $param3);
      if(count($array_tids) > 0){
        foreach($array_tids as $tid){
          if($tid != ''){
            if(is_numeric($tid)){
              if(!in_array($tid, array(6323,6330,6106,6000,6017,6260))){
                $term_filter = $this->avisos->get_taxonomy($tid);
                $url = quitar_filtro_url($tid, $parametros, '/search/result/');
                $data['filtros'][] = array('tid' => $term_filter['tid'], 'nombre' => $term_filter['name'], 'url' => $url);
              }
            } else {
              $url = quitar_filtro_url($tid, $parametros, '/search/result/');
              $data['filtros'][] = array('tid' => $tid, 'nombre' => $tid, 'url' => $url);
            }
          }
        }
      }
      
      $filtros = '';
      if(strpos($tids, 'filters=')===0) {
        $filtros = substr($tids, 8);
      }
      $pattern = '/([^\s]*):("([^"]*)"|\[(.*)\]|([^\s]*))/';
      preg_match_all($pattern, urldecode($filtros), $matches); // Separamos por espacios que no esten entre comillas.
      $filters_array = $matches[0];
      
      foreach($filters_array as $filtro) {
        $data_filtro = explode(':', $filtro);
        if(strpos($data_filtro[0], 'is_uid') !== FALSE) {
          //$this->load->model('admin');
          $usuario = $this->admin->post_datos('datos_usuario', 'uid='.$data_filtro[1]);
          if(!empty($usuario->uid) && is_numeric($usuario->uid)) {
            $nombre = 'Vendedor';
            if(!empty($usuario->roles->{4})) {
              $nombre = $usuario->profile_nombre.' '.$usuario->profile_apellido;
            } else {
              if(!empty($usuario->profile_nombre_comercial)) {
                $nombre = $usuario->profile_nombre_comercial;
              }
            }
            for($i=0; $i<count($data['filtros']); $i++) {
              if($data['filtros'][$i]['nombre'] == 'Vendedor') {
                $data['filtros'][$i]['nombre'] = $nombre;
              }
            }
          }
        }
        
        // session fechas alojamientos
        unset($_SESSION['search-checkin']);
        unset($_SESSION['search-checkout']);
        if(strpos( $filtro, 'im_reserved:') !== FALSE) {
          $fechas = explode('im_reserved:[', $filtro);
          $fechas = explode('] im_available:', $fechas[1]);
          $fechas = explode(' TO ', $fechas[0]);
          $fecha_checkin = substr($fechas[0], 0, 4).'-'.substr($fechas[0], 4, 2).'-'.substr($fechas[0], 6, 2);
          $fecha_checkout = substr($fechas[1], 0, 4).'-'.substr($fechas[1], 4, 2).'-'.substr($fechas[1], 6, 2);
          $_SESSION['search-checkin'] = $fecha_checkin;
          $_SESSION['search-checkout'] = $fecha_checkout;
        }
        
      }
      
      //Title Head
      $texto_filtros = '';
      if(isset($data['filtros'])){
        foreach($data['filtros'] as $filtros){
          if($texto_filtros == '')
            $texto_filtros .= $filtros['nombre'];
          else
            $texto_filtros .= ', '.$filtros['nombre'];
        }
      }
      
      //Avisos
      $key = '';
      $filters = '';
      $page = '';
      if(strpos($param1,'filters=')===false){
        if($param1 != null) {
          if(strpos($param1,'page=')===false)
            $key = $param1;
          else
            $page = $param1;
        }
        if($param2 != null){
          if(strpos($param2,'filters=')===false)
            $page = $param2;
          else
            $filters = $param2;
        }
        if($param3 != null){
          if(strpos($param3,'filters=')===false)
            $page = $param3;
          else
            $filters = $param3;
        }
      } else {
        $filters = $param1;
        if($param2 != null){
          if(strpos($param2,'page=')===false)
            $key = $param2;
          else
            $page = $param2;
        }
        if($param3 != null){
          if(strpos($param3,'page=')===false)
            $key = $param3;
          else
            $page = $param3;
        }
      }
      $data['key']=$key;
      if(!empty($key)) {
        //Caso especial clasifotos. Si la busqueda es un numero de mas de 5 digitos redirige al aviso.
        $nid = trim($key);
        if(is_numeric($nid) && strlen($nid)>5) {
          $result = $this->avisos->get_alias($nid);
          if(!empty($result['alias'])) {
            redirect($result['alias']);
            return;
          }
        }
      }
      
      $key = array('key' => $key, 'filters' => $filters, 'page' => $page);
      
      $data['listado']=$this->avisos->get_listado_busqueda_filtros($key);
      
      //$data['head_title']='Resultado de '.$data['listado']->titulo;
      //$data['head_description'] = 'Resultados de búsqueda de avisos de '.$data['listado']->titulo.' en Clasificados La Voz. Se publica de todo, se vende todo.';
      
      if(strpos($filters, 'tid:6323') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6323;
      } elseif(strpos($filters, 'tid:6330') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6330;
      } elseif(strpos($filters, 'tid:6106') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6106;
      } elseif(strpos($filters, 'tid:6000') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6000;
      } elseif(strpos($filters, 'tid:6017') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6017;
      }
      
      $data['usuario'] = $this->session->userdata('logged_in');
      $contacto = array();
      $contacto['contacto_consulta']  = isset($_SESSION['contactar_vendedor_form_consulta'])?$_SESSION['contactar_vendedor_form_consulta']:'';
      if(empty($contacto['contacto_consulta']))
        $contacto['contacto_consulta'] = 'Estoy interesado en este aviso que vi en Clasificados La Voz y quisiera que me contacten. Gracias.';
      if(!empty($data['usuario']) && $data['usuario']->uid) {
        $data_user = $this->admin->get_usuario_perfil($data['usuario']->uid);
        if(empty($contacto['contacto_nombre']))
          $contacto['contacto_nombre'] = $data_user['nombre'].' '.$data_user['apellido'];
        if(empty($contacto['contacto_telefono']))
          $contacto['contacto_telefono'] = $data_user['telefono'];
        if(empty($contacto['contacto_mail']))
          $contacto['contacto_mail'] = $data_user['username'];
      }
      $data['contacto'] = $contacto;
      
      if(!empty($data['es_buscador_alojamiento'])) {
        $this->load->view($this->config->item('carpeta_sitio').'/listado_alojamientos',$data);
      } else {
        $this->load->view($this->config->item('carpeta_sitio').'/listado_infinito',$data);
      }
    }
  
    /**
     * Map Search for this controller.
     *
     */
    public function map($param1 = NULL, $param2 = NULL, $param3 = NULL, $param4 = NULL, $param5 = NULL)
    {
      $this->output->cache(15); // 15 min
      
      //Filtro rubros
      $data['rubros']=$this->avisos->get_lista_rubros();
      
      //Subfiltro
      $tids = '';
      if(strpos($param1,'filters=')!==false)
        $tids = $param1;
      if(strpos($param2,'filters=')!==false)
        $tids = $param2;
      $data['tids'] = $tids;
      $array_tids = obtener_tids_url($tids);
          
      //Filtros aplicados
      $parametros = array($param1, $param2, $param3, $param4, $param5);
      if(count($array_tids) > 0){
        foreach($array_tids as $tid){
          if($tid != ''){
            if(is_numeric($tid)){
              if(!in_array($tid, array(6323,6330,6106,6000,6017,6260))){
                $term_filter = $this->avisos->get_taxonomy($tid);
                $url = quitar_filtro_url($tid, $parametros, '/search/map/');
                $data['filtros'][] = array('tid' => $term_filter['tid'], 'nombre' => $term_filter['name'], 'url' => $url);
              }
            } else {
              $url = quitar_filtro_url($tid, $parametros, '/search/map/');
              $data['filtros'][] = array('tid' => $tid, 'nombre' => $tid, 'url' => $url);
            }
          }
        }
      }
      $filtros = '';
      if(strpos($tids, 'filters=')===0) {
        $filtros = substr($tids, 8);
      }
      $pattern = '/([^\s]*):("([^"]*)"|\[(.*)\]|([^\s]*))/';
      preg_match_all($pattern, urldecode($filtros), $matches); // Separamos por espacios que no esten entre comillas.
      $filters_array = $matches[0];
      foreach($filters_array as $filtro) {
        $data_filtro = explode(':', $filtro);
        if(strpos($data_filtro[0], 'is_uid') !== FALSE) {
          //$this->load->model('admin');
          $usuario = $this->admin->post_datos('datos_usuario', 'uid='.$data_filtro[1]);
          if(!empty($usuario->uid) && is_numeric($usuario->uid)) {
            $nombre = 'Vendedor';
            if(!empty($usuario->roles->{4})) {
              $nombre = $usuario->profile_nombre.' '.$usuario->profile_apellido;
            } else {
              if(!empty($usuario->profile_nombre_comercial)) {
                $nombre = $usuario->profile_nombre_comercial;
              }
            }
            for($i=0; $i<count($data['filtros']); $i++) {
              if($data['filtros'][$i]['nombre'] == 'Vendedor') {
                $data['filtros'][$i]['nombre'] = $nombre;
              }
            }
          }
        }
      }
      
      //Title Head
      $texto_filtros = '';
      if(isset($data['filtros'])){
        foreach($data['filtros'] as $filtros){
          if($texto_filtros == '')
            $texto_filtros .= $filtros['nombre'];
          else
            $texto_filtros .= ', '.$filtros['nombre'];
        }
      }
      $data['head_title']='Buscador Mapa '.$texto_filtros.' - Clasificados La Voz';
      
      //Avisos
      $key = ''; $filters = ''; $page = ''; $lat = '-31.39893'; $long = '-64.182129';
      foreach($parametros as $parametro){
        if(!empty($parametro)){
          if(strpos($parametro,'=')===false){
            $key = $parametro;
          }
          if(strpos($parametro,'filters=')!==false){
            $filters = $parametro;
          }
          if(strpos($parametro,'page=')!==false){
            $page = $parametro;
          }
          if(strpos($parametro,'latitud=')!==false){
            $lat = $parametro;
          }
          if(strpos($parametro,'longitud=')!==false){
            $long = $parametro;
          }
        }
      }
      $data['key']=$key;
      
      $key = array('key' => $key, 'filters' => $filters, 'page' => $page, 'latitud' => $lat, 'longitud' => $long);
      $data['listado']=$this->avisos->get_listado_busqueda_mapa($key);
      
      if(strpos($filters, 'tid:6323') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6323;
      } elseif(strpos($filters, 'tid:6330') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6330;
      } elseif(strpos($filters, 'tid:6106') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6106;
      } elseif(strpos($filters, 'tid:6000') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6000;
      } elseif(strpos($filters, 'tid:6017') !== FALSE) {
        $data['rubro_tid_seleccionado'] = 6017;
      }
      
      $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
      $this->load->view($this->config->item('carpeta_sitio').'/listado_map',$data);
      $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
    }
  
    /**
     * Obtener sugerencias.
     *
     */
    public function get_sugerencias($texto)
    {
      $sugerencias = $this->avisos->get_sugerencias($texto); 
      print($sugerencias);
    }
  
    /**
     * Guardar busqueda como sugerencias.
     *
     */
    public function set_sugerencias($texto)
    {
      $this->avisos->set_sugerencias($texto);
    }
  
    /**
    * Obtener sugerencias.
    *
    */
    public function ajax_resultado_mapa()
    {
      $post = $_POST;
      $key = array('filters' => $post['filtros'], 'latitud' => 'latitud='.$post['latitude'], 'longitud' => 'longitud='.$post['longitude']);
      $listado=json_encode($this->avisos->get_listado_busqueda_mapa($key, 1));
      print_r($listado);
    }
    
    public function check_url_sitio($path) {
      //$this->load->model('admin');
      $json = $this->admin->post_datos('uid_from_url', 'dst='.$path);
      if(!empty($json->uid) && is_numeric($json->uid)) {
        $url = 'search/result/filters=is_uid:'.$json->uid.'%20';
        header("HTTP/1.1 302 Found");
        header('Location: /' . $url);
      } else {
        show_404();
      }
    }
  
}

/* End of file search.php */
/* Location: ./application/controllers/search.php */