<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends CI_Controller {
  
  function __construct() {
    // Call the Model constructor
    parent::__construct();
    $this->data['tipo'] = 'admin';
    $this->data['head_tipo'] = 'admin';
    $data['title'] = 'Error desconocido';
    $data['head_title'] = 'Error desconocido';
  }

  function index() {
  	$data['title'] = 'Página no encontrada';
  	$data['head_title'] = 'Página no encontrada';
  	$data['html'] = 'Página no encontrada';
    $this->output->set_status_header('404');
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/error', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
}