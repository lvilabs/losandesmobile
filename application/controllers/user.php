<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
  
  function __construct() {
    parent::__construct();
    $this->load->helper('Functions_lvi');
    $this->load->library(array('session', 'form_validation'));
    $this->load->helper(array('url', 'form'));
    $this->load->model('admin');
  }
  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -  
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  public function index() {
    if($this->admin->session_valida()!=true) {
        $data['token'] = $this->token();
        $data['titulo'] = 'Login de usuario';
        if( FALSE != ($mensaje = $this->session->flashdata('mensaje'))){
          $data['mensaje'] = $mensaje;
        }
        $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
        $this->load->view($this->config->item('carpeta_sitio').'/login', $data);
        $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
    } else {
      redirect(''); 
    }
  }
  
  public function edit($uid, $mensaje = NULL, $campos_error = NULL) {
    if($this->admin->session_valida()!=true) {
      redirect('user', 'refresh'); 
    } else {
      $usuario = $this->session->userdata('logged_in');
      if($usuario->uid == $uid) {     
        $data['mensaje'] = $mensaje;
        if( FALSE != ($mensaje = $this->session->flashdata('mensaje'))){
          $data['mensaje'] = $mensaje;
        }
        $data['token'] = $this->session->userdata('token');
        $data['user_session'] = $usuario;
        $data['campos_error'] = $campos_error;
        $data['es_registro'] = 1;
        $data['options'] = $this->admin->get_options_form_users();
        $data['data_user'] = $this->admin->get_usuario_perfil($uid);
        $data['tipo_usuario'] = $data['data_user']['tipo_usuario'];
        
        $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
        $this->load->view($this->config->item('carpeta_sitio').'/register', $data);
        $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
      } else {
        redirect('', 'refresh'); 
      }
    }
  }
  
  function login() {
    //This method will have the credentials validation
    //$this->load->library('form_validation');
    $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

    if($this->form_validation->run() == FALSE) {
      //Field validation failed.  User redirected to login page
      $this->index();
    } else {
      //Go to private area
      if(!isset($_POST['redirect']))
        redirect('administrar/mis-avisos.html', 'refresh');
      else
        redirect($_POST['redirect'], 'refresh');
    }
  }
  
  function register($tipo_usuario, $mensaje = NULL, $campos_error = NULL) {
    if($this->admin->session_valida()!=true) {
      $data['mensaje'] = $mensaje;
      $data['campos_error'] = $campos_error;
      $data['token'] = $this->token();
      //$data['tipo'] = 'admin';
      $data['tipo_usuario'] = $tipo_usuario;
      $data['es_registro'] = 1;
      $data['options'] = $this->admin->get_options_form_users();
      
      $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
      $this->load->view($this->config->item('carpeta_sitio').'/register', $data);
      $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
    } else {
      redirect('', 'refresh');
    }
  }
  
  function send_register() {
    $post = $_POST;
    if(isset($post['telefono_whatsapp']) && !empty($post['telefono_whatsapp'])) {
      if(isset($post['codigo_area_whatsapp']) && !empty($post['codigo_area_whatsapp']))
        $post['telefono_whatsapp'] = '+549'.$post['codigo_area_whatsapp'].$post['telefono_whatsapp'];
      else
        $post['telefono_whatsapp'] = '+549'.$post['telefono_whatsapp'];
    }
    $status = validar_form_register($post);
    if($status['validado']){
      $mensaje = '';
      $data=$this->admin->insert_usuario($post);
      if($data->status) {
        if($post['accion'] == 'register') {
          $this->session->set_userdata('logged_in', $data);
          $this->session->set_flashdata('mensaje', $data->mensaje);
          redirect('administrar/mis-avisos.html', 'refresh');
        } else {
          $this->session->set_flashdata('mensaje', $data->mensaje);
          redirect('user/'.$post['uid'].'/edit', 'refresh');
        }
      } else {
        if($post['accion'] == 'register')
          $this->register($post['tipo_usuario'], $data->mensaje);
        else
          $this->edit($post['uid'], $data->mensaje);
      }
    } else {
      if($post['accion'] == 'register')
        $this->register($post['tipo_usuario'], $status['mensaje'], $status['campos']);
      else
        $this->edit($post['uid'], $status['mensaje'], $status['campos']);
    }
  }
  
  function password($mensaje = NULL, $campos_error = NULL) {
    if($this->admin->session_valida()!=true) {
      $data['mensaje'] = $mensaje;
      if( FALSE != ($mensaje = $this->session->flashdata('mensaje'))){
        $data['mensaje'] = $mensaje;
      }
      $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
      $this->load->view($this->config->item('carpeta_sitio').'/password', $data);
      $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
    } else {
      redirect(''); 
    }
  }
  
  function send_password() {
    $post = $_POST;
    $data=$this->admin->post_datos('user/recovery_password', array('usermail'=>$post['usermail']));
    if($data->status) {
      $this->session->set_flashdata('mensaje', $data->mensaje);
      redirect('/user');
    } else {
      $this->password($data->mensaje);
    }
  }
  
  function reset($uid, $login, $token) {
    $data=$this->admin->post_datos('user/reset_password', array('uid'=>$uid, 'login'=>$login, 'token'=>$token));
    $this->session->set_flashdata('mensaje', $data->mensaje);
    if($data->status) {
      $this->session->set_userdata('token', $token);
      $this->session->set_userdata('logged_in', $data);
      redirect('/user/'.$uid.'/edit');
    } else {
      redirect('/user/password');
    }
  }

  function check_database($password) {
    //Field validation succeeded.  Validate against database
    $username = $this->input->post('username');
    $token = $this->input->post('token');
    $password = md5($password);

    $result = $this->admin->post_datos('user/login', array('username'=>$username, 'password'=>$password, 'token'=>$token));

    //Retrieving session data and other data
    $session_token = $this->session->userdata('token');
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    if(empty($result)) {
      $this->form_validation->set_message('check_database', 'Error al ingresar intente nuevamente');
      return false;
    }
    if($session_token != $token) {
      $this->form_validation->set_message('check_database', 'Error de sesión, intente nuevamente');
      return false;
    }
    if($result->status != 1) {
      $this->form_validation->set_message('check_database', 'Usuario o contraseña no válida');
      return false;
    } else {
      $this->session->set_userdata('logged_in', $result);
      return true;
    }
    return false;
  }
  
  public function token()	{
    $token = md5(uniqid(rand(), true));
    $this->session->set_userdata('token', $token);
    return $token;
  }

  public function logout() {
    $this->session->unset_userdata('token');
    $this->session->unset_userdata('logged_in');
    $this->session->sess_destroy();
    redirect('user');
  }
  
  /*
   * Datos de la cuenta que se cargan por ajax.
   * Muestra los iconos para administracion si esta logueado.
   */
  public function datos_cuenta() {
    $respuesta = array();
    $respuesta['status'] = 1;
    if($this->admin->session_valida()==true) {
      $datos['usuario'] = $this->session->userdata('logged_in');
      $respuesta['usuario'] = $datos['usuario'];
      $respuesta['html'] = $this->load->view($this->config->item('carpeta_sitio').'/datos_cuenta', $datos, true);
    } else {
      $respuesta['html'] = $this->load->view($this->config->item('carpeta_sitio').'/datos_cuenta', false, true);
      $respuesta['usuario'] = NULL;
    }
    echo json_encode($respuesta); 
  }
  
  /*
   * Datos del menu que se cargan por ajax.
   *
   */
  public function datos_menu() {
    $respuesta = array();
    $respuesta['status'] = 1;
    if($this->admin->session_valida()==true) {
      $datos['usuario'] = $this->session->userdata('logged_in');
      $respuesta['usuario'] = $datos['usuario'];
      $respuesta['html'] = $this->load->view($this->config->item('carpeta_sitio').'/datos_menu', $datos, true);
    } else {
      $respuesta['html'] = $this->load->view($this->config->item('carpeta_sitio').'/datos_menu', false, true);
      $respuesta['usuario'] = NULL;
    }
    echo json_encode($respuesta); 
  }
  
  /*
   * Actualizar datos de favoritos via ajax.
   *
   */
  public function update_favoritos() {
    if($this->admin->session_valida()==true) {
      $datos['usuario'] = $this->session->userdata('logged_in');
      $uid = $datos['usuario']->uid;
      $result=$this->admin->post_datos('user/set_favoritos', array('uid'=>$uid,'favoritos'=>$_GET['favoritos']));
      $datos['usuario']->favoritos = json_encode($result);
      $this->session->set_userdata('logged_in', $datos['usuario']);
      echo json_encode($result); 
    }
  }
  
}

/* End of file user.php */