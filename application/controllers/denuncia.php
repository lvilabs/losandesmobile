<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Denuncia extends CI_Controller {

	/**
	 * Denuncia for this controller.
	 *
	 */
	public function denuncia_aviso()
	{
    $get = $_GET;
    
    $this->load->helper('Functions_lvi');
		$this->load->model('avisos');
    
    //Validacion de campos
    $mensaje = '';
    $status = 'ok';
    foreach($get as $kparm => $dparam){
      if($kparm == 'denuncia_motivo'){
        if($dparam == '' || $dparam == 0){
          $mensaje .= 'Seleccioná un motivo.<br>';
          $status = 'error';
        }
      }
      if($kparm == 'denuncia_descripcion'){
        if($dparam == '' || $dparam == 'Escribe tu denuncia aquí.'){
          $mensaje .= 'La denuncia es requerida.<br>';
          $status = 'error';
        }
      }
      if($kparm == 'denuncia_mail'){
        if($dparam == '' || $dparam == 'E-mail'){
          $mensaje .= 'Tu E-mail es requerido.<br>';
          $status = 'error';
        }
      }
      if($status == 'ok'){
        if($kparm == 'denuncia_aviso' && $dparam == ''){
          $mensaje .= 'Error al tratar de enviar la denunica.<br>';
          $status = 'error';
        } 
      }
      if($status == 'ok'){
        if($kparm == 'denuncia_robot' && $dparam != 0){
          $mensaje .= 'Error al tratar de enviar la denunica.<br>';
          $status = 'error';  
        }
      }       
    }
    
    if($status == 'ok'){
      $data=$this->avisos->insert_denuncia($get);
      print 'status='.$data['status'].',mensaje='.$data['mensaje'];
    } else {
      print 'status='.$status.',mensaje='.$mensaje;
    }
    
	}
}

/* End of file denuncia.php */
/* Location: ./application/controllers/denuncia.php */