<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller {

  	function __construct() {
        parent::__construct();
        //$this->load->model('xmlsitemap');        
    }

    public function index() {
        $content = '';
        if(isset($_GET['page'])) {
            $content = $this->generar_page($_GET['page']);
        } else {
            $content = $this->generar_index();	
        }
        if($content==FALSE) {
            show_error('La página que intenta acceder no está disponible en este momento', 503, 'Error en sitemap');
        }
        header("Content-Type:text/xml");
        print $content;
    }

    function generar_index() {
        $nombre_archivo = 'xmlsitemap/index.xml';
        $fp = fopen($nombre_archivo, 'r');
        if(file_exists($nombre_archivo)) {
            if(time()-filemtime($nombre_archivo)<64800) {
                // Si el archivo tiene menos de 18hs no lo genero.
                return file_get_contents($nombre_archivo);
            }
        }
        $fp = fopen($nombre_archivo, 'w');
        $handle = fopen($this->config->item('sitio_origen')."/sitemap.xml", "r");
        if($handle) {
            while(($line = fgets($handle)) !== false) {
                // process the line read.
                $line = str_replace($this->config->item('sitio_origen'), $this->config->item('sitio_host'), $line);
                if(strpos($line, 'sitemap.xsl')!==FALSE) {
                    continue;
                }
                fwrite($fp, $line);
            }
            fclose($handle);
            fclose($fp);
            return file_get_contents($nombre_archivo);
        }
        return FALSE;
    }

    function generar_page($page) {
        if(!is_numeric($page)) {
            return FALSE;
        }
        $nombre_archivo = 'xmlsitemap/'.$page.'.xml';
        $fp = fopen($nombre_archivo, 'r');
        if(file_exists($nombre_archivo)) {
            if(time()-filemtime($nombre_archivo)<64800) {
                // Si el archivo tiene menos de 18hs no lo genero.
                return file_get_contents($nombre_archivo);
            }
        }
        $fp = fopen($nombre_archivo, 'w');
        $handle = fopen($this->config->item('sitio_origen')."/sitemap.xml?page=".$page, "r");
        if($handle) {
            while(($line = fgets($handle)) !== false) {
                // process the line read.
                $line = str_replace($this->config->item('sitio_origen'), $this->config->item('sitio_host'), $line);
                if(strpos($line, 'sitemap.xsl')!==FALSE) {
                    continue;
                }
                if(strpos($line, '/category/')!==FALSE) {
                    // Quito los links de taxonomias, en mobile existen pero redireccionana otra url.
                    continue;
                }
                fwrite($fp, $line);
            }
            fclose($handle);
            fclose($fp);
            return file_get_contents($nombre_archivo);
        }
        return FALSE;
    }
}

/* End of file sitemap.php */
/* Location: ./application/controllers/sitemap.php */