<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class error_404 extends CI_Controller {

  public function __construct() {
    parent::__construct();
  }

  public function index() {
    $this->output->set_status_header('404');
    $data['errorMessage'] = ' Ooops! Page not found';
    //$this->load->view($this->config->item('carpeta_sitio').'/error_404', $data);//loading view 
    //$this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/error_404', $data);
    //$this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
}
