<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->output->cache(5); // 5 min
		$this->config->load('adslot_config', TRUE);
    $this->load->helper('Functions_lvi');
    $this->load->library('session');
		// $this->load->model('avisos');
    $this->load->model('notas');
    //URL Canonical
    $data['canonical']='';
    $data['tipo'] = 'home';
    $data['head_tipo'] = 'home';
    $data['rubro_tid_seleccionado'] = 0;
    $data['adslots'] = $this->config->item('adslot_home','adslot_config');
    $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
    $data['notas'] = $this->notas->get_destacados_nota('all', 3);
    if($this->config->item('carpeta_sitio') == 'clvi') {
      // $data['destacados']=$this->avisos->get_destacados();
      $this->load->view($this->config->item('carpeta_sitio').'/listado_destacados',$data);
      //$this->load->view($this->config->item('carpeta_sitio').'/nota_destacados',$data);
    }
		$this->load->view($this->config->item('carpeta_sitio').'/footer',$data);

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */