<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Node extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function id($id)
	{
    session_start();
    $this->load->library('session');
    $this->load->helper('url');
		$this->load->helper('Functions_lvi');
		$this->load->model('avisos');
    $this->load->model('admin');
    $this->config->load('adslot_config', TRUE);
    
    $this->data['token'] = $this->session->userdata('token');
    $this->data['session'] = array('token'=>$this->session->userdata('token'));
    $data['usuario'] = $this->session->userdata('logged_in');
    
    $contacto = array();
    $contacto['contacto_consulta']  = isset($_SESSION['contactar_vendedor_form_consulta'])?$_SESSION['contactar_vendedor_form_consulta']:'';
    if(empty($contacto['contacto_consulta']))
      $contacto['contacto_consulta'] = 'Estoy interesado en este aviso que vi en Clasificados Los Andes y quisiera que me contacten. Gracias.';
    if(!empty($data['usuario']) && $data['usuario']->uid) {
      $data_user = $this->admin->get_usuario_perfil($data['usuario']->uid);
      if(empty($contacto['contacto_nombre']))
        $contacto['contacto_nombre'] = $data_user['nombre'].' '.$data_user['apellido'];
      if(empty($contacto['contacto_telefono']))
        $contacto['contacto_telefono'] = $data_user['telefono'];
      if(empty($contacto['contacto_mail']))
        $contacto['contacto_mail'] = $data_user['username'];
    }
    $data['contacto'] = $contacto;
    
    $data['mensaje']['status']= $this->session->userdata('status');
    $data['mensaje']['mensaje']= $this->session->userdata('mensaje');
    $this->session->unset_userdata('status');
    $this->session->unset_userdata('mensaje');
    
		$data['rubros']=$this->avisos->get_lista_rubros();
    //URL Canonical
    $data['canonical']=obtener_url_web($this->uri->uri_string());

    $data['node']=$this->avisos->get_node($id);


        if(isset($data['node']['datos_venta']) && !empty($data['node']['datos_venta']))
      $data['tipo']='tienda';
    if(isset($data['node']['datos_alojamiento']) && !empty($data['node']['datos_alojamiento']))
      $data['calendario_front']=true;
    //Title Head
    $data['head_title']=$data['node']['title'];
    $data['head_description']='Ficha de aviso, '.$data['node']['title'].' en Clasificados Los Andes';
    $data['head_tipo']='aviso';
    
    $data['rubro_tid_seleccionado'] = 0;
    foreach($data['node']['taxonomy'] as $tid => $term) {
      if($tid == 8688) {
        $data['rubro_tid_seleccionado'] = 8688;
      } elseif($tid == 6323) {
        $data['rubro_tid_seleccionado'] = 6323;
      } elseif($tid == 6330) {
        $data['rubro_tid_seleccionado'] = 6330;
      } elseif($tid == 6106) {
        $data['rubro_tid_seleccionado'] = 6106;
      } elseif($tid == 6000) {
        $data['rubro_tid_seleccionado'] = 6000;
      } elseif($tid == 6017) {
        $data['rubro_tid_seleccionado'] = 6017;
      }
    }
    
    $data['adslots'] = $this->config->item('adslot_ficha','adslot_config');
    $ads_keys = ads_get_keys_category(texto_a_class($data['node']['cxense']['gcl-categories']));
    $data['adslots']['target'] = $ads_keys;  
    
    $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
		if(empty($data['calendario_front'])) {
      $this->output->cache(30); // 30 min
      $this->load->view($this->config->item('carpeta_sitio').'/content',$data);
    }
		$this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
	}
  
  public function alias($alias) {
    $this->output->cache(30); // 30 min
    $this->load->helper('url');
    $this->load->helper('Functions_lvi');
    $this->load->model('avisos');
    $this->load->database();
    $query = $this->db->query('SELECT * FROM url_alias WHERE dst="'.str_replace('?q=','',urldecode($this->uri->uri_string())).'" LIMIT 1');
    $source = FALSE;
    $row = $query->row();
    if(!empty($row))
      $source = explode('/', $row->src);
    if(empty($source) || $source[0]!='node' || !is_numeric($source[1])) {
      show_404();
      return;
    }
    $id = $source[1];
    $data['rubros']=$this->avisos->get_lista_rubros();
    //URL Canonical
    $data['canonical']=obtener_url_web($this->uri->uri_string());
    $data['node']=$this->avisos->get_node($id);
    if(isset($data['node']['datos_venta']) && !empty($data['node']['datos_venta']))
      $data['tipo']='tienda';
    //Title Head
    $data['head_title']=$data['node']['title'].' - Clasificados Los Andes';
    $data['head_tipo']='aviso';
    $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
    $this->load->view($this->config->item('carpeta_sitio').'/content',$data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  /*
   * Registra visita, vista de telefono, whatsapp y SMS a un aviso.
   */
  public function contador_vista($tipo, $nid) {
    $this->load->model('avisos');
    $this->load->library('session');
    $post = $_POST;
    $contacto_uid = 0;
    $contacto_mail = '';
    $contacto_nombre = '';
    $contacto_telefono = '';
    $this->data['token'] = $this->session->userdata('token');
    $data['usuario'] = $this->session->userdata('logged_in');
    if(isset($data['usuario']->uid)) {
      $contacto_uid = $data['usuario']->uid;
    } elseif(isset($post['contacto'])) {
      $post['contacto'] = json_decode($post['contacto']);
      foreach($post['contacto'] as $datos_contacto) {
        if($datos_contacto[0] == 'contactar_vendedor_nombre')
          $contacto_nombre = $datos_contacto[1];
        if($datos_contacto[0] == 'contactar_vendedor_telefono')
          $contacto_telefono = $datos_contacto[1];
        if($datos_contacto[0] == 'contactar_vendedor_mail')
          $contacto_mail = $datos_contacto[1];
      }
    }
    $data_contacto = array(
                            'uid'       => $contacto_uid,
                            'nombre'    => $contacto_nombre,
                            'mail'      => $contacto_mail,
                            'telefono'  => $contacto_telefono,
                            'tipo'      => $tipo
                          );
    $respuesta = $this->avisos->contador_vistas($tipo, $nid, $data_contacto);
    echo $respuesta;
  }
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */