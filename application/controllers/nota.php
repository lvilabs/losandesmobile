<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nota extends CI_Controller {

	function __construct() {
    parent::__construct();
    $this->load->helper('url');
    $this->load->helper('Functions_lvi');
    $this->load->library('session');
    $this->load->model('notas');
    $this->config->load('adslot_config', TRUE);
  }
  
	public function id($id)
	{
		$this->output->cache(30); // 30 min
    //URL Canonical
    $data['canonical']=obtener_url_web($this->uri->uri_string());
		$data['nota']=$this->notas->get_nota($id);
    
    $data['adslots'] = $this->config->item('adslot_nota','adslot_config');
    $data['adslots']['target'][] = array('key' => 'clasificados_tipo', 'value' => 'nota');  
    
    //Title Head
    $data['head_title']=$data['nota']['title'].' - Clasificados La Voz';
    $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
		$this->load->view($this->config->item('carpeta_sitio').'/nota',$data);
		$this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
	}
  
  public function listado()
	{
		$get = $_GET;
    $page = 0;
    if(isset($get['page']))
      $page = $get['page'];
    //URL Canonical
    $data['canonical']=obtener_url_web($this->uri->uri_string());
		$data['listado']=$this->notas->get_listado_nota($page);
    //Title Head
    $data['head_title'] = 'Noticias - Clasificados La Voz';
    $this->load->view($this->config->item('carpeta_sitio').'/header',$data);
		$this->load->view($this->config->item('carpeta_sitio').'/listado_notas',$data);
		$this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
	}
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */