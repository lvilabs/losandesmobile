<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Compra extends CI_Controller {
  
  var $data = array();
  
  function __construct() {
    parent::__construct();
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->helper('Functions_lvi');
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('avisos');
    $this->load->model('pages');
    $this->load->model('admin');
    $this->data['rubros'] = $this->avisos->get_lista_rubros();
    $this->data['tipo'] = 'admin';
    $this->data['head_tipo'] = 'admin';
    $this->data['token'] = $this->session->userdata('token');
    $this->data['session'] = array('token'=>$this->session->userdata('token'));
    $this->data['canonical'] = obtener_url_web($this->uri->uri_string()); //URL Canonical
  }
  
  function completar_datos($compra_id) {
    $this->check_session();
    $data = $this->data;
    $submit = $this->input->post('submit');
    if(!empty($submit)) {
      $this->completar_datos_submit();
      return;
    }
    $respuesta = $this->admin->post_datos('admin/datos_usuario', array('compra_id'=>$compra_id, 'token'=>$data['token']));
    //$this->check_status($respuesta->status, $respuesta->mensaje);
    if($respuesta->status!=1) {
      // Mostrar mensaje de error.
      $this->session->set_flashdata('mensaje', 'Ocurrió un errór, intente nuevamente. '.$respuesta->mensaje);
      redirect('compra/'.$compra_id.'/completar_datos');
    }
    if(FALSE != ($mensaje = $this->session->flashdata('mensaje'))) {
      $data['mensaje'] = $mensaje;
    }
    if(FALSE != ($errores = $this->session->flashdata('errores'))) {
      $data['errores'] = $errores;
    }
    $data['respuesta'] = $respuesta;
    $data['tipo'] = 'admin';
    $data['head_title'] = 'Completar datos del usuario - Clasificados La Voz';
    $data['head_tipo'] = 'admin';
    $data['titulo'] = 'Completar datos del usuario';
    $data['compra_id'] = $compra_id;
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/completar_datos', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  function completar_datos_submit() {
    
    $this->check_session();
    $data = $this->data;
    $datos = array('token' => $data['token']);
    $datos['compra_id'] = $this->input->post('compra_id');
    $datos['nombre'] = $this->input->post('profile_nombre');
    $datos['apellido'] = $this->input->post('profile_apellido');
    $datos['calle'] = $this->input->post('profile_calle');
    $datos['altura'] = $this->input->post('profile_altura');
    $datos['piso'] = $this->input->post('profile_piso');
    $datos['provincia'] = $this->input->post('profile_provincia');
    $datos['ciudad'] = $this->input->post('profile_ciudad');
    $datos['cp'] = $this->input->post('profile_cp');
    $datos['condicion_iva'] = $this->input->post('profile_condicion_iva');
    $datos['cuit'] = $this->input->post('profile_cuit');
    $datos['dni'] = $this->input->post('profile_dni');
    
    $data['nid'] = $nid;
    $data['errores'] = $errores;
    $data['mensaje'] = $mensaje;
    $data['form'] = $form;
    $data['respuesta'] = $this->admin->post_datos('admin/datos_usuario/submit', $datos);
    $this->check_status($data['respuesta']->status, $data['respuesta']->mensaje);
    $this->session->set_flashdata('mensaje', $data['respuesta']->mensaje);
    $this->session->set_flashdata('errores', (array)$data['respuesta']->errores);
    if(empty($data['respuesta']->errores))
        redirect('compra/'.$datos['compra_id'].'/confirmar');
    else
        redirect('compra/'.$datos['compra_id'].'/completar-datos');
    return;
  }

  public function confirmar($compra_id) {
    $this->check_session();
    $data = $this->data;
    $respuesta = $this->admin->post_datos('admin/compra', array('compra_id'=>$compra_id, 'token'=>$data['token']));
    $this->check_status($respuesta->status, $respuesta->mensaje);
    if($respuesta->status!==0 && $respuesta->status!=1) {
      $respuesta->mensaje = 'Error al obtener datos de la compra';
      $respuesta->compra->id_cd = $compra_id;
    }
    $id_cd = $respuesta->compra->id_cd;
    $user = $respuesta->usuario;
    if(empty($user->profile_nombre) || empty($user->profile_apellido)) {
      // Si no tiene nombre y apellido redirigimos a pag. intermedia para cargar esos datos.
      redirect('compra/'.$id_cd.'/completar-datos');
      return;
    }
    if($respuesta->compra->total>=5000) {
      $iva = $respuesta->iva_afip;
      if(empty($user->profile_calle) || empty($user->profile_altura) || empty($user->profile_provincia) || 
        (empty($user->profile_cuit) && empty($user->profile_dni)) ||
        empty($user->profile_cp) || empty($user->profile_condicion_iva) || 
        ($user->profile_condicion_iva!='Consumidor Final' && !validar_cuit($user->profile_cuit)) ||
        (!empty($user->profile_cuit) && !empty($iva) && $user->profile_condicion_iva!=$iva) || 
        ($user->profile_condicion_iva=='Consumidor Final' && strlen($user->profile_dni)<7)
        ) {
        // Si falta algun datos de facturacion redirigimos a pag. intermedia para cargar esos datos.
        redirect('compra/'.$id_cd.'/completar-datos');
        return;
      }
    }
    $data['respuesta'] = $respuesta;
    $data['tipo'] = 'admin';
    $data['head_title'] = 'Pago del destaque, confirmar - Clasificados La Voz';
    $data['head_tipo'] = 'admin';
    $data['titulo'] = 'Pago del destaque';
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/compra_confirmar', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }

  function redirigir($compra_id, $plataforma) {
    $this->check_session();
    $data = $this->data;
    $html = '';
    switch($plataforma) {
      case 'dineromail':
      case 'mercadopago':
        $respuesta = $this->admin->post_datos('admin/compra/plataforma', array('compra_id'=>$compra_id, 'plataforma' => $plataforma, 'token'=>$data['token']));
        $this->check_status($respuesta->status, $respuesta->mensaje);
        if($respuesta->status!=1) {
          // Mostrar mensaje de error.
          $html = 'Error. Status: '.$respuesta->status;
        } else {
          $html = $respuesta->html;    
        }
        break;
      default:
        $html = 'Error';
    }
    $data['tipo'] = 'admin';
    $data['head_title'] = 'Pago del destaque - Clasificados La Voz';
    $data['head_tipo'] = 'admin';
    $data['titulo'] = 'Pago del destaque';
    $data['html'] = $html;
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/content_html', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }

  function estado($compra_id, $estado, $plataforma) {
    switch($estado) {
      case 'ok':
        $titulo = 'Pago en Proceso';
        $texto = 'El pago está siendo procesado, pronto estará acreditado.<br>Gracias por su compra!';
        break;
      case 'pending':
        $titulo = 'Pago en pendiente';
        $texto = 'El pago se encuentra pendiente.';
        break;
      case 'cancel':
        $titulo = 'Pago Cancelado';
        $texto = 'El pago no se ha concretado.';
        break;
      default:
        $titulo = 'Error';
        $texto = 'Error';
        break;
    }
    $data['texto'] = $texto;
    $data['estado'] = $estado;
    $data['tipo'] = 'admin';
    $data['head_title'] = $titulo.' - Clasificados La Voz';
    $data['head_tipo'] = 'admin';
    $data['titulo'] = $titulo;
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/compra_estado', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }

  function check_session() {
    if($this->admin->session_valida()!=true) {
      redirect('user'); 
    }
  }

  function check_status($status, $mensaje) {
    if($status===0) {
      $this->session->set_flashdata('mensaje', $mensaje);
      $this->session->sess_destroy();// Borramos datos de session del login.
      redirect('user');
    }
  }
}