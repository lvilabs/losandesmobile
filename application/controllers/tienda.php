<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tienda extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
  public function index()
	{
		/*$data_head['tipo'] = 'tienda';
		$this->output->cache(15); // 15 min
		$this->load->helper('Functions_lvi');
		$this->load->model('notas');
    $this->load->model('tiendas');
    $this->load->view('header',$data_head);
		$key = array('key' => '', 'filters' => '', 'page' => '');
    $data['productos']=$this->tiendas->get_listado_busqueda_filtros($key);
		$this->load->view('listado_tienda',$data);
		$this->load->view('footer');*/
	} 
  
  public function como_comprar()
	{
    /*$data_head['tipo'] = 'tienda';
		$this->output->cache(60); // 1 hora
		$this->load->helper('Functions_lvi');
		$this->load->view('header',$data_head);
		$this->load->view('tienda-como-comprar');
		$this->load->view('footer');*/
	}
  
  public function compra_datos()
	{
    $post = $_POST;
    if(!empty($post)){
      $this->load->helper('Functions_lvi');
      $this->load->model('tiendas');
      $this->load->model('avisos');
      $data=$this->tiendas->insert_compra_datos($post);
      unset($_POST);
      if($data['status'] == 'ok') {
        $data['node'] = $this->avisos->get_node($post['compra_nid']);
        $this->load->view($this->config->item('carpeta_sitio').'/tienda-compra-entrega',$data);
      } else {
        print json_encode($data);
      }
    } else {
      print json_encode(array('status' => 'error', 'mensaje' => 'Error al realizar la compra.'));
    } 
	} 
  
  public function compra_entregas()
	{
    $post = $_POST;
    if(!empty($post)){
      $this->load->helper('Functions_lvi');
      $this->load->model('tiendas');
      $this->load->model('avisos');
      $data=$this->tiendas->insert_compra_entregas($post);
      unset($_POST);
      if($data['status'] == 'ok') {
        $data['node'] = $this->avisos->get_node($post['compra_nid']);
        $data['entrega_lugar'] = $post['entrega_lugar'];
        $this->load->view($this->config->item('carpeta_sitio').'/tienda-compra-pago',$data);
      } else {
        print json_encode($data);
      }
    } else {
      print json_encode(array('status' => 'error', 'mensaje' => 'Error al realizar la compra.'));
    } 
	}

  public function compra_pagos($servicio)
	{
    $post = $_POST;
    if(!empty($post)){
      $this->load->helper('Functions_lvi');
      $this->load->model('tiendas');
      $data=$this->tiendas->insert_compra_pagos($post, $servicio);
      unset($_POST);
      if($data['status'] == 'ok') {
        switch($servicio){
          case 'mercadopago':
            $this->load->view($this->config->item('carpeta_sitio').'/tienda-compra-pago-mercadopago',$data);
            break;
          case 'dineromail':
            $this->load->view($this->config->item('carpeta_sitio').'/tienda-compra-pago-dineromail',$data);
            break;
          case 'todopago':
            $this->load->view($this->config->item('carpeta_sitio').'/tienda-compra-pago-todopago', $data);
            break;
          case 'domicilio':
            print json_encode($data);
            break;
        }
      } else {
        print json_encode($data);
      }
    } else {
      print json_encode(array('status' => 'error', 'mensaje' => 'Error al realizar la compra.'));
    } 
	} 
  
  public function back_url($venta_id, $status, $servicio)
	{
    $data_head['tipo'] = 'tienda';
    $this->load->helper('Functions_lvi');
    $this->load->model('tiendas');
    $this->load->view($this->config->item('carpeta_sitio').'/header',$data_head);
    $data=$this->tiendas->insert_compra_status_final($venta_id, $status, $servicio);
    $data['status'] =  $status;
    $this->load->view($this->config->item('carpeta_sitio').'/tienda-compra-estado-final',$data); 
    $this->load->view($this->config->item('carpeta_sitio').'/footer');
	}
  
  public function compra_zona_cobertura()
	{
    $post = $_POST;
    if(!empty($post)){
      $this->load->helper('Functions_lvi');
      $this->load->model('tiendas');
      $data=$this->tiendas->obtener_zona_cobertura($post);
      unset($_POST);
      print json_encode($data);
    } else {
      print json_encode(array('status' => 'error', 'mensaje' => 'Error al obtener cobertura de envío.'));
    } 
	}
  
}

/* End of file tienda.php */
/* Location: ./application/controllers/tienda.php */