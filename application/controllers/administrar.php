<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrar extends CI_Controller {
  
  var $data = array();
  
  function __construct() {
    parent::__construct();
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->helper('Functions_lvi');
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('avisos');
    $this->load->model('pages');
    $this->load->model('admin');
    $this->data['rubros'] = $this->avisos->get_lista_rubros();
    $this->data['tipo'] = 'admin';
    $this->data['head_tipo'] = 'admin';
    $this->data['token'] = $this->session->userdata('token');
    $this->data['session'] = array('token'=>$this->session->userdata('token'));
    $this->data['canonical'] = obtener_url_web($this->uri->uri_string()); //URL Canonical
  }
  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -  
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  public function index() {
    $this->check_session();
    $data = $this->data;
    $data['head_title'] = 'Administración de avisos - Clasificados La Voz';
    $data['titulo'] = 'Mis avisos';
    if( FALSE != ($mensaje = $this->session->flashdata('mensaje'))) {
      $data['mensaje'] = $mensaje;
    }
    $_POST['token'] = $data['token']; 
    $filtros = '';
    $_POST['tids'] = '';
    if(!empty($_POST['rubro'])) {
      $_POST['tids'] .= $_POST['rubro'].' ';
    }
    if(!empty($_POST['provincia'])) {
      $_POST['tids'] .= $_POST['provincia'].' ';
    }
    if(!empty($_POST['ciudad'])) {
      $_POST['tids'] .= $_POST['ciudad'].' ';
    }
    if(!empty($_POST['barrio'])) {
      $_POST['tids'] .= $_POST['barrio'].' ';
    }
    $data['respuesta'] = $this->admin->post_datos('admin/avisos', $_POST);
    $this->check_status($data['respuesta']->status, isset($data['respuesta']->mensaje)?$data['respuesta']->mensaje:'');
    if(!empty($data['respuesta']->mensaje)) {
      $data['mensaje'] = $data['respuesta']->mensaje;
    }
    $data['calendario_front']=true;
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/filtros_admin', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/mis-avisos', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function publicar($nid, $errores="", $mensaje='', $form=array()) {
    $this->check_session();
    $data = $this->data;
    $data['nid'] =  $nid;
    if($errores != "") $data['errores'] = $errores;
    $data['mensaje'] =  $mensaje;
    $data['form'] =  $form;
    $submit = $this->input->post('submit');
    if(!empty($submit)) {
      $this->publicar_submit();
      return;
    }
    $data['publicacion'] = $this->admin->post_datos('admin/opciones_publicacion', array('nid'=>$nid, 'token'=>$data['token']));
    $this->check_status($data['publicacion']->status, isset($data['publicacion']->mensaje)?$data['publicacion']->mensaje:'');
    $data['head_title'] = 'Administración de avisos - Publicación - Clasificados La Voz';
    $data['titulo'] = 'Publicación del aviso';
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/publicacion', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function publicar_submit() {
    $this->check_session();
    $data = $this->data;
    $submit = $this->input->post('submit');
    if(empty($submit)) {
      $this->session->set_flashdata('mensaje', 'Datos erróneos al publicar');
      redirect('administrar/mis-avisos.html');
    }
    unset($_POST['submit']); // Importante para no entrar en bucle infinito.
    $nid = $this->input->post('nid');
    $espacio = $this->input->post('espacio');
    $mejoras = array();
    foreach($_POST as $campo => $valor) {
      if(strpos($campo, 'credito')===0) {
        $mejoras[$campo] = $valor;
      }
    }
    $select_credito_banderola = $this->input->post('select_credito_banderola');
    $params = array('nid'=>$nid, 'token'=>$data['token'], 'espacio'=>$espacio, 'mejoras'=>serialize($mejoras), 'select_credito_banderola'=>$select_credito_banderola);
    $web_papel = $this->input->post('web_papel');
    if(!empty($web_papel)) {
      $params['web_papel'] = $web_papel;
    }
    $web_papel = $this->input->post('web_papel_concesionaria');
    if(!empty($web_papel)) {
      $params['web_papel'] = $web_papel;
    }
    $web_papel = $this->input->post('web_papel_inmobiliaria');
    if(!empty($web_papel)) {
      $params['web_papel'] = $web_papel;
    }
    //sitios_externos
    $sitios_externos = $this->input->post('sitios_externos');
    if(!empty($sitios_externos)) {
      $params['sitios_externos'] = serialize($this->input->post('sitios_externos'));
    }
    $data['respuesta'] = $this->admin->post_datos('admin/publicar', $params);
    $this->check_status($data['respuesta']->status, $data['respuesta']->mensaje);
    if(isset($data['respuesta']->errores) && !empty($data['respuesta']->errores)) {
      // Hubo error volvemos a cargar el formulario de publicacion.
      $this->publicar($nid, (array)$data['respuesta']->errores, $data['respuesta']->mensaje, $data['respuesta']->form);
      return;
    }
    if($data['respuesta']->status!==0 && $data['respuesta']->status!==1 && empty($data['respuesta']->mensaje)) {
      $data['respuesta']->mensaje = 'Error desconocido al publicar';
    }
    $this->session->set_flashdata('mensaje', $data['respuesta']->mensaje);
    if(!empty($data['respuesta']->form->compra_id)) {
      // Si compra algun destaque lo redirigimos a confirmar.
      redirect('compra/'.$data['respuesta']->form->compra_id.'/confirmar');
      return;
    }
    redirect('administrar/mis-avisos.html');// podriamos usar $this->index(); pero flasdata requiere redireccion.
    return;
  }
  
  public function despublicar($nid, $errores=FALSE, $mensaje='', $form=array()) {
    $this->check_session();
    $data = $this->data;
    $data['nid'] =  $nid;
    $data['errores'] =  $errores;
    $data['mensaje'] =  $mensaje;
    $data['form'] =  $form;
    $submit = $this->input->post('submit');
    if(!empty($submit)) {
      $this->despublicar_submit();
      return;
    }
    $confirmar = $this->input->post('confirmar');
    $params = array('nid'=>$nid, 'token'=>$data['token'], 'confirmar'=>$confirmar);
    $data['respuesta'] = $this->admin->post_datos('admin/opciones_despublicar', $params);
    $this->check_status($data['respuesta']->status, $data['respuesta']->mensaje);
    $data['head_title'] = 'Administración de avisos - Fin Publicación - Clasificados La Voz';
    $data['titulo'] = 'Finalizar publicación del aviso';
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/despublicar', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function despublicar_submit() {
    $this->check_session();
    $data = $this->data;
    $nid = $this->input->post('nid');
    $data['nid'] = $nid;
    $data['errores'] = $errores;
    $data['mensaje'] = $mensaje;
    $data['form'] = $form;
    $submit = $this->input->post('submit');
    if(empty($submit)) {
      $this->session->set_flashdata('mensaje', 'Datos erróneos al despublicar');
      redirect('administrar/mis-avisos.html');
    }
    $confirm = $this->input->post('confirm');
    $data['respuesta'] = $this->admin->post_datos('admin/despublicar', array('nid'=>$nid, 'token'=>$data['token'], 'confirm'=>$confirm));
    $this->check_status($data['respuesta']->status, $data['respuesta']->mensaje);
    $this->session->set_flashdata('mensaje', $data['respuesta']->mensaje);
    redirect('administrar/mis-avisos.html');
    return;
  }
  
  public function estado_cuenta() {
    $this->check_session();
    $data = $this->data;
    $data['head_title'] = 'Administración de avisos - Estado de Cuenta - Clasificados La Voz';
    $data['titulo'] = 'Estado de cuenta';
    if(FALSE != ($mensaje = $this->session->flashdata('mensaje'))) {
      $data['mensaje'] = $mensaje;
    }
    $respuesta = $this->admin->post_datos('admin/estado_cuenta', array('token'=>$data['token']));
    $this->check_status($respuesta->status, $respuesta->mensaje);
    //$this->session->set_flashdata('mensaje', $respuesta->mensaje);
    $data = array_merge($data, (array)$respuesta);
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/estado_cuenta', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function contactos_recibidos() {
    $this->check_session();
    $data = $this->data;
    $respuesta = $this->admin->post_datos('admin/contactos_recibidos', array('token'=>$data['token']));
    $this->check_status($respuesta->status, $respuesta->mensaje);
    $data['contactos'] = $respuesta->contactos;
    $data['tipo'] = 'admin';
    //Title Head
    $data['head_title'] = 'Contactos recibidos - Clasificados La Voz';
    $data['head_tipo'] = 'admin';
    $data['titulo'] = 'Contactos recibidos';
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/contactos', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function filtros($param1 = NULL, $param2 = NULL)	{
    $this->check_session();
    $data = $this->data;
    //$this->output->cache(15); // 15 min
		
    //Tipos de avisos
    $array_principales = array(6323,6330,6106,6000,6017,6260);
    $array_princ_sec = array(6323,6330,6106,6000,6017,6260,6325,6329,6324,6326,6328,6327,6338,6331,6339,6334,6332,6337,6335,6336,6333);
    
    //Filtro rubros
    //$data['rubros']=$this->avisos->get_lista_rubros();
    
    //Parametros filtro
    $filtros = '';
    $data['key'] = '';
    if(strpos($param1,'filters=')!==false)
      $filtros = $param1;
    else
      $data['key'] = $param1;
    if(strpos($param2,'filters=')!==false)
      $filtros = $param2;
    $data['parametros_filtro']=$filtros;
        
    //URL Canonical
    //$data['canonical']=obtener_url_web($this->uri->uri_string());
    
    //Subfiltro
    $array_tids = obtener_tids_url($filtros);
        
    //Campos de filtros
    $filtros_todos = $this->avisos->get_filtros_avanzados();
    if(!empty($array_tids) && array_intersect($array_tids, $array_princ_sec)){
      $data['campos_filtros'] = obtener_filtros_busqueda($array_tids, $filtros_todos);
    } else {
      $data['campos_filtros'] = $filtros_todos;
    }
    
    //dejar solo el ultimo tid de filtro
    foreach($data['campos_filtros'] as $k => $campo){
      if(isset($campo['filtro'])){
        $data['campos_filtros'][$k]['filtro'] = obtener_ultimo_tid($campo['filtro']);
      }
      if(isset($campo['child_data'])){
        foreach($campo['child_data'] as $k2 => $campo2){
          if(isset($campo2['filtro'])){
            $data['campos_filtros'][$k]['child_data'][$k2]['filtro'] = obtener_ultimo_tid($campo2['filtro']);
          }
          if(isset($campo2['child_data'])){
            foreach($campo2['child_data'] as $k3 => $campo3){
              if(isset($campo3['filtro'])){
                $data['campos_filtros'][$k]['child_data'][$k2]['child_data'][$k3]['filtro'] = obtener_ultimo_tid($campo3['filtro']);
              }
              if(isset($campo3['child_data'])){
                foreach($campo3['child_data'] as $k4 => $campo4){
                  if(isset($campo4['filtro'])){
                    $data['campos_filtros'][$k]['child_data'][$k2]['child_data'][$k3]['child_data'][$k4]['filtro'] = obtener_ultimo_tid($campo4['filtro']);
                  }
                  if(isset($campo4['child_data'])){
                    foreach($campo4['child_data'] as $k5 => $campo5){
                      if(isset($campo5['filtro'])){
                        $data['campos_filtros'][$k]['child_data'][$k2]['child_data'][$k3]['child_data'][$k4]['child_data'][$k5]['filtro'] = obtener_ultimo_tid($campo5['filtro']);
                      }
                    }
                  }
                }
              }
            }     
          }
        }
      }
    }
                
    //Provincias y ciudades
    $data['campos_provincia'] = $this->avisos->get_filtros_avanzados_provincia();
    
    //Filtros aplicados
    $parametros = array($param1, $param2, $param3);
    $rubros_principales = array();
    $data['filtros_nombres'] = array();
    foreach($array_tids as $tid){
      if($tid != ''){
        if(is_numeric($tid)){
          $rubros_principales[] = $tid;
          if(!in_array($tid, $array_principales)){
            $term_filter = $this->avisos->get_taxonomy($tid);
            $url = quitar_filtro_url($tid, $parametros, '/filtros/avanzados_map/');
            $data['filtros'][] = array('tid' => $term_filter['tid'], 'nombre' => $term_filter['name'], 'url' => $url);
            $data['filtros_nombres'][] = $term_filter['tid'];
          }
        } else {
          $url = quitar_filtro_url($tid, $parametros, '/filtros/avanzados_map/');
          $data['filtros'][] = array('tid' => $tid, 'nombre' => $tid, 'url' => $url);
          $data['filtros_nombres'][] = $tid;
        }
      }
    }
    
    $data['rubros_principales'] = $rubros_principales;
    $data['head_title'] = 'Filtros de Avisos - Clasificados La Voz';
    
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/filtros_admin', $data);
		$this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
	}
  
  public function acciones_masivas_avisos($accion) {
    $this->check_session();
    $data = $this->data;
    $_POST['token'] = $data['token'];
    if($accion == 'eliminar') {
      $respuesta = $this->admin->post_datos('admin/avisos/eliminado_masivo', $_POST);
      $this->session->set_flashdata('mensaje', $respuesta);
    } elseif($accion == 'despublicar') {
      $respuesta = $this->admin->post_datos('admin/avisos/despublicado_masivo', $_POST);
      $this->session->set_flashdata('mensaje', $respuesta);
    } 
    redirect('administrar/mis-avisos.html');
  }
  
  public function favoritos($param1 = NULL) {
    $data['head_title'] = 'Administración de Favoritos';
    $data['titulo'] = 'Mis Favoritos';
    
    $_POST['favoritos'] = $_COOKIE["clviFavourites"];

    $page = 0;
    if(strpos($param1,'page=')!==false)
      $page = str_replace('page=', '', $param1);
    $_POST['page'] = $page;
    
    $data['listado'] = $this->admin->post_datos('admin/favoritos', $_POST);
    
    //print_r($data);
    
    $data['tipo'] = 'admin';
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/listado_favoritos', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function infinity_favoritos($param1 = NULL) {
    
    $_POST['favoritos'] = $_COOKIE["clviFavourites"];
    $page = 0;
    if(strpos($param1,'page=')!==false)
      $page = str_replace('page=', '', $param1);
    $_POST['page'] = $page;  
      
    $data['listado'] = $this->admin->post_datos('admin/favoritos', $_POST);
    
    $this->load->view($this->config->item('carpeta_sitio').'/favoritos_infinito', $data);
  }
  
  public function historial($param1 = NULL) {
    $data['head_title'] = 'Historial de avisos visitados';
    $data['titulo'] = 'Historial de avisos';
    $historial = json_decode($_COOKIE["clviHistorial"]);
    $_POST['uid'] = '';
    $ids = array();
    foreach($historial as $aviso){
      if(!in_array($aviso[0], $ids))
        $ids[] = $aviso[0];
    }
    $ids = json_encode($ids);
    $_POST['favoritos'] = $ids;
    $page = 0;
    if(strpos($param1,'page=')!==false)
      $page = str_replace('page=', '', $param1);
    $_POST['page'] = $page;
    $_POST['paginacion'] = 60;
    $data['listado'] = $this->admin->post_datos('admin/favoritos', $_POST);
    $data['historial'] = $historial;
    $data['tipo'] = 'admin';
    $this->load->view($this->config->item('carpeta_sitio').'/header', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/listado_historial', $data);
    $this->load->view($this->config->item('carpeta_sitio').'/footer', $data);
  }
  
  public function infinity_historial($param1 = NULL) {
    $historial = json_decode($_COOKIE["clviHistorial"]);
    $_POST['uid'] = '';
    $ids = array();
    foreach($historial as $aviso){
      if(!in_array($aviso[0], $ids))
        $ids[] = $aviso[0];
    }
    $ids = json_encode($ids);
    $_POST['favoritos'] = $ids;
    $page = 0;
    if(strpos($param1,'page=')!==false)
      $page = str_replace('page=', '', $param1);
    $_POST['page'] = $page;
    $data['listado'] = $this->admin->post_datos('admin/favoritos', $_POST);
    $data['historial'] = $historial;
    $this->load->view($this->config->item('carpeta_sitio').'/historial_infinito', $data);
  }
  
  function _remap($metodo, $params = array()) {
    switch($metodo) {
      case '':
      case 'mis-avisos.html':
        $this->index();
        break;
      case 'publicar':
        $this->publicar($params[0]);
        break;
      case 'publicar_submit':
        $this->publicar_submit();
        break;
      case 'despublicar':
        $this->despublicar($params[0]);
        break;
      case 'despublicar_submit':
        $this->despublicar_submit();
        break;
      case 'estado_cuenta':
        $this->estado_cuenta();
        break;
      case 'contactos_recibidos':
        $this->contactos_recibidos();
        break;
      case 'filtros':
        $this->filtros();
        break;
      case 'filtros':
        $this->filtros();
        break;
      default:
        // Si no coincide con ningun metodo, vemos si coincide con alguno del controlador.
        if(method_exists($this, $metodo)) {
          return call_user_func_array(array($this, $metodo), $params);
        }
        show_404();
    }
  }
  
  public function obtener_precio_uva($monto, $moneda) {
    $precio_uva = $this->admin->get_precio_uva($monto, $moneda);
    print $precio_uva;
  }
  
  function check_session() {
    if($this->admin->session_valida()!=true) {
      redirect('user'); 
    }
  }
  
  function check_status($status, $mensaje) {
    if($status===0) {
      $this->session->set_flashdata('mensaje', $mensaje);
      $this->session->sess_destroy();// Borramos datos de session del login.
      redirect('user');
    }
  }
}

/* End of file administrar.php */