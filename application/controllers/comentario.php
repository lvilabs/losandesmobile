<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comentario extends CI_Controller {

	/**
	 * Comentario for this controller.
	 *
	 */
	public function envio()
	{
    $this->load->driver('cache', array('adapter'=>'file'));
    $this->load->library('session');
    $this->load->helper('url');
		$this->load->model('avisos');
    
    $post = $_POST;
    
    //Validacion de campos
    $mensaje = '';
    $status = 'ok';
    foreach($post as $kparm => $dparam){
      if($kparm == 'comment'){
        if($dparam == ''){
          $mensaje .= 'Escribe el comentario.<br>';
          $status = 'error';
        }
      }
      if($post['tipo'] == 'comentario') {
        if($kparm == 'subject'){
          if($dparam == ''){
            $mensaje .= 'Escribe un título al comentario.<br>';
            $status = 'error';
          }
        }
        if($kparm == 'fivestar'){
          if($dparam == '' || $dparam == 0){
            $mensaje .= 'Valora tu estadía.<br>';
            $status = 'error';
          }
        }
      }
    }
    if($status == 'ok'){
      $data=$this->avisos->insert_comentario($post);
      $this->cache->file->delete('clvi_node_reputacion_'.$post['nid']);
      $this->session->set_userdata(array('status' => $data['status'],'mensaje' => $data['mensaje']));
      redirect($post['redirect']); 
    } else {
      $this->session->set_userdata(array('status' => $status, 'mensaje' => $mensaje));
      redirect($post['redirect']);
    }
    
	}
}

/* End of file comentario.php */
/* Location: ./application/controllers/comentario.php */