<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicios extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_cotizacion_uva()
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $uva = $this->cache->file->get($this->config->item('token_sitio').'cotizacion_uva'))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/admin/cotizacion_uva';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $json_decode = json_decode($json,true);
              $uva = $json_decode['uva'];
              if(!empty($json_decode))
                $this->cache->file->save($this->config->item('token_sitio').'cotizacion_uva', $uva, 14400); // 4hs
            }
        }
        return json_decode($uva);
    }
    
    function set_cookie(){
        header('Location: '.$this->config->item('sitio_origen').'/servicio_movil/set_cookie'.$this->config->item('not_cache'));
        exit;
    }
    
    
}