<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notas extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_nota($id)
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        if(is_numeric($id)){
          $id_final = $id;
        } else {
          $this->load->database();
          $query = $this->db->query("SELECT CONVERT(REPLACE(ua.src, 'node/', ''),UNSIGNED INTEGER) as id FROM url_alias ua WHERE ua.dst = 'nota/".$id."' LIMIT 1;");
          if($query->num_rows()!=0){
            foreach ($query->result() as $row){
              $id_final = $row->id;
            }
          }
        }
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'nota_'.$id_final))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/nota/'.$id_final;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $this->cache->file->save($this->config->item('token_sitio').'nota_'.$id_final, $json, 1800);  // 30min
            }
        }
        return json_decode($json,true);
    }
    
  function get_listado_nota($page) 
  {
    $this->load->driver('cache', array('adapter'=>'file'));
    if ($cache==FALSE || ! $json = $this->cache->file->get($this->config->item('token_sitio').'listado_notas_page_'.$page)) {
      $url = $this->config->item('sitio_origen').'/servicio_movil/notas_listado?page='.$page;
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $json = curl_exec($curl);
      $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close ($curl);
      if($httpCode != 404 && $httpCode != 503) {
        $this->cache->file->save($this->config->item('token_sitio').'listado_notas_page_'.$page, $json, 300); // 5min
      }
    }
    return json_decode($json);
  }
  
  function get_destacados_nota($rubro, $cantidad) 
  {
    $this->load->driver('cache', array('adapter'=>'file'));
    if ($cache==FALSE || ! $json = $this->cache->file->get($this->config->item('token_sitio').'destacados_notas_'.$rubro.'_'.$cantidad)) {
      $url = $this->config->item('sitio_origen').'/servicio_movil/notas_destacados/'.$rubro.'/'.$cantidad;
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $json = curl_exec($curl);
      $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close ($curl);
      if($httpCode != 404 && $httpCode != 503) {
        $this->cache->file->save($this->config->item('token_sitio').'destacados_notas_'.$rubro.'_'.$cantidad, $json, 300); // 5min
      }
    }
    return json_decode($json);
  }
    
}