<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Model {

  function __construct() {
    // Call the Model constructor
    parent::__construct();
    $this->load->library('session');
  }
  
  function session_valida() {
    $token = $this->session->userdata('token');
    $usuario = $this->session->userdata('logged_in');
    if(empty($token)) {
      return false;
    }
    if(empty($usuario) || !is_numeric($usuario->uid)) {
      return false;
    }
    return true;
  }
  
  function get_datos($path) {
    $url = $this->config->item('sitio_origen').'/servicio_movil/'.$path;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $json = curl_exec($curl);
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    return json_decode($json);
  }
  
  function post_datos($path, $post) {  
    $url = $this->config->item('sitio_origen').'/servicio_movil/'.$path;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $json = curl_exec($curl);
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    if($httpCode!=200 && $httpCode!=301 && $httpCode!=302) {
      log_message('error', 'Error en admin->post_datos '.$path.' HTTP code: '.$httpCode);
      $json = array('status'=>2, 'mensaje'=>'Error del servidor. Código: '.$httpCode);
      $json = json_encode($json);
      $json = json_decode($json);
      return $json;
    }
    return json_decode($json);
  }
  
  function get_options_form_users()
  {
      $this->load->driver('cache', array('adapter'=>'file'));
      if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'opciones_usuario'))
      {
          $url= $this->config->item('sitio_origen').'/servicio_movil/opciones_creacion_usuario';
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
          $json .= curl_exec($curl);
          $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
          curl_close ($curl);
          if($httpCode != 404 && $httpCode != 503) {
            $json_decode = json_decode($json,true);
            if(!empty($json_decode))
              $this->cache->file->save($this->config->item('token_sitio').'opciones_usuario', $json, 3600);  // 1hora
          }
      }
      return json_decode($json,true);
  }
  
  function insert_usuario($post)
  {
      $url= $this->config->item('sitio_origen').'/servicio_movil/user/register';
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $json = curl_exec($curl);
      $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      if($httpCode != 404 && $httpCode != 503) {
        $json_decode = json_decode($json);
      }
      return json_decode($json);
  }
  
  function get_usuario_perfil($uid)
  {
      $url= $this->config->item('sitio_origen').'/servicio_movil/user/get_perfil';
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, 'uid='.$uid);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $json = curl_exec($curl);
      $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      if($httpCode != 404 && $httpCode != 503) {
        $json_decode = json_decode($json);
      }
      return json_decode($json, true);
  }
  
  function get_usuario_favoritos($token)
  {
      $url= $this->config->item('sitio_origen').'/servicio_movil/user/get_favoritos';
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, 'token='.$token);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $json = curl_exec($curl);
      $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      if($httpCode != 404 && $httpCode != 503) {
        $json_decode = json_decode($json);
      }
      return json_decode($json, true);
  }
  
  function get_precio_uva($monto, $moneda)
  {
      $url= $this->config->item('sitio_origen').'/ajax/obtener_precios_uva/'.$monto.'/'.$moneda;
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($curl);
      curl_close($curl);
      return $result;
  }
  
}