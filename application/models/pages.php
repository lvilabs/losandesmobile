<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Model {

  function __construct() {
    // Call the Model constructor
    parent::__construct();
  }
  
  function get_page_content($page, $cache = TRUE, $args = FALSE) {
    $this->load->driver('cache', array('adapter'=>'file'));
    if ($cache==FALSE || ! $json = $this->cache->file->get($this->config->item('token_sitio').'page_'.$page)) {
      $url = $this->config->item('sitio_origen').'/servicio_movil/pages/content/'.$page;
      $curl = curl_init();
      if(!empty($args)) {
        $url .= '?'.http_build_query($args);
      }
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $json = curl_exec($curl);
      $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close ($curl);
      if($httpCode != 404 && $httpCode != 503) {
        $this->cache->file->save($this->config->item('token_sitio').'page_'.$page, $json, 300); // 5min
      }
    }
    return json_decode($json);
  }
  
  function post_datos($path, $post) {  
    $url = $this->config->item('sitio_origen').'/servicio_movil/'.$path;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $json = curl_exec($curl);
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    if($httpCode!=200 && $httpCode!=301 && $httpCode!=302) {
      log_message('error', 'Error en admin->post_datos '.$path.' HTTP code: '.$httpCode);
      $json = array('status'=>2, 'mensaje'=>'Error del servidor. Código: '.$httpCode);
      $json = json_encode($json);
      $json = json_decode($json);
      return $json;
    }
    return json_decode($json);
  }
  
}