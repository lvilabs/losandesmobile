<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avisos extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_destacados()
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'avisos_destacados'))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/avisos_destacados';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);    
            if($httpCode != 404 && $httpCode != 503) {
              $this->cache->file->save($this->config->item('token_sitio').'avisos_destacados', $json, 300); // 5min
            }
        }
        return json_decode($json);
    }
    
    function get_lista_rubros()
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'lista_rubros'))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/lista_rubros';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 500 && $httpCode != 503) {
              $this->cache->file->save($this->config->item('token_sitio').'lista_rubros', $json, 3600); // 1hr
            }
        }
        return json_decode($json);
    }
    
    function get_listado_busqueda_filtros($key)
    {
        $filtro = '';
        foreach($key as $kkey => $vkey){
          if($kkey == 'key' && !empty($vkey)){
            if($filtro == '') $filtro = 'keys='.$vkey; else $filtro .= '&keys='.$vkey;
          }
          if($kkey == 'filters' && !empty($vkey)){
            if($filtro == '') $filtro = $vkey; else $filtro .= '&'.$vkey;
          }
          if($kkey == 'page' && !empty($vkey)){
            $filtro .= '&'.$vkey;
          }
        }
        
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'listado_busqueda_'.str_replace(':', '-', str_replace('%3A', '-', $filtro))))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/buscador?'.str_replace(' ', '%20', $filtro);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $this->cache->file->save($this->config->item('token_sitio').'listado_busqueda_'.str_replace(':', '-', str_replace('%3A', '-', $filtro)), $json, 300); // 5min
            }
        }
        return json_decode($json);
    }
    
    function get_listado_busqueda_mapa($key, $ajax = 0)
    {
        $filtro = '';
        foreach($key as $kkey => $vkey){
          if($kkey == 'key' && !empty($vkey)){
            if($filtro == '') $filtro = 'keys='.$vkey; else $filtro .= '&keys='.$vkey;
          }
          if($kkey == 'filters' && !empty($vkey)){
            if($filtro == '') $filtro = $vkey; else $filtro .= '&'.$vkey;
          }
          if($kkey == 'page' && !empty($vkey)){
            $filtro .= '&'.$vkey;
          }
          if($kkey == 'latitud' && !empty($vkey)){
            $filtro .= '&'.$vkey;
          }
          if($kkey == 'longitud' && !empty($vkey)){
            $filtro .= '&'.$vkey;
          }
        }
        log_message('error', print_r($filtro, TRUE));
        if($ajax) {
          $url= $this->config->item('sitio_origen').'/servicio_movil/buscador_proximidad_v3?'.str_replace(' ', '%20', $filtro);
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
          $json = curl_exec($curl);
          $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
          curl_close ($curl);
        } else {
          $this->load->driver('cache', array('adapter'=>'file'));
          if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'listado_busqueda_map_'.str_replace(':', '-', str_replace('%3A', '-', $filtro))))
          {
              $url= $this->config->item('sitio_origen').'/servicio_movil/buscador_proximidad_v3?'.str_replace(' ', '%20', $filtro);
              $curl = curl_init();
              curl_setopt($curl, CURLOPT_URL, $url);
              curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
              $json = curl_exec($curl);
              $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
              curl_close ($curl);
              if($httpCode != 404 && $httpCode != 503) {
                $this->cache->file->save($this->config->item('token_sitio').'listado_busqueda_map_'.str_replace(':', '-', str_replace('%3A', '-', $filtro)), $json, 300); // 5min
              }
          }
        }
        return json_decode($json);
    }
    
    function get_node($id)
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'node_'.$id))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/ficha_aviso/'.$id;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $this->cache->file->save($this->config->item('token_sitio').'node_'.$id, $json, 1800);  // 30min
            }
        }
        return json_decode($json,true);
    }
    
    function get_taxonomy($tid)
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'taxonomy_'.$tid))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/get_taxonomy/'.$tid;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $this->cache->file->save($this->config->item('token_sitio').'taxonomy_'.$tid, $json, 3600); // 1hr
            }
        }
        return json_decode($json,true);
    }
    
    function insert_contacto($param_post)
    {
        $url= $this->config->item('sitio_origen').'/servicio_movil/contacto_vendedor';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);        
        $json = curl_exec($curl);
        curl_close ($curl);
        return json_decode($json,true);
    }
    
    function get_sugerencias($texto)
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'sugerencia_'.str_replace(' ', '_', $texto)))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/get_sugerencias?busqueda='.str_replace(' ', '%20', $texto);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $json_decode = json_decode($json,true);
              if(!empty($json_decode))
                $this->cache->file->save($this->config->item('token_sitio').'sugerencia_'.str_replace(' ', '_', $texto), $json, 1800);  // 30min
            }
        }
        return $json; // Devolvemos el json puro.
    }
    
    function set_sugerencias($texto)
    {
        $url= $this->config->item('sitio_origen').'/servicio_movil/set_sugerencias?busqueda='.str_replace(' ', '%20', $texto);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        curl_close ($curl);
    }
    
    function get_options_form_avisos()
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'opciones_avisos'))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/opciones_creacion_aviso';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $json_decode = json_decode($json,true);
              if(!empty($json_decode))
                $this->cache->file->save($this->config->item('token_sitio').'opciones_avisos', $json, 3600);  // 1hora
            }
        }
        return json_decode($json,true);
    }
    
    function get_options_ubicacion_form_avisos()
    {
        // $this->load->driver('cache', array('adapter'=>'file'));
        // if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'ubicacion_avisos'))
        //{
            $url= $this->config->item('sitio_origen').'/servicio_movil/ubicacion_creacion_aviso';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $json_decode = json_decode($json,true);
              //if(!empty($json_decode))
                //$this->cache->file->save($this->config->item('token_sitio').'ubicacion_avisos', $json, 3600);  // 1hora
            }
        // }
        return json_decode($json,true);
    }
    
    function insert_aviso($param_post)
    {
        $postfields = http_build_query($param_post, '', '&');
        
        $url= $this->config->item('sitio_origen').'/servicio_movil/creacion_aviso';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);        
        $json = curl_exec($curl);
        curl_close ($curl);
        return json_decode($json,true);
    }
    
    function get_filtros_avanzados()
    {        
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'filtros_avanzados'))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/filtros_avanzados';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $json_decode = json_decode($json,true);
              if(!empty($json_decode))
                $this->cache->file->save($this->config->item('token_sitio').'filtros_avanzados', $json, 21600);  // 6hora
            }
        }
        return json_decode($json,true);
    }
    
    function get_filtros_avanzados_provincia()
    {        
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'filtros_provincia'))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/filtro_provincia_ciudad_barrio';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $json_decode = json_decode($json,true);
              if(!empty($json_decode))
                $this->cache->file->save($this->config->item('token_sitio').'filtros_provincia', $json, 21600);  // 6hora
            }
        }
        return json_decode($json,true);
    }
    
    function insert_denuncia($param_post)
    {
        $url= $this->config->item('sitio_origen').'/servicio_movil/denuncia_aviso';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);        
        $json = curl_exec($curl);
        curl_close ($curl);
        return json_decode($json,true);
    }
    
    function get_node_reputacion($id)
    {
        $this->load->driver('cache', array('adapter'=>'file'));
        if ( ! $json = $this->cache->file->get($this->config->item('token_sitio').'node_reputacion_'.$id))
        {
            $url= $this->config->item('sitio_origen').'/servicio_movil/aviso_reputacion/'.$id;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            if($httpCode != 404 && $httpCode != 503) {
              $this->cache->file->save($this->config->item('token_sitio').'node_reputacion_'.$id, $json, 1800);  // 30min
            }
        }
        return json_decode($json,true);
    }
    
    function insert_comentario($param_post)
    {
        $url= $this->config->item('sitio_origen').'/servicio_movil/insert_comentario';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);        
        $json = curl_exec($curl);
        curl_close ($curl);
        return json_decode($json,true);
    }

    function get_alias($nid) {
        $url = $this->config->item('sitio_origen').'/servicio_movil/get_alias/'.$nid;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        curl_close ($curl);
        return json_decode($json, true);
    }
    
    function contador_vistas($tipo, $nid, $data_contacto) {
        header('Content-Type: application/json');
        $url = $this->config->item('sitio_origen').'/servicio_movil/contador/'.$tipo.'/'.$nid;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_contacto);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        curl_close ($curl);
        print $json;
    }

}