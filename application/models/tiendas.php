<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tiendas extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function insert_compra_datos($post)
    {
        $url= $this->config->item('sitio_origen').'/servicio_movil/compra/datos';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
        $json .= curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close ($curl);
        return json_decode($json,true);
    }
    
    function insert_compra_entregas($post)
    {
        $url= $this->config->item('sitio_origen').'/servicio_movil/compra/entrega';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
        $json .= curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close ($curl);
        return json_decode($json,true);
    }
    
    function insert_compra_pagos($post, $servicio)
    {
        $url= $this->config->item('sitio_origen').'/servicio_movil/compra/pagos/'.$servicio;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
        $json .= curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close ($curl);
        return json_decode($json,true);
    }
    
    function insert_compra_status_final($venta_id, $status, $servicio)
    {
        $url= $this->config->item('sitio_origen').'/servicio_movil/compra/status/'.$venta_id.'/'.$status.'/'.$servicio;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
        $json .= curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close ($curl);
        return json_decode($json,true);
    }
    
    function obtener_zona_cobertura($post)
    {
        $url= $this->config->item('sitio_origen').'/servicio_movil/compra/zona_cobertura';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
        $json .= curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close ($curl);
        return json_decode($json,true);
    }

}